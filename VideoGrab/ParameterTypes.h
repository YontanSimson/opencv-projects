#pragma once


struct SCornerDetectorParams{

	int MAX_COUNT;
	cv::TermCriteria termcrit;
	cv::Size subPixWinSize, winSize;
	double qualityLevel;
	double minDistance;
	int blockSize;
	bool useHarrisDetector;
	double k;

	//Default Ctor
	SCornerDetectorParams() :
		MAX_COUNT(500),
		termcrit(CV_TERMCRIT_ITER | CV_TERMCRIT_EPS, 20, 0.03),
		subPixWinSize(10, 10),
		winSize(31, 31),
		qualityLevel(0.01),
		minDistance(20),
		blockSize(5),
		useHarrisDetector(false),
		k(0.04)
	{}

	//Ctor
	SCornerDetectorParams(const int MAX_COUNT_,
		cv::TermCriteria termcrit_,
		cv::Size subPixWinSize_,
		cv::Size winSize_,
		double qualityLevel_,
		double minDistance_,
		int blockSize_,
		bool useHarrisDetector_,
		double k_) :
		MAX_COUNT(MAX_COUNT_),
		termcrit(termcrit_),
		subPixWinSize(subPixWinSize_),
		winSize(winSize_),
		qualityLevel(qualityLevel_),
		minDistance(minDistance_),
		blockSize(blockSize_),
		useHarrisDetector(useHarrisDetector_),
		k(k_)
	{}

};


struct SFeatureDetectionParams
{
	float ratio; // max ratio between 1st and 2nd NN
	bool refineF; // if true will refine the F matrix
	double distance; // min distance to epipolar
	double confidence; // confidence level (probability)

	//Default Ctor
	SFeatureDetectionParams():
		ratio(0.65f), 
		refineF(true), 
		confidence(0.99), 
		distance(3.0)
	{}

	//Ctor
	SFeatureDetectionParams(
		float ratio_,
		bool refineF_,
		double distance_,
		double confidence_):
		ratio(ratio_),
		refineF(refineF_),
		confidence(distance_),
		distance(confidence_)
	{}
};