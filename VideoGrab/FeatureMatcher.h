#pragma once

#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <string>
#include <vector>


class CFeatureMatcher
{
public:
	class CFeatureMatcher(int minHessian);
	~CFeatureMatcher();

	void calculateMatchingSURFPoints(const cv::Mat& img_1, const cv::Mat& img_2);
	void calculateMatchingSIFTPoints(const cv::Mat& img_1, const cv::Mat& img_2);
	void calculateMatchingORBPoints(const cv::Mat& img_1, const cv::Mat& img_2);

	void showGoodMatches(const std::string& win_name);
private:

	void detectSURFKeyPoints(const cv::Mat& img_1, const cv::Mat& img_2);
	void calculateSURFDescriptors();
	void matchDescriptors();
	void findGoodMatches();

	int ratioTest(std::vector<std::vector<cv::DMatch> >
		&matches);
	void symmetryTest(const std::vector<std::vector<cv::DMatch> >& matches1,
		const std::vector<std::vector<cv::DMatch> >& matches2,
		std::vector<cv::DMatch>& symMatches);

private:
	cv::SurfFeatureDetector m_detector;
	std::vector<cv::KeyPoint> m_keypoints_1;
	std::vector<cv::KeyPoint> m_keypoints_2;

	cv::SurfDescriptorExtractor m_extractor;
	cv::Mat m_descriptors_1;
	cv::Mat m_descriptors_2;

	cv::FlannBasedMatcher m_matcher;
	std::vector< cv::DMatch > m_matches;
	std::vector< cv::DMatch > m_good_matches;

	cv::Mat m_Img_1;
	cv::Mat m_Img_2;

	float m_ratio; // max ratio between 1st and 2nd NN
};

