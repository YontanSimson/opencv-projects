// main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "VideoGrabber.h"
#include "FeatureMatcher.h"
#include "Eigen/Core"
#include <Eigen/Eigenvalues>

#include <iostream>

using namespace cv;
using namespace Eigen;
using namespace std;

/** @function main */
int main(int argc, const char** argv)
{

	//Grab streams from two webCams
	CVideoGrabber webcamStreamer;

	Mat Im1, Im2;

	webcamStreamer.grab2Images(Im1, Im2);
	waitKey(10);//Wait to let web-cams stabilize
	webcamStreamer.grab2Images(Im1, Im2);

	webcamStreamer.grabVideoStream();
	// the camera will be deinitialized automatically in VideoCapture destructor


	return 0;
}

