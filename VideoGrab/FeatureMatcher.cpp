#include "stdafx.h"

#include "FeatureMatcher.h"
#include "opencv2\imgproc\imgproc.hpp"

#include <cmath>

using namespace cv;

CFeatureMatcher::CFeatureMatcher(int minHessian) :
	m_detector(minHessian),
	m_ratio(0.65f)
{
}

CFeatureMatcher::~CFeatureMatcher()
{
}

void CFeatureMatcher::calculateMatchingSURFPoints(const cv::Mat& img_1, const cv::Mat& img_2)
{
	CV_Assert(img_1.data != NULL);
	CV_Assert(img_2.data != NULL);

	detectSURFKeyPoints(img_1, img_2);
	calculateSURFDescriptors();
	matchDescriptors();
	findGoodMatches();
}

void CFeatureMatcher::calculateMatchingSIFTPoints(const cv::Mat& img_1, const cv::Mat& img_2)
{
	cvtColor(img_1, m_Img_1, COLOR_BGR2GRAY);
	cvtColor(img_2, m_Img_2, COLOR_BGR2GRAY);

	// vector of keypoints
	std::vector<cv::KeyPoint> keypoints1, keypoints2;

	//Smart pointer for feature point detector
	Ptr<FeatureDetector> featureDetector = FeatureDetector::create("SIFT");

	// Detect the keypoints
	featureDetector->detect(m_Img_1, m_keypoints_1);
	featureDetector->detect(m_Img_2, m_keypoints_2);

	//Similarly, we create a smart pointer to the SIFT extractor.
	Ptr<DescriptorExtractor> featureExtractor = DescriptorExtractor::create("SIFT");

	// Compute the 128 dimension SIFT descriptor at each keypoint.
	// Each row in "descriptors" correspond to the SIFT descriptor for each keypoint
	featureExtractor->compute(m_Img_1, m_keypoints_1, m_descriptors_1);
	featureExtractor->compute(m_Img_2, m_keypoints_2, m_descriptors_2);

	// If you would like to draw the detected keypoint just to check
	//Mat outputImage;
	//Scalar keypointColor = Scalar(255, 0, 0);     // Blue keypoints.
	//drawKeypoints(img_1, keypoints1, outputImage, keypointColor, DrawMatchesFlags::DEFAULT);


	matchDescriptors();
	findGoodMatches();
}

/*
Parameters:
nfeatures � The maximum number of features to retain.
scaleFactor � Pyramid decimation ratio, greater than 1. scaleFactor==2 means the classical pyramid, where each next level has 4x less pixels than the previous, but such a big scale factor will degrade feature matching scores dramatically. On the other hand, too close to 1 scale factor will mean that to cover certain scale range you will need more pyramid levels and so the speed will suffer.
nlevels � The number of pyramid levels. The smallest level will have linear size equal to input_image_linear_size/pow(scaleFactor, nlevels).
edgeThreshold � This is size of the border where the features are not detected. It should roughly match the patchSize parameter.
firstLevel � It should be 0 in the current implementation.
WTA_K � The number of points that produce each element of the oriented BRIEF descriptor. The default value 2 means the BRIEF where we take a random point pair and compare their brightnesses, so we get 0/1 response. Other possible values are 3 and 4. For example, 3 means that we take 3 random points (of course, those point coordinates are random, but they are generated from the pre-defined seed, so each element of BRIEF descriptor is computed deterministically from the pixel rectangle), find point of maximum brightness and output index of the winner (0, 1 or 2). Such output will occupy 2 bits, and therefore it will need a special variant of Hamming distance, denoted as NORM_HAMMING2 (2 bits per bin). When WTA_K=4, we take 4 random points to compute each bin (that will also occupy 2 bits with possible values 0, 1, 2 or 3).
scoreType � The default HARRIS_SCORE means that Harris algorithm is used to rank features (the score is written to KeyPoint::score and is used to retain best nfeatures features); FAST_SCORE is alternative value of the parameter that produces slightly less stable keypoints, but it is a little faster to compute.
patchSize � size of the patch used by the oriented BRIEF descriptor. Of course, on smaller pyramid layers the perceived image area covered by a feature will be larger.
*/
void CFeatureMatcher::calculateMatchingORBPoints(const cv::Mat& img_1, const cv::Mat& img_2)
{
	cvtColor(img_1, m_Img_1, COLOR_BGR2GRAY);
	cvtColor(img_2, m_Img_2, COLOR_BGR2GRAY);

	int nfeatures = 500;
	float scaleFactor = 1.2f;
	int nlevels = 8;
	int edgeThreshold = 31;
	int firstLevel = 0;
	int WTA_K = 2;
	int scoreType = ORB::HARRIS_SCORE;
	int patchSize = 31;

	cv::Ptr<cv::FeatureDetector> detector = new cv::OrbFeatureDetector(nfeatures, scaleFactor, nlevels, edgeThreshold, firstLevel, WTA_K, scoreType, patchSize);

	// Compute object keypoints
	detector->detect(m_Img_1, m_keypoints_1);
	detector->detect(m_Img_2, m_keypoints_2);


	cv::Ptr<cv::DescriptorExtractor> extractor = new cv::OrbDescriptorExtractor;
	// Compute object feature descriptors
	extractor->compute(m_Img_1, m_keypoints_1, m_descriptors_1);
	extractor->compute(m_Img_2, m_keypoints_2, m_descriptors_2);


	// If you would like to draw the detected keypoint just to check
	Mat outputImage;
	Scalar keypointColor = Scalar(255, 0, 0);     // Blue keypoints.
	drawKeypoints(m_Img_1, m_keypoints_1, outputImage, keypointColor, DrawMatchesFlags::DEFAULT);

	cv::Ptr<cv::DescriptorMatcher> matcher = new cv::BFMatcher(cv::NORM_HAMMING, false);

	// 1. Match the two image descriptors
	// from object image to scene image
	// based on k nearest neighbours (with k=2)
	std::vector<std::vector<cv::DMatch> > matches1;
	matcher->knnMatch(m_descriptors_1,
		m_descriptors_2,
		matches1, // vector of matches (up to 2 per entry)
		2);        // return 2 nearest neighbours
	// from scene image to object image
	// based on k nearest neighbours (with k=2)
	std::vector<std::vector<cv::DMatch> > matches2;
	matcher->knnMatch(m_descriptors_2,
		m_descriptors_1,
		matches2, // vector of matches (up to 2 per entry)
		2);        // return 2 nearest neighbours

	// 2. Remove matches for which NN ratio is
	// > than threshold
	// clean 1st image -> 2nd image matches
	int removed = ratioTest(matches1);
	// clean 2nd image -> 1st image matches
	removed = ratioTest(matches2);

	// 3. Remove non-symmetrical matches
	std::vector<cv::DMatch> symMatches;
	symmetryTest(matches1, matches2, symMatches);

}

// Clear matches for which NN ratio is > than threshold
// return the number of removed points
// (corresponding entries being cleared,
// i.e. size will be 0)
int CFeatureMatcher::ratioTest(std::vector<std::vector<cv::DMatch> >
	&matches) 
{
	int removed = 0;
	// for all matches
	for (std::vector<std::vector<cv::DMatch> >::iterator
		matchIterator = matches.begin();
		matchIterator != matches.end(); ++matchIterator) {
		// if 2 NN has been identified
		if (matchIterator->size() > 1) {
			// check distance ratio
			if ((*matchIterator)[0].distance /
				(*matchIterator)[1].distance > m_ratio) {
				matchIterator->clear(); // remove match
				removed++;
			}
		}
		else { // does not have 2 neighbours
			matchIterator->clear(); // remove match
			removed++;
		}
	}
	return removed;
}

// Insert symmetrical matches in symMatches vector
void CFeatureMatcher::symmetryTest(const std::vector<std::vector<cv::DMatch> >& matches1,
	const std::vector<std::vector<cv::DMatch> >& matches2,
	std::vector<cv::DMatch>& symMatches) 
{
	// for all matches image 1 -> image 2
	for (std::vector<std::vector<cv::DMatch> >::
		const_iterator matchIterator1 = matches1.begin();
		matchIterator1 != matches1.end(); ++matchIterator1) {
		// ignore deleted matches
		if (matchIterator1->size() < 2)
			continue;
		// for all matches image 2 -> image 1
		for (std::vector<std::vector<cv::DMatch> >::
			const_iterator matchIterator2 = matches2.begin();
			matchIterator2 != matches2.end();
		++matchIterator2) {
			// ignore deleted matches
			if (matchIterator2->size() < 2)
				continue;
			// Match symmetry test
			if ((*matchIterator1)[0].queryIdx ==
				(*matchIterator2)[0].trainIdx &&
				(*matchIterator2)[0].queryIdx ==
				(*matchIterator1)[0].trainIdx) {
				// add symmetrical match
				symMatches.push_back(cv::DMatch((*matchIterator1)[0].queryIdx,
					(*matchIterator1)[0].trainIdx,
					(*matchIterator1)[0].distance));
				break; // next match in image 1 -> image 2
			}
		}
	}
}

void CFeatureMatcher::detectSURFKeyPoints(const cv::Mat& img_1, const cv::Mat& img_2)
{
	cvtColor(img_1, m_Img_1, COLOR_BGR2GRAY);
	cvtColor(img_2, m_Img_2, COLOR_BGR2GRAY);


	m_detector.detect(m_Img_1, m_keypoints_1);
	m_detector.detect(m_Img_1, m_keypoints_2);

	//An alternative TODO check it out
	//ORB::ORB(int nfeatures = 500, float scaleFactor = 1.2f, int nlevels = 8, int edgeThreshold = 31, int firstLevel = 0, int WTA_K = 2, int scoreType = ORB::HARRIS_SCORE, int patchSize = 31)
	//void ORB::operator()(InputArray image, InputArray mask, vector<KeyPoint>& keypoints, OutputArray descriptors, bool useProvidedKeypoints=false )

	//This is much faster
	//FREAK::FREAK(bool orientationNormalized=true, bool scaleNormalized=true, float patternScale=22.0f, int nOctaves=4, const vector<int>& selectedPairs=vector<int>() )

}


void CFeatureMatcher::calculateSURFDescriptors()
{
	m_extractor.compute(m_Img_1, m_keypoints_1, m_descriptors_1);
	m_extractor.compute(m_Img_2, m_keypoints_2, m_descriptors_2);
}

void CFeatureMatcher::matchDescriptors()
{
	m_matcher.match(m_descriptors_1, m_descriptors_2, m_matches);
}

void CFeatureMatcher::findGoodMatches()
{

	double max_dist = 0; double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	for (int i = 0; i < m_descriptors_1.rows; i++)
	{
		double dist = m_matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}

	//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
	//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
	//-- small)
	//-- PS.- radiusMatch can also be used here.
	for (int i = 0; i < m_descriptors_1.rows; i++)
	{
		if (m_matches[i].distance <= __max(2 * min_dist, 0.02))
		{
			m_good_matches.push_back(m_matches[i]);
		}
	}
}

/*

void CFeatureMatcher::calcHwithRansac()
{

	double max_dist = 0; double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	for (int i = 0; i < m_descriptors_1.rows; i++)
	{
		double dist = m_matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}

	//-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist,
	//-- or a small arbitary value ( 0.02 ) in the event that min_dist is very
	//-- small)
	//-- PS.- radiusMatch can also be used here.
	for (int i = 0; i < m_descriptors_1.rows; i++)
	{
		if (m_matches[i].distance <= __max(2 * min_dist, 0.02))
		{
			m_good_matches.push_back(m_matches[i]);
		}
	}
}

*/


void CFeatureMatcher::showGoodMatches(const std::string& win_name)
{
	//-- Draw only "good" matches
	Mat img_matches;
	drawMatches(m_Img_1, m_keypoints_1, m_Img_2, m_keypoints_2,
		m_good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	//-- Show detected matches
	namedWindow(win_name, CV_WINDOW_AUTOSIZE);
	imshow(win_name, img_matches);

	for (int i = 0; i < (int)m_good_matches.size(); i++)
	{
		printf("-- Good Match [%d] Keypoint 1: %d  -- Keypoint 2: %d  \n", i, m_good_matches[i].queryIdx, m_good_matches[i].trainIdx);
	}
	waitKey(0);
}
