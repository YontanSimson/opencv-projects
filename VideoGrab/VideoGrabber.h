#pragma once
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/features2d.hpp"

#include "ParameterTypes.h"

static const int NUM_OF_CAMERAS = 2;


class CVideoGrabber
{
public:
	CVideoGrabber();
	~CVideoGrabber();

	void grabVideoStream();
	void grab2Images(cv::Mat& img_1, cv::Mat& img_2);

private:
	//return formatted string of time and data
	std::string return_current_time_and_date();

	//detect GFTT points and track with LK
	void trackPointsAndDraw(cv::Mat& image,
		cv::Mat& gray,
		const cv::Mat& prevGray,
		const SCornerDetectorParams& CDParams,
		std::vector<cv::Point2f>* points,
		bool& needToInit);

	//detect keypoints with feature detector and track with LK
	void trackKeyPointsAndDraw(const cv::FeatureDetector& detector,
		cv::Mat& image,
		cv::Mat& gray,
		const cv::Mat& prevGray,
		const SCornerDetectorParams& CDParams,
		std::vector<cv::KeyPoint>* keypoints,
		bool& needToInit);

	//Detect Key points to be tracked
	void detectKeyPoints(const cv::FeatureDetector& detector,
		const cv::Mat& gray,
		std::vector<cv::KeyPoint>* keypoints);

	//Track and draw Key Points
	void trackKeyPoints(cv::Mat& image,
		const cv::Mat& gray,
		const cv::Mat& prevGray,
		const SCornerDetectorParams& CDParams,
		std::vector<cv::KeyPoint>* keypoints);


	/*
	* Update keypoints with points
	* status       Valid points to keep
	* points       The updated points to be copied to keypoints
	* keypoints    The keypoints to be updated
	*/
	void updateKeyPoints(
		const std::vector<uchar>& status,
		const std::vector<cv::Point2f> points,
		std::vector<cv::KeyPoint>& keypoints);

	/*
	* Mark keypoints on color image
	* image        The image with keypoints
	* keypoints    The keypoints to be marked
	*/
	void markKeyPointsOnImage(cv::Mat& image,
		std::vector<cv::KeyPoint> keypoints);

private:

	std::vector<int> m_compression_params;
	cv::VideoCapture m_cap[NUM_OF_CAMERAS];
	cv::Mat m_frame[NUM_OF_CAMERAS];

	//for stacking two frames horizontally
	cv::Rect m_Roi[NUM_OF_CAMERAS];
	cv::Mat m_stackedView;

	enum EREFCUR
	{
		REF_FRAME = 0,
		CUR_FRAME = 1,
		NUM_CONSECUTIVE_FRAMES
	};

	//For feature detection and matching
	SCornerDetectorParams m_CDParams;
	SFeatureDetectionParams m_FDParams;
	cv::Ptr<cv::FeatureDetector> m_detector;
	cv::Ptr<cv::DescriptorExtractor> m_descriptor;
	cv::Ptr<cv::DescriptorMatcher> m_matcher;

};

