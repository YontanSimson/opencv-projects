========================================================================
    CONSOLE APPLICATION : Main Project Overview
========================================================================


This project grabs 2 Streams simultaneously from two USB webcams. 
This is a stub with a future goal for 3D reconstruction from two webcam images.


This file contains a summary of what you will find in each of the files that 
make up the VideoGrab application.

VideoGrabber.h/cpp
Video grabber Object for two web-cams - in future will do feature point matching too

VideoGrab.vcxproj

VideoGrab.vcxproj.filters

main.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named ConsoleApplication1.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
