#include "stdafx.h"
#include "VideoGrabber.h"
#include "FeatureMatcherClass.h"

#include <iostream> //std::cout
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string

#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/features2d/features2d.hpp>


using namespace cv;
CVideoGrabber::CVideoGrabber():
	m_CDParams()
{
	m_compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
	m_compression_params.push_back(100);

	//init webcam read 
	for (int i = 0; i < NUM_OF_CAMERAS; i++)
	{
		m_cap[i].open(i);
		if (!m_cap[i].isOpened())  // check if we succeeded
			CV_Assert("Can't open Web-Cam");

		//grab frames in order to get input image size 
		bool bSuccess = m_cap[i].read(m_frame[i]); // read a new frame from video

		if (!bSuccess) //if not success, break loop
		{
			std::cout << "Cannot read the frame from video file" << std::endl;
			break;
		}
	}

	CV_ARE_SIZES_EQ(m_frame + 0, m_frame + 1);

	//for stacking two frames horizontally
	int offsetX = 0;
	for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
	{
		m_Roi[i] = Rect(offsetX, 0, m_frame[i].cols, m_frame[i].rows);
		offsetX += m_frame[i].cols;
	}
	m_stackedView.create(m_frame[0].rows, m_frame[0].cols*NUM_OF_CAMERAS, m_frame[0].type());


}

CVideoGrabber::~CVideoGrabber()
{
}

void CVideoGrabber::grabVideoStream()
{
	//Display both frames side by side
	namedWindow("3DVision", CV_WINDOW_AUTOSIZE);

	bool needToInit = true;

	Mat gray[NUM_OF_CAMERAS], prevGray[NUM_OF_CAMERAS], image[NUM_OF_CAMERAS];
	vector<Point2f> points[NUM_OF_CAMERAS][NUM_CONSECUTIVE_FRAMES];
	vector<KeyPoint> keypoints[NUM_OF_CAMERAS][NUM_CONSECUTIVE_FRAMES];

	double hessianThreshold = 1000;
	int nOctaves = 4;
	int nOctaveLayers = 2;
	bool extended = true;
	bool upright = false;

	m_detector = new SurfFeatureDetector;//TBD - move to Ctor of Video Grab
	//m_detector = new OrbFeatureDetector(m_CDParams.MAX_COUNT, 1.2f, 3, 25);
	FeatureMatchingClass featureMatcher;//TBD - move to Ctor of Video Grab

	for (;;)
	{
		//grab frames in order to get input image size 
		for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			m_cap[i] >> m_frame[i]; // get a new frame from camera

		//Convert to gray scale
		for (int i = 0; i < NUM_OF_CAMERAS; i++)
		{
			image[i] = m_frame[i];//m_frame[0].copyTo(image);
			cvtColor(image[i], gray[i], COLOR_BGR2GRAY);
		}

		////////////////////////////////////////////
		//Detect points of interest and track them//
		////////////////////////////////////////////
		if (needToInit)//Detection
		{
			double t = (double)getTickCount();
			for (int i = 0; i < NUM_OF_CAMERAS; i++)
			{
				detectKeyPoints(*m_detector, gray[i], keypoints[i]);
			}
			t = ((double)getTickCount() - t) / getTickFrequency();
			std::cout << "detection time [s]: " << t / 1.0 << std::endl;

			t = (double)getTickCount();
			//Feature point matching Frame 0 <-> Frame 1 taken at the same time
			std::vector<cv::DMatch> matches;//matching points
			std::vector<cv::Point2f> points1;
			std::vector<cv::Point2f> points2;
			cv::Mat fundamental = featureMatcher.match(
				gray[0], // input reference image
				gray[1], // input current image
				keypoints[0][CUR_FRAME], // input computed object keypoints
				keypoints[1][CUR_FRAME], // input computed object keypoints
				matches, // output matches
				points1, // output object keypoints (Point2f)
				points2); // output scene keypoints (Point2f)
			t = ((double)getTickCount() - t) / getTickFrequency();
			std::cout << "Matching time [s]: " << t / 1.0 << std::endl;

			//-- Draw only "good" matches
			Mat img_matches;
			drawMatches(gray[0], keypoints[0][CUR_FRAME], gray[1], keypoints[1][CUR_FRAME],
				matches, img_matches, Scalar::all(-1), Scalar::all(-1),
				vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

			namedWindow("Matching Points", CV_WINDOW_AUTOSIZE);
			imshow("Matching Points", img_matches);
			waitKey(10);

			//Write matching Points to file
			FileStorage fs("FCalibration.yml", FileStorage::WRITE);
			fs << "fundamental Matrix" << fundamental;
			fs << "Frame 0 Key Points" << points1;
			fs << "Frame 1 Key Points" << points2;
			fs.release();
		}
		else//Tracking 
		{
			for (int i = 0; i < NUM_OF_CAMERAS; i++)
			{
				trackKeyPoints(image[i], gray[i], prevGray[i],
					m_CDParams, keypoints[i]);
			}
		}

		//Check if too many keypoints were lost during tracking
		needToInit = false;
		for (int i = 0; i < NUM_OF_CAMERAS; i++)
		{
			if (keypoints[i][1].size() < m_CDParams.MAX_COUNT / 2)
				needToInit |= true;
		}

		//Display both streams stacked horizontally
		for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			m_frame[i].copyTo(m_stackedView(m_Roi[i]));
		imshow("3DVision", m_stackedView);

		int c = waitKey(10);
		if ((char)c == 'q') 
		{	
			break; 
		}
		else if ( (char)c == 'c' )
		{
			//Get time and date of shot
			string time_str = return_current_time_and_date();

			//save both frames to disk
			for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			{
				string filename = "WebCam" + std::to_string(i+1) + "_" + time_str + ".jpg";
				imwrite(filename, m_frame[i], m_compression_params);
			}
		}

		for (int i = 0; i < NUM_OF_CAMERAS; i++)
		{
			std::swap(points[i][CUR_FRAME], points[i][REF_FRAME]);
			std::swap(keypoints[i][CUR_FRAME], keypoints[i][REF_FRAME]);
			cv::swap(prevGray[i], gray[i]);
		}
	}
}

void CVideoGrabber::trackPointsAndDraw(cv::Mat& image, 
	cv::Mat& gray,
	const cv::Mat& prevGray,
	const SCornerDetectorParams& CDParams,
	std::vector<cv::Point2f>* points,
	bool& needToInit)
{

	cvtColor(image, gray, COLOR_BGR2GRAY);

	if (needToInit)	// detect
	{
		goodFeaturesToTrack(gray, 
			points[1], 
			CDParams.MAX_COUNT, 
			CDParams.qualityLevel, 
			CDParams.minDistance, Mat(), 
			CDParams.blockSize, 
			CDParams.useHarrisDetector, 
			CDParams.k);
		cornerSubPix(gray, 
			points[1], 
			CDParams.subPixWinSize, 
			Size(-1, -1), 
			CDParams.termcrit);
	}
	else if (!points[0].empty())//track
	{
		vector<uchar> status;
		vector<float> err;
		if (prevGray.empty())
			gray.copyTo(prevGray);
		calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, CDParams.winSize,
			3, CDParams.termcrit, 0, 0.001);
		size_t i, k;
		for (i = k = 0; i < points[1].size(); i++)
		{
			if (!status[i])
				continue;

			points[1][k++] = points[1][i];
			circle(image, points[1][i], 3, Scalar(0, 255, 0), -1, 8);
		}
		points[1].resize(k);
	}

	if (points[1].size() < CDParams.MAX_COUNT / 5)
		needToInit = true;
	else
		needToInit = false;
}

void CVideoGrabber::trackKeyPointsAndDraw(const cv::FeatureDetector& detector,
	cv::Mat& image,
	cv::Mat& gray,
	const cv::Mat& prevGray,
	const SCornerDetectorParams& CDParams,
	std::vector<cv::KeyPoint>* keypoints,
	bool& needToInit)
{

	cvtColor(image, gray, COLOR_BGR2GRAY);

	if (needToInit)
	{

		double t = (double)getTickCount();
		detector.detect(gray, keypoints[CUR_FRAME]);
		keypoints[REF_FRAME] = keypoints[CUR_FRAME];//for first frame to avoid having an empty structure
		//detector.detect(imgB, keypointsB);
		t = ((double)getTickCount() - t) / getTickFrequency();
		std::cout << "detection time [s]: " << t / 1.0 << std::endl;
	}
	else if (!keypoints[REF_FRAME].empty())
	{
		vector<uchar> status;
		vector<float> err;
		if (prevGray.empty())
			gray.copyTo(prevGray);

		std::vector<cv::Point2f> points[NUM_CONSECUTIVE_FRAMES];

		KeyPoint::convert(keypoints[REF_FRAME], points[REF_FRAME]);
		KeyPoint::convert(keypoints[CUR_FRAME], points[CUR_FRAME]);

		calcOpticalFlowPyrLK(prevGray, gray, points[REF_FRAME], points[CUR_FRAME], status, err, CDParams.winSize,
			3, CDParams.termcrit, 0, 0.001);
		
		//update keypoints
		updateKeyPoints(status, points[CUR_FRAME], keypoints[CUR_FRAME]);

		//mark points on image
		markKeyPointsOnImage(image, keypoints[CUR_FRAME]);
	}

	if (keypoints[1].size() < CDParams.MAX_COUNT / 3)
		needToInit = true;
	else
		needToInit = false;
}


//Detect Key points to be tracked
void CVideoGrabber::detectKeyPoints(const cv::FeatureDetector& detector,
	const cv::Mat& gray,
	std::vector<cv::KeyPoint>* keypoints)
{
		detector.detect(gray, keypoints[CUR_FRAME]);
		keypoints[REF_FRAME] = keypoints[CUR_FRAME];//for first frame to avoid having an empty structure
}

//Track and draw Key Points
void CVideoGrabber::trackKeyPoints(
	cv::Mat& image,
	const cv::Mat& gray,
	const cv::Mat& prevGray,
	const SCornerDetectorParams& CDParams,
	std::vector<cv::KeyPoint>* keypoints)
{

	if (!keypoints[REF_FRAME].empty())
	{
		vector<uchar> status;
		vector<float> err;
		if (prevGray.empty())
			gray.copyTo(prevGray);

		std::vector<cv::Point2f> points[NUM_CONSECUTIVE_FRAMES];

		KeyPoint::convert(keypoints[REF_FRAME], points[REF_FRAME]);
		KeyPoint::convert(keypoints[CUR_FRAME], points[CUR_FRAME]);

		calcOpticalFlowPyrLK(prevGray, gray, points[REF_FRAME], points[CUR_FRAME], status, err, CDParams.winSize,
			3, CDParams.termcrit, 0, 0.001);

		//update keypoints
		updateKeyPoints(status, points[CUR_FRAME], keypoints[CUR_FRAME]);

		//mark points on image
		markKeyPointsOnImage(image, keypoints[CUR_FRAME]);
	}

}

void CVideoGrabber::grab2Images(cv::Mat& img_1, cv::Mat& img_2)
{
	//Display both frames side by side
	namedWindow("3DVision", CV_WINDOW_AUTOSIZE);
	//grab frames in order to get input image size
	m_cap[0] >> img_1;
	m_cap[1] >> img_2;

	img_1.copyTo(m_stackedView(m_Roi[0]));
	img_2.copyTo(m_stackedView(m_Roi[1]));

	imshow("3DVision", m_stackedView);
	waitKey(10);
}

std::string CVideoGrabber::return_current_time_and_date()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%d%m%Y_%I%M%S");
	return ss.str();
}

void CVideoGrabber::updateKeyPoints(
	const std::vector<uchar>& status, 
	const std::vector<cv::Point2f> points,
	std::vector<cv::KeyPoint>& keypoints)
{
	size_t i = 0, k = 0;
	auto it_status = status.begin();
	auto it_keypoints = keypoints.begin();
	for (auto it = points.begin(); it != points.end(); ++it_status, ++it, ++i)
	{
		if (!(*it_status))
			continue;

		*it_keypoints = keypoints[i];//copy general data
		it_keypoints->pt = points[i];//update point

		k++;
		it_keypoints++;
	}
	keypoints.resize(k);
}


void CVideoGrabber::markKeyPointsOnImage(cv::Mat& image,
	std::vector<cv::KeyPoint> keypoints)
{
	for (auto it_keypoints = keypoints.begin(); it_keypoints != keypoints.end(); ++it_keypoints)
	{
		circle(image, it_keypoints->pt, 3, Scalar(0, 255, 0), -1, 8);
	}
}
