// main.cpp : Defines the entry point for the console application.
//
#include "LinkedLists.h"
#include <cstdio>
#include <unordered_set>

using namespace std;

void BuildOneTwoThree()
{
	CLinkedList myList;
	myList.appendToTail(1);
	myList.appendToTail(2);
	myList.appendToTail(3);
	myList.Print();
	myList.deleteNode(myList.GetHead());
	myList.Print();
	return;
}

void ArrToList(const int* arr, int arr_size, CLinkedList& _list)
{
	for (int i = 0; i < arr_size; i++, ++arr)
	{
		_list.appendToTail(*arr);
	}
}

//Add two lists with single digit numbers
CNode* AddLists(CNode* list1, CNode* list2)
{
	if (list1 == nullptr || list2 == nullptr)
	{
		return nullptr;
	}
	CNode* newList = nullptr, *outHead = nullptr;
	int carry = 0;
	int sum = 0;

	while (list1 != nullptr && list2 != nullptr)
	{
		sum = list1->m_data + list2->m_data + carry;
		carry = sum / 10;
		sum %= 10;

		if (outHead == nullptr)
		{
			newList = new CNode(sum);
			outHead = newList;
		}
		else
		{ 
			newList->next = new CNode(sum);
			newList = newList->next;
		}
		list1 = list1->next;
		list2 = list2->next;
	}
	if (carry != 0)//carry over
	{
		newList->next = new CNode(carry);
	}
	return outHead;
}

void DestroyList(CNode* head)
{
	CNode * ptr = head;
	while (ptr != nullptr)
	{
		CNode * ptr_prev = ptr;
		ptr = ptr->next;
		delete ptr_prev;
	}
}



/*Given a circular linked list, implement an algorithm which returns node at the beginning of the loop*/
//A->B->C->D->E->C(same C as before) ->return C

//using a hash table/unordered set
CNode* CheckCircular(CNode* head)
{
	std::unordered_set<CNode *> table;

	CNode* ptr = head;
	while (ptr != nullptr)
	{
		auto it = table.find(ptr);
		if (it == table.end())
		{
			table.insert(ptr);
		}
		else
		{
			return ptr;
			break;
		}

		ptr = ptr->next;
	}
	
	return nullptr;
}

//using to pointers with different speeds - Cracking the code interview solution
CNode* FindBegining(CNode* head)
{
	CNode* ptr1 = head;//slow pointer
	CNode* ptr2 = head;//fast pointer

	while (ptr2->next != nullptr)
	{
		ptr1 = ptr1->next;
		ptr2 = ptr2->next->next;
		if (ptr1 == ptr2)
		{
			break;
		}
	}

	//check if no cycles
	if (ptr2->next == nullptr)
	{
		return nullptr;
	}

	//send ptr1 back to head and leave ptr2 at the meeting place
	//advance both untill they meet. The meeting place is the begining of the cycle
	ptr1 = head;
	while (ptr1 != ptr2)
	{
		ptr1 = ptr1->next;
		ptr2 = ptr2->next;
	}

	return ptr1;
}

CNode* addNode(CNode* tail, int d)
{
	if (tail == nullptr)
	{
		return new CNode(d);
	}
	else
	{
		tail->next = new CNode(d);
		return tail->next;
	}
}



int main()
{
	BuildOneTwoThree();

	CLinkedList myList;
	myList.appendToTail(1);
	myList.appendToTail(2);
	myList.appendToTail(3);
	myList.appendToTail(2);
	myList.appendToTail(2);
	myList.appendToTail(4);
	myList.appendToTail(4);
	myList.Print();

	CNode* first = myList.nThToLast(0);
	CNode* middle = myList.nThToLast(4);
	CNode* last = myList.nThToLast(6);
	CNode* out = myList.nThToLast(7);

	myList.RemoveDuplicatesNoBuffer();

	printf("Duplicates removed\n");
	myList.Print();

	//Add two number represented by a linked list
	//e.g. (3 -> 1 -> 5) + (5 -> 9 -> 2) = (8->0->8)
	int arr1[] = { 9, 9, 9 };
	int arr2[] = { 9, 9, 9 };

	CLinkedList list1;
	ArrToList(arr1, 3, list1);

	CLinkedList list2;
	ArrToList(arr2, 3, list2);
	CNode* sumList = AddLists(list1.GetHead(), list2.GetHead());
	DestroyList(sumList);
	sumList = nullptr;

	//circular node test
	CNode * head;
	CNode * circular = head = addNode(nullptr, 1);
	circular = addNode(circular, 2);
	circular = addNode(circular, 3);
	CNode * temp = circular;
	circular = addNode(circular, 4);
	circular->next = temp;

	CNode* cycle1 = CheckCircular(head);
	CNode* cycle2 = FindBegining(head);
	circular->next = nullptr;
	DestroyList(head);

	return 0;
}
