#pragma once

//A Nice source on linked lists :
//http://cslibrary.stanford.edu/105/LinkedListProblems.pdf

class CNode
{
public:
	CNode* next;
	int m_data;

	CNode() :
		next(nullptr)
	{}

	CNode(int data) :
		m_data(data),
		next(nullptr)
	{}
	~CNode() 
	{}
};

class CLinkedList
{
public:
	CLinkedList();
	~CLinkedList();

	void Print();
	int Length();
	void appendToTail(int data);
	static CNode *advance(CNode* ptr, int offset);
	CNode* GetHead() { return m_head; }

	void RemoveDuplicates();
	void RemoveDuplicatesNoBuffer();
	//My solution - uses a stack of pointers
	CNode* GetNthNodeFromEnd(int n);
	//Cracking the code interveiw solution - uses two pointers
	CNode* nThToLast(int n);
	bool deleteNode(CNode* node);

private:
	CNode *m_head;

	// user-defined copy assignment, copy-and-swap form
	CLinkedList& operator=(CLinkedList other)
	{
		return *this;
	}
};

