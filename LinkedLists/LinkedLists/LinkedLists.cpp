#include "LinkedLists.h"
#include <cstdio>
#include <cassert>
#include <unordered_map> //hash functions
#include <stack>
#include <cassert>

using namespace std;

CLinkedList::CLinkedList():
	m_head(nullptr)
{
}

CLinkedList::~CLinkedList()
{
	//destroy list
	CNode * ptr = m_head;
	while (ptr->next != nullptr)
	{
		CNode * ptr_prev = ptr;
		ptr = ptr->next;
		delete ptr_prev;
	}
	delete ptr;
	m_head = nullptr;
}

void CLinkedList::Print()
{
	CNode * ptr = m_head;
	while (ptr->next != nullptr)
	{
		printf("%d->", ptr->m_data);
		ptr = ptr->next;
	}
	printf("%d->NULL\n", ptr->m_data);
}

int CLinkedList::Length()
{
	CNode * ptr = m_head;
	int counter = 0;
	while (ptr->next != nullptr)
	{
		counter++;
		ptr = ptr->next;
	}
	return 0;
}

//for a single linked list
void CLinkedList::appendToTail(int data)
{
	//advance pointer to end
	CNode * tail = m_head;

	if (tail == nullptr)
	{
		m_head = new CNode(data);
	}
	else
	{
		while (tail->next != nullptr)
		{
			tail = tail->next;
		}
		tail->next = new CNode(data);
	}
}

//Not complete yet
/*
CNode * CLinkedList::deleteNode(int d)
{
	if (m_head == nullptr)
	{
		return nullptr;
	}
	else
	{
		CNode* tail = m_head;
		while (tail->next != nullptr && tail->m_data != d)
		{
			tail = tail->next;
		}
	}
	return nullptr;
}*/

CNode * CLinkedList::advance(CNode * ptr, int offset)
{
	for (int i = 0; i < offset; i++)
	{
		assert(ptr->next != nullptr);
		ptr = ptr->next;
	}
	return ptr;
}

void CLinkedList::RemoveDuplicates() 
{
	//std::unordered_map<int, std::pair<CNode*, CNode*>> hashTable;
	std::unordered_map<int, bool> hashTable;

	CNode* previous = nullptr;
	CNode* ptr = m_head;

	while (ptr != nullptr)
	{
		if (hashTable.find(ptr->m_data) != hashTable.end())
		{

			previous->next = ptr->next;

			CNode* temp = previous;
			delete ptr;
			ptr = temp;
		}
		else
		{
			hashTable[ptr->m_data] = true;//std::make_pair(previous, ptr->next);
			previous = ptr;
		}
		ptr = ptr->next;
	}
}

void CLinkedList::RemoveDuplicatesNoBuffer()
{

	CNode* previous = nullptr;
	CNode* ptr = m_head;

	while (ptr != nullptr)
	{
		CNode* ptr_inner = ptr->next;
		previous = ptr;
		while (ptr_inner)
		{
			if (ptr_inner->m_data == ptr->m_data)
			{
				previous->next = ptr_inner->next;

				CNode* temp = previous;
				delete ptr_inner;
				ptr_inner = temp;
			}
			else
			{
				previous = ptr_inner;
			}

			ptr_inner = ptr_inner->next;
		}

		ptr = ptr->next;
	}
}

CNode* CLinkedList::GetNthNodeFromEnd(int n)
{
	//Parse list and create a copy of the same list only in reverse order
	CNode* ptr = m_head;
	std::stack<CNode*> nodeStack;

	int length = 0;
	while (ptr != nullptr)
	{
		nodeStack.push(ptr);
		ptr = ptr->next;
		length++;
	}
	assert(n < length);

	for (int i = 0; i < n; i++)
	{
		nodeStack.pop();
	}
	CNode* nThNode = nodeStack.top();

	return nThNode;
}

CNode * CLinkedList::nThToLast(int n)
{
	CNode* ptrFirst  = m_head;
	CNode* ptrSecond = nullptr;

	int length = 0;
	while (length < n && ptrFirst != nullptr)
	{
		ptrFirst = ptrFirst->next;
		length++;
	}

	if (ptrFirst == nullptr)
	{
		return nullptr;
	}

	ptrSecond = m_head;
	while (ptrFirst->next != nullptr)
	{
		ptrFirst = ptrFirst->next;
		ptrSecond = ptrSecond->next;
	}

	return ptrSecond;
}

bool CLinkedList::deleteNode(CNode* node)
{
	if (node == nullptr || node->next == nullptr)
	{
		return false;
	}
	CNode * nextNode = node->next;
	node->next = nextNode->next;
	node->m_data = nextNode->m_data;
	delete nextNode;
	return true;
}
