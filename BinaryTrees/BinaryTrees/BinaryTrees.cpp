// BinaryTrees.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "stdlib.h"
#include <cassert>

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif

struct node
{
	int data;
	struct node* left;
	struct node* right;

};

struct node *NewNode(const int& data)
{
	struct node* root = (struct node*)malloc(sizeof(node));
	root->data = data;
	root->left = NULL;
	root->right = NULL;
	return root;
}

static int LookUp(struct node* tree, int target)
{
	if (tree == NULL)
	{
		return 0;
	}
	else
	{
		if (tree->data == target)
		{
			return 1;
		}
		else if (tree->data < target) 
		{
			return LookUp(tree->right, target);
		}
		else 
		{
			return LookUp(tree->left, target);
		}
	}
}

void ReleaseTree(struct node* tree)
{
	if (tree == NULL)
	{
		return;
	}
	else
	{
		if (tree->left == NULL && tree->right == NULL)
		{
			printf("Freeing %d\n", tree->data);
			free(tree);
			return;
		}
		else 
		{
			if (tree->left != NULL)
			{
				ReleaseTree(tree->left);
				tree->left = NULL;
			}

			if (tree->right != NULL)
			{
				ReleaseTree(tree->right);
				tree->right = NULL;
			}

			ReleaseTree(tree);
			tree = NULL;
			return;
		}
		assert(false);//this should never happen
	}
}

struct node * insert(struct node* tree, int val)
{

	if (tree == NULL)
	{
		return NewNode(val);
	}
	else
	{
		if (tree->data > val)
		{
			tree->left = insert(tree->left, val);
		}
		else
		{
			tree->right = insert(tree->right, val);
		}
		return tree;
	}
};

int GetSize(struct node* tree)
{
	int leftSide = 0, rightSide = 0;

	if (tree == NULL)
	{
		return 0;
	}
	else 
	{
		leftSide = GetSize(tree->left);
		rightSide = GetSize(tree->right);

		return leftSide + rightSide + 1;
	}

}

int MaxDepth(struct node* tree)
{
	int leftSide = 0, rightSide = 0;

	if (tree == NULL)
	{
		return 0;
	}
	else
	{
		leftSide = MaxDepth(tree->left);
		rightSide = MaxDepth(tree->right);

		return (leftSide < rightSide) ? rightSide + 1 : leftSide + 1;
	}

}

int MinValue(struct node* tree)
{
	if (tree->left == NULL)
	{
		return tree->data;
	}
	else
	{
		return MinValue(tree->left);
	}
}

//For each node, the strategy is : recur left, print the node data, recur right.
//Prints in order
void printTree(struct node* tree) 
{
	if (tree == NULL)
	{
		return;
	}
	printTree(tree->left);
	printf("%d\n", tree->data);
	printTree(tree->right);
	return;
}

//Print starting from bottom levels in the binary tree
void printPostorder(struct node* tree)
{
	if (tree == NULL)
	{
		return;
	}

	printTree(tree->left);
	printTree(tree->right);
	printf("%d\n", tree->data);
}

int main()
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	int arr[] = { 4, 2, 5, 1, 3 };


	struct node * root = insert(NULL, arr[0]);
	int numOfNodes = sizeof(arr) / sizeof(arr[0]);

	for (int i = 1; i < numOfNodes; ++i)
	{
		insert(root, arr[i]);
	}

	int size = GetSize(root);
	int depth = MaxDepth(root);
	int minValue = MinValue(root);
	printf("Print in order:\n");
	printTree(root);
	printf("Print post order:\n");
	printPostorder(root);

	ReleaseTree(root);
    return 0;
}

