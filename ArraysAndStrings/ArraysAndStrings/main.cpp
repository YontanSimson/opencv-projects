//#include "FileUtiles.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif

//O(n)
void ReverseString(char buffer[])
{
	if (buffer == NULL)
		return;
	char temp;
	int length = strlen(buffer);
	for (int i = 0; i < length / 2; ++i)
	{
		temp = buffer[i];
		buffer[i] = buffer[length - 1 - i];
		buffer[length - 1 - i] = temp;
	}
}

//no extra memory version
bool Unique(char buffer[])
{
	bool unique = true;
	int length = strlen(buffer);
	for (int i = 0; i < length; ++i)
	{
		char currChar = buffer[i];
		for (int j = i+1; j < length; ++j)
		{
			if (buffer[j] == currChar)
			{
				return false;
			}
		}
	}
	return unique;
}

//bit wise version
bool isUniqueChars(char buffer[]) 
{
	int length = strlen(buffer);
	unsigned long long checker = 0;
	for (int i = 0; i < length; ++i) 
	{
		int val = buffer[i] - 'a';
		unsigned long long mask = (1i64 << unsigned long long(val));
		if ((checker & mask) != 0) return false;
		checker |= ((unsigned long long)1 << val);
	}
	return true;
}

/*Design an algorithm and write code to remove the duplicate characters in a string 
without using any additional buffer. NOTE: One or two additional variables are fine. 
An extra copy of the array is not.*/
//O(n^2) - no extra memory at all
void RemoveDuplicates(char buffer[])
{
	if (buffer == NULL)
		return;
	int length = strlen(buffer);
	if (length < 1)
		return;

	for (int i = 0; i < length; ++i)
	{
		char currChar = buffer[i];
		for (int j = i + 1; j < length; ++j)
		{
			while (buffer[j] == currChar)// while - in case we have consecutive letters that are the same
			{
				//shift all chars down by one char
				//also move '\0' down by one
				for (int k = j; k < length; ++k)
				{
					buffer[k] = buffer[k + 1];
				}
				
				length--;
			}
		}
	}
}

//Algorithm�With Additional Memory of Constant Size
void removeDuplicatesEff(char str[]) 
{
	if (str == NULL)
		return;
		
	int len = strlen(str);
	if (len < 1) 
		return;

	bool hit[256] = {false};

	hit[str[0]] = true;
	int tail = 1;
	for (int i = 1; i < len; ++i) 
	{
		if (!hit[str[i]]) 
		{
			str[tail] = str[i];
			++tail;
			hit[str[i]] = true;
		}
	}
	str[tail] = '\0';
}

//Helper function for the Anagram check - assume initiliazed histogram
void MakeCharHist(const char str[], int hist[256])
{
	size_t len = strlen(str);
	for (size_t i = 0; i < len; i++)
	{
		hist[str[i]]++;
	}
}

//The idea is to build two histograms and compare to each other
bool IsAnagram(const char str1[], const char str2[])
{
	static const int CHAR_BUFFER_SIZE = 256;
	int hist1[CHAR_BUFFER_SIZE] = { 0 };
	int hist2[CHAR_BUFFER_SIZE] = { 0 };

	MakeCharHist(str1, hist1);
	MakeCharHist(str2, hist2);

	//compare 
	for (size_t i = 0; i < CHAR_BUFFER_SIZE; i++)
	{
		if (hist1[i] != hist2[i])
		{
			return false;
		}
	}

	return true;
}

// The idea is to find is there exist a permutation that is a Palindrome (reverse order is the same)
// Simple solution check histogram see if every character exist exactly twice (one may exist once if the size is odd)
// Ignore space characters
// Case sensitive
bool PermutationPalindrom(const char str[])
{
	//check if either string is null
	if (str == NULL)
	{
		return false;
	}

	//check that both strings are of the same length
	int len = strlen(str);


	//make histogram
	static const int CHAR_BUFFER_SIZE = 256;
	int hist[CHAR_BUFFER_SIZE] = { 0 };
	MakeCharHist(str, hist);
	int numOfSpaceChars = hist[32];
	len -= numOfSpaceChars;

	//split 
	if (len % 2 == 0)//is even
	{
		for (int i = 0; i < 256; ++i)
		{
			if (hist[i] != 2)
			{
				return false;
			}
		}
	}
	else
	{
		int numOfSingleChars = 0;
		for (int i = 0; i < 256; ++i)
		{
			if (i == 32)//ignore space characters
			{
				continue;
			}
			if (!(hist[i] == 2 || hist[i] == 0))
			{
				if (hist[i] == 1)
				{
					numOfSingleChars++;
				}
				else
				{
					return false;
				}
			}

			if (numOfSingleChars >= 2)
			{
				return false;
			}
		}
	}

	return true;
}

//Requires two passes and one extra buffer
void ReplaceSpacesWith02(char** strInOut)
{
	if (strInOut == NULL || *strInOut == NULL) return;
	char * str = *strInOut;

	int len = strlen(str);
	if (len < 1) return;
	
	//count white spaces
	int countWhiteSpace = 0;
	for (int i = 0; i < len; i++)
	{
		if (str[i] == ' ')
		{
			countWhiteSpace++;
		}
	}

	if (countWhiteSpace == 0) return;

	//init new buffer
	int newLength = len + 2 * countWhiteSpace + 1;//+1 for '\0'
	char* outStr = (char *)malloc(sizeof(char)*newLength);

	//copy input to output
	char* inStr = *strInOut;
	int tail = 0;
	for (int i = 0; i <= len; i++, ++str)
	{
		if (*str == ' ')
		{
			outStr[tail++] = '%';
			outStr[tail++] = '2';
			outStr[tail++] = '0';
		}
		else
		{
			outStr[tail++] = *str;
		}
	}
	
	*strInOut = outStr;

	//release previous memory
	free(inStr);
}

void PrintMatrix(const int *arr, const int rows, const int cols)
{
	for (int i = 0; i < rows; ++i)
	{
		for (int j = 0; j < cols; j++, ++arr)
		{
			printf("%02d ", *arr);
		}
		printf("\n");
	}
}

//Rotate square array CW
//The idea here is to rotate elements in a circle CW from the our layer to the inner most layer
void RotateMatrixClockWise(int *arr, const int N)
{
	int *ptr2D = NULL;
	for (int i = 0; i < N/2; i++)
	{
		int length = N - 2*i;
		ptr2D = arr + i*N + i;//diagonal pointer
		for (int j = 0; j < length-1; j++)
		{
			int top = ptr2D[j];
			//left to top
			ptr2D[j] = ptr2D[(length - 1 - j)*N];

			//bottom to left
			ptr2D[(length - 1 - j)*N] = ptr2D[(length - 1)*N + length - 1 - j];

			//right to bottom
			ptr2D[(length - 1)*N + length - 1 - j] = ptr2D[j*N + length - 1];

			//top to right
			ptr2D[j*N + length - 1] = top;

			//printf("Single round %d\n", j);
			//PrintMatrix(arr, N, N);
		}

		//printf("Rim %d\n", i);
		//PrintMatrix(arr, N, N);
	}
}

//helper function 
//Check if str2 is a substring of str1
bool isSubString(const char * str1, const char * str2)
{
	return strstr(str1, str2) != NULL;
}

/*
Given two strings, s1 and s2, write code to check if s2 is a rotation of s1 using only one call to isSubstring (i.e., �waterbottle� is a rotation of �erbottlewat�).
*/
bool IsRotation(const char * str1, const char * str2)
{
	int len = strlen(str1);
	if (len != strlen(str2))//not the same size
	{
		return false;
	}
	
	char * strDouble = (char *)malloc(sizeof(char)*(2 * len + 1));
	strcpy(strDouble, str1);
	strcat(strDouble, str1);

	return isSubString(strDouble, str2);
}

// N choose K combinations
// Code from http://rosettacode.org/wiki/Combinations#C
/* Type marker stick: using bits to indicate what's chosen.  The stick can't
* handle more than 32 items, but the idea is there; at worst, use array instead */
typedef unsigned long marker;
static marker one = 1;

void comb_rec(int pool, int need, marker chosen, int at)
{
	if (pool < need + at) return; /* not enough bits left */

	if (!need) {
		/* got all we needed; print the thing.  if other actions are
		* desired, we could have passed in a callback function. */
		for (at = 0; at < pool; at++)
			if (chosen & (one << at)) printf("%d ", at);
		printf("\n");
		return;
	}
	/* if we choose the current item, "or" (|) the bit to mark it so. */
	comb_rec(pool, need - 1, chosen | (one << at), at + 1);
	comb_rec(pool, need, chosen, at + 1);  /* or don't choose it, go to next */
}

void comb(int m, int n, unsigned char *c)
{
	int i;
	for (i = 0; i < n; i++) c[i] = n - i;

	while (1) {
		for (i = n; i--;)
			printf("%d%c", c[i], i ? ' ' : '\n');

		/* this check is not strictly necessary, but if m is not close to n,
		it makes the whole thing quite a bit faster */
		if (c[i]++ < m) continue;

		for (i = 0; c[i] >= m - i;) if (++i >= n) return;
		for (c[i]++; i; i--) c[i - 1] = c[i] + 1;
	}
}

int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	//Permutation palindrome test
	const char * palindrome = "tact coa";
	bool isPalindrome1 = PermutationPalindrom(palindrome);

	const char * notPalindrome = "quick cat";
	bool isPalindrome2 = PermutationPalindrom(notPalindrome);

	//n choose k test
	comb_rec(5, 3, 0, 0);
	unsigned char buf[5];
	comb(5, 3, buf);

	//string tests
	char buffer1[] = "abcdefg";
	char buffer2[] = "abcdEafg";
	char buffer3[] = "aaa";
	char buffer4[] = "aaabbb";
	char buffer5[] = "abababa";
	char buffer6[] = "";

	printf("Before: %s\n", buffer1);
	ReverseString(buffer1);
	printf("After reverse: %s\n", buffer1);

	//Check for unique letters - no repetitions of any letters
	printf("First unique: %s\n", Unique(buffer1) ? "true" : "false");
	printf("Second unique: %s\n", Unique(buffer2) ? "true" : "false");

	//Test cases for Anagrams
	char buffer7[] = "dEafabcg";
	char buffer8[] = "abababab";
	char buffer9[] = "aababab";

	bool anagram1 = IsAnagram(buffer2, buffer7);
	bool anagram2 = IsAnagram(buffer9, buffer5);
	bool anagram3 = IsAnagram(buffer8, buffer5);

	//Test case for remove duplicates
	RemoveDuplicates(NULL);
	RemoveDuplicates(buffer1);
	RemoveDuplicates(buffer2);
	RemoveDuplicates(buffer3);
	RemoveDuplicates(buffer4);
	RemoveDuplicates(buffer5);

	//Test case for replace ' ' with '%20'
	char buffer10[] = "the fox jumped over the fence";
	char* str = (char *)malloc(sizeof(char)*(strlen(buffer10) + 1));
	strcpy_s(str, strlen(buffer10) + 1, buffer10);
	ReplaceSpacesWith02(&str);
	free(str);

	//Test cases for rotate matrix
	int arr0[] = { 1 };
	int arr1[] = {
		1, 2,
		3, 4
	};
	int arr2[] = {
		1, 2, 3,
		4, 5, 6,
		7, 8, 9
	};
	int arr3[] = {
		 1,  2,  3,  4, 
		 5,  6,  7,  8, 
		 9, 10, 11, 12,
		13, 14, 15, 16
	};

	printf("Before\n");
	PrintMatrix(arr0, 1, 1);
	RotateMatrixClockWise(arr0, 1);
	printf("After\n");
	PrintMatrix(arr0, 1, 1);

	printf("Before\n");
	PrintMatrix(arr1, 2, 2);
	RotateMatrixClockWise(arr1, 2);
	printf("After\n");
	PrintMatrix(arr1, 2, 2);

	printf("Before\n");
	PrintMatrix(arr2, 3, 3);
	RotateMatrixClockWise(arr2, 3);
	printf("After\n");
	PrintMatrix(arr2, 3, 3);

	printf("Before\n");
	PrintMatrix(arr3, 4, 4);
	RotateMatrixClockWise(arr3, 4);
	printf("After\n");
	PrintMatrix(arr3, 4, 4);

	//rotation test case
	char * str1 = "waterbottle";
	char * str2 = "erbottlewat";

	char * str1a = "aaaabbb";
	char * str2a = "bbaaaab";
	char * str2b = "bbaaabb";

	bool rotation1 = IsRotation(str1, str2);

	bool rotation2 = IsRotation(str1a, str2a);
	bool rotation3 = IsRotation(str1a, str2b);

	return 0;
}