========================================================================
    CONSOLE APPLICATION : Main Project Overview
========================================================================

A tutorial to Open CV written by Anton Belodedenko. Downloaded from GitHub:
https://github.com/BloodAxe/OpenCV-Tutorial/tree/master/OpenCV%20Tutorial

Made a few changes to enable compilation in visual Studio

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named ConsoleApplication1.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
