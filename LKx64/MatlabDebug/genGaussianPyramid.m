function gaussianPyramid = genGaussianPyramid(INPUT , N)

% THIS FUNCTION GENERATES A GAUSSIAN PYRAMID WITH N LEVELS
% input = INPUT (MxN matrix), N (number of levels in the pyramid)
% output = gaussianPyramid (cell array)

gaussianPyramid = cell(1, N);

gaussianPyramid{1} = double(INPUT);
for lvl_idx = 2:N,
    gaussianPyramid{lvl_idx} = reduceIm(gaussianPyramid{lvl_idx-1});
end

end