clear; close all;
%% Get inputs


% refIm = im2double(imread('..\RefIm.jpg'));
% curIm = im2double(imread('..\CurIm.jpg'));
templateIm = im2double(rgb2gray(imread('..\church01.jpg')));
% refIm = curIm(1:end, 2:end);

refIm      = templateIm(1:end, 2:end);
templateIm = templateIm(1:end, 1:end-1);

% refIm = templateIm(2:end, 1:end);
% refIm = padarray(refIm, [1 0], 'post', 'replicate');


%% Params
nLevels = 3;
nIterations = 9;
% HSmooth = fspecial('gaussian', [9 9], 3);
% HSmooth = fspecial('gaussian', [7 7], 1.5);
% HSmooth = fspecial('gaussian', [5 5], 1.0);
HSmooth = fspecial('disk', 7);

%% Make pyramids
curPyramid = genGaussianPyramid(templateIm , nLevels);
refPyramid = genGaussianPyramid(refIm , nLevels);

%% Iterate over pyramid levels starting from smallest
[rows_l, cols_l] = size(curPyramid{nLevels});

U = zeros(rows_l, cols_l, 'single');
V = zeros(rows_l, cols_l, 'single');

imWarped = refPyramid{nLevels};
for idx_level = nLevels:-1:1,

    if (idx_level < nLevels)
        imWarped = imWarp_(U, V, refPyramid{idx_level});
    end
    
    [Ul, Vl, imWarped] = singleLKLevel(curPyramid{idx_level}, imWarped, ...
        HSmooth, nIterations);
    
    U = Ul + U;
    V = Vl + V;

    showVectorField(curPyramid{idx_level}, U, V, 2^(nLevels - idx_level + 1));
    
    if ( idx_level ~= 1 )%expand for next level
        U = 2*imresize( U, size(U)*2, 'bilinear');
        V = 2*imresize( V, size(V)*2, 'bilinear');
    end
    
    err = mean2((curPyramid{idx_level} - imWarped).^2)
end


