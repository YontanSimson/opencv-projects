function [ B ] = imWarp_( Vx, Vy, Bin )

%This function warps B towards A

[x, y] = meshgrid(1:size(Bin,2),1:size(Bin,1));

B = interp2(Bin, x+Vx, y+Vy, 'cubic');
B(isnan(B)) = Bin(isnan(B));

end
