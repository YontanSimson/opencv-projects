function [U, V, warpedIm] = singleLKLevel(curIm, refIm, HSmooth, nIterations)
% THIS FUNCTION Calculates a LK warp (simple shifted version) 
% for one level of a gaussian pyramid
% INPUT:
%   curIm - current image
%   refIm - reference image
%, HSmooth, nIterationsinput = INPUT (MxN matrix), N (number of levels in the pyramid)
% OUTPUT:
% CALLS:
%   calcImageGradient, imWarp

[rows, cols] = size(curIm);

%Calculate Ix, Iy
[Ix, Iy] = calcImageGradient(curIm);

% Calculate Ix^2, Ix*Iy and Iy^2
Ix2 = Ix.*Ix;
Iy2 = Iy.*Iy;
Ixy = Ix.*Iy;

Ix2Smooth = imfilter(Ix2, HSmooth, 'symmetric');
Iy2Smooth = imfilter(Iy2, HSmooth, 'symmetric');
IxySmooth = imfilter(Ixy, HSmooth, 'symmetric');

Vx = zeros(rows, cols, 'single');%for sub pixel accuracy
Vy = zeros(rows, cols, 'single');
U = zeros(rows, cols, 'single');%for sub pixel accuracy
V = zeros(rows, cols, 'single');

Lamda1 = zeros(rows, cols, 'single');
Lamda2 = zeros(rows, cols, 'single');

warpedIm = refIm;
for iter = 1:nIterations,
    % RHS of the LK equation

    It = curIm - warpedIm;
    Ixt = Ix.*It;
    Iyt = Iy.*It;

    % Smooth elements of the time difference x the gradient Ix/y
    IxtSmooth = imfilter(Ixt, HSmooth, 'symmetric');
    IytSmooth = imfilter(Iyt, HSmooth, 'symmetric');

    for ii = 1:rows,
        for jj = 1:cols,
            H = [Ix2Smooth(ii, jj) IxySmooth(ii, jj); 
                 IxySmooth(ii, jj) Iy2Smooth(ii, jj)];
            b = [IxtSmooth(ii, jj); IytSmooth(ii, jj)]; 
            [eigV, Lamda] = eig(H);
            
            %Calc Trace/2
			T2 = trace(H)*0.5;%trace divided by 2
			
			%Calc determinant
			Determ = det(H);
			
			%calc Lamdas - Eigen Values
			L1 = T2 + sqrt( T2.^2 - Determ );
			L2 = T2 - sqrt( T2.^2 - Determ );
			
			%Calc Eigen vectors
			if ( H(1,2) > eps )

				V1 = [L1 - H(2,2);
				      H(1,2);];

				V2 = [L2 - H(2,2);
				      H(1,2);];

				%Normalize the matrices to get autonormal Eigen Vector Matrix
				%norm(InputArray src1, int normType=NORM_L2)
				V1 = V1/(norm(V1) + eps);
				V2 = V2/(norm(V2) + eps);

            else% In this case we will expect to suffer from the aperture problem

				V1 = [1; 0];
				V2 = [0; 1];
            end
        
%             LL = diag(1./([Lamda1 Lamda2]+ eps)) ;
%             VV = [V1 V2];
%             VUtag = VV*LL*VV'*b;
%             eigV = [V1 V2];
%             pinvH = eigV*(diag(1./([L1 L2]+eps)))*eigV';
            pinvH = pinv(H);
            VU = pinvH*b;
            Vx(ii, jj) = VU(1);
            Vy(ii, jj) = VU(2);
            Lamda1(ii, jj) = Lamda(1, 1);
            Lamda2(ii, jj) = Lamda(2, 2);
        end
    end
    
    %Smooth with weights
    weights = min(Lamda1, Lamda2);
    wSmooth = imfilter(weights, fspecial('gaussian', [5 5], 1));
    Vx = imfilter(weights.*Vx, fspecial('gaussian', [5 5], 1))./wSmooth;
    Vy = imfilter(weights.*Vy, fspecial('gaussian', [5 5], 1))./wSmooth;
    
    %Warp image - for image registration
    U = U + Vx;
    V = V + Vy;

    warpedIm = imWarp_( Vx, Vy, warpedIm );

end
