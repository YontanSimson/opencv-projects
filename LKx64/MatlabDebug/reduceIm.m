function next_level = reduceIm(current_level)

% THIS FUNCTION CREATES THE NEXT LEVEL IN A GAUSSIAN PYRAMID
% input = current_level (current level in the gaussian pyramid)
% output = next_level (next level in the gaussian pyramid)
a = 0.375;
c = 1/4 - a/2;
b = 1/4;



weights = [c b a b c];

current_level_pad = linear_ext_matrix2(current_level);

im_filt_hv = conv2(weights', weights, current_level_pad, 'valid');

[rows, cols] = size(im_filt_hv);

 
if (mod((cols-1)/2, 2) == 1)%if odd
    x_start = 2;
else
    x_start = 1;
end

if (mod((rows-1)/2, 2) == 1)%if odd
    y_start = 2;
else
    y_start = 1;
end
next_level = im_filt_hv(y_start:2:end, x_start:2:end);


%linear extrapolate im on each side 
    function A_ext = linear_ext_matrix2(A)
        A_ext = [A 2*A(:,end) - A(:, end-1) 2*A(:,end) - A(:, end-2)];
        A_ext = [2*A_ext(:,1) - A_ext(:, 3) 2*A_ext(:,1) - A_ext(:, 2) A_ext];
        
        A_ext = [A_ext; 2*A_ext(end, :) - A_ext(end-1, :); 2*A_ext(end, :) - A_ext(end-2, :)];
        A_ext = [2*A_ext(1, :) - A_ext(3, :); 2*A_ext(1, :) - A_ext(2, :); A_ext];
    end
end