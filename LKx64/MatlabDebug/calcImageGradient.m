%Calculate Ix, Iy
function [Ix, Iy] = calcImageGradient(curIm)


%Sobel

HSobel = [-1 0 1;
          -2 0 2;
          -1 0 1];

HSmooth = fspecial('gaussian', [3 3], 0.5);      
ImSmoothed = imfilter(curIm, HSmooth, 'symmetric');
Ix = imfilter(ImSmoothed, HSobel, 'symmetric');
Iy = imfilter(ImSmoothed, HSobel', 'symmetric');

      

