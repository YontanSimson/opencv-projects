function showVectorField(Im, Vx, Vy, stepSize)

assert(size(Im, 1) == size(Vx, 1));
assert(size(Im, 2) == size(Vx, 2));

assert(size(Im, 1) == size(Vy, 1));
assert(size(Im, 2) == size(Vy, 2));

[rows, cols, ~] = size(Im);
[XX, YY] = meshgrid(1:stepSize:cols, 1:stepSize:rows);

figure;
imagesc(Im);
axis image;
colormap gray;
hold on;
quiver(XX, YY, Vx(XX), Vy(YY));


