#include "LK.h"
#include <cstdint> // for uintxx types
#include <numeric> // std::accumulate
#include <iostream> // std::endl

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/video/tracking.hpp"
#include "DisplayFunctions.h"

using namespace cv;
using namespace std;

#define SQR(x) ((x)*(x))

template<class T>
inline T clip(T input, T minVal, T MaxVal)
{
	if (input < minVal)
		return minVal;
	else if (input > MaxVal)
		return MaxVal;
	else
		return input;
}

CLK::CLK() 
{
}

CLK::CLK(const SParams& LKParams) :
	m_LKParams(LKParams),
	m_FilterKernel(LKParams.winSize, LKParams.filterType)
{
}

CLK::~CLK()
{
}

void CLK::createGaussianPyramid(const cv::Mat inputIm, const int& pyramidLevels, std::vector<cv::Mat>& pyramid)
{
	pyramid.resize(pyramidLevels, Mat());
	pyramid[0] = inputIm;//use copyTo?

	for (size_t i = 1; i < pyramidLevels; i++)
	{
		pyrDown(pyramid[i - 1], pyramid[i], Size(pyramid[i - 1].cols / 2, pyramid[i - 1].rows / 2), BORDER_REPLICATE);
	}
}

void CLK::doLKTransform(const cv::Mat& refIm, const cv::Mat& curIm)
{
	CV_Assert(refIm.channels() == 1);
	CV_Assert(refIm.channels() == curIm.channels());//must by gray image
	CV_Assert(refIm.rows == curIm.rows);
	CV_Assert(refIm.cols == curIm.cols);

	//Mat refIm32F;
	//refIm.convertTo(refIm32F, CV_32F, 1.f / 255.f, .0f);

	//Make pyramids
	vector<Mat> curPyramid, refPyramid;
	createGaussianPyramid(curIm, m_LKParams.pyramidLevels, curPyramid);
	createGaussianPyramid(refIm, m_LKParams.pyramidLevels, refPyramid);

	//Iterate over pyramid levels starting from smallest
	int rows_l = curPyramid[m_LKParams.pyramidLevels - 1].rows;
	int cols_l = curPyramid[m_LKParams.pyramidLevels - 1].cols;


	Mat U(Size(cols_l, rows_l), CV_32FC1, Scalar(0.0));
	Mat V(Size(cols_l, rows_l), CV_32FC1, Scalar(0.0));

	Mat imWarped, Us, Vs;
	refPyramid[m_LKParams.pyramidLevels - 1].copyTo(imWarped);

	for (int idx_level = m_LKParams.pyramidLevels - 1; idx_level >= 0; --idx_level)
	{
		rows_l = curPyramid[idx_level].rows;
		cols_l = curPyramid[idx_level].cols;

		if (idx_level < m_LKParams.pyramidLevels - 1)
		{
			imWarped.create(Size(cols_l, rows_l), refPyramid[idx_level].type());
			imWarp<uint8_t>(refPyramid[idx_level], U, V, imWarped);
		}

		singleLKLevel(imWarped,
			curPyramid[idx_level],
			m_FilterKernel,
			Us, Vs);

		U = Us + U;
		V = Vs + V;

		if ( idx_level != 0 ) // expand for next level
		{
			//nputArray src, OutputArray dst, Size dsize, double fx=0, double fy=0, int interpolation=INTER_LINEAR
			resize(U * 2, U, U.size() * 2, 0.0, 0.0, INTER_LINEAR);
			resize(V * 2, V, V.size() * 2, 0.0, 0.0, INTER_LINEAR);
		}
	}

}

//Warp image according to displacment vectors
//This is faster than doing:
//Mat_<float> XX, YY;
//makeXYGrid(XX, YY, rows_l, cols_l);
//remap(refPyramid[idx_level], imWarped, U + XX, V + YY, CV_INTER_LINEAR, BORDER_REPLICATE);
template<class T>
void CLK::imWarp(const cv::Mat& InputIm, 
	const cv::Mat& Vx,
	const cv::Mat& Vy, 
	cv::Mat& warpedIm)
{
	CV_Assert(InputIm.channels() == 1);
	CV_Assert(Vx.channels() == 1);
	CV_Assert(Vy.channels() == 1);
	CV_Assert(InputIm.rows == Vx.rows);
	CV_Assert(InputIm.cols == Vx.cols);
	CV_Assert(InputIm.rows == Vy.rows);
	CV_Assert(InputIm.cols == Vy.cols);

	//create temporary image in case InputIm and warpedIm are the same
	Mat TempIm;
	if (InputIm.data == warpedIm.data)
		warpedIm.copyTo(TempIm);//full depth copy
	else
		TempIm = warpedIm;//copies reference to warpedIm

	TempIm.create(InputIm.size(), InputIm.type());

	int rows = InputIm.rows;
	int cols = InputIm.cols;

	for (int i = 0; i < rows; ++i)
	{
		
		T* pWarpedIm = TempIm.ptr<T>(i);
		const float* pVx = Vx.ptr<float>(i); 
		const float* pVy = Vy.ptr<float>(i);

		for (int j = 0; j < cols; ++j, ++pWarpedIm, ++pVx, ++pVy)
		{
			const float PosX = float(j) + *pVx;
			const float PosY = float(i) + *pVy;
			T warpedValue = linearIterp<T>(InputIm, PosX, PosY);
			*pWarpedIm = warpedValue;
		}
	}

	if (InputIm.data == warpedIm.data)
		TempIm.copyTo(warpedIm);
}

//Creates c++ equivalent of matlabs 1:n. This will generate a vector of 0:n-1.
//isCol = false if you wish to create row vector. The default is a column vetor
template<class T>
void CLK::create1toNVec(cv::Mat_<T>& xx, const int& n, const bool isCol)
{
	float* p_x = NULL;

	if ( isCol )
		xx.create(Size(1, n));
	else
		xx.create(Size(n, 1));
	xx(0, 0) = 0.0;

	p_x = xx.ptr<float>(0) + 1;
	for (int i = 1; i < n; ++i, ++p_x)
		*p_x = *(p_x - 1) + 1;
}

//Creates a standard grid equivalent to matlabs [XX, YY] = meshgrid(1:cols, 1:rows);
template<class T>
void CLK::makeXYGrid(cv::Mat_<T>& XX, cv::Mat_<T>& YY, const int& rows, const int& cols)
{
	Mat_<T> x;
	create1toNVec(x, cols, false);
	XX = repeat(x, rows, 1);

	Mat_<T> y;
	create1toNVec(y, rows, true);
	YY = repeat(y, 1, cols);
}

template<class T>
T CLK::linearIterp(const cv::Mat& srcIm, const float& PosX, const float& PosY)
{
	int rows = srcIm.rows;
	int cols = srcIm.cols;

	int x = (int)floorf(PosX);
	int y = (int)floorf(PosY);

	//x = clip(x, 0, cols - 2);
	//y = clip(y, 0, rows - 2);
	//Check if on border - border replicate
	if (y < 0)//Top
	{
		if (x < 0)//TL
			return srcIm.at<T>(0, 0);
		else if ( x >= cols - 1 )//TR
			return srcIm.at<T>(0, cols - 1);
		else
		{
			const T& TL = srcIm.at<T>(0, x);
			const T& TR = srcIm.at<T>(0, x + 1);

			float alpha = PosX - static_cast<float>(x);
			return static_cast<T>((1 - alpha) * float(TL) + alpha * float(TR));
		}
	}

	if (y >= rows - 1)//Bottom
	{
		if (x < 0)//BL
			return srcIm.at<T>(rows - 1, 0);
		else if (x >= cols - 1)//BR
			return srcIm.at<T>(rows - 1, cols - 1);
		else
		{
			const T& BL = srcIm.at<T>(rows - 1, x);
			const T& BR = srcIm.at<T>(rows - 1, x + 1);

			float alpha = PosX - static_cast<float>(x);
			return static_cast<T>((1 - alpha) * float(BL) + alpha * float(BR));
		}
	}

	if (x < 0)//Left
	{
		const T& Top = srcIm.at<T>(y, 0);
		const T& Bot = srcIm.at<T>(y + 1, 0);
		float beta = PosY - static_cast<float>(y);
		return static_cast<T>((1 - beta) * float(Top) + beta * float(Bot));
	}

	if (x >= cols - 1)//Right
	{
		const T& Top = srcIm.at<T>(y, cols - 1);
		const T& Bot = srcIm.at<T>(y + 1, cols - 1);
		float beta = PosY - static_cast<float>(y);
		return static_cast<T>((1 - beta) * float(Top) + beta * float(Bot));
	}

	//Calc reference points TL, TR, BL, BR
	const T& TL = srcIm.at<T>(y, x);
	const T& TR = srcIm.at<T>(y, x + 1);
	const T& BL = srcIm.at<T>(y + 1, x);
	const T& BR = srcIm.at<T>(y + 1, x + 1);

	float alpha = PosX - static_cast<float>(x);
	float beta  = PosY - static_cast<float>(y);

	T Top = static_cast<T>((1 - alpha) * float(TL) + alpha * float(TR));
	T Bot = static_cast<T>((1 - alpha) * float(BL) + alpha * float(BR));

	return static_cast<T>((1 - beta) * float(Top) + beta * float(Bot));
}

//Lucas Kanade for a single level of the pyramid
void CLK::singleLKLevel(const cv::Mat& refIm, 
	const cv::Mat& curIm, 
	const SFilterKernel& smoothingFilter,
	cv::Mat& vx, 
	cv::Mat& vy)
{
	CV_Assert(refIm.channels() == 1);
	CV_Assert(refIm.channels() == curIm.channels());//must by gray image
	CV_Assert(refIm.rows == curIm.rows);
	CV_Assert(refIm.cols == curIm.cols);

	int rows = curIm.rows;
	int cols = curIm.cols;

	//convert uint8 inputs refIm, curIm to floating point
	Mat imWarped32F, curIm32F, refIm32F;
	refIm.convertTo(refIm32F, CV_32F, 1.f / 255.f, .0f);
	curIm.convertTo(curIm32F, CV_32F, 1.f / 255.f, .0f);
	refIm32F.copyTo(imWarped32F);

	//Calculate Ix, Iy
	calcImageGradient(curIm32F);

	//Hessian
	//Calculate Ix^2, Ix*Iy and Iy^2
	Mat Ix2, Iy2, Ixy;
	multiply(m_Ix, m_Ix, Ix2, 1.0, CV_32FC1);
	multiply(m_Iy, m_Iy, Iy2, 1.0, CV_32FC1);
	multiply(m_Ix, m_Iy, Ixy, 1.0, CV_32FC1);

	//Smooth all elements of Hessian with Gaussian window of 5x5
	Mat Ix2Smooth, Iy2Smooth, IxySmooth;

	smoothingFilter.applyfilter2D(Ix2, Ix2Smooth);
	smoothingFilter.applyfilter2D(Iy2, Iy2Smooth);
	smoothingFilter.applyfilter2D(Ixy, IxySmooth);

	const float *pIx2, *pIy2, *pIxy, *pIxt, *pIyt;//input
	float *pLamda1, *pLamda2;
	float *pVx, *pVy;//output

	vx.create(rows, cols, CV_32FC1);//for sub pixel accuracy
	vy.create(rows, cols, CV_32FC1);

	vx = Scalar(0);
	vy = Scalar(0);

	m_Lamda1.create(rows, cols, CV_32FC1);
	m_Lamda2.create(rows, cols, CV_32FC1);

	vector<float> mean_vx(m_LKParams.nIterations);
	vector<float> mean_vy(m_LKParams.nIterations);

	Mat Us(rows, cols, CV_32FC1), Vs(rows, cols, CV_32FC1), LWeights(rows, cols, CV_32FC1);
	for (size_t iter = 0; iter < m_LKParams.nIterations; iter++)
	{
		//RHS of the LK equation
		Mat It;
		subtract(curIm32F, imWarped32F, It, noArray(), CV_32FC1);

		Mat Ixt, Iyt;
		multiply(m_Ix, It, Ixt, 1.0, CV_32FC1);
		multiply(m_Iy, It, Iyt, 1.0, CV_32FC1);

		//Smooth elements of the time difference x the gradient
		Mat IxtSmooth, IytSmooth;
		smoothingFilter.applyfilter2D(Ixt, IxtSmooth);
		smoothingFilter.applyfilter2D(Iyt, IytSmooth);

		for (int i = 0; i < rows; i++)
		{

			pIx2 = Ix2Smooth.ptr<float>(i);
			pIy2 = Iy2Smooth.ptr<float>(i);
			pIxy = IxySmooth.ptr<float>(i);
			pIxt = IxtSmooth.ptr<float>(i);
			pIyt = IytSmooth.ptr<float>(i);

			pVx = Us.ptr<float>(i);
			pVy = Vs.ptr<float>(i);

			pLamda1 = m_Lamda1.ptr<float>(i);
			pLamda2 = m_Lamda2.ptr<float>(i);

			for (int j = 0; j < cols; j++, ++pIx2, ++pIy2, ++pIxy, ++pIxt, ++pIyt,
				++pVx, ++pVy, ++pLamda1, ++pLamda2)
			{
				//Hessian
				Mat_<float> H(2, 2);
				H(0, 0) = *pIx2; H(0, 1) = *pIxy;
				H(1, 0) = *pIxy; H(1, 1) = *pIy2;

				//H = [Ix^2 Ix*Iy; Ix*Iy Iy^2]
				Mat_<float> eigenvalues, eigenvectors;
				eigen(H, eigenvalues, eigenvectors);
				Mat_<float> b(2, 1);
				b(0) = static_cast<float>(*pIxt);
				b(1) = static_cast<float>(*pIyt);

				*pLamda1 = eigenvalues(0, 0);
				*pLamda2 = eigenvalues(1, 0);

				//[Vx, Vy]^T = H^-1*[-Ixt, -Iyt]^T
				Mat Temp = Mat::diag(1. / (eigenvalues + FLT_EPSILON));
				Mat_<float> InvH = eigenvectors*Mat::diag(1. / (eigenvalues + FLT_EPSILON))*eigenvectors.t();//H = V^T*Lambda**V
				Mat_<float> X = InvH*b;

				*pVx = X(0);
				*pVy = X(1);
			}
		}

		LWeights = min(m_Lamda1, m_Lamda2);

		//smooth Us, Vs with Lambda weights
		Mat_<float> wSmooth, UsSmooth, VsSmooth, UTemp, VTemp;
		GaussianBlur(LWeights, wSmooth, Size(5, 5), 0, 0, BORDER_REPLICATE);
		UTemp = wSmooth.mul(Us);
		VTemp = wSmooth.mul(Vs);

		GaussianBlur(UTemp, UsSmooth, Size(5, 5), 0, 0, BORDER_REPLICATE);
		GaussianBlur(VTemp, VsSmooth, Size(5, 5), 0, 0, BORDER_REPLICATE);

		UsSmooth /= wSmooth;
		VsSmooth /= wSmooth;

		imWarp<float>(imWarped32F, UsSmooth, VsSmooth, imWarped32F);

		vx += UsSmooth;
		vy += VsSmooth;

		mean_vx[iter] = (float)Scalar(mean(vx))[0];
		mean_vy[iter] = (float)Scalar(mean(vy))[0];
	}

	/*
	Mat flow;
	double pyr_scale = 0.5;
	int levels = 8;
	int winsize = 5;
	int iterations = 3;
	int poly_n = 5;
	double poly_sigma = 1.2;
	int flags = 0;

	calcOpticalFlowFarneback(refIm, curIm, flow, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags);
	//	cvtColor(prevgray, cflow, COLOR_GRAY2BGR);
	//	drawOptFlowMap(flow, cflow, 16, 1.5, Scalar(0, 255, 0));
	Mat Vx(rows, cols, CV_32FC1, Scalar(0.0));
	Mat Vy(rows, cols, CV_32FC1, Scalar(0.0));

	Mat mvbegin[2] = { Vx, Vy };
	split(flow, mvbegin);
	*/

	Mat cflow, flow;
	cvtColor(refIm, cflow, COLOR_GRAY2BGR);
	Mat mvbegin[2] = { vx, vy };
	merge(mvbegin, 2, flow);

	drawOptFlowMap(flow, cflow, 16, 16.0, Scalar(0, 255, 0));
	namedWindow("Dense Vector Optical Flow", CV_WINDOW_AUTOSIZE);
	imshow("Dense Vector Optical Flow", cflow);
	waitKey(10);


	Mat rgbFlow;
	FlowToRGB(vx, vy, curIm, rgbFlow);


	namedWindow("Dense Optical Flow", CV_WINDOW_AUTOSIZE);
	imshow("Dense Optical Flow", rgbFlow);
	waitKey(10);


}
//Calculate the Ix, Iy images (m_Ix, m_Iy)
void CLK::calcImageGradient(const cv::Mat& grayIm)
{

	cv::Mat grayImSmoothed;

	static const double scale = 1;
	static const int delta = 0;
	static const int ksize = 3;
	static const int ddepth = CV_32FC1;

	GaussianBlur(grayIm, grayImSmoothed, Size(3, 3), 0, 0, BORDER_REFLECT);

	/// Gradient X
	Sobel(grayImSmoothed, m_Ix, ddepth, 1, 0, ksize, scale, delta, BORDER_REFLECT);

	/// Gradient Y
	Sobel(grayImSmoothed, m_Iy, ddepth, 0, 1, ksize, scale, delta, BORDER_REFLECT);
}

//Display all type of images. Display is scaled to [0, 1]
template<class T>
void CLK::showImNormalized(const Mat& Im, const string winName)
{
	//Extract a single channel from matrix and display
	Mat_<float> Temp(Im.rows, Im.cols);

	//copy data
	for (int i = 0; i < Im.rows; i++)
	{
		float* p_temp = Temp.ptr<float>(i);
		for (int j = 0; j < Im.cols; j++, p_temp++)
		{
			*p_temp = (float)Im.at<T>(i, j);
		}
	}

	//Get Max, Min values and normalize all values from 0.0->1.0 for display
	double minVal, maxVal;
	minMaxIdx(Temp, &minVal, &maxVal);

	Temp -= minVal;
	Temp /= (maxVal - minVal);

	namedWindow(winName, CV_WINDOW_AUTOSIZE);
	imshow(winName, Temp);
	waitKey(10);
}

//Convert matrix from integer type to floating point
template<class SrcType>
void CLK::ConvertMatrixTypes(const cv::Mat& Src, cv::Mat& Trg)
{
	size_t rows = Src.rows;
	size_t cols = Src.cols;

	//Initialize Matrix
	Trg.create(rows, cols, CV_32FC1);

	//It will be scaled to 0->1
	double minVal;
	double maxVal;
	minMaxIdx(Src, &minVal, &maxVal);


	//copy data
	for (size_t i = 0; i < rows; i++)
	{
		float* p_trg = Trg.ptr<float>(i);
		const SrcType* p_src = Src.ptr<SrcType>(i);
		for (size_t j = 0; j < cols; j++, p_src++, p_trg)
		{
			*p_trg = (float(*p_src) - (float)minVal) / float(maxVal - minVal);
		}
	}
	
}

// (Vx, Vy) + Gray Image -> Color coded optical flow in RGB format
//convert (Vx,Vy) to polar coordinates -> Hue and Saturation
//The Hue is the angle in degrees and the Saturation is Magnitude scaled to [0,1]
//The Value sould come from the original image which too will be scaled to [0,1]
void CLK::FlowToRGB(const cv::Mat & Vx, const cv::Mat & Vy,
	const cv::Mat & Value,
	cv::Mat & rgbFlow)
{
	CV_ARE_SIZES_EQ(&Vx, &Vy);
	if ( Vx.empty() || Vy.empty() ) return;
	if ( Vx.depth() != CV_32F )
		throw(std::exception("FlowToRGB: error Vx wrong data type ( has be CV_32FC2"));
	if ( Vy.depth() != CV_32F )
		throw(std::exception("FlowToRGB: error Vy wrong data type ( has be CV_32FC2"));

	int rows = Vx.rows;
	int cols = Vx.cols;

	cv::Mat Hue(Size(cols, rows), CV_32FC1);
	cv::Mat Saturation(Size(cols, rows), CV_32FC1);

	cartToPolar(Vx, Vy,
		Saturation, Hue,
		true);

	//cut off all values over 95% - to remove outliers
	float quantile95 = 1.0;//get_quantile(Saturation, 0.95, 64);
	threshold(Saturation, Saturation, quantile95, quantile95, THRESH_TRUNC);

	//Scale Saturation to [0,1]
	double satMaxVal, satMinVal;
	cv::minMaxLoc(Saturation, &satMinVal, &satMaxVal);

	Saturation -= satMinVal;
	Saturation *= 1.f / (satMaxVal - satMinVal);

	//Saturation = Scalar(1.f);
	showImNormalized<float>(Saturation, "Saturation");

	//Convert to floating point and scale Value to [0,1]
	Mat Value32F;
	Value.convertTo(Value32F, CV_32F, 1.f / 255.f, .0f);

	showImNormalized<float>(Value32F, "Value32F");

	//merge 3 channels
	Mat planes[3] = { Hue, Saturation, Value32F};
	Mat HSV;
	merge(planes, 3, HSV);

	cv::Mat rgbFlow32F(rows, cols, CV_32FC3);
	cv::cvtColor(HSV, rgbFlow32F, CV_HSV2BGR);

	rgbFlow32F.convertTo(rgbFlow, CV_8UC3, 255.f);

}

//find the value matching the desired percentile of the accumalated histogram
//Percentile must be between values [0, 1]
float CLK::get_quantile(const cv::Mat &Im, const double Percentile, const int numOfBins)
{
	CV_Assert(Percentile >= 0.0 && Percentile <= 1.0);

	//Calc histogram
	double MaxVal, MinVal;
	cv::minMaxLoc(Im, &MinVal, &MaxVal);

	cv::Mat hist;
	float ImRanges[] = { (float)MinVal, (float)MaxVal };
	const float* ranges[] = { ImRanges };
	cv::calcHist(&Im, 1, 0, cv::Mat(), hist, 1, &numOfBins, ranges);

	//Calc Accumalated histogram
	auto *hist_ptr = hist.ptr<float>(0);
	for (size_t i = 1; i != (size_t)numOfBins; ++i){
		hist_ptr[i] += hist_ptr[i - 1];
	}

	//Calculate mid point values of each bin
	float binWidth = (float)(MaxVal - MinVal) / (float)numOfBins;
	vector <float> bin_mid_point(numOfBins, binWidth);
	bin_mid_point[0] = binWidth / 2;
	for (auto it = bin_mid_point.begin() + 1; it != bin_mid_point.end(); ++it)
	{
		*it += *(it - 1);
	}

	//get matching bin for the percentile
	float histSum = (float)Scalar(hist_ptr[numOfBins - 1])[0];
	int bin_idx = 0;
	while (bin_idx < (size_t)numOfBins && histSum * Percentile >  hist.at<float>(bin_idx))
	{
		++bin_idx;
	}

	return static_cast<float>(bin_mid_point[bin_idx]);

}