#pragma once
#include "opencv2/core/core.hpp"

#include "FilterKernel.h"

struct SParams{
	//LK Parameters
	int pyramidLevels;
	int nIterations;
	std::string filterType;
	int winSize;

	SParams(int pyramidLevels_,
		int nIterations_,
		std::string filterType_,
		int winsize_) :
		pyramidLevels(pyramidLevels_),
		nIterations(nIterations_),
		filterType(filterType_),
		winSize(winsize_)
	{

	}

	//Default Ctor
	SParams() :
		pyramidLevels(3),
		nIterations(8),
		filterType("disk"),
		winSize(7)
	{

	}
};


class CLK
{
public:
	//Default Ctor
	CLK();
	//Ctor
	CLK(const SParams& LKParams);
	//Dctor
	~CLK();

	void doLKTransform(const cv::Mat& refIm, const cv::Mat& curIm);
	void getLKShift(cv::Mat& vx, cv::Mat& vy) const { vx = m_vx; vy = m_vy; }

private:
	//Calculate the Ix, Iy images (m_Ix, m_Iy)
	void calcImageGradient(const cv::Mat& grayIm);

	//Display all type of images. Display is scaled to [0, 1]
	template<class T>
	void showImNormalized(const cv::Mat& Im, const std::string winName);
	
	//Convert matrix from integer type to floating point
	template<class SrcType>
	void ConvertMatrixTypes(const cv::Mat& Src, cv::Mat& Trg);

	// (Vx, Vy) + Gray Image -> Color coded optical flow in RGB format
	void FlowToRGB(const cv::Mat & Vx, const cv::Mat & Vy,
		const cv::Mat & Value,
		cv::Mat & rgbFlow);

	//find the value matching the desired percentile of the accumalated histogram
	//Percentile must be between values [0, 1]
	float get_quantile(const cv::Mat &Im, const double Percentile, const int numOfBins = 64);

	//Lucas Kanade for a single level of the pyramid
	void singleLKLevel(const cv::Mat& refIm,
		const cv::Mat& curIm,
		const SFilterKernel& smoothingFilter,
		cv::Mat& vx,
		cv::Mat& vy);

	//Warp image according to displacment vectors - allows in place operations
	template<class T>
	void imWarp(const cv::Mat& InputIm,
		const cv::Mat& Vx,
		const cv::Mat& Vy,
		cv::Mat& warpedIm);

	//Creates c++ equivalent of matlabs 1:n. This will generate a vector of 0:n-1.
	//isCol = false if you wish to create row vector. The default is a column vetor
	template<class T>
	void create1toNVec(cv::Mat_<T>& xx, const int& n, const bool isCol = true);

	//Creates a standard grid equivalent to matlabs [XX, YY] = meshgrid(1:cols, 1:rows);
	template<class T>
	void makeXYGrid(cv::Mat_<T>& XX, cv::Mat_<T>& YY, const int& rows, const int& cols);

	template<class T>
	T linearIterp(const cv::Mat& srcIm, const float& PosX, const float& PosY);

	//Creates Gaussian pyramid
	void createGaussianPyramid(const cv::Mat inputIm, const int& pyramidLevels, std::vector<cv::Mat>& pyramid);
private:
	cv::Mat m_Ix;
	cv::Mat m_Iy;

	cv::Mat m_Lamda1;
	cv::Mat m_Lamda2;

	cv::Mat m_vx;
	cv::Mat m_vy;

	SParams m_LKParams;
	SFilterKernel m_FilterKernel;
	
};


