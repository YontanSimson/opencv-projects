// main.cpp : Defines the entry point for the console application.
//

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <algorithm>
#include <iostream>
#include <stdio.h>

#include "LK.h"

using namespace std;
using namespace cv;

/** Function Headers */
void concatenate2Images(const Mat& im1, const Mat& im2, Mat& stackIm);

/** @function main */
int main(int argc, const char** argv)
{

	string inputImFile1 = "church01.JPG";
	string inputImFile2 = "church03.JPG";

	Mat img_1 = imread(inputImFile1, CV_LOAD_IMAGE_GRAYSCALE);
	Mat img_2 = imread(inputImFile2, CV_LOAD_IMAGE_GRAYSCALE);

	SFilterKernel myKernal(9, "disk");

	if (!img_1.data || !img_2.data)
	{
		cerr << "Input Images not found" << std::endl;
		return -1;
	}

//	int rows[2] = { img_1.rows, img_2.rows };
//	int cols[2] = { img_1.cols, img_2.cols };
	//int maxRows = std::max(rows[0], rows[1]);
	//int minRows = std::min(rows[0], rows[1]);

	//int maxCols = std::max(cols[0], cols[1]);
	//int minCols = std::min(cols[0], cols[1]);


	//crop larger image
	//Rect ROI(0, 0, minCols, minRows);
	//Mat imgCropped[2];
	//imgCropped[0] = img_1(ROI);
	//imgCropped[1] = img_2(ROI);

	//Sanity chack for LK
	Mat imgCropped[2];
	Rect ROI1(0, 0, img_1.cols - 1, img_1.rows);
	Rect ROI2(1, 0, img_1.cols - 1, img_1.rows);
	imgCropped[0] = img_1(ROI1);
	imgCropped[1] = img_1(ROI2);

	//show both images concatenated
	Mat stackedView;
	concatenate2Images(imgCropped[0], imgCropped[1], stackedView);

	namedWindow("stackedView", CV_WINDOW_AUTOSIZE);
	imshow("stackedView", stackedView);
	waitKey(10);

	int pyramidLevels = 3;
	int nIterations = 4; 
	std::string filterType("disk");
	int winsize = 7;
	
	SParams LKParams(pyramidLevels, nIterations, filterType, winsize);
	CLK lk(LKParams);

	lk.doLKTransform(imgCropped[0], imgCropped[1]);


	return 0;
}

void concatenate2Images(const Mat& im1, const Mat& im2, Mat& stackIm)
{
	CV_ARE_SIZES_EQ(&im1, &im2);
	CV_Assert(im1.rows == im2.rows);
	CV_Assert(im1.cols == im2.cols);
	CV_Assert(im1.channels() == im2.channels());

	int rows = im1.rows;
	int cols = im1.cols;

	//show both images contcatenated
	stackIm.create(rows, cols * 2, im1.type());

	Rect Roi[2];
	int offsetX = 0;
	for (size_t i = 0; i < 2; i++)
	{
		Roi[i] = Rect(offsetX, 0, cols, rows);
		offsetX += cols;
	}
	im1.copyTo(stackIm(Roi[0]));
	im2.copyTo(stackIm(Roi[1]));
}