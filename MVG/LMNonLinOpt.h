// This file is part of MVG, a small library of functions for Mutiple Veiw Geometry
// Most of the code is based on R. Hartley and A. Zisserman, "Multiple Veiw Gemeotry in Computer vision", 2nd Ed.
// and Matlab code from the companion wesite: http://www.robots.ox.ac.uk/~vgg/hzbook/
//
// Copyright (c) 2014. Yonatan Simson <simsonyee[at]gmail[dot]com>.
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#pragma once

#include "Eigen/Core"
#include "Eigen/SVD"// Provides Jacobi SVD

#include <unsupported/Eigen/KroneckerProduct>
#include <unsupported/Eigen/NonLinearOptimization>
#include <unsupported/Eigen/NumericalDiff>

#include <iostream>//for std::cout

using Eigen::Dynamic;

// Generic functor
template<typename _Scalar, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
struct Functor
{
	typedef _Scalar Scalar;
	enum {
		InputsAtCompileTime = NX,
		ValuesAtCompileTime = NY
	};
	typedef Eigen::Matrix<Scalar, InputsAtCompileTime, 1> InputType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
	typedef Eigen::Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

	int m_inputs;//dimension of input variable (parameters to be optimized)
	int m_values;//dimension of output function

	Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
	Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

	int inputs() const { return m_inputs; }
	int values() const { return m_values; }

};

///////////////////////////////////////////////////
/// Simple Example of non-linear optimization /////
///////////////////////////////////////////////////
struct my_functor : Functor<double>
{
	my_functor(void) : 
		Functor<double>(2, 2),
		a(sqrt(2))
	{}
	double a; //a parameter of the function
	void set_a(double a_){ a = a_; }
	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec) const
	{
		//The Rosenbrock banana function
		// Implement y = 100*(x1-x0^2)^2 + (1-x0)^2
		// Split to f0 = 10*(x1-x0^2), 
		//          f1 = a-x0
		fvec(0) = 10.0*(x(1) - x(0)*x(0));
		fvec(1) = a - x(0);

		return 0;
	}
};

struct my_functor_w_df : Eigen::NumericalDiff<my_functor> {};

//////////////////////////////////////////////////////////////////////////////
/// Minimizer of Algebraic error for calculation of the Fundamental Matrix ///
/// Uses LM solver of Eigen library //////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////


//Functor for minimizing Algebraic Error
//f_i, e_i, eps_i = func(A, e_(i-1), f_(i-1))
struct F_functor : Functor<double>
{
	//Ctor
	F_functor(int A_rows) :
		Functor<double>(3, A_rows),
		m_epsilon_cost_i(DBL_MAX),
		m_L_COST(1.0)
	{}

	//Functor operator
	int operator()(const Eigen::VectorXd &x, Eigen::VectorXd &fvec)
	{
		Eigen::VectorXd err_i_new;//err(i)
		Eigen::VectorXd f_i_new;//f(i)

		// err_i = func(A,f, x = e)
		// eps_cost = ||err_i||^2
		double epsilon_cost_i = costAlgebraic(x, err_i_new, f_i_new);
		fvec = err_i_new;//err_i = A*fi

		if (epsilon_cost_i < m_epsilon_cost_i)
		{
			m_epsilon_cost_i = epsilon_cost_i;
			m_f_i = f_i_new;//keep since this is the end result we aim to compute
			m_e_i = x;//keep for next iteration for sign check
		}
		return 0;
	}

	////Calculate Jacobian - using forward method
	int df(const Eigen::VectorXd &_x, Eigen::MatrixXd &fjac) const
	{
		eigen_assert(_x.size() == inputs());
		int nfev = 0;
		fjac.resize(values(), inputs());

		Eigen::VectorXd f_i_new;//dummy 
		const double eps = sqrt(((std::max)(0., Eigen::NumTraits<double>::epsilon())));

		Eigen::VectorXd x = _x;
		Eigen::VectorXd val1, val2;
		val1.resize(Functor::values());
		val2.resize(Functor::values());

		costAlgebraic(x, val1, f_i_new); nfev++;

		for (int j = 0; j < inputs(); j++)
		{
			double h = eps * abs(x[j]);
			if (h == 0.) {
				h = eps;
			}
			x[j] += h;
			costAlgebraic(x, val2, f_i_new); nfev++;

			x[j] = _x[j];
			fjac.col(j) = (val2 - val1) / h;
		}
		eigen_assert(fjac.isZero() != 1);

		return nfev;
	}

	//Set get functions
	void set_A(const Eigen::MatrixXd& A){ m_A = A; }
	void set_e(const Eigen::VectorXd& e_i){ m_e_i = e_i; }
	void set_f(const Eigen::VectorXd& f){ m_f_i = f; };
	void set_eps_cost(const double& epsilon_cost){ m_epsilon_cost_i = epsilon_cost; }
	void set_l_cost(const double& l_cost){ m_L_COST = l_cost; }

	Eigen::VectorXd get_f()const { return m_f_i; }
	double get_eps_cost()const { return m_epsilon_cost_i; }

private:
	//Cost function
	//err_i = A*f_i
	double costAlgebraic(const Eigen::VectorXd &e_i,
		Eigen::VectorXd& err_i_new,
		Eigen::VectorXd& f_i_new) const
	{
		eigen_assert(m_A.rows() >= 8);//else 8-point alg is not possible

		//(ii) compute Ei = [ei]_x,
		// then find f_i = Ei*mi that minimizes || A f_i || subject to || f_i || = 1
		Eigen::MatrixXd e_cross(3, 3);
		e_cross << 0, -e_i(2), e_i(1),
			e_i(2), 0, -e_i(0),
			-e_i(1), e_i(0), 0;

		Eigen::MatrixXd E(9, 9);
		E = kroneckerProduct(Eigen::MatrixXd::Identity(3, 3), e_cross).eval();

		const_min_subject_span_space(m_A, E, 6, f_i_new);

		//(iii)compute epsilon_i = A f_i and correct its sign
		double sign_eval = e_i.transpose()*m_e_i;
		double sign = sign_eval >= 0 ? 1 : -1;
		//sign correction is required since f_i is correct up to *-1
		f_i_new = sign*f_i_new;
		err_i_new = m_A*f_i_new;

		double epsilon_cost_i_new = pow(err_i_new.norm(), m_L_COST);
		return epsilon_cost_i_new;
	}



//members
private:
	Eigen::MatrixXd m_A;
	Eigen::VectorXd m_e_i; //e(i-1) - the epipole of Fi
	Eigen::VectorXd m_err_i;//err(i)
	Eigen::VectorXd m_f_i;//f(i) - vectorized version of Fi
	double m_epsilon_cost_i;
	double m_L_COST;
};


struct my_functor_w_jac : F_functor 
{
	my_functor_w_jac(int A_rows):
		F_functor(A_rows)
	{}
};
	
