// This file is part of MVG, a small library of functions for Mutiple Veiw Geometry
// Most of the code is based on R. Hartley and A. Zisserman, "Multiple Veiw Gemeotry in Computer vision", 2nd Ed.
// and Matlab code from the companion wesite: http://www.robots.ox.ac.uk/~vgg/hzbook/
//
// Copyright (c) 2014. Yonatan Simson <simsonyee[at]gmail[dot]com>.
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.


#pragma once
#include "Eigen/Core"// Provides basic Matrix LA
#include "Eigen/SVD"// Provides Jacobi SVD
#include "Eigen/LU"// Provides inverse()

#include <unsupported/Eigen/NonLinearOptimization>
#include <unsupported/Eigen/NumericalDiff>

#include "MatrixOperations.h"
#include "LMNonLinOpt.h"

#include <iostream>//for std::cout

using Eigen::Matrix;
using Eigen::Dynamic;
using Eigen::RowMajor;
using Eigen::ColMajor;

/////////////////////////////////////
// Homographic transform class //////
/////////////////////////////////////
template<class T>
class CHomographicTransform{
	Matrix<T, 3, 3, RowMajor> m_H;//Homographic transform

	//Construct A Matrix - Ah=0 - using one point
	void constructAFrom1point(const Matrix<T, 3, 1>& XSrc, const Matrix<T, 3, 1>& XTrg,
		Matrix<T, 2, 9>& Asub)
	{
		assert(XSrc.cols() == 1 && XTrg.cols() == 1);//must be a single point in column vector
		//XSrc = (x_src, y_src, w_src) source point
		//XTrg = (x_trg, y_trg, w_trg) target point

		//000^T
		Asub.block(0, 0, 1, 3) = Matrix<T, 1, 3>::Zero();

		//-w_trg*XSrc^T
		Asub.block(0, 3, 1, 3) = -XTrg(2)*XSrc.transpose();

		//y_trg*XSrc^T
		Asub.block(0, 6, 1, 3) = XTrg(1)*XSrc.transpose();

		//second row
		//w_trg*XSrc^T
		Asub.block(1, 0, 1, 3) = XTrg(2)*XSrc.transpose();

		//000^T
		Asub.block(1, 3, 1, 3) = Matrix<T, 1, 3>::Zero(1, 3);

		//x_trg*XSrc^T
		Asub.block(1, 6, 1, 3) = -XTrg(0)*XSrc.transpose();
	}

public:

	//DLT - over at least 4 points to extract H
	void calcHFromPoints(const Matrix<T, 3, Dynamic>& XSrc, const Matrix<T, 3, Dynamic>& XTrg)
	{
		assert(XSrc.rows() == 3 && XTrg.rows() == 3);//Must be in homograpic form
		assert(XSrc.cols() == XTrg.cols());//Must be same number of points

		size_t numOfPoints = XSrc.cols();
		assert(numOfPoints >= 4);//must have at least 4 points

		//Point normalization - move to center of mass and and scale average distance to Sqrt(2)
		Matrix<T, 3, 3> TSrc = calcNormalizationMatrix(XSrc);
		Matrix<T, 3, 3> TTrg = calcNormalizationMatrix(XTrg);
		
		Matrix<T, 3, Dynamic> XSrcNorm = TSrc*XSrc;
		Matrix<T, 3, Dynamic> XTrgNorm = TTrg*XTrg;

		//Construct A Matrix - Ah=0 - using one point pair at a time
		Matrix<T, Dynamic, 9> A(2 * numOfPoints, 9);//two equations are produced per matching point
		for (size_t i = 0; i < numOfPoints; i++)//concatenate Asub matrices for [numOfPoints] points
		{
			Matrix<T, 2, 9> Asub;
			constructAFrom1point(XSrcNorm.col(i), XTrgNorm.col(i), Asub);
			A.row(i * 2) = Asub.row(0);
			A.row((i + 1) * 2 - 1) = Asub.row(1);//concatenate row wise
		}

		//find h that minimizes min||Ah|| subject to ||h||=1
		//The solution is last Column vector of V.  A = UDV^T
		Matrix<T, Dynamic, 1> h = DLT<T>(A);

		//h->H raster scan, H is row-Major or col-Major
		vec2Matrix(h, m_H);

		//decondition
		m_H = TTrg.inverse()*m_H*TSrc;
	}

	//Get H
	Matrix<T, 3, 3> getH() const { return m_H; }

	//Calc Transform
	Matrix<T, 3, Dynamic> doH( const Matrix<T, 3, Dynamic>& X ) const { return m_H*X; }

	//Calc Transform and normalize by w (last value)
	Matrix<T, 3, Dynamic> doHNorm(const Matrix<T, Dynamic, Dynamic>& X) const { 
		Matrix<T, Dynamic, Dynamic> Out = m_H*X;
		for (size_t j = 0; j < (size_t)Out.cols(); j++)
		{
			for (size_t i = 0; i < (size_t)Out.rows(); i++)
			{
				Out(i, j) /= Out(Out.rows()-1, j);//normalize by last value
			}
		}
		return Out;
	}

	//Ctor
	CHomographicTransform(){}
	//Dctor
	~CHomographicTransform(){}

};

/////////////////////////////////////////
// Fundamental Matrix class /////////////
/////////////////////////////////////////

struct FParams
{
	bool normalize;
	double l_cost;

	FParams(bool normalize_, double l_cost_):
		normalize(normalize_),
		l_cost(l_cost_)
	{}
};


//template<class T>
//struct m_functor : Eigen::NumericalDiff<F_functor<T>> {};

template<class T>
class CFundamental{
	Matrix<T, 3, 3, RowMajor> m_F;//Fundamental Matrix
	Matrix<T, Dynamic, 9> m_A;// "A" matrix for computing F. from Eq 11.3 ZH, MVG
	FParams m_FParams;

	//struct m_functor : Eigen::NumericalDiff<F_functor> {};

	//Set last eigen value to Zero - Enforces epipolar lines to go through a single points
	Matrix<T, 3, 3, RowMajor> enforceConstraint(Matrix<T, 3, 3, RowMajor> F)
	{
		JacobiSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>> svd(F, ComputeFullV | ComputeFullU);
		Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> U = svd.matrixU();
		Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> S = svd.singularValues();
		Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> V = svd.matrixV();

		S(S.size() - 1) = 0;

		Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> Fconstrained = U*S.asDiagonal()*V.transpose();
		return Fconstrained;
	}


public:

	//XLeft'*F*XRight = 0 for XLeft & XRight matching points
	//Linear calculation using DLT
	//F normalized 8point algorithm
	void calcF_FromPoints(const Matrix<T, 3, Dynamic>& XLeft, const Matrix<T, 3, Dynamic>& XRight)
	{
		eigen_assert(XLeft.rows() == 3 && XRight.rows() == 3);//Must be in homograpic form
		eigen_assert(XRight.cols() == XRight.cols());//Must be same number of matching points

		size_t numOfPoints = XLeft.cols();
		eigen_assert(numOfPoints >= 8);//must have at least 8 points

		//Calc nomalization matrixes
		Matrix<T, 3, 3> Tleft = calcNormalizationMatrix(XLeft);
		Matrix<T, 3, 3> Tright = calcNormalizationMatrix(XRight);

		//Calc Normalized points
		Matrix<T, 3, Dynamic> XLeftNorm = Tleft*XLeft;
		Matrix<T, 3, Dynamic> XRightNorm = Tright*XRight;

		//Obtain linear set of equations for solving F
		makeA4FundementalMat(XLeftNorm, XRightNorm, m_A);

		Matrix<T, Dynamic, 1> f = DLT<T>(m_A);

		Matrix<T, 3, 3> F;
		vec2Matrix(f, m_F);

		//replacing "F" by "F constrained", the closest singular matrix to F under Frobenius Norm
		m_F = enforceConstraint(m_F);

		//denormalization
		m_F = Tleft.transpose()*m_F*Tright;

	}

	//Iterative calculation of Fundemental Matrix using Levenburg-Marquadt (Alg 11.2 pg 284 ZH)
	void calcF_LM(const Matrix<T, 3, Dynamic>& XLeft, const Matrix<T, 3, Dynamic>& XRight)
	{
		
		//Normalize inputs
		Matrix<T, 3, Dynamic> XLeftNorm, XRightNorm;
		Matrix<T, 3, 3> Tleft, Tright;
		if (m_FParams.normalize)
		{
			Tleft  = calcNormalizationMatrix(XLeft);
			Tright = calcNormalizationMatrix(XRight);
		}
		else
		{
			Tleft  = Eigen::Matrix<T, Dynamic, Dynamic>::Identity(3, 3);
			Tright = Eigen::Matrix<T, Dynamic, Dynamic>::Identity(3, 3);
		}

		//Calc Normalized points
		XLeftNorm = Tleft*XLeft;
		XRightNorm = Tright*XRight;

		//Calc starting point F0 using 8 point normalized Alg
		calcF_FromPoints(XLeftNorm, XRightNorm);
		Matrix<T, 3, 3> F0 = m_F;

		//F0_T = F0, vectorized row-wise
		Matrix<T, 9, 1> F0_T = vectorizeMatrix(F0);
		F0_T /= F0_T.norm();

		////Obtain linear set of equations for solving F
		//Matrix<T, Dynamic, 9> A;
		//makeA4FundementalMat(XLeftNorm, XRightNorm, A);

		//std::cout << "A = " << std::endl << A << std::endl;
		//std::cout << "m_A = " << std::endl << m_A << std::endl;
	
		T eps_cost_i = pow(Matrix<T, Dynamic, 1>(m_A*F0_T).norm(), m_FParams.l_cost); //initial cost
		
		// (i)finding an initial estimate of the epipole from an initial estimate of F
		Matrix<double, 3, 3> F0_d;
		for (int i = 0; i < F0_d.rows(); i++)
		{
			for (int j = 0; j < F0_d.cols(); j++)
			{
				F0_d(i, j) = (double)F0(i, j);
			}
		}
		
		//Initial point for Functor
		Eigen::VectorXd e = getEpipoleOfF<double>(F0_d);//setting e0

		//Setting up functor with it's internal paramaters
		my_functor_w_jac functor(m_A.rows());//functor with inbuilt numeric differentiator for Jacobian Matrix
		functor.set_A(m_A);//set A of Functor
		functor.set_l_cost(m_FParams.l_cost);//power of cost function
		functor.set_eps_cost(eps_cost_i);//setting eps_cost0
		functor.set_e(e);//Set x0 for LM 

		//Setting up LM minimizer and setting it's parameters
		Eigen::LevenbergMarquardt<my_functor_w_jac> lm(functor);
		lm.parameters.maxfev = 2000;//Max evaluations
		lm.parameters.xtol = 1.0e-10;
		int ret = lm.minimize(e);
		
		//Turn vector f->F back into a matrix
		Matrix<T, 9, 1> f = functor.get_f();
		Matrix<T, 3, 3> F;
		vec2Matrix(f, F);

		//denormalization F
		m_F = Tleft.transpose()*F*Tright;
	}


	//get F Transform
	Matrix<T, 3, 3> getF() const { return m_F; }

	void set_params(bool normalize_, double l_cost_) { m_FParams.normalize = normalize_;  m_FParams.l_cost = l_cost_; };
	
	//Ctor
	CFundamental():
		m_FParams(true, 1.0)
	{
	}
};
