// main.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MVG.h"
#include "LMNonLinOpt.h"
#include "MatrixOperations.h"

#include <Eigen/Eigenvalues>//EigenSolver
#include <unsupported/Eigen/NonLinearOptimization>

using namespace std;
using namespace Eigen;


int main(int argc, char** argv[])
{
	////////////////////////////////
	//Non Linear Optimization///////
	////////////////////////////////
	Eigen::VectorXd x(2);
	x(0) = -1.2;
	x(1) = 1.0;
	std::cout << "x: " << x << std::endl;

	my_functor_w_df functor;
	functor.set_a(sqrt(3));//set parameter of Functor
	Eigen::LevenbergMarquardt<my_functor_w_df> lm(functor);

	lm.parameters.maxfev = 2000;
	lm.parameters.xtol = 1.0e-10;
	std::cout << lm.parameters.maxfev << std::endl;

	int ret = lm.minimize(x);
	std::cout << "Converged in "<< lm.iter <<" iterations"<<std::endl;
	std::cout << ret << std::endl;

	std::cout << "x that minimizes the function: " << std::endl << x << std::endl;


	////////////////////////////////////////
	//Homographic transform calcualation
	////////////////////////////////////////

	//Src 
	MatrixXd VecSrc(3, 4);
	VecSrc << 10, 100, 120,  20,
		10,  30,  90, 100,
		1,   1,   1,   1;

	//Trg 
	MatrixXd VecTrg(3, 4);
	VecTrg << 50, 100, 130, 40,
		      30, 10, 110, 120, 
			  1,   1,   1,   1;

	CHomographicTransform<double> HomographicT;
	HomographicT.calcHFromPoints(VecSrc, VecTrg);
	Matrix<double, Dynamic, Dynamic> VecTrgEst = HomographicT.doHNorm(VecSrc);

	cout << "Trg Points" << endl << VecTrg << endl;
	cout << "Estimated Trg Points" << endl << VecTrgEst << endl;//Check to see if H transform is correct

	////////////////////////////////////////
	//Fundamental Matrix calculation
	Matrix<double, 3, Dynamic> XRight(3, 8);
	XRight << 1157, 1373, 2622, 3167,  2768, 1592, 3860, 3548,
			  2645,  694,  823, 2765,  1576, 1408,  845, 1332,
			     1,    1,    1,    1,     1,    1,    1,    1;
	Matrix<double, 3, Dynamic> XLeft(3, 8);
	XLeft << 396, 486, 1842, 2472, 2032, 804, 2977, 2736,
		2699, 575, 686, 2516, 1424, 1337, 684, 1147,
		1, 1, 1, 1, 1, 1, 1, 1;

	CFundamental<double> FMat;
	FMat.calcF_FromPoints(XLeft, XRight);

	cout << endl << "F=" << endl << FMat.getF() << endl;

	/////////////////////////////////////////////////////////////
	/// Eigen Value Vector Demo /////////////////////////////////
	/////////////////////////////////////////////////////////////

	MatrixXd MM(3, 3);
	MM << -149, -50, -154,
		537, 180, 546,
		-27, -9, -25;

	cout << "A regular Eigen vector example" << endl;
	cout << "Here is a random 3x3 matrix, MM:" << endl << MM << endl << endl;

	EigenSolver<MatrixXd> es(MM);
	cout << "The eigenvalues of MM are:" << endl << es.eigenvalues().real() << endl;
	cout << "The matrix of eigenvectors, V, is:" << endl << es.eigenvectors().real() << endl << endl;

	//////////////////////////////////////////////////////
	//Check Levenburg Marquadt version of Eigen library //
	//For iterative calculation of F                   ///
	//////////////////////////////////////////////////////

	Matrix<double, 3, Eigen::Dynamic> XRight1(3, 8);
	XRight1 << 507, 640, 240, 356, 406, 313, 319, 184,
		281, 144, 245, 172, 211, 251, 306, 479,
		1, 1, 1, 1, 1, 1, 1, 1;
		
	Matrix<double, 3, Eigen::Dynamic> XLeft1(3, 8);
	XLeft1 << 444, 570, 132, 148, 247, 200, 401, 197,
		265, 153, 368, 393, 364, 365, 199, 418,
		1, 1, 1, 1, 1, 1, 1, 1;

	//Now first check 8 point Alg
	FMat.calcF_FromPoints(XLeft1, XRight1);
	cout << endl << "8 point Alg, F=" << endl << FMat.getF() << endl;

	//Now LM Iterative verison - minimizes algebraic error
	FMat.calcF_LM(XLeft1, XRight1);
	cout << endl << "F fromLM, F=" << endl << FMat.getF() << endl;


	return 0;
}

