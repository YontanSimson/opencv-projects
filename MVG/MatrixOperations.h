// This file is part of MVG, a small library of functions for Mutiple View Geometry
// Most of the code is based on R. Hartley and A. Zisserman, "Multiple View Gemeotry in Computer vision", 2nd Ed.
// and Matlab code from the companion wesite: http://www.robots.ox.ac.uk/~vgg/hzbook/
//
// Copyright (c) 2014. Yonatan Simson <simsonyee[at]gmail[dot]com>.
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.

#pragma once
#include "Eigen/Core"


//copies a column vector to matrix row-wise
//This is neccesary because it is not possible to know whether the Matrix is row Major or column major before hand
template<class MType, class VType>
void vec2Matrix(const VType& Vec, MType& Mat)
{
	assert(Mat.size() == Vec.size());//number of elements must be equal
	assert(Vec.cols() == 1);//input vector must be a single column

	size_t nRows = Mat.rows();
	size_t nCols = Mat.cols();

	for (size_t i = 0; i < nRows; i++)
	{
		Mat.row(i) << Vec.block(i*nRows, 0, nRows, 1).transpose();
	}
}

//X - 2d homographic points (x, y, w)^T stacked in columns
template<class T>
Eigen::Matrix<T, 3, 3, Eigen::RowMajor> calcNormalizationMatrix(const Eigen::Matrix<T, 3, Eigen::Dynamic>& X)
{
	//centor of mass
	double x_avg = X.row(0).mean();
	double y_avg = X.row(1).mean();

	size_t numOfPoints = X.cols();

	double dist_avg = 0;
	for (size_t j = 0; j < numOfPoints; j++)
	{
		dist_avg += sqrt((X(0, j) - x_avg)*(X(0, j) - x_avg) + (X(1, j) - y_avg)*(X(1, j) - y_avg));
	}
	dist_avg /= numOfPoints;
	double scaling = sqrt(2) / dist_avg;

	//Normalization matrix 
	Eigen::Matrix<T, 3, 3, Eigen::RowMajor> TNorm;
	TNorm << scaling, 0, -scaling*x_avg,
		0, scaling, -scaling*y_avg,
		0, 0, 1;
	return TNorm;
}

//Direct Linear Transform
template<class T>
Eigen::Matrix<T, Eigen::Dynamic, 1> DLT(Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> A)
{
	//find h that minimizes min||Ax|| subject to ||x||=1
	//The solution is last Column vector of V.  A = UDV^T
	JacobiSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>> svd(A, ComputeFullV);
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> V = svd.matrixV();

	//get last column vector of V
	return V.col(V.cols() - 1);
}


//Inverse using SVD. If the input A is not square this is the Moore�Penrose pseudoinverse
template<class T>
Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> inverseSVD(Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> A)
{
	JacobiSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> > svd(A, ComputeFullV | ComputeFullU);
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> U = svd.matrixU();
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> S = svd.singularValues();
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> V = svd.matrixV();
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> invS = 1 / svd.singularValues().array();//invert every eigen value
	return V*invS.asDiagonal()*U.transpose();
}

/*
template<class T>
Matrix<T, Dynamic, 1, Eigen::ColMajor> vectorizeMatrix(const Matrix<T, Dynamic, Dynamic>& InputMat, bool isRowWise = true)
{
Matrix<T, Dynamic, Dynamic> Vectorized;

if (isRowWise)
Vectorized = InputMat.transpose();
else
Vectorized = InputMat;

return Vectorized.resize(Vectorized.size(), 1);
}
*/

//converts Matrix to a column vector
template<class T, int NX = Eigen::Dynamic, int NY = Eigen::Dynamic>
Eigen::Matrix<T, NX*NY, 1, Eigen::ColMajor> vectorizeMatrix(const Eigen::Matrix<T, NX, NY>& InputMat, bool isRowWise = true)
{
	Eigen::Matrix<T, NX*NY, 1> Vectorized;

	if (isRowWise)
		Vectorized = Eigen::Map<Eigen::Matrix<T, NX*NY, 1>, Eigen::Unaligned>(Eigen::Matrix<T, NX, NY, Eigen::ColMajor>(InputMat.transpose()).data(), NX*NY, 1);
	else
		Vectorized = Eigen::Map<Eigen::Matrix<T, NX*NY, 1>, Eigen::Unaligned>(Eigen::Matrix<T, NX, NY, Eigen::ColMajor>(InputMat).data(), NX*NY, 1);

	return Vectorized;
}


//Calculates the right hand Epipole of Fundemental Matrix (Right hand Null Space) F*e=0
template<class T>
Eigen::Matrix<T, 3, 1, Eigen::ColMajor> getEpipoleOfF(const Eigen::Matrix<T, 3, 3>& F)
{
	JacobiSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>> svd(F, ComputeFullV);
	Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> V = svd.matrixV();
	eigen_assert(V.cols() == 3);//must have 3 columns

	return V.rightCols(1);//V(:, end)
}

template<class T>
void makeA4FundementalMat(const Eigen::Matrix<T, 3, Eigen::Dynamic>& XLeft, const Eigen::Matrix<T, 3, Eigen::Dynamic>& XRight,
	Eigen::Matrix<T, Eigen::Dynamic, 9>& A)
{
	assert(XLeft.cols() == XRight.cols());

	size_t numOfPoints = XLeft.cols();
	assert(numOfPoints >= 8);//requires at least 8 matching points

	A.resize(numOfPoints, 9);

	for (size_t j = 0; j < numOfPoints; j++)
	{
		//Stacking horizontally: x_left*XRight' y_left*XRight' w_left*XRight'
		A.row(j) << XLeft(0, j)*XRight.col(j).transpose(),
			XLeft(1, j)*XRight.col(j).transpose(),
			XLeft(2, j)*XRight.col(j).transpose();
	}
}

//finds the vector x that maximizes || A x || subject to the constraint || x || = 1 and x = G x_hat
//where G has rank r
//Alg A5.6, pg 595, ZH, MVG
void const_min_subject_span_space(const Eigen::MatrixXd &A, const Eigen::MatrixXd &G, const int &r, Eigen::VectorXd &x) 
{
	//A = U*S*V'
	Eigen::JacobiSVD<Eigen::MatrixXd> svd(G, Eigen::ComputeFullU | Eigen::ComputeFullV);
	Eigen::MatrixXd Ug = svd.matrixU();

	Eigen::MatrixXd Uprime = Ug.leftCols(r);//Ug[:, 1:r]

	svd.compute(A*Uprime, Eigen::ComputeFullV);
	Eigen::MatrixXd Va = svd.matrixV();

	Eigen::VectorXd x_p = Va.rightCols(1);//V[:, end]

	x = Uprime * x_p;

	return;
}






