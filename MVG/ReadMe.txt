========================================================================
    CONSOLE APPLICATION : main Project Overview
========================================================================

MVG.h
	This is Template library of basic functions for Multiple View Geometry.
	Most of the code is based on R. Hartley and A. Zisserman, "Multiple View Geometry in Computer vision", 2nd Ed.
	and Matlab code from the companion website: http://www.robots.ox.ac.uk/~vgg/hzbook/
	
	* Normalized 4-point Alg for calculation Homography H
	* Normalized 8-point Alg for calculation of Fundamental Matrix F
	* Computation of Fundamental Matrix F by iteratively minimizing algebraic error

LMNonLinOpt.h
	Contains demo for using LM optimization
	Contains Alg 11.2 from ZH, pg 284. Algebraic method for calculating Fundamental Matrix

MatrixOperations.h
	Useful Helper functions
	Such as conversion between Vectors and and Matrices
	Calculations of Null Spaces
	DLT
	Alg A5.6 from ZH pg. 595
	
main.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named main.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
TODO:
	
	Add non-linear calculation of Homography H with LM
	Extraction of P, P' from F
	Triangulation X = f(P, P', x, x')
	The Golden Standard algorithm for estimating F (Alg 11.3)

/////////////////////////////////////////////////////////////////////////////
Acknowledgements:
	Code from Matlab exchange "Fundamental Matrix Computation" by  Omid Aghazadeh, inspired the Algebraic calculation of the Fundamental matrix
	http://www.mathworks.com/matlabcentral/fileexchange/27541-fundamental-matrix-computation