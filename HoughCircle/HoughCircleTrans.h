#pragma once
#include "opencv2/core/core.hpp"
#include <vector>
#include <list>
#include <string>

using std::vector;
using cv::Mat;
using std::string;
using std::list;

//Macros
#define SQR(X) (X)*(X)

//Class 
class CHoughCircleTrans{

protected:
	Mat m_HoughSpace;
	int* m_Radii;
	int m_numOfRadii;
	//typedef vector<int>::iterator vec_it;

	//Hough Paramaters
	static const int rho_delta = 1;
	int m_NHoodXY;
	int m_NHoodRho;
	
	//Image dimensions
	int m_Rows;
	int m_Cols;
	
	//Circle drawing 
	//CDrawCircle m_drawCircle;

protected:
	//Transfer a point in image domain to circle in hough domain
	void point2Circle(int xCenter, int yCenter, int radius, int radius_idx);

	//Transfer a point in image domain to circle in hough domain
	//Fast version that uses pre-allocated circles for each radius
	void point2CircleFast(int xCenter, int yCenter, const int* x_idx, const int* y_idx, const size_t& circ_size, 
		int radius_idx);

	//optimzed version that saves time on unneeded multiplications
	void point2CircleOptimized(int xCenter, int yCenter, const int* x_idx, const int* y,
		const int* y_idxTimesCols, const size_t& circ_size, int radius_idx);

	//Show single layer of Hough - for debug
	template<class T>
	void showSingleLayerOfMat(const Mat& Mat3d, int layerIdx, const string LayerName);

	//Find max point in hough Space
	void getMaxPeak(int maxIdx[3], double& maxVal);

	//normalize bins to give small radius planes same weight as large radius planes
	void normalizeBinsByRadii();

public:
	//Hough Circle Init
	//nhood_xy - x0, y0 spatial nhood for non-maximal supression - must be odd
	//nhood_xy - radius nhood for non-maximal supression - must be odd
	CHoughCircleTrans(int nhood_xy, int	nhood_rho);

	//Dctor
	~CHoughCircleTrans();

	//Transform edge image to Hough
	void edgeIm2Hough(const Mat& m_Edge, unsigned minRadius, unsigned maxRadius);

	//Get Circles detected in Hough Space
	void getHoughPeaks(vector<cv::Vec3i>& circles, double scoreRatio, unsigned maxNumCircs);
};