#include "stdafx.h"
#include "HoughCircleWithGrad.h"

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"//for debug

#include <algorithm>
#include <cstdint>

using namespace cv;
using namespace std;

#include "opencv2/imgproc/imgproc.hpp"

//Hough Circle Init
//nhood_xy - x0, y0  nhood for non-maximal supression - must be odd
//nhood_xy - radius nhood for non-maximal supression - must be odd

CHoughCircleWithGrad::CHoughCircleWithGrad(int nhood_xy, int	nhood_rho) :
	CHoughCircleTrans(nhood_xy, nhood_rho)
{

}

//Dctor
CHoughCircleWithGrad::~CHoughCircleWithGrad()
{

}

//Transfer a point in image domain to 2 points in Hough Space.
//The point is defined by (x0,y0). 
//The two points are calculated by (xR, yR) = (x0,y0) +/- R*(nx, ny)
//where R, is the radius and (nx, ny) the normalized gradient information.
//The normalized gradient information is (nx,ny) = (gx, gy)/sqrt(gx^2+gy^2) 
//Try prove this equation as an exercise - see below
void CHoughCircleWithGrad::pointTo2Points(const cv::Mat& Gx, const cv::Mat& Gy, int x0, int y0, int radius, int radius_idx)
{
	//get ptr to dimension appropiate to radius
	Mat SingleLayer(m_Rows, m_Cols, m_HoughSpace.type(), (void *)m_HoughSpace.ptr<uint16_t>(radius_idx));

	//Calculate points using gradient information
	int xR[2], yR[2];

	calc2PointsFromGradient(x0, y0, radius, Gx, Gy, xR, yR);

	//Vote in hough space
	for (size_t i = 0; i < 2; ++i)
	{
		if (0 <= xR[i] && xR[i] < m_Cols && 0 <= yR[i] && yR[i] < m_Rows)
		{
			SingleLayer.at<uint16_t>(yR[i], xR[i])++;
		}
	}
	
}

//Transfer a point in image domain to 2 points in Hough Space.
//The point is defined by (x0,y0). 
//The two points are calculated by (xR, yR) = (x0,y0) +/- R*(nx, ny)
//where R, is the radius and (nx, ny) the normalized gradient information.
void CHoughCircleWithGrad::pointTo2Points(int x0, int y0, int radius, uint16_t* radius_ptr)
{

	//Calculate points using gradient information
	int xR[2], yR[2];

	calc2PointsFromNormalizedGrad(x0, y0, radius, m_nx.at<float>(y0, x0), m_ny.at<float>(y0, x0), xR, yR);

	//Vote in Hough space
	for (size_t i = 0; i < 2; ++i)
	{
		if (0 <= xR[i] && xR[i] < m_Cols && 0 <= yR[i] && yR[i] < m_Rows)
		{
			radius_ptr[xR[i] + m_Cols*yR[i]]++;
		}
	}

}

//(xR, yR) = (x0,y0) +/- R*(nx, ny)
//(nx,ny) = (gx, gy)/sqrt(gx^2+gy^2) 
void CHoughCircleWithGrad::calc2PointsFromGradient(const int& x0, const int& y0, const int& R, const cv::Mat& Gx, const cv::Mat& Gy, 
	int xR[2], int yR[2])
{
	float nx, ny, mag;

	int16_t gx = Gx.at<int16_t>(y0, x0);
	int16_t gy = Gy.at<int16_t>(y0, x0);

	mag = sqrtf(SQR(float(gx)) + SQR(float(gy)));
	nx = float(gx) / mag;
	ny = float(gy) / mag;

	calc2PointsFromNormalizedGrad(x0, y0, R, nx, ny, xR, yR);
}

//(xR, yR) = (x0,y0) +/- R*(nx, ny)
void CHoughCircleWithGrad::calc2PointsFromNormalizedGrad(const int& x0, const int& y0, const int& R, const float& nx, const float& ny,
	int xR[2], int yR[2])
{
	int sign[2] = { +1, -1 };
	for (size_t i = 0; i < 2; i++)
	{
		xR[i] = (int)roundf(x0 + float(sign[i]) * float(R) * nx);
		yR[i] = (int)roundf(y0 + float(sign[i]) * float(R) * ny);
	}
}

//calc normalized gradient
void CHoughCircleWithGrad::calcNormalizedGrad(const cv::Mat& blurred_gray, const cv::Mat& Edges,
	cv::Mat& nx, cv::Mat& ny)
{
	Mat Gx;
	Mat Gy;

	static const double scale = 1;
	static const int delta = 0;
	static const int ksize = 3;
	static const int ddepth = CV_16SC1;

	// Gradient X
	Sobel(blurred_gray, Gx, ddepth, 1, 0, ksize, scale, delta, BORDER_REFLECT);

	// Gradient Y
	Sobel(blurred_gray, Gy, ddepth, 0, 1, ksize, scale, delta, BORDER_REFLECT);

	nx.create(blurred_gray.rows, blurred_gray.cols, CV_32FC1);
	ny.create(blurred_gray.rows, blurred_gray.cols, CV_32FC1);

	const uchar* pEdges;
	for (size_t i = 0; i < (size_t)blurred_gray.rows; i++)
	{
		pEdges = Edges.ptr<uchar>(i);
		for (size_t j = 0; j < (size_t)blurred_gray.cols; j++, pEdges++)
		{
			if (*pEdges != 0)//skip over pixels with no edges
			{
				int16_t gx = Gx.at<int16_t>(i, j);
				int16_t gy = Gy.at<int16_t>(i, j);

				float mag = sqrtf(SQR(float(gx)) + SQR(float(gy))) + FLT_EPSILON;
				m_nx.at<float>(i, j) = float(gx) / mag;
				m_ny.at<float>(i, j) = float(gy) / mag;
			}
		}
	}

}

//Transform gray image to Hough Space using gradient infomation
void CHoughCircleWithGrad::Im2Hough(const cv::Mat& blurred_gray, unsigned minRadius, unsigned maxRadius)
{
	CV_Assert(blurred_gray.channels() == 1);
	CV_Assert(blurred_gray.channels() == 1);
	CV_Assert(blurred_gray.type() == CV_8UC1);

	m_Rows = blurred_gray.rows;
	m_Cols = blurred_gray.cols;

	//calc edges
	Mat Edges;
	Canny(blurred_gray, Edges, 40, 90);

	//calc normalized gradient
	calcNormalizedGrad(blurred_gray, Edges, m_nx, m_ny);

	//Calc Radii 
	m_numOfRadii = (int)ceil((maxRadius - minRadius + 1) / float(rho_delta));
	//preallocate radii - TBD add suport for increments other than 1
	if (m_Radii != NULL)
		delete[] m_Radii;
	m_Radii = new int[m_numOfRadii];
	//fill radii with incrementing numbers of rho_delta
	m_Radii[0] = minRadius;
	for (int rad_idx = 1; rad_idx < m_numOfRadii; rad_idx++)
		m_Radii[rad_idx] = m_Radii[rad_idx - 1] + rho_delta;

	//Allocate cube for voting space
	int sz[] = { m_numOfRadii, m_Rows, m_Cols };//no support for circle center out of image
	m_HoughSpace.create(3, sz, CV_16UC1);

	//Check if allocation succeeded
	if (m_HoughSpace.empty())
	{
		CV_Assert("Mat m_HoughSpace Not Allocated");
	}

	//set cube to zeros
	m_HoughSpace = Scalar(0.0);

	//convert every edge point to a circle in hough space (for each radius)
	const uchar* pEdges;
	for (int rho_idx = 0; rho_idx < m_numOfRadii; ++rho_idx)
	{
		//get ptr to dimension appropiate to radius
		uint16_t* radius_ptr = m_HoughSpace.ptr<uint16_t>(rho_idx);

		for (int row_idx = 0; row_idx < m_Rows; row_idx++)
		{
			pEdges = Edges.ptr<uchar>(row_idx);
			for (int col_idx = 0; col_idx < m_Cols; col_idx++)
			{
				if (pEdges[col_idx] != 0)//skip over pixels with no edges
				{
					//vote for 2 points in Hough Space 
					//Utilizes gradient information
					pointTo2Points(col_idx, row_idx, m_Radii[rho_idx], radius_ptr);
				}
			}
		}

		////get ptr to dimension appropiate to radius
		//Mat SingleLayer(m_Rows, m_Cols, m_HoughSpace.type(), (void *)m_HoughSpace.ptr<uint16_t>(rho_idx));
		//double minVal, maxVal;
		//cv::minMaxIdx(SingleLayer, &minVal, &maxVal, NULL, NULL);
		//cv::namedWindow("SingleLayer", CV_WINDOW_NORMAL);
		//cv::imshow("SingleLayer", SingleLayer * (((1 << 16) - 1)) / maxVal);
		//cv::waitKey(10);
	}

	//normalize bins to give small radius planes same weight as large radius planes
	normalizeBinsByRadii();

}


//Utilizing gradient information
//Solve the two following equations to extract (xR, yR). 
//It has two possible solutions since we are searching for intersections between a line and a circle
//1) (xR-x0)^2 + (yR-y0)^2 = R^2
//2) gy*(xR-x0)-gx*(yR-y0) = 0

