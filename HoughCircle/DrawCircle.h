#pragma once

//This Class draws a circle using Mid-Point/Bresenhams Circle Algorithm

class CDrawCircle{

	//Circle parameter
	int m_Radius;

	//circumference points
	int m_x;
	int m_y;
	int m_RadiusError;

public:

	CDrawCircle(){}

	CDrawCircle( int Radius ) :
		m_Radius(Radius),
		m_x(Radius),
		m_y(0),
		m_RadiusError(1 - m_x)
	{}

	void init( int Radius )
	{
		m_Radius = Radius;

		m_x = Radius;
		m_y = 0;
		m_RadiusError = 1 - m_x;

	}

	//Go to next point on circle
	bool calcPoint()
	{
		bool isNotDone = (m_x >= m_y);
		if ( isNotDone )
		{
			m_y++;
			if (m_RadiusError<0)
			{
				m_RadiusError += 2 * m_y + 1;
			}
			else {
				m_x--;
				m_RadiusError += 2 * (m_y - m_x + 1);
			}
		}
		return isNotDone;
	}

	int getX() const { return m_x; }
	int getY() const { return m_y; }

	void getX(int &x) const { x=m_x; }
	void getY(int &y) const { y=m_y; }

	//Preallocate a circle using Bresenham's Circle Algorithm (Mid-Point alg)
	//Input:
	//	Radius of desired circle
	//Output:
	//  Rows - rows of target image
	//  Cols -columns of target image
	//  x_idx, y_idx - array of circle xy indexs - around a center of (0,0)
	//  circ_size - size of each arrays: x_idx, y_idx
	void preallocateBresenhamCircle(const int& Radius, const int& Rows, const int& Cols,
		int** x_idx, int** y_idx, size_t& circ_size);
};


