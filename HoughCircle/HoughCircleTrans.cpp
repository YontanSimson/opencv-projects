#include "stdafx.h"
#include "HoughCircleTrans.h"
#include "DrawCircle.h"

#include "opencv2/highgui/highgui.hpp"//for debug
#include <algorithm>
#include <cstdint>

using namespace cv;
using namespace std;

//Hough Circle Init
//nhood_xy - x0, y0  nhood for non-maximal supression - must be odd
//nhood_xy - radius nhood for non-maximal supression - must be odd
CHoughCircleTrans::CHoughCircleTrans(int nhood_xy, int nhood_rho):
	m_NHoodXY(nhood_xy),
	m_NHoodRho(nhood_rho),
	m_Radii(NULL)
{
	//if nhood_xy or nhood_rho are not add make them odd
	if (m_NHoodXY % 2 == 0)
		m_NHoodXY++;
	if (m_NHoodRho % 2 == 0)
		m_NHoodRho++;
}

CHoughCircleTrans::~CHoughCircleTrans()
{
	if (m_Radii != NULL)
		delete[] m_Radii;
}

//Transfer a point in image domain to circle in Hough Space
//center point is defined by (x,y). A circle of radius will be voted around the center
void CHoughCircleTrans::point2Circle(int xCenter, int yCenter, int radius, int radius_idx)
{
	//get ptr to dimension appropiate to radius
	uint16_t* rad_ptr = m_HoughSpace.ptr<uint16_t>(radius_idx);

	CDrawCircle drawCircle;

	drawCircle.init(radius);

	int x[8];
	int y[8];

	do
	{
		int xx = drawCircle.getX();
		int yy = drawCircle.getY();
		x[0] = xx + xCenter;
		y[0] = yy + yCenter;
		x[1] = yy + xCenter;
		y[1] = xx + yCenter;
		x[2] = -xx + xCenter;
		y[2] = yy + yCenter;
		x[3] = -yy + xCenter;
		y[3] = xx + yCenter;

		x[4] = -xx + xCenter;
		y[4] = -yy + yCenter;
		x[5] = -yy + xCenter;
		y[5] = -xx + yCenter;
		x[6] = xx + xCenter;
		y[6] = -yy + yCenter;
		x[7] = yy + xCenter;
		y[7] = -xx + yCenter;

		//place point in all 8 octaves
		for (int oct_idx = 0; oct_idx < 8; oct_idx++)
		{
			if (0 <= x[oct_idx] && x[oct_idx] < m_Cols &&
				0 <= y[oct_idx] && y[oct_idx] < m_Rows)
			{
				m_HoughSpace.at<uint16_t>(radius_idx, y[oct_idx], x[oct_idx])++;
			}
		}
	} while (drawCircle.calcPoint());

}

//simple version
void CHoughCircleTrans::point2CircleFast(int xCenter, int yCenter, const int* x_idx, const int* y_idx, 
	const size_t& circ_size, int radius_idx)
{
	//get ptr to dimension appropiate to radius
	uint16_t* rad_ptr = m_HoughSpace.ptr<uint16_t>(radius_idx);
	
	//sketch circle in Hough space from preallocated circle
	int x, y;
	for (size_t draw_idx = 0; draw_idx < circ_size; draw_idx++)
	{
		x = xCenter + x_idx[draw_idx];
		y = yCenter + y_idx[draw_idx];

		if (0 <= x && x < m_Cols && 0 <= y && y < m_Rows)
		{
			rad_ptr[y*m_Cols + x]++;
		}
	}

}

//optimized version
void CHoughCircleTrans::point2CircleOptimized(int xCenter, int yCenter, const int* x_idx, const int* y_idx, 
	const int* y_idxTimesCols, const size_t& circ_size, int radius_idx)
{
	//get ptr to dimension appropiate to radius
	uint16_t* rad_ptr = m_HoughSpace.ptr<uint16_t>(radius_idx);
	
	//sketch circle in Hough space from preallocated circle
	int x, y, yTimesCols;
	int yCenterxCols = yCenter*m_Cols;//calculate before hand to save time
	for (size_t draw_idx = 0; draw_idx < circ_size; draw_idx++)
	{
		x = xCenter + x_idx[draw_idx];
		y = yCenter + y_idx[draw_idx];//for checking limits
		yTimesCols = yCenterxCols + y_idxTimesCols[draw_idx];

		if (0 <= x && x < m_Cols && 0 <= y && y < m_Rows)
		{
			rad_ptr[yTimesCols + x]++;
		}
	}

}

//Transform edge image to Hough Space
void CHoughCircleTrans::edgeIm2Hough(const Mat& Edges, unsigned minRadius, unsigned maxRadius)
{
	CV_Assert(Edges.channels() == 1);
	CV_Assert(Edges.type() == CV_8UC1);

	m_Rows = Edges.rows;
	m_Cols = Edges.cols;

	m_numOfRadii = (int)ceil((maxRadius - minRadius + 1) / float(rho_delta));

	//preallocate radii - TBD add suport for increments other than 1
	if (m_Radii != NULL)
		delete[] m_Radii;
	m_Radii = new int[m_numOfRadii];

	//fill radii with incrementing numbers of rho_delta
	m_Radii[0] = minRadius;
	for (int rad_idx = 1; rad_idx < m_numOfRadii; rad_idx++)
		m_Radii[rad_idx] = m_Radii[rad_idx-1] + rho_delta;
	
	//Allocate cube for voting space
	int sz[] = { m_numOfRadii, m_Rows, m_Cols };//no support for circle center out of image
	m_HoughSpace.create(3, sz, CV_16UC1);

	//Check if allocation succeeded
	if (m_HoughSpace.empty())
	{
		CV_Assert("Mat m_HoughSpace Not Allocated");
	}


	//preallocate circles - for speed
	CDrawCircle	m_drawCircle;
	int**   ppCircleX   = new int*[m_numOfRadii];
	int**   ppCircleY   = new int*[m_numOfRadii];
	int**   ppCircleYTimescols = new int*[m_numOfRadii];

	size_t* pCircleSize = new size_t[m_numOfRadii];
	for (int rho_idx = 0; rho_idx < m_numOfRadii; ++rho_idx)
	{
		ppCircleX[rho_idx] = NULL;
		ppCircleY[rho_idx] = NULL;
		m_drawCircle.preallocateBresenhamCircle(m_Radii[rho_idx], m_Rows, m_Cols,
			&ppCircleX[rho_idx], &ppCircleY[rho_idx], pCircleSize[rho_idx]);

		//multiply y by m_Cols to speed up calculations
		ppCircleYTimescols[rho_idx] = new int[pCircleSize[rho_idx]];
		int* pCircleYTimescols = ppCircleYTimescols[rho_idx];
		int* pCircleY          = ppCircleY[rho_idx];
		size_t circLength = pCircleSize[rho_idx];
		for (size_t i = 0; i < circLength; i++)
		{
			pCircleYTimescols[i] = pCircleY[i]*m_Cols;
		}
	}

	//set cube to zeros
	m_HoughSpace = Scalar(0.0);

	//convert every edge point to a circle in hough space (for each radius)
	for (int rho_idx = 0; rho_idx < m_numOfRadii; ++rho_idx)
	{
		const uchar* ptr;
		for (int row_idx = 0; row_idx < m_Rows; row_idx++)
		{
			ptr = Edges.ptr<uchar>(row_idx);
			for (int col_idx = 0; col_idx < m_Cols; col_idx++)
			{
				if (ptr[col_idx] != 0)//skip over pixels with no edges
				{
					point2CircleOptimized(col_idx, row_idx, ppCircleX[rho_idx], ppCircleY[rho_idx], ppCircleYTimescols[rho_idx],
						pCircleSize[rho_idx], rho_idx);
					//					point2CircleFast(col_idx, row_idx, ppCircleX[rho_idx], ppCircleY[rho_idx],
					//						pCircleSize[rho_idx], rho_idx);
					//					point2Circle(col_idx, row_idx, m_Radii[rho_idx], rho_idx);
				}
			}
		}
	}


	//normalize bins to give small radii more votes
	normalizeBinsByRadii();

	//////////////////////////////////////
	//////Deallocate circles//////////////
	//////////////////////////////////////
	for (int rho_idx = 0; rho_idx < m_numOfRadii; rho_idx++)
	{
		delete[] ppCircleX[rho_idx];
		delete[] ppCircleY[rho_idx];
		delete[] ppCircleYTimescols[rho_idx];
	}
	delete[] ppCircleX;
	delete[] ppCircleY;
	delete[] ppCircleYTimescols;
	delete[] pCircleSize;

}

void CHoughCircleTrans::normalizeBinsByRadii()
{
	int maxIdx[3];
	double maxVal;
	minMaxIdx(m_HoughSpace, 0, &maxVal, 0, maxIdx);

	//normalize bins to give small radii more votes
	for (int rho_idx = 0; rho_idx < m_numOfRadii; ++rho_idx)
	{
		Mat SingleLayer(m_Rows, m_Cols, m_HoughSpace.type(), (void *)m_HoughSpace.ptr<uint16_t>(rho_idx));
		SingleLayer *= float(m_Radii[m_numOfRadii - 1]) / float(m_Radii[rho_idx]);
	}
}

void CHoughCircleTrans::getMaxPeak(int maxIdx[3], double& maxVal)
{
	minMaxIdx(m_HoughSpace, 0, &maxVal, 0, maxIdx);
}


//get peaks for detecting circles
//scoreRatio  - ratio for rejecting peaks smaller the the best peak
//maxNumCircs - maximum nuber of circles to reject 
//circles     - circle parameters arranged X0, Y0, R
void CHoughCircleTrans::getHoughPeaks(vector<cv::Vec3i>& circles, double scoreRatio, unsigned maxNumCircs)
{
	CV_Assert(m_Radii != NULL);
	CV_Assert(!m_HoughSpace.empty());

	circles.resize(0);
	double bestScore = DBL_MIN;
	double maxVal = DBL_MAX;

	while (maxVal >= scoreRatio*bestScore && circles.size() < maxNumCircs)
	{
		Vec3i circParams;
		getMaxPeak(circParams.val, maxVal);

		bestScore = max(bestScore, maxVal);
		Vec3i circOutParams;
		circOutParams.val[0] = circParams.val[2];//X0
		circOutParams.val[1] = circParams.val[1];//Y0
		circOutParams.val[2] = m_Radii[circParams.val[0]];//R

		circles.push_back(circOutParams);

		if (circles.size() >= maxNumCircs)
			break;

		//supress neighborhood [NHoodXY * NHoodXY * NHoodRho]
		int dxy = (m_NHoodXY - 1) / 2;
		int dr  = (m_NHoodRho - 1) / 2;

		for (int rho_idx = circParams.val[0] - dr; rho_idx < circParams.val[0] + dr; rho_idx++)
		{
			if (0 > rho_idx || rho_idx >= m_numOfRadii)
				continue;
			for (int row_idx = circParams.val[1] - dxy; row_idx < circParams.val[1] + dxy; row_idx++)
			{
				if (0 > row_idx || row_idx >= m_Rows)
					continue;
				for (int col_idx = circParams.val[2] - dxy; col_idx < circParams.val[2] + dxy; col_idx++)
				{
					if (0 > col_idx || col_idx >= m_Cols)
						continue;

					uint16_t* temp_ptr = (uint16_t *)(m_HoughSpace.data +
						  rho_idx*m_HoughSpace.step.p[0] + 
						  row_idx*m_HoughSpace.step.p[1] +
						  col_idx*m_HoughSpace.step.p[2]);
					*temp_ptr = 0;
				}
			}
		}
	}

}

//for debug
template<class T>
void CHoughCircleTrans::showSingleLayerOfMat(const Mat& Mat3d, int layerIdx, const string LayerName)
{
	//Extract a single channel from matrix and display
	Mat SingleLayer(m_Rows, m_Cols, CV_32FC1, (void *)Mat3d.ptr<T>(layerIdx)); 

	Mat Temp;
	SingleLayer.copyTo(Temp);

	//Get Max, Min values and normalize all values from 0.0->1.0 for display
	double minVal, maxVal;
	minMaxIdx(Temp, &minVal, &maxVal);

	Temp -= minVal;
	Temp /= (maxVal - minVal);

	namedWindow(LayerName, CV_WINDOW_AUTOSIZE);
	imshow(LayerName, Temp);
	waitKey(10);
}

