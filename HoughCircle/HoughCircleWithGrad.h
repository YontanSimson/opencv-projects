#pragma once
#include "opencv2/core/core.hpp"

#include "stdint.h"//not cross platfrom
#include "HoughCircleTrans.h"

#include <vector>
#include <list>
#include <string>

using std::vector;
using cv::Mat;
using std::string;
using std::list;

class CHoughCircleWithGrad : public CHoughCircleTrans{

protected:

	//normalized gradient
	cv::Mat m_nx;
	cv::Mat m_ny;

protected:
	//Transfer a point in image domain to 2 points in Hough Space
	//The point is defined by (x0,y0). 
	//The two points are calculated by (xR, yR) = (x0,y0) +/- R*(nx, ny)
	//where R, is the radius and (nx, ny) the normalized gradient information
	void pointTo2Points(const cv::Mat& Gx, const cv::Mat& Gy, int x0, int y0, int radius, int radius_idx);
	void pointTo2Points(int x0, int y0, int radius, uint16_t* radius_ptr);

	//(xR, yR) = (x0,y0) +/- R*(nx, ny)
	//(nx,ny) = (gx, gy)/sqrt(gx^2+gy^2) 
	void calc2PointsFromGradient(const int& x0, const int& y0, const int& R, const cv::Mat& Gx, const cv::Mat& Gy,
		int xR[2], int yR[2]);

	//(xR, yR) = (x0,y0) +/- R*(nx, ny)
	void calc2PointsFromNormalizedGrad(const int& x0, const int& y0, const int& R, const float& nx, const float& ny,
		int xR[2], int yR[2]);

	//calc normalized gradient
	void calcNormalizedGrad(const cv::Mat& blurred_gray, const cv::Mat& Edges,
		cv::Mat& nx, cv::Mat& ny);
public:

	//Hough Circle Init
	//nhood_xy - x0, y0 spatial nhood for non-maximal supression - must be odd
	//nhood_xy - radius nhood for non-maximal supression - must be odd
	CHoughCircleWithGrad(int nhood_xy, int	nhood_rho);

	//Dctor
	~CHoughCircleWithGrad();

	//Transform edge image to Hough
	void Im2Hough(const cv::Mat& blurred_gray, unsigned minRadius, unsigned maxRadius);

};