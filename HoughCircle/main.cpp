#include "stdafx.h"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include "HoughCircleTrans.h"
#include "HoughCircleWithGrad.h"

#include <iostream>
#include <stdio.h>
#include <chrono>

using namespace cv;
using namespace std;

int main(int argc, char* argv[])
{
	
	Mat src, src_gray, gray_blurred;

	/// Read the image
	if ( argv[1] )
	{
		string input_image = argv[1];
		src = imread(input_image, 1);
	}
	else
	{
		cerr << "Please supply an input image" << endl;
	}

	if (!src.data)
	{
		return -1;
	}

	/// Convert it to gray
	cvtColor(src, src_gray, CV_BGR2GRAY);

	/////////////////////////////////////////////////////////
	//Hough Circle transform - single point to full circle//
	/////////////////////////////////////////////////////////
	vector<cv::Vec3i> circles_i;
	{
		auto tt1 = std::chrono::high_resolution_clock::now();

		/// Reduce the noise to avoid false circle detection
		GaussianBlur(src_gray, gray_blurred, Size(9, 9), 2, 2);

		//Find edges using Canny
		Mat edge;
		Canny(gray_blurred, edge, 40, 90);

		CHoughCircleTrans CHT(11, 9);
		CHT.edgeIm2Hough(edge, 220, 350);
		double scoreRatio = 0.5;
		int maxNumCircs = 1;
		CHT.getHoughPeaks(circles_i, scoreRatio, maxNumCircs);
		auto tt2 = std::chrono::high_resolution_clock::now();

		std::cout << "Vanilla HoughCircles took "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(tt2 - tt1).count()
			<< " milliseconds\n";

	}

	/////////////////////////////////////////////////////////////////////////////
	//Hough Circle transform using gradient information - 1 point to two points//
	/////////////////////////////////////////////////////////////////////////////
	vector<cv::Vec3i> circles_g;
	{
		auto tu1 = std::chrono::high_resolution_clock::now();

		/// Reduce the noise to avoid false circle detection
		GaussianBlur(src_gray, gray_blurred, Size(9, 9), 2, 2);

		//Mat SimpleCircle(src.rows, src.cols, CV_8UC1, Scalar(0));
		// add full circle
		//circle(SimpleCircle, Point(240, 256), 200, Scalar(255), -1, 8);

		CHoughCircleWithGrad CHWG(11, 9);
		CHWG.Im2Hough(gray_blurred, 220, 350);
		
		double scoreRatio = 0.5;
		int maxNumCircs = 1;
		CHWG.getHoughPeaks(circles_g, scoreRatio, maxNumCircs);

		auto tu2 = std::chrono::high_resolution_clock::now();
		std::cout << "Hough Circle detection with gradient information took "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(tu2 - tu1).count()
			<< " milliseconds\n";

		int i = 0;
		Point center(cvRound(circles_g[i][0]), cvRound(circles_g[i][1]));
		int radius = cvRound(circles_g[i][2]);

	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//Open CV's Hough Circle transform also using gradient information - 1 point to two points////
	//////////////////////////////////////////////////////////////////////////////////////////////
	vector<Vec3f> circles_opencv;
	{
		auto t1 = std::chrono::high_resolution_clock::now();

		/// Reduce the noise to avoid false circle detection
		GaussianBlur(src_gray, gray_blurred, Size(9, 9), 2, 2);

		/// Apply the Hough Transform to find the circles
		HoughCircles(gray_blurred, circles_opencv, CV_HOUGH_GRADIENT, 1, src_gray.rows / 8, 90, 47, 220, 350);

		auto t2 = std::chrono::high_resolution_clock::now();
		std::cout << "Open CV HoughCircles took "
			<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
			<< " milliseconds\n";
	}

	/// Draw the circles detected with vanilla Hough Circle detection
	for (size_t i = 0; i < circles_i.size(); i++)
	{
		Point center(cvRound(circles_i[i][0]), cvRound(circles_i[i][1]));
		int radius = cvRound(circles_i[i][2]);
		// circle center
		circle(src, center, 3, Scalar(0, 255, 0), -1, 8, 0);
		// circle outline
		circle(src, center, radius, Scalar(0, 0, 255), 1, 8, 0);
	}

	/// Draw the circles detected with vanilla Hough Circle detection
	for (size_t i = 0; i < circles_g.size(); i++)
	{
		Point center(cvRound(circles_g[i][0]), cvRound(circles_g[i][1]));
		int radius = cvRound(circles_g[i][2]);
		// circle center
		circle(src, center, 3, Scalar(0, 240, 240), -1, 8, 0);
		// circle outline
		circle(src, center, radius, Scalar(255, 0, 0), 1, 8, 0);
	}

	/// Draw the circles detected with Open CV
	for (size_t i = 0; i < circles_opencv.size(); i++)
	{
		Point center(cvRound(circles_opencv[i][0]), cvRound(circles_opencv[i][1]));
		int radius = cvRound(circles_opencv[i][2]);
		// circle center
		circle(src, center, 3, Scalar(255, 0, 0), 1, 8, 0);
		// circle outline
		circle(src, center, radius, Scalar(0, 255, 0), 1, 8, 0);
	}

	/// Show the results
	namedWindow("Hough Circle Transform Demo", CV_WINDOW_AUTOSIZE);
	imshow("Hough Circle Transform Demo", src);
	waitKey(0);

	return 0;
}

