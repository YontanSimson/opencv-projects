========================================================================
    CONSOLE APPLICATION : Main Project Overview
========================================================================

A comparison between 3 versions of hough circle transform:

A)	A version of Hough Circle transform that transform each point in the image to a complete circle in Hough space. This is very, very slow compared to OpenCVs houghCircle()

B)	A version of Hough Circle transform that utilizes gradient information to speed up the circle detection. Requiring only 2 votes per point pixel instead of complete circle. This is presumable how Open CV houghCircle() gets its speed,  though this version is still not as fast.

C)	OpenCV's houghCircle() builtin function


LIST OF FILES:

HoughCircle.vcxproj
	Main project definitions

HoughCircle.vcxproj.filters
	Project filters

HoughCircleTrans.cpp/h
	Object for Vanilla Circular Hough transform. Each edge point is transformed into a circle in a 2D dimensional Hough Space according to the relevant radius.
	
HoughCircleWithGrad.cpp/h
	Object for Hough circle transform and circle detection that utilizes gradient information to reduce number of votes for each edge point in the image domain. It requires only 2 votes instead of a complete circle.
	
DrawCircle.cpp/h
	Object for creating circles. Use Mid-Point algorithm. Popularly know as Bresenhams Circle algorithm.

main.cpp
    This is the main application source file.

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named ConsoleApplication1.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////
