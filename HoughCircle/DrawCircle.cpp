#include "stdafx.h" 
#include <list>
#include "DrawCircle.h"

using std::list;

void CDrawCircle::preallocateBresenhamCircle(const int& Radius, const int& Rows, const int& Cols,
	int** x_idx, int** y_idx, size_t& circ_size)
{
	//list<int> TempCirc;
	list<int> xPos;
	list<int> yPos;

	//init Mid Point Alg
	init(Radius);

	//center around zero
	static const int x_c = 0;
	static const int y_c = 0;

	int x[8];
	int y[8];
	circ_size = 0;
	do
	{
		int xx = getX();
		int yy = getY();
		x[0] = xx + x_c;
		y[0] = yy + y_c;
		x[1] = yy + x_c;
		y[1] = xx + y_c;
		x[2] = -xx + x_c;
		y[2] = yy + y_c;
		x[3] = -yy + x_c;
		y[3] = xx + y_c;

		x[4] = -xx + x_c;
		y[4] = -yy + y_c;
		x[5] = -yy + x_c;
		y[5] = -xx + y_c;
		x[6] = xx + x_c;
		y[6] = -yy + y_c;
		x[7] = yy + x_c;
		y[7] = -xx + y_c;

		//place point in all 2 octaves
		for (int oct_idx = 0; oct_idx < 8; oct_idx++)
		{
			xPos.push_back(x[oct_idx]);
			yPos.push_back(y[oct_idx]);
			circ_size++;
		}
	} while (calcPoint());

	//When done adding to list, allocate momory for circle indexs
	*x_idx = new int[circ_size];
	*y_idx = new int[circ_size];

	list<int>::iterator list_it_x = xPos.begin();
	list<int>::iterator list_it_y = yPos.begin();

	int* p_x_idx = *x_idx;
	int* p_y_idx = *y_idx;

	for (size_t i = 0; i < circ_size; i++, list_it_x++, list_it_y++)
	{
		p_x_idx[i] = *list_it_x;
		p_y_idx[i] = *list_it_y;
	}
}

/* Sample code for testint Mid point Alg

//Creat circle for testing Bresenham alg
Mat Circ(100, 100, CV_8UC1, Scalar(0));
CDrawCircle drawCircle;

drawCircle.init(40);

int m_x0 = Circ.cols/2;
int m_y0 = Circ.rows/2;

int x[8];
int y[8];

do
{
int xx = drawCircle.getX();
int yy = drawCircle.getY();
x[0] =  xx + m_x0;
y[0] =  yy + m_y0;
x[1] =  yy + m_x0;
y[1] =  xx + m_y0;
x[2] = -xx + m_x0;
y[2] =  yy + m_y0;
x[3] = -yy + m_x0;
y[3] =  xx + m_y0;

x[4] = -xx + m_x0;
y[4] = -yy + m_y0;
x[5] = -yy + m_x0;
y[5] = -xx + m_y0;
x[6] =  xx + m_x0;
y[6] = -yy + m_y0;
x[7] =  yy + m_x0;
y[7] = -xx + m_y0;

//place point in all 2 octaves
for (int oct_idx = 0; oct_idx < 8; oct_idx++)
{
	if (0 <= x[oct_idx] && x[oct_idx] < Circ.cols &&
		0 <= y[oct_idx] && y[oct_idx] < Circ.rows)
	{
		Circ.at<uchar>( Point2d(x[oct_idx], y[oct_idx]) ) = 255;
	}
} while (drawCircle.calcPoint());

//Print circle to screen
namedWindow("Show Bresenham circle", CV_WINDOW_AUTOSIZE);
imshow("Show Bresenham circle", Circ);
waitKey(10);
*/
