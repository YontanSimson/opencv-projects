#pragma once

#include <unordered_map>
#include <unordered_set>
#include <cstdint>//int64_t
#include <string>

class C2Sum
{
public:
	C2Sum(){}
	~C2Sum(){}


	void Init(const std::string& fileName, const bool& useSet);
	size_t Run();
private:

	std::unordered_map<int64_t, int64_t> m_hash_map;
	std::unordered_set<int64_t> m_set_map;
	bool m_useSet;
};