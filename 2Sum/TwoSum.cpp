// 2Sum.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "TwoSum.h"
#include "Utils.h"

void C2Sum::Init(const std::string& fileName, const bool& useSet)
{
	m_useSet = useSet;
	size_t NumOfLines = GetNumberOfLinesInFile(fileName);

	if (m_useSet)
	{
		ReadArrayFromFileIntoSet(fileName, m_set_map, NumOfLines);
		//float z = m_set_map.max_load_factor();
		//std::cout << m_set_map.bucket_count() << std::endl;
		//m_set_map.max_load_factor(z / 4.0f);
		//std::cout << m_set_map.bucket_count();
	}
	else
	{
		ReadArrayFromFileIntoHash(fileName, m_hash_map, NumOfLines);
	}
}

size_t C2Sum::Run()
{
	size_t NumXYSums = 0;

	if (m_useSet)
	{
		for (int64_t t = -10000; t <= 10000; t++)
		{
			bool XpYexists = false;
			//does x + y = t exist in hash?
			for (auto it = m_set_map.begin(); it != m_set_map.end(); it++)
			{
				const int64_t& x = *it;
				const int64_t& y = t - x;
				if (m_set_map.find(y) != m_set_map.end() && x != y)
				{
					XpYexists = true;
					break;
				}
			}
			NumXYSums += XpYexists;
		}
	}
	else
	{
		for (int64_t t = -10000; t <= 10000; t++)
		{
			bool XpYexists = false;
			//does x + y = t exist in hash?
			for (auto it = m_hash_map.begin(); it != m_hash_map.end(); it++)
			{
				const int64_t& x = it->first;
				const int64_t& y = t - x;
				if (m_hash_map.find(y) != m_hash_map.end() && x != y)
				{
					XpYexists = true;
					break;
				}
			}
			NumXYSums += XpYexists;
		}
	}



	return NumXYSums;
}
