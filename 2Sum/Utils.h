#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <cstdint>//int64_t

size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

//Read array from text file and intialize array
static void ReadArrayFromFileIntoHash(const std::string& fileName, std::unordered_map<int64_t, int64_t>& hash_map, const size_t& arrSize)
{

	hash_map.reserve(arrSize);

	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{
				int64_t val = stoll(line);
				hash_map.insert(std::make_pair(val, val));
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

//Read array from text file and intialize array
static void ReadArrayFromFileIntoSet(const std::string& fileName, std::unordered_set<int64_t>& set_map, const size_t& arrSize)
{
	set_map.reserve(arrSize);

	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{
				int64_t val = stoll(line);
				set_map.insert(val);
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

