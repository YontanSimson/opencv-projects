#pragma once
#include <string>
#include <cstdint>


// representation of undirected edge (u, v) having weight 'weight'
struct SEdge {
	int u, v, weight;

	//friend void swap(SEdge& a, SEdge& b)
	//{
	//	using std::swap; // bring in swap for built-in types

	//	swap(a.u, b.u);
	//	swap(a.v, b.v);
	//	swap(a.weight, b.weight);
	//}

	class CWeightLesser
	{
	public:
		bool operator()(const SEdge& lhs, const SEdge& rhs) const { return lhs.weight < rhs.weight; }//lt
	};
};


class CUndirectedGraph
{
public:
	CUndirectedGraph();
	~CUndirectedGraph();

	static size_t GetNumberOfLinesInFile(const std::string& fileName);
	void Init(const std::string& fileName);
	void Init(const uint64_t& number_of_edges);
	void Run();
	void Sort();

	//getter and Setters
	const SEdge* GetEdge(const uint64_t& idx) const { return m_edges + idx; }
	void SetEdge(const uint64_t& idx, const SEdge& edge){ *(m_edges + idx) = edge; }

	const SEdge* cbegin() const { return m_edges; }
	const SEdge* cend() const { return m_edges + m_number_of_edges; }

	SEdge* begin() const { return m_edges; }
	SEdge* end() const { return m_edges + m_number_of_edges; }

	size_t GetNumberOfNodes() const { return m_number_of_nodes; }
	uint64_t GetNumberOfEdges() const { return m_number_of_edges; }

private:
	SEdge *m_edges;
	size_t m_number_of_nodes;
	uint64_t m_number_of_edges;
};

