#pragma once
#include "UndirectedGraph.h"

class CKruskalMST
{
public:
	CKruskalMST();
	~CKruskalMST();

	void Init(const std::string& fileName);
	void Run();
	int64_t GetTotalWeight() const { return m_mst_total_weight; }

private:
	CUndirectedGraph m_uGraph;
	int64_t m_mst_total_weight;
};

