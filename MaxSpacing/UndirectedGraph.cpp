#include <fstream>
#include <iostream>
#include <algorithm>

#include "UndirectedGraph.h"
//#include "UnionRank.h"


//class CWeightLesser 
//{
//public:
//	bool operator()(const SEdge& lhs, const SEdge& rhs) const { return lhs.weight < rhs.weight; }//lt
//};

using namespace std;

////solution from SO http://stackoverflow.com/a/11599/3481173
//namespace std
//{
//	template<>
//	void swap(SEdge& a, SEdge& b)
//	{
//		using std::swap; // bring in swap for built-in types
//
//		swap(a.u, b.u);
//		swap(a.v, b.v);
//		swap(a.weight, b.weight);
//	}
//}

size_t CUndirectedGraph::GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

CUndirectedGraph::CUndirectedGraph():
	m_edges(nullptr),
	m_number_of_nodes(0)
{
}

CUndirectedGraph::~CUndirectedGraph()
{
	if (m_edges != nullptr)
	{
		delete[] m_edges;
	}
	m_edges = nullptr;
}

void CUndirectedGraph::Init(const uint64_t& number_of_edges)
{
	m_number_of_edges = number_of_edges;
	m_edges = new SEdge[(size_t)m_number_of_edges]();
	m_number_of_nodes = -1;//not known at the moment
}

void CUndirectedGraph::Init(const std::string& fileName)
{
	m_number_of_edges = GetNumberOfLinesInFile(fileName) - 1;
	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_nodes] 
		sscanf_s(line.c_str(), "%d", &m_number_of_nodes);
		m_edges = new SEdge[(size_t)m_number_of_edges];
		for (int i = 0; i<m_number_of_edges; i++)  // Enter E undirected edges (u, v, weight)
		{   
			getline(inFile, line);
			SEdge edgeTemp;
			sscanf_s(line.c_str(), "%d %d %d", &(edgeTemp.u), &(edgeTemp.v), &(edgeTemp.weight));
			edgeTemp.u--;//make zero based
			edgeTemp.v--;//make zero based
			m_edges[i] = edgeTemp;
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
	}

}

//sort edge list from smallest to largest
void CUndirectedGraph::Sort()
{
	std::sort(m_edges, m_edges + m_number_of_edges, SEdge::CWeightLesser());
}

void CUndirectedGraph::Run()
{
	//int64_t m_mst_total_weight = 0;

	////Init MST
	//SEdge* mst = new SEdge[m_number_of_nodes]();//set to zeros

	////Make set
	//CUnionRank unionRank;
	//unionRank.Init(m_number_of_nodes);


	//SEdge* pEdge = m_edges;
	//int mst_i = 0;
	//for (int i = 0; i < m_number_of_edges; i++, pEdge++)
	//{
	//	if (!unionRank.is_connected(pEdge->u, pEdge->v))
	//	{
	//		mst[mst_i++] = *pEdge;
	//		unionRank.merge(pEdge->u, pEdge->v);
	//		m_mst_total_weight += pEdge->weight;
	//	}
	//}
}
