#include "MaxSpacing.h"
#include "UnionRank.h"

CMaxSpacing::CMaxSpacing()
{
}

CMaxSpacing::~CMaxSpacing()
{
}

void CMaxSpacing::Init(const std::string& fileName)
{
	m_uGraph.Init(fileName);
}

void CMaxSpacing::Run(const size_t& k)
{
	m_mst_total_weight = 0;
	size_t number_of_nodes = m_uGraph.GetNumberOfNodes();

	//Init MST
	CUndirectedGraph mst;
	mst.Init(number_of_nodes - 1);

	//Make set
	CUnionRank unionRank;
	unionRank.Init(number_of_nodes);

	//sort edge list from smallest to largest
	m_uGraph.Sort();

	const SEdge* pEdge = m_uGraph.cbegin();
	int mst_i = 0;
	for (int i = 0; i < m_uGraph.GetNumberOfEdges(); i++, pEdge++)
	{
		if (!unionRank.is_connected(pEdge->u, pEdge->v))
		{
			if (unionRank.get_number_of_disjoint_sets() != k)
			{
				unionRank.merge(pEdge->u, pEdge->v);
				mst.SetEdge(mst_i++, *pEdge);
				m_mst_total_weight += pEdge->weight;
			}
			else
			{
				m_maximum_spacing = pEdge->weight;
				break;
			}
		}


	}
}