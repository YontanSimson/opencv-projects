#pragma once
#include "UndirectedGraph.h"

class CMaxSpacing
{
public:
	CMaxSpacing();
	~CMaxSpacing();

	void Init(const std::string& fileName);
	void Run(const size_t& k);
	int64_t GetTotalWeight() const { return m_mst_total_weight; }
	int64_t GetMaximumSpacing() const { return m_maximum_spacing; }

private:
	CUndirectedGraph m_uGraph;
	int64_t m_mst_total_weight;
	int64_t m_maximum_spacing;
};

