#pragma once

class CUnionRank
{
public:
	CUnionRank();
	~CUnionRank();

	void Init(const size_t& size);
	size_t find(const size_t& x);//find which set meber x belongs to
	void merge(const size_t& x, const size_t& y);//union of sets X Y
	bool is_connected(const size_t& x, const size_t& y);
	size_t get_number_of_disjoint_sets() const { return m_number_of_disjoint_sets; }

private:
	size_t* m_parent;
	size_t* m_rank;
	size_t m_size;
	size_t m_number_of_disjoint_sets;
};

