#include "KruskalMST.h"
#include "UnionRank.h"

CKruskalMST::CKruskalMST()
{
}

CKruskalMST::~CKruskalMST()
{
}

void CKruskalMST::Init(const std::string& fileName)
{
	m_uGraph.Init(fileName);
}

void CKruskalMST::Run()
{
	m_mst_total_weight = 0;
	size_t number_of_nodes = m_uGraph.GetNumberOfNodes();

	//Init MST
	CUndirectedGraph mst;
	mst.Init(number_of_nodes - 1);

	//Make set
	CUnionRank unionRank;
	unionRank.Init(number_of_nodes);

	//sort edge list from smallest to largest
	m_uGraph.Sort();

	const SEdge* pEdge = m_uGraph.cbegin();
	int mst_i = 0;
	for (int i = 0; i < m_uGraph.GetNumberOfEdges(); i++, pEdge++)
	{
		if (!unionRank.is_connected(pEdge->u, pEdge->v))
		{
			unionRank.merge(pEdge->u, pEdge->v);
			mst.SetEdge(mst_i++, *pEdge);
			m_mst_total_weight += pEdge->weight;
		}
	}
}

