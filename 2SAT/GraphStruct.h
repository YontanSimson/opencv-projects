#pragma once 

#include <iostream>
#include <vector>
#include <list>
#include <unordered_map>
#include <string>
#include <cassert>
#include <cstdint>
using namespace std;

struct vertex{
	typedef pair<double, vertex*> ve;
	list<ve> adj; //cost of edge, destination vertex
	int64_t idx;
	vertex(const int64_t& _idx)
	{
		idx = _idx;
	}

};

struct is_pointing_to 
{
	is_pointing_to(const int64_t& _trg) : trg(_trg) {}
	int64_t trg;
	bool operator() (const pair<double, vertex*>& value) { return (value.second->idx == trg); }
};

//vertex map
typedef unordered_map<int64_t, vertex *> vmap;

//Graph structure
class CGraph
{
	vmap work;
public:

	void reserve(size_t numOfNodes);
	void addvertex(const int64_t&);
	void remove_vertex(const int64_t&);
	void addedge(const int64_t& from, const int64_t& to, double cost);
	void add_undirected_edge(const int64_t& idx1, const int64_t& idx2, double cost);
	void add_node_from_adjacency_list(const vector<int64_t>& node_and_edges);
	void add_node_from_adjacency_list_unweighted(const vector<int64_t>& node_and_edges);
	void add_node_and_weighted_edge_from_list(const vector< vector<int64_t> >& node_and_weighted_edges);
	void remove_vertex_undirected(const int64_t&idx);
	void contract_edge(const int64_t& u, const int64_t& v);
	void contract_undirected_edge(const int64_t& u, const int64_t& v);
	void print_graph() const;

	//getters
	size_t get_num_of_vertices() const { return work.size(); }
	size_t get_num_edges(const int64_t &idx) const;
	int64_t get_max_vertice_id() const {return work.rbegin()->first;}

	//const vertex* get_vertex(const T &idx) const { return (const vertex*)(work[idx]); }
	vertex* get_vertex(const int64_t &idx) { return work[idx]; }

	auto get_it_to_vertex(const int64_t &idx) const -> decltype(work.find(idx))
	{
		auto it_i = work.find(idx);
		assert(it_i != work.end());
		return it_i;
	}

	
	auto get_it_to_vertex(const int64_t &idx)-> decltype(work.find(idx))
	{
		auto it_i = work.find(idx);
		assert(it_i != work.end());
		return it_i;
	}

	auto begin() const -> decltype(work.cbegin()){ return work.cbegin(); }
	auto begin()-> decltype(work.begin()) { return work.begin(); }

	auto end() const -> decltype(work.cend()){ return work.cend(); }
	auto end() -> decltype(work.cend()) { return work.end(); }

	auto rbegin() const -> decltype(work.crbegin()){ return work.crbegin(); }
	auto rbegin()-> decltype(work.rbegin()){ return work.rbegin(); }

	auto rend() const -> decltype(work.crend()){ return work.crend(); }
	auto rend() -> decltype(work.rend()){ return work.rend(); }

	//setters

	//assignment operator
	CGraph& operator= (const CGraph& other);

	//reverse graph
	void reverse_graph(CGraph& rev) const;

	//clear
	void clear();

	//Ctr, Dctot, Copy Ctor
	CGraph(){}
	CGraph(const CGraph&);
	~CGraph();
};


