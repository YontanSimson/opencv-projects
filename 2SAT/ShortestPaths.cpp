﻿#include "ShortestPaths.h"


#include <algorithm> //min_element, find_if

struct compare_value{
	bool operator ()( const pair<size_t, double>& a, const pair<size_t, double>& b) const 
	{
		return a.second < b.second;
	}
};

struct IsKeyEqualTo{
	IsKeyEqualTo(const size_t& _val) :val(_val){}

	size_t val;
	bool operator ()(const pair<size_t, double>& a)
	{
		return a.first == val;
	}

};

std::deque<std::pair<size_t, double> >::iterator  CShortestPaths::GetNodeFromQueWithMinValue(std::deque<std::pair<size_t, double> >& Q)
{
	return min_element(Q.begin(), Q.end(), compare_value());
}

std::deque<std::pair<size_t, double> >::iterator CShortestPaths::GetNodeFromQue(std::deque<std::pair<size_t, double> >& Q, const size_t& id)
{
	auto it = std::find_if(Q.begin(), Q.end(), IsKeyEqualTo(id));
	return it;
}

size_t CShortestPaths::vertex_in_Q_with_min_dist(const std::set<size_t>& QQ)
{
	return 0;
}

//Dijkstra
void CShortestPaths::Run(const uint64_t& src_node)
{
	deque< pair<size_t, double> > Q;//key = node, value = distance
	size_t max_node = (size_t)m_Graph->get_max_vertice_id();//assumes 0 is not included
	vector<double> dist(max_node, DBL_MAX);// Distance from source to v
	vector<size_t> previous(max_node, 0);
	dist[0] = 0;

	// Initializations
	for (auto it = m_Graph->begin(); it != m_Graph->end(); it++)
	{
		if (it->first != src_node)
		{
			dist[(size_t)it->first - 1] = DBL_MAX;// Unknown distance function from source to v
			Q.push_back(make_pair((size_t)it->first, DBL_MAX));// All nodes initially in Q (unvisited nodes)
		}
		else
		{
			Q.push_back(make_pair((size_t)it->first, 0));// All nodes initially in Q (unvisited nodes)
		}
	}

	while (!Q.empty())// The main loop
	{
		auto u_it = GetNodeFromQueWithMinValue(Q);// Source node in first case
		size_t u = u_it->first;
		double dist_u = u_it->second;
		Q.erase(u_it);
		for (auto it_adj_u = m_Graph->get_it_to_vertex(u)->second->adj.begin(); it_adj_u != m_Graph->get_it_to_vertex(u)->second->adj.end(); ++it_adj_u)
		{
			double alt = dist_u + it_adj_u->first;
			auto v = GetNodeFromQue(Q, it_adj_u->second->idx);
			if ( v != Q.end() && alt < v->second)//Q in unexplored set and with smaller alternative
			{
				v->second = alt;
				dist[v->first-1] = alt;
				previous.at(v->first-1) = u;
			}
		}
	}

	//int idxs[10] = {7,37,59,82,99,115,133,165,188,197};
	//for (int i = 0; i < 10; ++i)
	//{
	//	cout << dist[idxs[i] - 1] << ",";
	//}
}
