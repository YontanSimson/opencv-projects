#pragma once

#include "GraphStruct.h"
#include <deque>
#include <set>

class CShortestPaths
{
	const CGraph* m_Graph;
public:
	CShortestPaths(const CGraph* Graph) : m_Graph(Graph){}
	~CShortestPaths(){}
	void Run(const uint64_t& src_node);


protected:

	std::deque<std::pair<size_t, double> >::iterator GetNodeFromQueWithMinValue(std::deque<std::pair<size_t, double> >& Q);
	std::deque<std::pair<size_t, double> >::iterator GetNodeFromQue(std::deque<std::pair<size_t, double> >& Q, const size_t& id);
	size_t vertex_in_Q_with_min_dist(const std::set<size_t>& QQ);
};