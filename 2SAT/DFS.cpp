﻿#include "DFS.h"

#include <stack>

using namespace std;

void CDFS::Init()
{
	m_t = 0;
	m_s = -1;
	m_leaders.clear();
	m_visited_nodes.clear();

	if (!m_according_finnishing_time)
		m_finishing_time.clear();
}

void CDFS::DFS_iterative(const CGraph& Graph, const int64_t& v)
{
	stack<int64_t> S;
	S.push(v);
	while (!S.empty())
	{
		uint64_t curr_v = S.top();
		S.pop();
		if (m_visited_nodes[curr_v] == 0)//if v is not labeled as discovered
		{
			m_visited_nodes[curr_v] = 1;//label v as discovered
			auto it_v = Graph.get_it_to_vertex(curr_v);

			for (auto it_adj = it_v->second->adj.begin(); it_adj != it_v->second->adj.end(); it_adj++)
			{
				S.push(it_adj->second->idx);
			}
		}
	}

}

void CDFS::DFS_recursive(const CGraph& Graph, const int64_t& i)
{
	m_visited_nodes[i] = 1;
	m_leaders[i] = m_s;
	auto it_i = Graph.get_it_to_vertex(i);
	for (auto it_adj = it_i->second->adj.begin(); it_adj != it_i->second->adj.end(); it_adj++)
	{
		uint64_t j = it_adj->second->idx;
		if (m_visited_nodes[j] == 0)
		{
			DFS_recursive(Graph, j);
		}
	}
	m_t++;

	if (!m_according_finnishing_time)
		m_finishing_time[m_t] = i;//reverse of f(i):=t as given in lecture
}

void CDFS::DFS_loop(const CGraph& Graph, bool according_finnishing_time)
{
	m_according_finnishing_time = according_finnishing_time;
	Init();
	if (m_according_finnishing_time)
	{
		for (auto rit = m_finishing_time.rbegin(); rit != m_finishing_time.rend(); ++rit)
		{
			uint64_t i = rit->second;
			m_s = i;//leader node
			if (m_visited_nodes.find(i) == m_visited_nodes.end())
			{
				DFS_recursive(Graph, i);
			}
		}

	}
	else
	{
		for (auto rit = Graph.rbegin(); rit != Graph.rend(); ++rit)
		{
			uint64_t i = rit->first;
			m_s = i;//leader node
			if (m_visited_nodes.find(i) == m_visited_nodes.end())
			{
				DFS_recursive(Graph, i);
			}
		}
	}

}

void CDFS::calc_SCC(const CGraph& Graph)
{
	CGraph revG;
	Graph.reverse_graph(revG);
	//cout << "Reversed Graph: " << std::endl;
	//revG.print_graph();
	DFS_loop(revG);
	DFS_loop(Graph, true);

	//end result is in m_leaders - each node is mapped to a leading node
	//TODO - insert into union rank structure
}

bool CDFS::IsSAT()
{
	//Check each positive clause to see if it's negatice ahs same leader
	bool SAT = true;
	for (const auto &it : m_leaders) // access by const reference
	{
		if (it.first > 0)
		{
			auto got = m_leaders.find(-it.first);
			if (got != m_leaders.end() && got->second == it.second)//positive and negative in the same clause->not SAT
			{
				SAT = false;
				return SAT;
			}
		}
	}

	return SAT;
}

void CDFS::AnalyzeSizesOfSCC(std::unordered_map<int64_t, int64_t> SCC)
{
	//SCC histogram
	for (const auto &it : m_leaders) // access by const reference
	{
		SCC[it.second]++;
	}

	//create ordered list from values of SCC map
	list<uint64_t> SCC_Sizes;
	for (auto &it : SCC) // access by const reference
	{
		SCC_Sizes.push_front(it.second);
	}
	SCC_Sizes.sort();

	//print 5 largest SCCs
	uint64_t i = 0;
	for (auto rit = SCC_Sizes.rbegin(); rit != SCC_Sizes.rend(); ++rit) // access by const reference
	{
		cout << *rit;
		i++;

		if (i >= 5)
		{
			cout << endl;
			break;
		}
		else
		{
			cout << ",";
		}
	}
}

