﻿#pragma once

#include "GraphStruct.h"

typedef unordered_map<int64_t, int64_t> tmap;
class CDFS
{
	tmap m_visited_nodes;
	tmap m_leaders;
	tmap m_finishing_time;

	const CGraph* m_Graph;

	int64_t m_t;
	int64_t m_s;
	bool m_according_finnishing_time;


	void Init();

public:
	CDFS() :m_t(0), m_s(-1){}
	~CDFS(){}

	void DFS_iterative(const CGraph& Graph, const int64_t& v);
	void DFS_recursive(const CGraph& Graph, const int64_t& v);
	void DFS_loop(const CGraph& Graph, bool according_finnishing_time = false);
	void calc_SCC(const CGraph& Graph);
	bool IsSAT();
	void AnalyzeSizesOfSCC(std::unordered_map<int64_t, int64_t> SCC);
};

