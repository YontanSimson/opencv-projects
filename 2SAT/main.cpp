#include <fstream>
#include <iostream>
#include <string>

#include <conio.h>
#include <cstdio>
#include <chrono>

#include "GraphStruct.h"
#include "Utils.h"
#include "MinCut.h"
#include "DFS.h"
#include "ShortestPaths.h"

using namespace std;

#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
	#ifndef DBG_NEW
		#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
		#define new DBG_NEW
	#endif
#endif


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	
	string fileListName;
	if (argc == 2)
	{
		fileListName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	std::vector<std::string> fileList;
	ReadFileList(fileListName, fileList);

	auto t0 = std::chrono::high_resolution_clock::now();

	for (auto& fileName : fileList)
	{
		CGraph graph;
		ReadClausesFromFileIntoGraph(fileName, &graph);
		//cout << "Org" << endl;
		//graph.print_graph();

		CDFS dfs;
		dfs.calc_SCC(graph);

		//Check if is SAT
		bool isSat = dfs.IsSAT();

		if (!isSat)
			cout << "Not ";
		cout << "SAT" << std::endl;
	}

	auto t1 = std::chrono::high_resolution_clock::now();
	std::cout << "Alg took " << std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
		<< " milliseconds\n";

	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}