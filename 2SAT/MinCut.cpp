#include "MinCut.h"
#include <iterator>     // std::advance
#include <algorithm>    // std::min_element, std::max_element
#include <math.h>

using namespace std;

uint64_t round64(double x)
{
	return (uint64_t)floorl(x + 0.5);
}

void CMinCut::Run()
{
	srand(m_rngSeed);
	uint64_t numOfContractions = (uint64_t)round64(m_Graph.get_num_of_vertices()*m_Graph.get_num_of_vertices()*log(m_Graph.get_num_of_vertices()));

	m_numOfMinCuts = INT_MAX;
	for (uint64_t i = 0; i < numOfContractions; i++)
	{
		m_numOfMinCuts = min(CalcSingleCut(), m_numOfMinCuts);
		cout << "Iteration # " << i + 1 << "/" << numOfContractions << endl;
		cout << "Min cut: " << m_numOfMinCuts << endl;
		//cout << "Percentage of work done: " << 100 * float(i) / numOfContractions << endl;
	}

}

uint64_t CMinCut::CalcSingleCut()
{
	CGraph SingleCut(m_Graph);
	
	uint64_t n = SingleCut.get_num_of_vertices();
	while (n>2)
	{

		int u = std::rand() % n;
		auto it_u = SingleCut.begin();
		advance(it_u, u);		//Move the iterator to that position
		uint64_t idx_u = it_u->first;

		uint64_t edge = SingleCut.get_num_edges(idx_u);
		uint64_t v = std::rand() % edge;
		auto it_v = SingleCut.get_vertex(idx_u)->adj.begin();
		std::advance(it_v, v);
		uint64_t idx_v = it_v->second->idx;

		//u<-v
		//SingleCut.contract_edge(idx_u, idx_v);
		SingleCut.contract_undirected_edge(idx_u, idx_v);
		n = SingleCut.get_num_of_vertices();

		//cout << "Contraction #" << n << endl;
		//SingleCut.print_graph();
	}

	uint64_t numOfCuts = SingleCut.begin()->second->adj.size();
	return numOfCuts;
}