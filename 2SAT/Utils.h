#pragma once 

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#include "GraphStruct.h"

size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

template<class T>
void split(const std::string &s, const char delim, std::vector<T>& elems) 
{
	elems.clear();
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back((T)stoull(item));
	}
}

template<class T>
std::vector<T> split(const std::string &s, const char delim )
{
	std::vector<T> elems;

	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back((T)stoull(item));
	}
	return elems;
}

//only works with vector<string>
template < class ContainerT >
void tokenize(const std::string& str, ContainerT& tokens,
	const std::string& delimiters = " ", bool trimEmpty = false)
{
	std::string::size_type pos, lastPos = 0;

	using value_type = typename ContainerT::value_type;
	using size_type = typename ContainerT::size_type;

	while (true)
	{
		pos = str.find_first_of(delimiters, lastPos);
		if (pos == std::string::npos)
		{
			pos = str.length();

			if (pos != lastPos || !trimEmpty)
				tokens.push_back(value_type(str.data() + lastPos,
				(size_type)pos - lastPos));

			break;
		}
		else
		{
			if (pos != lastPos || !trimEmpty)
				tokens.push_back(value_type(str.data() + lastPos,
				(size_type)pos - lastPos));
		}

		lastPos = pos + 1;
	}
}

//Read array from text file and intialize Graph - not weighted
inline void ReadCGraphFromFile(const std::string& fileName, CGraph* pGraph)
{

	std::ifstream inFile(fileName.c_str());
	std::string line;
	int i = 0;
	if (inFile.is_open())
	{
		//get first line for number of nodes to read
		getline(inFile, line);
		//read first line containing number of nodes
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{

				vector<int64_t> node_and_edges;
				split(line, ' ', node_and_edges);
				pGraph->add_node_from_adjacency_list(node_and_edges);
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

//Read file list from text file
inline void ReadFileList(const std::string& fileName, std::vector<std::string>& fileList)
{
	int numberOfLinesInFile = GetNumberOfLinesInFile(fileName);
	fileList.reserve(numberOfLinesInFile);
	std::ifstream inFile(fileName.c_str());
	std::string line;
	int i = 0;
	if (inFile.is_open())
	{
		//read first line containing number of nodes
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{
				fileList.push_back(line);
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

//Read array from text file and intialize Graph - not weighted - for 2SAT problem
inline void ReadClausesFromFileIntoGraph(const std::string& fileName, CGraph* pGraph)
{

	std::ifstream inFile(fileName.c_str());
	std::string line;
	int i = 0;
	if (inFile.is_open())
	{
		//get first line for number of lines to read
		getline(inFile, line);
		pGraph->reserve(std::stoi(line));
		//read first line containing number of nodes
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{

				vector<int64_t> node_and_edges;
				split(line, ' ', node_and_edges);
				//negate first node
				node_and_edges[0] = -node_and_edges[0];
				//add edge
				pGraph->add_node_from_adjacency_list(node_and_edges);

				std::reverse(node_and_edges.begin(), node_and_edges.end());
				//negate all nodes
				node_and_edges[0] = -node_and_edges[0];
				node_and_edges[1] = -node_and_edges[1];

				//add edge
				pGraph->add_node_from_adjacency_list(node_and_edges);
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

//Read array from text file and intialize Graph
inline void ReadWeightedGraphFromFile(const string& fileName, CGraph* pGraph)
{

	std::ifstream inFile(fileName.c_str());
	string line;
	int i = 0;
	if (inFile.is_open())
	{
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{

				vector<string> node_and_edges;
				split(line, '\t', node_and_edges);
				vector< vector<int64_t> > node_and_weighted_edges;
				auto it = node_and_edges.begin();
				node_and_weighted_edges.push_back(vector<int64_t>(1, stoull(*it)));
				it++;
				for (; it != node_and_edges.end(); it++)
				{
					node_and_weighted_edges.push_back(split<int64_t>(*it, ','));
				}
				pGraph->add_node_and_weighted_edge_from_list(node_and_weighted_edges);
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

static size_t GetWeightedDirectedGraphFromFile(const std::string& fileName, CGraph* pGraph)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

static uint32_t ReadFile(
	const std::string& fileName,
	std::vector<uint32_t>& value_list,
	std::vector<uint32_t>& weight_list)
{

	size_t NumOfLines = GetNumberOfLinesInFile(fileName);
	size_t NumOfNodes = NumOfLines - 1;
	uint32_t knapsack_size = 0;

	value_list.resize(NumOfNodes);
	weight_list.resize(NumOfNodes);


	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_lines][number_of_bits] 
		uint32_t number_of_items;

		sscanf_s(line.c_str(), "%d %d", &knapsack_size, &number_of_items);
		assert(number_of_items == NumOfNodes);
		int lineIdx = 0;
		for (size_t i = 0; i<number_of_items; ++i, ++lineIdx)  // Enter E undirected edges (u, v, weight)
		{
			getline(inFile, line);
			//convert string to bitset
			sscanf_s(line.c_str(), "%d %d", &value_list[lineIdx], &weight_list[lineIdx]);
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
		return -1;
	}

	return knapsack_size;

}
