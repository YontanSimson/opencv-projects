LIST OF CURRENT PROJECTS:

	-HoughCircle: 	HoughCircle transform for detecting circles. Mostly an exercise to learn OpenCV. Much slower than OpenCVs 'houghCircle()'.
	-MVG:			Basic Multiple View Geometry functions for calculating F, H from points. Includes Eigen. No need for OpenCV.
	-ChanVese:		"Active contours without edges" implementation of Chan and Veses "LeveL Set Segmentation Algorithms" - not finished.


OPENCV INSTALLATION INSTRUCTIONS:
	
Most of the Projects here require OpenCV. Here is  a brief explanation of how to install OpenCV 2.4.8 on Windows OS and integrate it into your MCVS Project after downloading it onto your computer:

A) 64 vs 32 bit conflicts
	Make sure the $OPENCV_DIR refers to the right version

	For example you have two alternatives in OpenCV 2.4.8:
	1) D:\OpenCV\opencv\build\x86\vc11     (32 bits)
	2) D:\OpenCV\opencv\build\x64\vc11     (64 bits)

	Make sure that your project definition is suited to the right bittage.

	How can you check/correct this?
	-Right click on solution->properties->Configuration Properties->Platform
	-Choose either 64/32 bits depending on what you want
	
	How to define $OPENCV_DIR?
	click on start -> Right click on "Computer" -> Properties -> Advanced System setting -> Choose "Advanced" tab -> Environment Variables -> Click on "New..." under System Variables -> For Variable Name enter: OPENCV_DIR -> For Variable Value enter relevant build directory. 

B) Including header directory:
	Make sure to add to the configuration Properties of the project:
	C++->General->Include Additional Directories->$(OPENCV_DIR)\..\..\include

C) Add OpenCV bin folder to:
	Linker->General->Additional Library Directories->$(OPENCV_DIR)\lib;

D) Add libs's to:
	Linker->Input->Additional Dependencies->
	
	For debug copy and paste:
	opencv_calib3d247d.lib;opencv_contrib247d.lib;opencv_core247d.lib;opencv_features2d247d.lib;opencv_flann247d.lib;opencv_gpu247d.lib;opencv_highgui247d.lib;opencv_imgproc247d.lib;opencv_legacy247d.lib;opencv_ml247d.lib;opencv_nonfree247d.lib;opencv_objdetect247d.lib;opencv_photo247d.lib;opencv_stitching247d.lib;opencv_superres247d.lib;opencv_ts247d.lib;opencv_video247d.lib;opencv_videostab247d.lib;

	For release copy and paste:
	opencv_calib3d247.lib;opencv_contrib247.lib;opencv_core247.lib;opencv_features2d247.lib;opencv_flann247.lib;opencv_gpu247.lib;opencv_highgui247.lib;opencv_imgproc247.lib;opencv_legacy247.lib;opencv_ml247.lib;opencv_nonfree247.lib;opencv_objdetect247.lib;opencv_photo247.lib;opencv_stitching247.lib;opencv_superres247.lib;opencv_ts247.lib;opencv_video247.lib;opencv_videostab247.lib;

E) For runtime make sure that you have the right DLL folder in your Path:
	e.g. %OPENCV_DIR%\bin
	
	





