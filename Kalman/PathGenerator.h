#pragma once

#include <list>
#include <cmath>
#include "opencv2/core/core.hpp"

class CGeneratePath
{
public:
	CGeneratePath();
	~CGeneratePath();

	void GenerateStraightPath(cv::Point2f initPos, float length, float step, float alpha);
	cv::Mat transformPathToImage();
	void ClearPath();

private:
	std::list<cv::Point2f> m_path;
};
