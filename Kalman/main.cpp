// main.cpp : Defines the entry point for the console application.
//

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <algorithm>
#include <iostream>
#include <stdio.h>

#include "PathGenerator.h"

using namespace std;
using namespace cv;

/** Function Headers */
void concatenate2Images(const Mat& im1, const Mat& im2, Mat& stackIm);

inline float DEG2RAD(float ang) { return CV_PI*ang / 180.f; }

/** @function main */
int main(int argc, const char** argv)
{

	CGeneratePath pathGen;

	pathGen.GenerateStraightPath(cv::Point2f(0, 0), 100, 7, DEG2RAD(45));

	cv::Mat img = pathGen.transformPathToImage();


	return 0;
}

void concatenate2Images(const Mat& im1, const Mat& im2, Mat& stackIm)
{
	CV_ARE_SIZES_EQ(&im1, &im2);
	CV_Assert(im1.rows == im2.rows);
	CV_Assert(im1.cols == im2.cols);
	CV_Assert(im1.channels() == im2.channels());

	int rows = im1.rows;
	int cols = im1.cols;

	//show both images contcatenated
	stackIm.create(rows, cols * 2, im1.type());

	Rect Roi[2];
	int offsetX = 0;
	for (size_t i = 0; i < 2; i++)
	{
		Roi[i] = Rect(offsetX, 0, cols, rows);
		offsetX += cols;
	}
	im1.copyTo(stackIm(Roi[0]));
	im2.copyTo(stackIm(Roi[1]));
}