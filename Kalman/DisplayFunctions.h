#pragma once
#include "opencv2/core/core.hpp"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

//Show histogram of 2d image
inline void showHist(cv::Mat Im, const int numOfBins = 64)
{

	double MaxVal, MinVal;
	cv::minMaxLoc(Im, &MinVal, &MaxVal);

	cv::Mat hist;
	bool uniform = true;
	bool accumulate = false;
	float ImRanges[] = { (float)MinVal, (float)MaxVal };
	const float* ranges[] = { ImRanges };

	cv::calcHist(&Im, 1, 0, cv::Mat(), hist, 1, &numOfBins, ranges, uniform, accumulate);

	cv::Mat histImage = cv::Mat::ones(200, 320, CV_8U) * 255;

	normalize(hist, hist, 0, histImage.rows, CV_MINMAX, CV_32F);
	histImage = cv::Scalar::all(255);
	int binW = cvRound((double)histImage.cols / numOfBins);

	for (int i = 0; i < numOfBins; i++)
		rectangle(histImage, cv::Point(i*binW, histImage.rows),
		cv::Point((i + 1)*binW, histImage.rows - cvRound(hist.at<float>(i))),
		cv::Scalar::all(0), -1, 8, 0);

	cv::namedWindow("histogram", 0);
	cv::imshow("histogram", histImage);
	cv::waitKey(10);
}

//Taken from OpenCV's dense optical flow example called "fback" - /sources/samples/cpp/fback.cpp
static void drawOptFlowMap(const cv::Mat& flow, cv::Mat& cflowmap, int step = 1,
	double scaleFactor = 1.0, const cv::Scalar& color = cv::Scalar(0, 255, 0))
{
	for (int y = 0; y < cflowmap.rows; y += step)
		for (int x = 0; x < cflowmap.cols; x += step)
		{
			const cv::Point2f& fxy = flow.at<cv::Point2f>(y, x)*scaleFactor;
			line(cflowmap, cv::Point(x, y), cv::Point(cvRound(x + fxy.x), cvRound(y + fxy.y)),
				color);
			circle(cflowmap, cv::Point(x, y), 1, color, -1);
		}
}