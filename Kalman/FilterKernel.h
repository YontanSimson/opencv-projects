#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <string>

struct SFilterKernel
{
	cv::Mat kernel;
	cv::Point anchor;

	//Default Ctor
	SFilterKernel()
	{
		const int winSize = 5;
		kernel = cv::Mat::ones(winSize, winSize, CV_32F);
		float filtSum = (float)cv::Scalar(sum(kernel))[0];

		//enforce all weights to sum to one
		kernel /= filtSum;
	}

	//Ctor
	SFilterKernel(const int& winSize, const std::string& filterType) :
		anchor(cv::Point(-1, -1))//anchor is at the center
	{
		CV_Assert(winSize % 2 == 1);//Filter size must be odd
		cv::Point anchor = cv::Point(-1, -1);
		cv::Point center = cv::Point(winSize - 1, winSize - 1)*0.5;
		if (!filterType.compare("gaussian"))
		{
			//~exp(-0.5/sigma(x-x0)^2 + -0.5/sigma(y-y0)^2)) - approximate with binomial coefficients
			cv::Mat KernalX(1, winSize, CV_32FC1);
			auto pKernalX = KernalX.ptr<float>(0);
			for (int i = 0; i < winSize; i++, pKernalX++)
			{
				*pKernalX = (float)BinomialCoeff(winSize - 1, i);
			}
			int KernalSum = 1 << (winSize - 1);
			KernalX /= float(KernalSum);

			kernel.create(winSize, winSize, CV_32FC1);
			kernel = KernalX.t()*KernalX;
		}
		else if (!filterType.compare("disk"))
		{
			kernel = cv::Mat::zeros(winSize, winSize, CV_32F);
			circle(kernel, center, (winSize - 1) / 2, cv::Scalar(1), -1, 4);
		}
		else if (!filterType.compare("box")) //- box is the default option
		{
			kernel = cv::Mat::ones(winSize, winSize, CV_32F);
		}


		//enforce all weights to sum to one
		float filtSum = (float)cv::Scalar(sum(kernel))[0];
		kernel /= filtSum;
	}

	void applyfilter2D(const cv::Mat& InIm, cv::Mat& OutIm) const 
	{
		/// Apply filter 
		filter2D(InIm, OutIm, CV_32F, kernel, anchor, 0.0, cv::BORDER_REPLICATE);
	}

	//B(n, k) = n!/(k!*(n-k)!)
	//See: http://rosettacode.org/wiki/Evaluate_binomial_coefficients#C.2B.2B
	double BinomialCoeff(const int& n, const int& k)
	{
		double result;
		if (n == 1)return n;
		result = Factorial(n) / (Factorial(k)*Factorial((n - k)));
		return result;
	}

	double Factorial(double nValue)
	{
		if (nValue == 0.0 || nValue == 1.0)
			return 1.0;
		double result = nValue;
		double result_next;
		double pc = nValue;
		do
		{
			result_next = result*(pc - 1);
			result = result_next;
			pc--;
		} while (pc>2);
		nValue = result;
		return nValue;
	}

	double EvaluateBinomialCoefficient(double nValue, double nValue2)
	{
		double result;
		if (nValue2 == 1)return nValue;
		result = (Factorial(nValue)) / (Factorial(nValue2)*Factorial((nValue - nValue2)));
		nValue2 = result;
		return nValue2;
	}
};