#include "PathGenerator.h"

using namespace std;

void CGeneratePath::ClearPath()
{
	m_path.clear();
}

void CGeneratePath::GenerateStraightPath(cv::Point2f initPos, float length, float step, float alpha)
{
	
	m_path.push_back(initPos);
	cv::Point2f unitDirection(cos(alpha), sin(alpha));
	size_t numOfSteps = round(length / step);
	for (size_t i = 0; i < numOfSteps; i++)
	{
		const cv::Point2f& pt = m_path.back();
		m_path.push_back(pt + step*unitDirection);
	}
}

cv::Mat CGeneratePath::transformPathToImage()
{
	cv::Point2f start(FLT_MAX, FLT_MAX), end(-FLT_MAX, -FLT_MAX);
	for (auto&& it : m_path)
	{
		start.x = min(it.x, start.x);
		end.x   = max(it.x, end.x);
		
		start.y = min(it.y, start.y);
		end.y   = max(it.y, end.y);
	}

	cv::Size imSize(end.x - start.x + 1, end.y - start.y + 1);

	cv::Mat pathIm(imSize, CV_8UC3);
	pathIm.setTo(128);
	auto&& it_prev = m_path.begin();
	for (auto&& it = m_path.begin(); it != m_path.end(); ++it)
	{
		if (it == m_path.begin())
		{
			cv::circle(pathIm, *it, 1, cv::Scalar(255, 0, 0), 1, 8);
		}
		else
		{
			cv::line(pathIm, *it, *it_prev, cv::Scalar(0, 255, 0), 1, 8);
			it_prev++;
		}
		
	}

	return pathIm;
}


CGeneratePath::CGeneratePath()
{
}

CGeneratePath::~CGeneratePath()
{
}