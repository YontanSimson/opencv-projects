#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <cstdint>
#include <algorithm>
#include <vector>
#include <cassert>

static size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

static uint32_t ReadFile(
	const std::string& fileName, 
	std::vector<uint32_t>& value_list, 
	std::vector<uint32_t>& weight_list)
{

	size_t NumOfLines = GetNumberOfLinesInFile(fileName);
	size_t NumOfNodes = NumOfLines - 1;
	uint32_t knapsack_size = 0;

	value_list.resize(NumOfNodes);
	weight_list.resize(NumOfNodes);


	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_lines][number_of_bits] 
		uint32_t number_of_items;
		
		sscanf_s(line.c_str(), "%d %d", &knapsack_size, &number_of_items);
		assert(number_of_items == NumOfNodes);
		int lineIdx = 0;
		for (size_t i = 0; i<number_of_items; ++i, ++lineIdx)  // Enter E undirected edges (u, v, weight)
		{
			getline(inFile, line);
			//convert string to bitset
			sscanf_s(line.c_str(), "%d %d", &value_list[lineIdx], &weight_list[lineIdx]);
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
		return -1;
	}

	return knapsack_size;

}