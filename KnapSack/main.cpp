#include "FileUtiles.h"

#include <iostream>
#include <string>
#include <conio.h>
#include <cstdio>
#include <chrono>
#include <memory>

using namespace std;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

uint32_t KnapSackNaive(
	const uint32_t& total_weight, 
	const std::vector<uint32_t>& value_list,
	const std::vector<uint32_t>& weight_list)
{
	uint32_t number_of_items = value_list.size();

	//Init table
	uint32_t* table = new uint32_t[(total_weight + 1)*(number_of_items + 1)]();
	//set top row to zeros - it is already zero from initialization but this is for clarity
	for (int32_t j = 0; (uint32_t)j < total_weight+1; j++)//loop over possible weights
	{
		table[j] = 0;
	}

	for (int32_t i = 1; (uint32_t)i < number_of_items + 1; ++i)//loop over items
	{
		for (int32_t j = 0; (uint32_t)j < total_weight + 1; j++)//loop over possible weights
		{
			int32_t wi = weight_list[i - 1];
			if (wi > j)
			{
				table[i*(total_weight + 1) + j] = table[(i - 1)*(total_weight + 1) + j];
			}
			else
			{
				table[i*(total_weight + 1) + j] = MAX(table[(i - 1)*(total_weight + 1) + j],
					table[(i - 1)*(total_weight + 1) + j - wi] + value_list[i - 1]);
			}
		}
	}

	uint32_t opt_weight = table[number_of_items*(total_weight + 1) + total_weight];

	////debug print table
	//for (int32_t i = 0; (uint32_t)i < number_of_items + 1; ++i)//loop over items
	//{
	//	for (int32_t j = 0; (uint32_t)j < total_weight + 1; j++)//loop over possible weights
	//	{
	//		cout << table[i*(total_weight + 1) + j] << ",";
	//	}
	//	cout << std::endl;
	//}

	delete[] table;

	return opt_weight;
}

//takes only O(W) space
//The previous version takes o(nW) space
uint32_t KnapSackOpt(
	const uint32_t& total_weight,
	const std::vector<uint32_t>& value_list,
	const std::vector<uint32_t>& weight_list)
{
	uint32_t number_of_items = value_list.size();

	//Init table
	uint32_t* tableCur  = new uint32_t[(total_weight + 1)]();
	uint32_t* tablePrev = new uint32_t[(total_weight + 1)]();

	//set top row to zeros - it is already zero from initialization but this is for clarity
	for (int32_t j = 0; (uint32_t)j < total_weight + 1; j++)//loop over possible weights
	{
		tablePrev[j] = 0;
	}


	for (int32_t i = 1; (uint32_t)i < number_of_items + 1; ++i)//loop over items
	{
		for (int32_t j = 0; (uint32_t)j < total_weight + 1; j++)//loop over possible weights
		{
			int32_t wi = weight_list[i - 1];
			if (wi > j)
			{
				tableCur[j] = tablePrev[j];
			}
			else
			{
				tableCur[j] = MAX(tablePrev[j], tablePrev[j - wi] + value_list[i - 1]);
			}
		}

		std::swap(tableCur, tablePrev);
	}

	std::swap(tableCur, tablePrev);
	uint32_t opt_weight = tableCur[total_weight];
	delete [] tableCur;
	delete[] tablePrev;

	return opt_weight;
}

int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}


	//read Data
	auto t0 = std::chrono::high_resolution_clock::now();

	std::vector<uint32_t> value_list;
	std::vector<uint32_t> weight_list;

	uint32_t total_weight = ReadFile(fileName, value_list, weight_list);
	//timing
	auto t1 = std::chrono::high_resolution_clock::now();

	//Alg
	//uint32_t opt_weight = KnapSackNaive(total_weight, value_list, weight_list);

	uint32_t opt_weight = KnapSackOpt(total_weight, value_list, weight_list);
	//timing
	auto t2 = std::chrono::high_resolution_clock::now();

	cout << "Optimal solution is: " << opt_weight << endl;

	
	std::cout << "**** took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds\n";



	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}