#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <cstdint>

static size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}


int ReadFile(const std::string fileName, float** d)
{
	size_t numOfLines = GetNumberOfLinesInFile(fileName);
	uint32_t numOfCities = 0;
	std::ifstream inFile(fileName.c_str());
	std::string line;

	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_lines][num of edges] 

		
		sscanf_s(line.c_str(), "%d", &numOfCities);
		assert(numOfCities == numOfLines - 1);
		

		*d = new float[2 * numOfCities];
		float* p_d = *d;
		int lineIdx = 0;
		for (size_t i = 0; i<numOfCities; ++i, ++lineIdx, p_d+= 2)  // Enter E undirected edges (u, v, weight)
		{
			getline(inFile, line);
			float x, y;
			sscanf_s(line.c_str(), "%f %f", &x, &y);
			*p_d       = x;
			*(p_d + 1) = y;
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
	}

	return numOfCities;
}
