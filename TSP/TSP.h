#pragma once
#include <cstdint>
#include <vector>
#include <numeric>
#include <iostream>
#include <algorithm>

template< class T > void SafeDelete(T*& pVal)
{
	delete pVal;
	pVal = nullptr;
}

template< class T > void SafeDeleteArray(T*& pVal)
{
	if (pVal != nullptr)
	{
		delete[] pVal;
	}
	pVal = nullptr;
}

inline uint64_t NPermuteK(int n, int k)
{
	uint64_t res = n;

	for (int i = 0; i < k - 1; i++)
	{
		n--;
		res *= n;
	}
	return res;
}

class CTSP
{
public:
	CTSP();
	~CTSP();

	void Init(const float* d, const int& numOfCities);
	float Run();
	void Print();
	void recursive();

	const float* m_d;
	int m_numOfCities;
	int m_src;
private:
	//Functions
	float dist(const float* xy1, const float* xy2);
	
	//Calculate the different combinations for N choose K
		//void NChooseKCombinations(
		//const size_t& n, const size_t& k,
		//std::vector< std::pair<int, std::vector<int> > >& combinations);
	
	void NChooseKCombinations(
		const size_t& n, const size_t& k,
		//std::vector< std::pair<int, std::vector<int> > >& combinations
		std::vector< int >& sets);
	//Calculate the NUMBER OF different combinations for N choose K
	uint64_t NChooseK(const size_t& N, const size_t& K);

	void PermGenerator(int n, int k, std::vector<std::vector<int> >& outputSet);
	float CalcTourCost(const std::vector< std::vector< float > >& cost, const std::vector<int>& tour);

	void GetIndexesInSet(const int& S, std::vector<int>& setIndexes);

	std::vector< std::vector< float > > m_graph;
	//std::vector< std::vector< float > > m_dp;
	std::vector< std::vector< float > > m_A;
	float TSP_rec(int status, int x);
	std::vector<int> TSP_bruteforce(float& minSol);
};

