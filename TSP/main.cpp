//#include "FileUtiles.h"

#include <iostream>
#include <string>
#include <conio.h>
#include <cstdio>
#include <chrono>
#include <memory>
#include <algorithm>    // std::next_permutation

#include "FileUtiles.h"
#include "TSP.h"

using namespace std;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	//read Data
	float* d = NULL;
	int numOfCities = ReadFile(fileName, &d);


	auto t0 = std::chrono::high_resolution_clock::now();

	CTSP tsp;
	tsp.Init(d, numOfCities);

	//tsp.recursive();
	float cost = tsp.Run();
	std::cout << "Held-Karp cost " << cost << std::endl;

	auto t1 = std::chrono::high_resolution_clock::now();
	std::cout << "Alg took "<< std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0).count()
		<< " milliseconds\n";

	SafeDeleteArray(d);
	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}