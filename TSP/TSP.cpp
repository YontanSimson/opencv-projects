#include "TSP.h"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <set>
#include <algorithm>

using namespace std;

CTSP::CTSP():
	m_d(nullptr),
	m_numOfCities(0)
{
}

CTSP::~CTSP()
{
//	SafeDeleteArray(m_2d);
}

void CTSP::Init(const float* d, const int& numOfCities)
{
	m_numOfCities = numOfCities;
	
	//init
	m_src = 0;
	m_graph = vector< vector< float > >(m_numOfCities, vector< float >(m_numOfCities, 0));
	
	m_d = d;
	//Construct matrix to represent distance between cities - from i->j.
	for (int i = 0; i < m_numOfCities; i++)
	{
		for (int j = 0; j < m_numOfCities; j++)
		{
			m_graph[i][j] = dist(m_d + i * 2, m_d + 2 * j);
		}
	} 
	
	m_A.resize(m_numOfCities, vector< float >(1 << m_numOfCities, FLT_MAX));
	//Base case
	m_A[0][1 << 0] = 0;//Has to start from city 0
	for (int i = 1; i < m_numOfCities; ++i)
		m_A[i][1 << i] = FLT_MAX;

}

void CTSP::GetIndexesInSet(const int& S, std::vector<int>& setIndexes)
{
	int idx = 0;
	int mask = 0;
	for (int k = 0; k < m_numOfCities; k++)
	{
		mask = 1 << k;
		if (S & mask)
		{
			setIndexes[idx] = k;
			idx++;
		}
		
	}
}

float CTSP::Run()
{
	
	for (int m = 2; m <= m_numOfCities; m++)
	{
		//first - the cities of the combination encoded in binary format. second - the indexes of the same cities given explicitely in a vector
		//std::vector< std::pair<int, std::vector<int> > > combinations;

		std::vector< int > sets;
		NChooseKCombinations(m_numOfCities, m, sets);

		for (auto& it : sets)
		{
			//j - index of final city
			for (int j = 1; j < m_numOfCities; j++)//j in subset S j!= 0
			{
				int mask_j = 1 << j;
				if (j == 0 || !(mask_j & it))
				{
					continue;
				}
				int BestIdx = -1;
				float minCost = FLT_MAX;
				//k - index of city one before final city
				for (int k = 0; k < m_numOfCities; k++)
				{
					int mask_k = 1 << k;
					if (!(mask_k & it) && k==j )
					{
						continue;
					}
					int status = (1 << j) ^ it;
					float c_kj = m_graph[k][j];
					if (k != j && m_A[k][status] + c_kj < minCost)
					{
						minCost = m_A[k][status] + c_kj;
					}
				}
				m_A[j][it] = minCost;
			}
		}
	}

	float minCost = FLT_MAX;
	int status = (1 << m_numOfCities) - 1;
	for (int j = 1; j < m_numOfCities; ++j)
	{
		float c_j0 = m_graph[j][0];
		if (m_A[j][status] + c_j0 < minCost)
		{
			minCost = m_A[j][status] + c_j0;
		}
	}
	return minCost;
}


//Brute force solution
void CTSP::recursive()
{

	float bf_cost;
	vector<int> tour_bf = TSP_bruteforce(bf_cost);
	cout << "Brute Force Cost: " << bf_cost << endl << "Path: ";
	for (int i = 0; i <= m_numOfCities; i++)
		cout << tour_bf[i % m_numOfCities] << " ";
	cout << endl;
}

//Code from SO: http://stackoverflow.com/questions/9330915/number-of-combinations-n-choose-r-in-c
//Calculate the NUMBER OF different combinations for N choose K
uint64_t CTSP::NChooseK(const size_t& N, const size_t& K)
{
	if (K > N)
	{
		return 0;
	}
	if (K == 0)
	{
		return 1;
	}

	size_t k = K;
	if (K * 2 > N)
	{
		k = N - k;
	}

	int64_t result = N;
	for (int64_t i = 2; i <= (int64_t)K; ++i)
	{
		result *= (N - i + 1);
		result /= i;
	}
	return result;
}


//Calculate the different combinations for N choose K
void CTSP::NChooseKCombinations(
	const size_t& n, const size_t& k,
	//std::vector< std::pair<int, std::vector<int> > >& combinations
	std::vector< int >& sets
	)
{
	std::vector<bool> v(n);
	std::fill(v.begin() + k, v.end(), true);
	uint64_t numOfCombinations = NChooseK(n, k);
	//combinations.resize(unsigned(numOfCombinations), std::pair<int, std::vector<int> >());
	sets.resize(numOfCombinations, 0);
	size_t combIdx = 0;
	do
	{
		//combinations[combIdx].first = 0;
		//combinations[combIdx].second.reserve(k);
		for (size_t i = 0; i < n; ++i)
		{
			if (!v[i])
			{
				//std::cout << (i) << " ";
				//combinations[combIdx].second.push_back((int)i);
				sets[combIdx] |= (1 << i);
			}
		}
		//std::cout << "\n";
		combIdx++;
	} while (std::next_permutation(v.begin(), v.end()));
}

void CTSP::PermGenerator(int n, int k, std::vector<std::vector<int> >& outputSet)
{
	std::vector<int> d(n);
	std::iota(d.begin(), d.end(), 0);//sets begin from 1
	//std::cout << "These are the Possible Combinations: " << std::endl;
	int permIdx = 0;
	uint64_t numOfCombinations = NPermuteK(n - 1, k - 1);//=n!/k!
	//
	outputSet.resize((int)numOfCombinations, std::vector<int>(n));
	do
	{
		outputSet[permIdx] = d;
		for (int i = 0; i < k; i++)
		{
			std::cout << d[i] << " ";
		}
		std::cout << std::endl;
		std::reverse(d.begin() + k, d.end());
		permIdx++;
	} while (next_permutation(d.begin(), d.end()));
}


// TSP recursive
std::vector<int> CTSP::TSP_bruteforce(float& minSol)
{

	std::vector< std::vector<int> > permutations;

	minSol = FLT_MAX;
	std::vector<int> opt_tour;	

	int k = m_numOfCities;
	std::vector<int> d(m_numOfCities);
	std::iota(d.begin(), d.end(), 0);//sets begin from 1
	//int permIdx = 0;
	//uint64_t numOfPermutations = NPermuteK(n, k);//=N!/K!
	do
	{
		//outputSet[permIdx] = d;
		//std::cout << "Tour: ";
		//for (int i = 0; i < k; i++)
		//{
		//	std::cout << d[i] << " ";
		//}
		//std::cout << std::endl;

		float total_cost = CalcTourCost(m_graph, d);
		//cout << "Tour Cost: " << total_cost << std::endl;

		if (total_cost < minSol)
		{
			opt_tour = d;
			minSol = total_cost;
		}
		std::reverse(d.begin() + k, d.end());

		//permIdx++;
	} while (next_permutation(d.begin(), d.end()));

	opt_tour.push_back(0);//add on last hop
	return opt_tour;
}

float CTSP::CalcTourCost(const std::vector< std::vector< float > >& cost, const std::vector<int>& tour)
{
	size_t numOfCities = cost.size();
	float total_cost = 0;
	for (int i = 1; i <= numOfCities; ++i)
	{
		total_cost += cost[tour[i - 1]][tour[i % numOfCities]];
	}
	return total_cost;
}

float CTSP::dist(const float* xy1, const float* xy2)
{
	float dx = xy1[0] - xy2[0];
	float dy = xy1[1] - xy2[1];
	return sqrt(dx*dx + dy*dy);
}


void CTSP::Print()
{
	std::cout << std::fixed;
	std::cout << std::setprecision(2);
	for (int i = 0; i < m_numOfCities; i++)
	{
		for (int j = 0; j < m_numOfCities; j++)
		{
			cout << " " << m_graph[i][j];//m_2d[j*m_numOfCities + i];
		}
		cout << endl;
	}
}


