This is a guide on how to set up your own opencv project from scratch

* Download CMake [https://cmake.org/download]
* Download git [https://git-scm.com/download/win]
* Download tortoise git [https://download.tortoisegit.org/tgit/1.8.16.0/TortoiseGit-1.8.16.0-64bit.msi]
* Open command window
* Type: cd c:\GitRepos
* Type: git clone -b 2.4.10 --single-branch https://github.com/Itseez/opencv.git c:\GitRepos\Opencv-2.4.10\Src    (for the latest branch, leave out -b branch_number)
* Open CMake
* Define the compiler you wish to use (e.g. x86/x64 and Visual studio version)
* Enter in as source directory: c:\GitRepos\Opencv-2.4.10\Src
* Enter as build directory: c:\GitRepos\Opencv-2.4.10\build
* Make sure BUILD_WITH_STATIC_CRT is unchecked (for using dynamic linked runtime in your project - else leave as default)
* Make sure BUILD_SHARED_LIBS is unchecked (for static build (lib) - else for dynamic build (dll) make sure it is checked)
* Click on Configure - until no red lines are left
* Click on Generate
* An OpenCV solution will created in c:\GitRepos\Opencv-2.4.10\build\OpenCV.sln - Open it
* Compile in release and debug modes
* Run python script by typing: python D:\SVN\trunk\Algorithms\Research\Python\opencv_build\make_include_dirs.py c:\GitRepos\Opencv-2.4.10\Src c:\GitRepos\Opencv-2.4.10\build c:\GitRepos\Opencv-2.4.10\opencv
* Now you have a portable and ready opencv project in c:\GitRepos\Opencv-2.4.10\opencv
* Rename environment variable OPENCV_PATH=c:\GitRepos\Opencv-2.4.10
* Open and close your project - then rebuild solution
* For more on how to link and build your own project to the OpenCV libraries [https://bitbucket.org/YontanSimson/opencv-projects/src/08ee8639e81ff78a4d5a11fafdbca6cbcc003b1e/README.md]
* Now you can enter and debug OpenCV functions
