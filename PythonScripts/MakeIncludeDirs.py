#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
@author: Yonatan Simson, simsonyee[at]google's mail
"""

import sys
import distutils.core

import os
import shutil


def copy_file_from_dir_to_dir(src_dir, trg_dir):
    src_files = os.listdir(src_dir)
    for file_name in src_files:
        full_file_name = os.path.join(src_dir, file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, trg_dir)


def make_dirs(new_dir):
    try:
        os.stat(new_dir)
    except:
        os.makedirs(new_dir)


def create_include_dirs(sources_dir, build_dir, target_dir):
 
    #copy contents from sources/include to build/include
    distutils.dir_util.copy_tree(sources_dir + '/include', build_dir + '/include')
    #TODO search for machine type in CMakeCache.txt: i.e. CMAKE_EXE_LINKER_FLAGS:STRING= /machine:x64
    #TODO  i.e. linker type CMAKE_LINKER:FILEPATH=C:/Program Files (x86)/Microsoft Visual Studio 11.0/VC/bin/x86_amd64/link.exe
    #make path for libs and pdb files
    binaries_trg_dir = os.path.join(target_dir, r'x86\vc11\lib')# use vc11\dll if you are doing a dynamic link to dll
    make_dirs(binaries_trg_dir)
    #os.mkdirs(binaries_trg_dir)
    binaries_src_dir_debug   = os.path.join(build_dir, 'lib\Debug')#look in bin\Debug if your are doing a dynamic link
    binaries_src_dir_release = os.path.join(build_dir, 'lib\Release')#look in bin\Release if your are doing a dynamic link

    #debug binaries
    copy_file_from_dir_to_dir(binaries_src_dir_debug,  binaries_trg_dir)
    #release binaries
    copy_file_from_dir_to_dir(binaries_src_dir_release, binaries_trg_dir)

    #Adding 3rdparty binaries
    third_party_dir_release = os.path.join(build_dir, '3rdparty\lib\Release')
    third_party_dir_debug   = os.path.join(build_dir, '3rdparty\lib\Debug')
    copy_file_from_dir_to_dir(third_party_dir_debug,  binaries_trg_dir)
    copy_file_from_dir_to_dir(third_party_dir_release,  binaries_trg_dir)


    ##########################
    #make include folders
    ##########################

    #include opencv
    opencv_include_src = os.path.join(sources_dir, r'include\opencv')
    opencv_include_trg = os.path.join(target_dir, r'include\opencv')
    make_dirs(opencv_include_trg)
    copy_file_from_dir_to_dir(opencv_include_src, opencv_include_trg)

    #include opencv2
    #get list of modules
    module_src_dir = os.path.join(sources_dir, r'modules')
    module_list = [x for x in os.listdir(module_src_dir) if os.path.isdir(os.path.join(module_src_dir, x))]

    module_trg_dir = os.path.join(target_dir, r'include\opencv2')
    for module_name in module_list:
        if module_name in ['java', 'python']:
            continue
        module_src_dir_h = os.path.join(module_src_dir, module_name, 'include\opencv2', module_name)
        if not os.path.isdir(module_src_dir_h):#try looking just in the include
            module_src_dir_h = os.path.join(module_src_dir, module_name, 'include')

        module_trg_dir_h = os.path.join(module_trg_dir, module_name)
        make_dirs(module_trg_dir_h)
        copy_file_from_dir_to_dir(module_src_dir_h, module_trg_dir_h)

    #Finally the opencv.hpp, opencv_modules.hpp -> module_trg_dir
    opencv_hpp_file_path = os.path.join(sources_dir, 'include\opencv2', 'opencv.hpp')
    opencv_modules_hpp_file_path = os.path.join(build_dir, 'opencv2', 'opencv_modules.hpp')

    if os.path.isfile(opencv_hpp_file_path):
            shutil.copy(opencv_hpp_file_path, module_trg_dir)
    else:
        raise ValueError('File not found: ', opencv_hpp_file_path)

    if os.path.isfile(opencv_modules_hpp_file_path):
            shutil.copy(opencv_modules_hpp_file_path, module_trg_dir)
    else:
        raise ValueError('File not found: ', opencv_modules_hpp_file_path)





if __name__ == '__main__':

    sources_dir = sys.argv[1]#as defines in CMake
    build_dir = sys.argv[2]#as defined in CMake
    target_dir = sys.argv[3]
        
    create_include_dirs(sources_dir, build_dir, target_dir)
