#include "Clusters.h"
#include "Utils.h"

CClusters::CClusters()
{
}

CClusters::~CClusters()
{
}

void CClusters::Init(const std::string& fileName)
{
	//assuming node are labeled from 1...m
	int numberOfBits = ReadFile(fileName, m_hashTable);
	m_unionRank.Init(m_hashTable.size());

	//init combinations
	m_combinations.clear();
	NChooseKCombinations(numberOfBits, 1, m_combinations);
	NChooseKCombinations(numberOfBits, 2, m_combinations);

}

std::bitset<24> CClusters::changeSingleComb(std::bitset<24> key, const std::vector<int>& bitsTochange)
{
	 for (auto&& i : bitsTochange) // access by reference
	 {
		 key[i] = !key[i];
	 }
	 return key;
}

void CClusters::Run()
{
	//loop over each key in hash table
	for (auto&& entry : m_hashTable) // access by reference
	{
		//std::cout << entry.first << "-> " << entry.second << std::endl;
		//loop over combinations
		for (auto&& comb : m_combinations) 
		{
			std::bitset<24> changedKey = changeSingleComb(entry.first, comb);
			//Get node if it exists
			auto val = m_hashTable.find(changedKey);
			if ( val != m_hashTable.end() )
			{
				//std::cout << "merging " << val->second << " with " << entry.second <<  std::endl; 
				//If it exists merge in Union Rank
				m_unionRank.merge(entry.second, val->second);
			}
		}
	}
	std::cout << "Number of clusters " << m_unionRank.get_number_of_disjoint_sets() << std::endl;

}

//Code from SO: http://stackoverflow.com/questions/9430568/generating-combinations-in-c
//Calculate the different combinations for N choose K
void CClusters::NChooseKCombinations(const size_t& N, const size_t& K, std::vector< std::vector<int> >& combinations)
{
	std::vector<bool> v(N);
	std::fill(v.begin() + K, v.end(), true);
	
	size_t numberOfCombinations = (size_t)NChooseK(N, K);
	combinations.reserve(numberOfCombinations + combinations.size());


	do 
	{
		std::vector<int> singleComb;
		singleComb.reserve(K);
		for (size_t i = 0; i < N; ++i) 
		{
			if (!v[i]) {
				singleComb.push_back(i);
			}
		}
		combinations.push_back(singleComb);
		singleComb.clear();
	} 
	while (std::next_permutation(v.begin(), v.end()));
}

//Code from SO: http://stackoverflow.com/questions/9330915/number-of-combinations-n-choose-r-in-c
//Calculate the NUMBER OF different combinations for N choose K
uint64_t CClusters::NChooseK(const size_t& N, const size_t& K)
{
	if (K > N)
	{
		return 0;
	}
	if (K == 0)
	{
		return 1;
	}

	size_t k = K;
	if (K * 2 > N)
	{
		k = N-k;
	}

	int64_t result = N;
	for( int64_t i = 2; i <= K; ++i ) 
	{
		result *= (N-i+1);
		result /= i;
	}
	return result;
}