#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include <unordered_map>
#include <bitset>
#include <cstdint>
#include <algorithm>

inline void strip_space(std::string& in)
{
	in.erase(std::remove_if(in.begin(), in.end(), [](std::string::value_type ch){ return (ch == ' '); }), in.end());
}

template<size_t S>
int64_t string_to_size_t(std::string& in)
{
	//strip string of zeros
	strip_space(str);
	std::bitset<S>  x(str);
	int64_t temp = x.to_ullong();
}

template<size_t S>
std::bitset<S> string_to_bit_set(std::string& in)
{
	//strip string of zeros
	strip_space(in);
	std::bitset<S>  x(in);
	return x;
}

static size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

static int ReadFile(const std::string& fileName, std::unordered_map<std::bitset<24>, size_t>& table)
{

	size_t NumOfLines = GetNumberOfLinesInFile(fileName);
	size_t NumOfNodes = NumOfLines - 1;
	int numberOfBits;
	table.reserve(NumOfNodes);
	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_lines][number_of_bits] 
		int dummy;
		sscanf_s(line.c_str(), "%d %d", &dummy, &numberOfBits);

		size_t node = 0; 
		for (size_t i = 0; i<NumOfLines - 1; i++)  
		{
			getline(inFile, line);
			//convert string to bitset
			std::bitset<24> key = string_to_bit_set<24>(line);
			if ( table.find(key) == table.end() )//eliminate identical keys
			{
				table[key] = node++;//table[key] = node
			}
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
	}

	return numberOfBits;

}


