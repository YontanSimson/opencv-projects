#pragma once
#include <string>
#include <unordered_map>
#include <bitset>
#include <vector>
#include <cstdint>

#include "UnionRank.h"


class CClusters
{
public:
	CClusters();
	~CClusters();

	void Init(const std::string& fileName);
	void Run();
	size_t GetNumberOfClusters() const {return m_unionRank.get_number_of_disjoint_sets();}

private:
	//functions
	//Calculate the different combinations for N choose K
	void NChooseKCombinations(const size_t& N, const size_t& K, std::vector< std::vector<int> >& combinations);
	//Calculate the NUMBER OF different combinations for N choose K
	uint64_t NChooseK(const size_t& N, const size_t& K);
	std::bitset<24> changeSingleComb(std::bitset<24> key, const std::vector<int>& bitsTochange);

	//members
	std::unordered_map<std::bitset<24>, size_t> m_hashTable;
	std::vector< std::vector<int> > m_combinations;
	CUnionRank m_unionRank;
};

