//#include <fstream>
#include <iostream>
#include <string>
#include "Clusters.h"


#include <conio.h>
#include <cstdio>
#include <chrono>

using namespace std;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif



int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}



	//read Data
	auto t0 = std::chrono::high_resolution_clock::now();


	//std::unordered_map<std::bitset<24>, size_t> table;
	//ReadFile(fileName, table);

	//timing
	auto t1 = std::chrono::high_resolution_clock::now();

	//Alg
	CClusters clustering;
	clustering.Init(fileName);
	clustering.Run();

	//timing
	auto t2 = std::chrono::high_resolution_clock::now();


	std::cout << "Clustering with Hamming dist took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds\n";

	std::cout << "Number Of Clusters: " << clustering.GetNumberOfClusters() << std::endl;


	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}