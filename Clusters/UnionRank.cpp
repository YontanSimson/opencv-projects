#include "UnionRank.h"
#include <algorithm>
#include <cassert>
#include <stack>

CUnionRank::CUnionRank():
	m_parent(nullptr),
	m_rank(nullptr),
	m_size(0),
	m_number_of_disjoint_sets(0)
{
}

CUnionRank::~CUnionRank()
{
	if (m_rank != nullptr)
		delete[] m_rank;

	if (m_parent != nullptr)
		delete[] m_parent;
}

//Make Set
void CUnionRank::Init(const size_t& size)
{
	m_size = size;
	m_number_of_disjoint_sets = size;
	if (m_rank != nullptr)
		delete[] m_rank;

	if (m_parent != nullptr)
		delete[] m_parent;

	m_rank = new size_t[size]();//all zeros
	m_parent = new size_t[size];

	
	for (size_t i = 0; i < size; i++)
	{
		m_parent[i] = i;
	}
}

//find which set member x belongs to
size_t CUnionRank::find(const size_t& x)
{
	assert(x < m_size);

	std::stack<size_t> chain;
	size_t child = x;

	if (child == m_parent[child])
	{
		return child;
	}
	else
	{
		while (child != m_parent[child])
		{
			chain.push(child);
			child = m_parent[child];
		}

		//path compression
		while (!chain.empty())
		{
			m_parent[chain.top()] = child;
			chain.pop();
		}
		return child;
	}

}

//are two members in the same set
bool CUnionRank::is_connected(const size_t& x, const size_t& y)
{
	size_t xRoot = find(x);
	size_t yRoot = find(y);

	return (xRoot == yRoot);
}

//union of sets X Y
void CUnionRank::merge(const size_t& x, const size_t& y)
{
	size_t xRoot = find(x);
	size_t yRoot = find(y);

	if (xRoot == yRoot)
	{
		return;
	}
	m_number_of_disjoint_sets--;

	// x and y are not already in same set. Merge them.
	if (m_rank[xRoot] < m_rank[yRoot])
	{
		m_parent[xRoot] = yRoot;
	}
	else if (m_rank[xRoot] > m_rank[yRoot])
	{
		m_parent[yRoot] = xRoot;
	}
	else
	{
		m_parent[yRoot] = xRoot;
		m_rank[xRoot]++;
	}
}
