#pragma once
#include <queue>          // std::priority_queue
#include <functional>     // std::greater

class CHeap
{
public:
	CHeap(){}
	~CHeap(){}


	void Init(const int* arr, const size_t& m_arrSize);
	size_t Run();
private:

	const int* m_arr;
	size_t m_arrSize;

	std::priority_queue<int, std::vector<int>, std::less<int>> m_HLow;//Extract Max
	std::priority_queue<int, std::vector<int>, std::greater<int>> m_HHigh;//Extract Min


	int CalcMedian();
};