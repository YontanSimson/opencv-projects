#include <fstream>
#include <iostream>
#include <string>

#include <conio.h>
#include <cstdio>

#include "Utils.h"
#include "Heaps.h"

using namespace std;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	size_t NumOfLines = GetNumberOfLinesInFile(fileName);
	int *pArr = NULL;
	ReadArrayFromFile(fileName, &pArr, (int)NumOfLines);
	
	CHeap medMaint;
	medMaint.Init(pArr, NumOfLines);

	size_t sumOfMedians = medMaint.Run();

	cout << sumOfMedians % 10000 << endl;

	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");

	delete[] pArr;
	return 0;
}