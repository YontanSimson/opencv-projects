#pragma once

#include <fstream>
#include <iostream>
#include <string>

size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}

//Read array from text file and intialize array
static void ReadArrayFromFile(const std::string& fileName, int** ppArr, const int& arrSize)
{
	*ppArr = new int[arrSize]();
	int* pArr = *ppArr;

	std::ifstream inFile(fileName.c_str());
	std::string line;
	int i = 0;
	if (inFile.is_open())
	{
		while (inFile.good() && i < arrSize)
		{
			getline(inFile, line);
			if (line != "")
			{
				*pArr = stoi(line);
				pArr++;
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}