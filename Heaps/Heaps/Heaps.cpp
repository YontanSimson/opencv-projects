#include "Heaps.h"

using namespace std;

void CHeap::Init(const int* arr, const size_t& arrSize)
{
	m_arr = arr;
	m_arrSize = arrSize;
}

int CHeap::CalcMedian()
{
	if (m_HHigh.empty())
	{
		return m_HLow.top();
	}

	if (!m_HLow.empty() && !m_HHigh.empty())
	{
		if (m_HHigh.size() > m_HLow.size())
		{
			return m_HHigh.top();
		}
		else
		{
			return m_HLow.top();
		}
	}

	return -1;//should never be here
}

size_t CHeap::Run()
{
	size_t sumOfMedians = 0;
	const int* pArr = m_arr;
	int med;
	for (size_t i = 0; i < m_arrSize; i++, pArr++)
	{
		//add item to one of the heaps
		if (m_HLow.empty() && m_HHigh.empty())
		{
			m_HLow.push(*pArr);
		}
		else
		{
			int lowHeapTop = m_HLow.top();
			if (*pArr <= lowHeapTop)
			{
				m_HLow.push(*pArr);
			}
			else
			{
				m_HHigh.push(*pArr);
			}
		}

		//Balance the heaps
		if (m_HHigh.size() > m_HLow.size() + 1)
		{
			m_HLow.push(m_HHigh.top());
			m_HHigh.pop();
		}
		if (m_HLow.size() > m_HHigh.size() + 1)
		{
			m_HHigh.push(m_HLow.top());
			m_HLow.pop();
		}

		med = CalcMedian();
		sumOfMedians += med;

	}

	return sumOfMedians;
}
