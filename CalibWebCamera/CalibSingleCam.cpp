#include "CalibSingleCam.h"

int CCalibSingleCam::InitCapture(const char* inputFilename_, const int& cameraId_)
{
	m_cameraId = cameraId_;
	m_inputFilename = inputFilename_;

	if (m_inputFilename)
	{
		if (!m_videofile && readStringList(m_inputFilename, m_imageList))
			m_mode = CAPTURING;
		else
			m_capture.open(m_inputFilename);
	}
	else
		m_capture.open(m_cameraId);

	if (!m_capture.isOpened() && m_imageList.empty())
		return fprintf(stderr, "Could not initialize video (%d) m_capture\n", m_cameraId), -2;

	if (!m_imageList.empty())
		m_nframes = (int)m_imageList.size();

	if (m_capture.isOpened())
		printf("%s", m_liveCaptureHelp);

	return 0;

}


CCalibSingleCam::CCalibSingleCam(
	const Size& boardSize_,
	const Size& imageSize_,
	const float& squareSize_,
	const float& aspectRatio_,
	const char* outputFilename_,
	const int& nframes_,
	const bool& writeExtrinsics_,
	const bool& writePoints_,
	const bool& undistortImage_,
	const int& flags_,
	const bool& flipVertical_,
	const bool& showUndistorted_,
	const bool& videofile_,
	const int& delay_,
	const clock_t& prevTimestamp_,
	const int& mode_,
	const vector<string>& imageList_,
	const Pattern& pattern_) :
	m_boardSize(boardSize_),
	m_imageSize(imageSize_),
	m_squareSize(squareSize_),
	m_aspectRatio(aspectRatio_),
	m_outputFilename(outputFilename_),
	m_nframes(nframes_),
	m_writeExtrinsics(writeExtrinsics_),
	m_writePoints(writePoints_),
	m_undistortImage(undistortImage_),
	m_flags(flags_),
	m_flipVertical(flipVertical_),
	m_showUndistorted(showUndistorted_),
	m_videofile(videofile_),
	m_delay(delay_),
	m_prevTimestamp(prevTimestamp_),
	m_mode(mode_),
	m_imageList(imageList_),
	m_pattern(pattern_)
{

}

CCalibSingleCam::CCalibSingleCam():
	m_boardSize(9, 6),
	m_squareSize(1.f),
	m_aspectRatio(1.f),
	m_outputFilename("out_camera_data.yml"),
	m_inputFilename(NULL),
	m_nframes(10),
	m_writeExtrinsics(true),
	m_writePoints(true),
	m_undistortImage(true),
	m_flags(0),
	m_flipVertical(false),
	m_showUndistorted(false),
	m_videofile(false),
	m_delay(1000),
	m_prevTimestamp(0),
	m_mode(DETECTION),
	m_cameraId(0),
	m_pattern(CHESSBOARD)
{

}


int CCalibSingleCam::run()
{
	namedWindow("Image View", 1);

	for (int i = 0;; i++)
	{
		Mat view, viewGray;
		bool blink = false;

		if (m_capture.isOpened())
		{
			Mat view0;
			m_capture >> view0;
			view0.copyTo(view);
		}
		else if (i < (int)m_imageList.size())
			view = imread(m_imageList[i], 1);

		if (!view.data)
		{
			if (m_imagePoints.size() > 0)
				runAndSave();
			break;
		}

		m_imageSize = view.size();

		if (m_flipVertical)
			flip(view, view, 0);

		vector<Point2f> pointbuf;
		cvtColor(view, viewGray, COLOR_BGR2GRAY);

		bool found;
		switch (m_pattern)
		{
		case CHESSBOARD:
			found = findChessboardCorners(view, m_boardSize, pointbuf,
				CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
			break;
		case CIRCLES_GRID:
			found = findCirclesGrid(view, m_boardSize, pointbuf);
			break;
		case ASYMMETRIC_CIRCLES_GRID:
			found = findCirclesGrid(view, m_boardSize, pointbuf, CALIB_CB_ASYMMETRIC_GRID);
			break;
		default:
			return fprintf(stderr, "Unknown m_pattern type\n"), -1;
		}

		// improve the found corners' coordinate accuracy
		if (m_pattern == CHESSBOARD && found) cornerSubPix(viewGray, pointbuf, Size(11, 11),
			Size(-1, -1), TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

		if (m_mode == CAPTURING && found &&
			(!m_capture.isOpened() || clock() - m_prevTimestamp > m_delay*1e-3*CLOCKS_PER_SEC))
		{
			m_imagePoints.push_back(pointbuf);
			m_prevTimestamp = clock();
			blink = m_capture.isOpened();
		}

		if (found)
			drawChessboardCorners(view, m_boardSize, Mat(pointbuf), found);

		string msg = m_mode == CAPTURING ? "100/100" :
			m_mode == CALIBRATED ? "Calibrated" : "Press 'g' to start";
		int baseLine = 0;
		Size textSize = getTextSize(msg, 1, 1, 1, &baseLine);
		Point textOrigin(view.cols - 2 * textSize.width - 10, view.rows - 2 * baseLine - 10);

		if (m_mode == CAPTURING)
		{
			if (m_undistortImage)
				msg = format("%d/%d Undist", (int)m_imagePoints.size(), m_nframes);
			else
				msg = format("%d/%d", (int)m_imagePoints.size(), m_nframes);
		}

		putText(view, msg, textOrigin, 1, 1,
			m_mode != CALIBRATED ? Scalar(0, 0, 255) : Scalar(0, 255, 0));

		if (blink)
			bitwise_not(view, view);

		if (m_mode == CALIBRATED && m_undistortImage)
		{
			Mat temp = view.clone();
			undistort(temp, view, m_cameraMatrix, m_distCoeffs);
		}

		imshow("Image View", view);
		int key = 0xff & waitKey(m_capture.isOpened() ? 50 : 500);

		if ((key & 255) == 27)
			break;

		if (key == 'u' && m_mode == CALIBRATED)
			m_undistortImage = !m_undistortImage;

		if (m_capture.isOpened() && key == 'g')
		{
			m_mode = CAPTURING;
			m_imagePoints.clear();
		}

		if (m_mode == CAPTURING && m_imagePoints.size() >= (unsigned)m_nframes)
		{
			if (runAndSave())
				m_mode = CALIBRATED;
			else
				m_mode = DETECTION;
			if (!m_capture.isOpened())
				break;
		}
	}

	if (!m_capture.isOpened() && m_showUndistorted)
	{
		Mat view, rview, map1, map2;
		initUndistortRectifyMap(m_cameraMatrix, m_distCoeffs, Mat(),
			getOptimalNewCameraMatrix(m_cameraMatrix, m_distCoeffs, m_imageSize, 1, m_imageSize, 0),
			m_imageSize, CV_16SC2, map1, map2);

		for (int i = 0; i < (int)m_imageList.size(); i++)
		{
			view = imread(m_imageList[i], 1);
			if (!view.data)
				continue;
			//undistort( view, rview, m_cameraMatrix, m_distCoeffs, m_cameraMatrix );
			remap(view, rview, map1, map2, INTER_LINEAR);
			imshow("Image View", rview);
			int c = 0xff & waitKey();
			if ((c & 255) == 27 || c == 'q' || c == 'Q')
				break;
		}
	}

	return 0;
}

double CCalibSingleCam::computeReprojectionErrors(
	const vector<vector<Point3f> >& objectPoints,
	const vector<vector<Point2f> >& imagePoints,
	const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	const Mat& cameraMatrix, const Mat& distCoeffs,
	vector<float>& perViewErrors)
{
	vector<Point2f> imagePoints2;
	int i, totalPoints = 0;
	double totalErr = 0, err;
	perViewErrors.resize(objectPoints.size());

	for (i = 0; i < (int)objectPoints.size(); i++)
	{
		projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i],
			cameraMatrix, distCoeffs, imagePoints2);
		err = norm(Mat(imagePoints[i]), Mat(imagePoints2), CV_L2);
		int n = (int)objectPoints[i].size();
		perViewErrors[i] = (float)std::sqrt(err*err / n);
		totalErr += err*err;
		totalPoints += n;
	}

	return std::sqrt(totalErr / totalPoints);
}

void CCalibSingleCam::calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners, Pattern patternType)
{
	corners.resize(0);

	switch (patternType)
	{
	case CHESSBOARD:
	case CIRCLES_GRID:
		for (int i = 0; i < boardSize.height; i++)
			for (int j = 0; j < boardSize.width; j++)
				corners.push_back(Point3f(float(j*squareSize),
				float(i*squareSize), 0));
		break;

	case ASYMMETRIC_CIRCLES_GRID:
		for (int i = 0; i < boardSize.height; i++)
			for (int j = 0; j < boardSize.width; j++)
				corners.push_back(Point3f(float((2 * j + i % 2)*squareSize),
				float(i*squareSize), 0));
		break;

	default:
		CV_Error(CV_StsBadArg, "Unknown m_pattern type\n");
	}
}

bool CCalibSingleCam::runCalibration(vector<vector<Point2f> >& imagePoints,
	Size imageSize, Size boardSize, Pattern patternType,
	float squareSize, float aspectRatio,
	int flags, Mat& cameraMatrix, Mat& distCoeffs,
	vector<Mat>& rvecs, vector<Mat>& tvecs,
	vector<float>& reprojErrs,
	double& totalAvgErr,
	vector<vector<Point3f> >& objectPoints)
{
	cameraMatrix = Mat::eye(3, 3, CV_64F);
	if (flags & CV_CALIB_FIX_ASPECT_RATIO)
		cameraMatrix.at<double>(0, 0) = aspectRatio;

	distCoeffs = Mat::zeros(8, 1, CV_64F);

	objectPoints.resize(1);
	calcChessboardCorners(boardSize, squareSize, objectPoints[0], patternType);

	objectPoints.resize(imagePoints.size(), objectPoints[0]);

	double rms = calibrateCamera(objectPoints, imagePoints, imageSize, cameraMatrix,
		distCoeffs, rvecs, tvecs, flags | CV_CALIB_FIX_K4 | CV_CALIB_FIX_K5 | CV_CALIB_FIX_K6);
	///*|CV_CALIB_FIX_K3*/|CV_CALIB_FIX_K4|CV_CALIB_FIX_K5);
	printf("RMS error reported by calibrateCamera: %g\n", rms);

	bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);

	totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
		rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

	return ok;
}


void CCalibSingleCam::saveCameraParams(const string& filename,
	Size imageSize, Size boardSize,
	float squareSize, float aspectRatio, int flags,
	const Mat& cameraMatrix, const Mat& distCoeffs,
	const vector<Mat>& rvecs, const vector<Mat>& tvecs,
	const vector<float>& reprojErrs,
	const vector<vector<Point2f> >& imagePoints,
	const vector<vector<Point3f> >& objectPoints,//3D points
	double totalAvgErr)
{
	FileStorage fs(filename, FileStorage::WRITE);

	time_t tt;
	time(&tt);
	struct tm *t2 = localtime(&tt);
	char buf[1024];
	strftime(buf, sizeof(buf)-1, "%c", t2);

	fs << "calibration_time" << buf;

	if (!rvecs.empty() || !reprojErrs.empty())
		fs << "nframes" << (int)std::max(rvecs.size(), reprojErrs.size());
	fs << "image_width" << imageSize.width;
	fs << "image_height" << imageSize.height;
	fs << "board_width" << boardSize.width;
	fs << "board_height" << boardSize.height;
	fs << "square_size" << squareSize;

	if (flags & CV_CALIB_FIX_ASPECT_RATIO)
		fs << "m_aspectRatio" << aspectRatio;

	if (flags != 0)
	{
		sprintf(buf, "flags: %s%s%s%s",
			flags & CV_CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
			flags & CV_CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
			flags & CV_CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
			flags & CV_CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "");
		cvWriteComment(*fs, buf, 0);
	}

	fs << "flags" << flags;

	fs << "camera_matrix" << cameraMatrix;
	fs << "distortion_coefficients" << distCoeffs;

	fs << "avg_reprojection_error" << totalAvgErr;
	if (!reprojErrs.empty())
		fs << "per_view_reprojection_errors" << Mat(reprojErrs);

	if (!rvecs.empty() && !tvecs.empty())
	{
		CV_Assert(rvecs[0].type() == tvecs[0].type());
		Mat bigmat((int)rvecs.size(), 6, rvecs[0].type());
		for (int i = 0; i < (int)rvecs.size(); i++)
		{
			Mat r = bigmat(Range(i, i + 1), Range(0, 3));
			Mat t = bigmat(Range(i, i + 1), Range(3, 6));

			CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
			CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
			//*.t() is MatExpr (not Mat) so we can use assignment operator
			r = rvecs[i].t();
			t = tvecs[i].t();
		}
		cvWriteComment(*fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0);
		fs << "extrinsic_parameters" << bigmat;
	}

	if (!imagePoints.empty())
	{
		Mat imagePtMat((int)imagePoints.size(), (int)imagePoints[0].size(), CV_32FC2);
		for (int i = 0; i < (int)imagePoints.size(); i++)
		{
			Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
			Mat imgpti(imagePoints[i]);
			imgpti.copyTo(r);
		}
		fs << "image_points" << imagePtMat;
	}

	if (!objectPoints.empty())
	{
		Mat_<Point3f> imagePtMat((int)objectPoints.size(), (int)objectPoints[0].size());
		for (int i = 0; i < (int)objectPoints.size(); i++)
		{
			Mat r = imagePtMat.row(i).reshape(3, imagePtMat.cols);
			Mat imgpti(objectPoints[i]);
			imgpti.copyTo(r);
		}
		fs << "object_points" << imagePtMat;
	}
}

bool CCalibSingleCam::readStringList(const string& filename, vector<string>& l)
{
	l.resize(0);
	FileStorage fs(filename, FileStorage::READ);
	if (!fs.isOpened())
		return false;
	FileNode n = fs.getFirstTopLevelNode();
	if (n.type() != FileNode::SEQ)
		return false;
	FileNodeIterator it = n.begin(), it_end = n.end();
	for (; it != it_end; ++it)
		l.push_back((string)*it);
	return true;
}


bool CCalibSingleCam::runAndSave()
{
	vector<Mat> rvecs, tvecs;
	vector<float> reprojErrs;
	vector<vector<Point3f> > objectPoints;//3D points
	double totalAvgErr = 0;

	bool ok = runCalibration(m_imagePoints, m_imageSize, m_boardSize, m_pattern, m_squareSize,
		m_aspectRatio, m_flags, m_cameraMatrix, m_distCoeffs,
		rvecs, tvecs, reprojErrs, totalAvgErr, objectPoints);
	printf("%s. avg reprojection error = %.2f\n",
		ok ? "Calibration succeeded" : "Calibration failed",
		totalAvgErr);

	if (ok)
		saveCameraParams(m_outputFilename, m_imageSize,
		m_boardSize, m_squareSize, m_aspectRatio,
		m_flags, m_cameraMatrix, m_distCoeffs,
		m_writeExtrinsics ? rvecs : vector<Mat>(),
		m_writeExtrinsics ? tvecs : vector<Mat>(),
		m_writeExtrinsics ? reprojErrs : vector<float>(),
		m_writePoints ? m_imagePoints : vector<vector<Point2f> >(),
		m_writePoints ? objectPoints : vector<vector<Point3f> >(),
		totalAvgErr);
	return ok;
}
