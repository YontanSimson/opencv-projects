#pragma once

#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

class CStereoCalib
{
	cv::Mat m_cameraMatrix[2];
	cv::Mat m_distCoeffs[2];
	cv::Mat m_R;
	cv::Mat m_T;
	cv::Mat m_R1;
	cv::Mat m_R2;
	cv::Mat m_P1;
	cv::Mat m_P2;
	cv::Mat m_Q;
	
public:
	CStereoCalib(){}
	void Run(const std::vector<cv::Mat>& imagelist, std::vector<cv::Point2f> pointBuf[2], cv::Size boardSize, const float squareSize, bool useCalibrated = true, bool showRectified = true);

};

