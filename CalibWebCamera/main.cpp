// main.cpp : Defines the entry point for the console application.
//
#define DEBUG

#include <iostream> //std::cout
#include <chrono>  // chrono::system_clock
#include <ctime>   // localtime
#include <sstream> // stringstream
#include <iomanip> // put_time
#include <string>  // string

#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/calib3d/calib3d.hpp"

#include "CalibSingleCam.h"
#include "StereoCalib.h"

using namespace std;
using namespace cv;

/** Function Headers */
void concatenate2Images(const Mat& im1, const Mat& im2, Mat& stackIm);
std::string return_current_time_and_date();
void markKeyPointsOnImage(cv::Mat& image,
	std::vector<cv::KeyPoint> keypoints);

/** Global variables */
static const int NUM_OF_CAMERAS = 2;
static cv::VideoCapture m_cap[NUM_OF_CAMERAS];
static cv::Mat m_frame[NUM_OF_CAMERAS];

//for stacking two frames horizontally
static cv::Rect m_Roi[NUM_OF_CAMERAS];
static cv::Mat m_stackedView;
std::vector<int> m_compression_params;

const char * usage =
" \nexample command line for calibration from a live feed.\n"
"   calibration  -w 9 -h 6 -s 0.025 -o camera.yml -op -oe\n"
" \n"
" example command line for calibration from a list of stored images:\n"
"   imagelist_creator image_list.xml *.png\n"
"   calibration -w 9 -h 6 -s 0.025 -o camera.yml -op -oe image_list.xml\n"
" where image_list.xml is the standard OpenCV XML/YAML\n"
" use imagelist_creator to create the xml or yaml list\n"
" file consisting of the list of strings, e.g.:\n"
" \n"
"<?xml version=\"1.0\"?>\n"
"<opencv_storage>\n"
"<images>\n"
"view000.png\n"
"view001.png\n"
"<!-- view002.png -->\n"
"view003.png\n"
"view010.png\n"
"one_extra_view.jpg\n"
"</images>\n"
"</opencv_storage>\n";

const char* liveCaptureHelp =
"When the live video from camera is used as input, the following hot-keys may be used:\n"
"  <ESC>, 'q' - quit the program\n"
"  'g' - start capturing images\n"
"  'u' - switch undistortion on/off\n";

static void help()
{
	printf("This is a camera calibration sample.\n"
		"Usage: calibration\n"
		"     -w <board_width>         # the number of inner corners per one of board dimension\n"
		"     -h <board_height>        # the number of inner corners per another board dimension\n"
		"     [-pt <pattern>]          # the type of pattern: chessboard or circles' grid\n"
		"     [-n <number_of_frames>]  # the number of frames to use for calibration\n"
		"                              # (if not specified, it will be set to the number\n"
		"                              #  of board views actually available)\n"
		"     [-d <delay>]             # a minimum delay in ms between subsequent attempts to capture a next view\n"
		"                              # (used only for video capturing)\n"
		"     [-s <squareSize>]       # square size in some user-defined units (1 by default)\n"
		"     [-o <out_camera_params>] # the output filename for intrinsic [and extrinsic] parameters\n"
		"     [-op]                    # write detected feature points (image and 3d points)\n"
		"     [-oe]                    # write extrinsic parameters\n"
		"     [-zt]                    # assume zero tangential distortion\n"
		"     [-a <aspectRatio>]      # fix aspect ratio (fx/fy)\n"
		"     [-p]                     # fix the principal point at the center\n"
		"     [-v]                     # flip the captured images around the horizontal axis\n"
		"     [-V]                     # use a video file, and not an image list, uses\n"
		"                              # [input_data] string for the video file name\n"
		"     [-su]                    # show undistorted images after calibration\n"
		"     [input_data]             # input data, one of the following:\n"
		"                              #  - text file with a list of the images of the board\n"
		"                              #    the text file can be generated with imagelist_creator\n"
		"                              #  - name of video file with a video of the board\n"
		"                              # if input_data not specified, a live view from the camera is used\n"
		"\n");
	printf("\n%s", usage);
	printf("\n%s", liveCaptureHelp);
}

/** @function main */
int main(int argc, const char** argv)
{
	
	Size boardSize, imageSize;
	float squareSize = 1.f, aspectRatio = 1.f;
	Mat cameraMatrix, distCoeffs;
	const char* outputFilename = "out_camera_data.yml";
	const char* inputFilename = 0;

	int i, nframes = 10;
	bool writeExtrinsics = false, writePoints = false;
	bool undistortImage = false;
	int flags = 0;
	VideoCapture capture;
	bool flipVertical = false;
	bool showUndistorted = false;
	bool videofile = false;
	int delay = 1000;
	clock_t prevTimestamp = 0;
	int mode = DETECTION;
	int cameraId = 0;
	vector<vector<Point2f> > imagePoints;
	vector<string> imageList;
	Pattern pattern = CHESSBOARD;

	if (argc < 2)
	{
		help();
		return 0;
	}

	for (i = 1; i < argc; i++)
	{
		const char* s = argv[i];
		if (strcmp(s, "-w") == 0)
		{
			if (sscanf(argv[++i], "%u", &boardSize.width) != 1 || boardSize.width <= 0)
				return fprintf(stderr, "Invalid board width\n"), -1;
		}
		else if (strcmp(s, "-h") == 0)
		{
			if (sscanf(argv[++i], "%u", &boardSize.height) != 1 || boardSize.height <= 0)
				return fprintf(stderr, "Invalid board height\n"), -1;
		}
		else if (strcmp(s, "-pt") == 0)
		{
			i++;
			if (!strcmp(argv[i], "circles"))
				pattern = CIRCLES_GRID;
			else if (!strcmp(argv[i], "acircles"))
				pattern = ASYMMETRIC_CIRCLES_GRID;
			else if (!strcmp(argv[i], "chessboard"))
				pattern = CHESSBOARD;
			else
				return fprintf(stderr, "Invalid pattern type: must be chessboard or circles\n"), -1;
		}
		else if (strcmp(s, "-s") == 0)
		{
			if (sscanf(argv[++i], "%f", &squareSize) != 1 || squareSize <= 0)
				return fprintf(stderr, "Invalid board square width\n"), -1;
		}
		else if (strcmp(s, "-n") == 0)
		{
			if (sscanf(argv[++i], "%u", &nframes) != 1 || nframes <= 3)
				return printf("Invalid number of images\n"), -1;
		}
		else if (strcmp(s, "-a") == 0)
		{
			if (sscanf(argv[++i], "%f", &aspectRatio) != 1 || aspectRatio <= 0)
				return printf("Invalid aspect ratio\n"), -1;
			flags |= CV_CALIB_FIX_ASPECT_RATIO;
		}
		else if (strcmp(s, "-d") == 0)
		{
			if (sscanf(argv[++i], "%u", &delay) != 1 || delay <= 0)
				return printf("Invalid delay\n"), -1;
		}
		else if (strcmp(s, "-op") == 0)
		{
			writePoints = true;
		}
		else if (strcmp(s, "-oe") == 0)
		{
			writeExtrinsics = true;
		}
		else if (strcmp(s, "-zt") == 0)
		{
			flags |= CV_CALIB_ZERO_TANGENT_DIST;
		}
		else if (strcmp(s, "-p") == 0)
		{
			flags |= CV_CALIB_FIX_PRINCIPAL_POINT;
		}
		else if (strcmp(s, "-v") == 0)
		{
			flipVertical = true;
		}
		else if (strcmp(s, "-V") == 0)
		{
			videofile = true;
		}
		else if (strcmp(s, "-o") == 0)
		{
			outputFilename = argv[++i];
		}
		else if (strcmp(s, "-su") == 0)
		{
			showUndistorted = true;
		}
		else if (strcmp(s, "-i") == 0)
		{
			++i;
			string str(argv[i]);
			bool isAllDigits = std::all_of(str.begin(), str.end(), ::isdigit);//str.find_first_not_of("0123456789") == std::string::npos
			if (isAllDigits)
				cameraId = std::stoi(str);
			else
				inputFilename = str.c_str();
		}
		else
			return fprintf(stderr, "Unknown option %s", s), -1;
	}

	CCalibSingleCam camCalib(
		boardSize,
		imageSize,
		squareSize,
		aspectRatio,
		outputFilename,
		nframes,
		writeExtrinsics,
		writePoints,
		undistortImage,
		flags,
		flipVertical,
		showUndistorted,
		videofile,
		delay,
		prevTimestamp,
		mode,
		imageList,
		pattern);

	//camCalib.InitCapture(inputFilename, cameraId);
	//camCalib.run();


	cv::Ptr<cv::FeatureDetector> m_detector;

	m_compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
	m_compression_params.push_back(100);

	for (int i = 0; i < NUM_OF_CAMERAS; i++)
	{
		m_cap[i].open(i);
		if (!m_cap[i].isOpened())  // check if we succeeded
			CV_Assert("Can't open Web-Cam");

		//grab frames in order to get input image size 
		bool bSuccess = m_cap[i].read(m_frame[i]); // read a new frame from video

		if (!bSuccess) //if not success, break loop
		{
			std::cout << "Cannot read the frame from video file" << std::endl;
			break;
		}
	}

	//Display grab
	CV_ARE_SIZES_EQ(m_frame + 0, m_frame + 1);

	//for stacking two frames horizontally
	int offsetX = 0;
	for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
	{
		m_Roi[i] = Rect(offsetX, 0, m_frame[i].cols, m_frame[i].rows);
		offsetX += m_frame[i].cols;
	}
	m_stackedView.create(m_frame[0].rows, m_frame[0].cols*NUM_OF_CAMERAS, m_frame[0].type());


	//Display both frames side by side
	namedWindow("3DVision", CV_WINDOW_AUTOSIZE);

	bool needToInit = true;

	Mat gray[NUM_OF_CAMERAS], prevGray[NUM_OF_CAMERAS], image[NUM_OF_CAMERAS];
	vector<Point2f> points[NUM_OF_CAMERAS];
	vector<KeyPoint> keypoints[NUM_OF_CAMERAS];

	double hessianThreshold = 1000;
	int nOctaves = 4;
	int nOctaveLayers = 2;
	bool extended = true;
	bool upright = false;

	int threshold = 30;
	m_detector = new FastFeatureDetector(threshold);//TBD - move to Ctor of Video Grab

	bool isKeyPointDisplayed = false;
	bool isCheckerBoardDisplayed = false;
	bool found[2];

	//for checkerbaord detect
	vector<Point2f> pointBuf[2];
	Size patternsize(9, 6); //interior number of corners

#ifdef DEBUG
	string filename[2] = { "WebCam1_30082014_090134.jpg" ,
						   "WebCam2_30082014_090134.jpg" };

	for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
		m_frame[i] = imread(filename[i]); // get a new frame from camera

	//Convert to gray scale
	for (int i = 0; i < NUM_OF_CAMERAS; i++)
	{
		image[i] = m_frame[i];//m_frame[0].copyTo(image);
		cvtColor(image[i], gray[i], COLOR_BGR2GRAY);
		found[i] = findChessboardCorners(gray[i], patternsize, pointBuf[i],
			CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
	}

	if (found[0] && found[1])
	{
		std::vector<cv::Mat> imagelist;
		////Get time and date of shot
		//string time_str = return_current_time_and_date();

		////save both frames to disk
		//for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
		//{
		//	string filename = "WebCam" + std::to_string(i + 1) + "_" + time_str + ".jpg";
		//	imwrite(filename, m_frame[i], m_compression_params);
		//}

		for (int i = 0; i < NUM_OF_CAMERAS; i++)
		{
			cornerSubPix(gray[i], pointBuf[i], Size(11, 11), Size(-1, -1),
				TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

			imagelist.push_back(gray[0]);
			imagelist.push_back(gray[1]);
		}

		CStereoCalib stereo_calib;
		stereo_calib.Run(imagelist, pointBuf, patternsize, 2.5, true, true);

		for (int i = 0; i < NUM_OF_CAMERAS; i++)
		{
			drawChessboardCorners(gray[i], patternsize, Mat(pointBuf[i]), found);
		}
	}

#else

	//Display stream and detected keypoints
	for (;;)
	{
		//grab frames in order to get input image size 
		for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			m_cap[i] >> m_frame[i]; // get a new frame from camera

		//Convert to gray scale
		for (int i = 0; i < NUM_OF_CAMERAS; i++)
		{
			image[i] = m_frame[i];//m_frame[0].copyTo(image);
			cvtColor(image[i], gray[i], COLOR_BGR2GRAY);
			found[i] = findChessboardCorners(gray[i], patternsize, pointBuf[i],
				CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FAST_CHECK | CV_CALIB_CB_NORMALIZE_IMAGE);
		}



		if (found[0] && found[1])
		{
			////Get time and date of shot
			//string time_str = return_current_time_and_date();

			////save both frames to disk
			//for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			//{
			//	string filename = "WebCam" + std::to_string(i + 1) + "_" + time_str + ".jpg";
			//	imwrite(filename, m_frame[i], m_compression_params);
			//}

			for (int i = 0; i < NUM_OF_CAMERAS; i++)
			{
				cornerSubPix(gray[i], pointBuf[i], Size(11, 11), Size(-1, -1),
					TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 30, 0.1));

				drawChessboardCorners(gray[i], patternsize, Mat(pointBuf[i]), found);
			}

			CStereoCalib stereo_calib;
			std::vector<cv::Mat> imagelist;
			imagelist.push_back(gray[0]);
			imagelist.push_back(gray[1]);
			stereo_calib.Run(imagelist, pointBuf, patternsize, true, true);
		}

		int c = waitKey(10);
		if ((char)c == 'q')//quit
		{
			break;
		}
		else if ((char)c == 'p')//capture images -> save as Jpeg
		{
			//Get time and date of shot
			string time_str = return_current_time_and_date();

			//save both frames to disk
			for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			{
				string filename = "WebCam" + std::to_string(i + 1) + "_" + time_str + ".jpg";
				imwrite(filename, m_frame[i], m_compression_params);
			}
		}
		else if ((char)c == 'k')//show keypoints
		{
			isKeyPointDisplayed = true;
			isCheckerBoardDisplayed = false;
			m_stackedView.create(gray[0].rows, gray[0].cols*NUM_OF_CAMERAS, gray[0].type());
		}
		else if ((char)c == 'c') //color display
		{
			m_stackedView.create(m_frame[0].rows, m_frame[0].cols*NUM_OF_CAMERAS, m_frame[0].type());
			isKeyPointDisplayed = false;
			isCheckerBoardDisplayed = false;
		}
		else if ((char)c == 'h') //checker board display
		{
			isKeyPointDisplayed = false;
			isCheckerBoardDisplayed = true;
			m_stackedView.create(gray[0].rows, gray[0].cols*NUM_OF_CAMERAS, gray[0].type());
		}

		if ( isKeyPointDisplayed )
		{
			//Display both streams stacked horizontally
			for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			{	
				//detect key points
				m_detector->detect(gray[i], keypoints[i]);

				//add keypoints to gray
				markKeyPointsOnImage(gray[i], keypoints[i]);
				gray[i].copyTo(m_stackedView(m_Roi[i]));
			}
		}
		else if ( isCheckerBoardDisplayed )
		{ 
			//Display both streams stacked horizontally
			for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
			{
				gray[i].copyTo(m_stackedView(m_Roi[i]));
			}
		}
		else
		{
			//Display both streams stacked horizontally
			for (size_t i = 0; i < NUM_OF_CAMERAS; i++)
				m_frame[i].copyTo(m_stackedView(m_Roi[i]));
		}
		imshow("3DVision", m_stackedView);

	}

#endif

	return 0;
}

void markKeyPointsOnImage(cv::Mat& image,
	std::vector<cv::KeyPoint> keypoints)
{
	for (auto it_keypoints = keypoints.begin(); it_keypoints != keypoints.end(); ++it_keypoints)
	{
		circle(image, it_keypoints->pt, 3, Scalar(0, 255, 0), -1, 8);
	}
}

std::string return_current_time_and_date()
{
	auto now = std::chrono::system_clock::now();
	auto in_time_t = std::chrono::system_clock::to_time_t(now);

	std::stringstream ss;
	ss << std::put_time(std::localtime(&in_time_t), "%d%m%Y_%I%M%S");
	return ss.str();
}

void concatenate2Images(const Mat& im1, const Mat& im2, Mat& stackIm)
{
	CV_ARE_SIZES_EQ(&im1, &im2);
	CV_Assert(im1.rows == im2.rows);
	CV_Assert(im1.cols == im2.cols);
	CV_Assert(im1.channels() == im2.channels());

	int rows = im1.rows;
	int cols = im1.cols;

	//show both images contcatenated
	stackIm.create(rows, cols * 2, im1.type());

	Rect Roi[2];
	int offsetX = 0;
	for (size_t i = 0; i < 2; i++)
	{
		Roi[i] = Rect(offsetX, 0, cols, rows);
		offsetX += cols;
	}
	im1.copyTo(stackIm(Roi[0]));
	im2.copyTo(stackIm(Roi[1]));
}