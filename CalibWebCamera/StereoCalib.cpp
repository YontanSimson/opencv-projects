#include "StereoCalib.h"
#include "opencv2/contrib/contrib.hpp"

using namespace cv;
using namespace std;

void CStereoCalib::Run(const std::vector<cv::Mat>& imagelist, std::vector<cv::Point2f> pointBuf[2], cv::Size boardSize, const float squareSize, bool useCalibrated, bool showRectified)
{
	if (imagelist.size() % 2 != 0)
	{
		cout << "Error: the image list contains odd (non-even) number of elements\n";
		return;
	}

	bool displayCorners = false;//true;
	// ARRAY AND VECTOR STORAGE:
	vector<vector<Point2f> > imagePoints[2];
	vector<vector<Point3f> > objectPoints;
	Size imageSize = imagelist[0].size();

	int nimages = 1;//number of image pairs
	int i = 0;//single pair

	imagePoints[0].resize(nimages);
	imagePoints[1].resize(nimages);
	vector<cv::Mat> goodImageList;

	goodImageList.push_back(imagelist[0]);
	goodImageList.push_back(imagelist[1]);
	imagePoints[0][0] = pointBuf[0];
	imagePoints[1][0] = pointBuf[1];

	cout << "A pair has been successfully detected.\n";

	imagePoints[0].resize(nimages);
	imagePoints[1].resize(nimages);
	objectPoints.resize(nimages);

	for (int j = 0; j < boardSize.height; j++)
		for (int k = 0; k < boardSize.width; k++)
			objectPoints[i].push_back(Point3f(j*squareSize, k*squareSize, 0));

	cout << "Running stereo calibration ...\n";

	
	m_cameraMatrix[0] = Mat::eye(3, 3, CV_64F);
	m_cameraMatrix[1] = Mat::eye(3, 3, CV_64F);
	Mat E, F;

	double rms = stereoCalibrate(objectPoints, imagePoints[0], imagePoints[1],
		m_cameraMatrix[0], m_distCoeffs[0],
		m_cameraMatrix[1], m_distCoeffs[1],
		imageSize, m_R, m_T, E, F,
		TermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 100, 1e-5),
		CV_CALIB_FIX_ASPECT_RATIO +
		CV_CALIB_ZERO_TANGENT_DIST +
		CV_CALIB_SAME_FOCAL_LENGTH +
		CV_CALIB_RATIONAL_MODEL +
		CV_CALIB_FIX_K3 + CV_CALIB_FIX_K4 + CV_CALIB_FIX_K5);
	cout << "done with RMS error=" << rms << endl;

	// CALIBRATION QUALITY CHECK
	// because the output fundamental matrix implicitly
	// includes all the output information,
	// we can check the quality of calibration using the
	// epipolar geometry constraint: m2^t*F*m1=0
	double err = 0;
	int npoints = 0;
	vector<Vec3f> lines[2];
	int npt = (int)imagePoints[0][i].size();
	Mat imgpt[2];
	for (int k = 0; k < 2; k++)
	{
		imgpt[k] = Mat(imagePoints[k][i]);
		undistortPoints(imgpt[k], imgpt[k], m_cameraMatrix[k], m_distCoeffs[k], Mat(), m_cameraMatrix[k]);
		computeCorrespondEpilines(imgpt[k], k + 1, F, lines[k]);
	}
	for (int j = 0; j < npt; j++)
	{
		double errij = fabs(imagePoints[0][i][j].x*lines[1][j][0] +
			imagePoints[0][i][j].y*lines[1][j][1] + lines[1][j][2]) +
			fabs(imagePoints[1][i][j].x*lines[0][j][0] +
			imagePoints[1][i][j].y*lines[0][j][1] + lines[0][j][2]);
		err += errij;
	}
	npoints += npt;

	cout << "average reprojection err = " << err / npoints << endl;

	// save intrinsic parameters
	FileStorage fs("intrinsics.yml", CV_STORAGE_WRITE);
	if (fs.isOpened())
	{
		fs << "M1" << m_cameraMatrix[0] << "D1" << m_distCoeffs[0] <<
			"M2" << m_cameraMatrix[1] << "D2" << m_distCoeffs[1];
		fs.release();
	}
	else
		cout << "Error: can not save the intrinsic parameters\n";

	Rect validRoi[2];

	stereoRectify(m_cameraMatrix[0], m_distCoeffs[0],
		m_cameraMatrix[1], m_distCoeffs[1],
		imageSize, m_R, m_T, m_R1, m_R2, m_P1, m_P2, m_Q,
		CALIB_ZERO_DISPARITY, 1, imageSize, &validRoi[0], &validRoi[1]);

	fs.open("extrinsics.yml", CV_STORAGE_WRITE);
	if (fs.isOpened())
	{
		fs << "R" << m_R << "T" << m_T << "R1" << m_R1 << "R2" << m_R2 << "P1" << m_P1 << "P2" << m_P2 << "Q" << m_Q;
		fs.release();
	}
	else
		cout << "Error: can not save the intrinsic parameters\n";

	// OpenCV can handle left-right
	// or up-down camera arrangements
	bool isVerticalStereo = fabs(m_P2.at<double>(1, 3)) > fabs(m_P2.at<double>(0, 3));

	// COMPUTE AND DISPLAY RECTIFICATION
	if (!showRectified)
		return;

	Mat rmap[2][2];
	// IF BY CALIBRATED (BOUGUET'S METHOD)
	if (useCalibrated)
	{
		// we already computed everything
	}
	// OR ELSE HARTLEY'S METHOD
	else
		// use intrinsic parameters of each camera, but
		// compute the rectification transformation directly
		// from the fundamental matrix
	{
		vector<Point2f> allimgpt[2];
		for (int k = 0; k < 2; k++)
		{
			std::copy(imagePoints[k][i].begin(), imagePoints[k][i].end(), back_inserter(allimgpt[k]));
		}
		F = findFundamentalMat(Mat(allimgpt[0]), Mat(allimgpt[1]), FM_8POINT, 0, 0);
		Mat H1, H2;
		stereoRectifyUncalibrated(Mat(allimgpt[0]), Mat(allimgpt[1]), F, imageSize, H1, H2, 3);

		m_R1 = m_cameraMatrix[0].inv()*H1*m_cameraMatrix[0];
		m_R2 = m_cameraMatrix[1].inv()*H2*m_cameraMatrix[1];
		m_P1 = m_cameraMatrix[0];
		m_P2 = m_cameraMatrix[1];
	}

	//Precompute maps for cv::remap()
	initUndistortRectifyMap(m_cameraMatrix[0], m_distCoeffs[0], m_R1, m_P1, imageSize, CV_16SC2, rmap[0][0], rmap[0][1]);
	initUndistortRectifyMap(m_cameraMatrix[1], m_distCoeffs[1], m_R2, m_P2, imageSize, CV_16SC2, rmap[1][0], rmap[1][1]);

	Mat canvas;
	double sf;
	int w, h;
	if (!isVerticalStereo)
	{
		sf = 600. / MAX(imageSize.width, imageSize.height);
		w = cvRound(imageSize.width*sf);
		h = cvRound(imageSize.height*sf);
		canvas.create(h, w * 2, CV_8UC3);
	}
	else
	{
		sf = 300. / MAX(imageSize.width, imageSize.height);
		w = cvRound(imageSize.width*sf);
		h = cvRound(imageSize.height*sf);
		canvas.create(h * 2, w, CV_8UC3);
	}

	for (int k = 0; k < 2; k++)
	{
		Mat img = goodImageList[i * 2 + k], rimg, cimg;
		remap(img, rimg, rmap[k][0], rmap[k][1], CV_INTER_LINEAR);
		cvtColor(rimg, cimg, COLOR_GRAY2BGR);
		Mat canvasPart = !isVerticalStereo ? canvas(Rect(w*k, 0, w, h)) : canvas(Rect(0, h*k, w, h));
		resize(cimg, canvasPart, canvasPart.size(), 0, 0, CV_INTER_AREA);
		if (useCalibrated)
		{
			Rect vroi(cvRound(validRoi[k].x*sf), cvRound(validRoi[k].y*sf),
				cvRound(validRoi[k].width*sf), cvRound(validRoi[k].height*sf));
			rectangle(canvasPart, vroi, Scalar(0, 0, 255), 3, 8);
		}
	}

	if (!isVerticalStereo)
		for (int j = 0; j < canvas.rows; j += 16)
			line(canvas, Point(0, j), Point(canvas.cols, j), Scalar(0, 255, 0), 1, 8);
	else
		for (int j = 0; j < canvas.cols; j += 16)
			line(canvas, Point(j, 0), Point(j, canvas.rows), Scalar(0, 255, 0), 1, 8);
	imshow("rectified", canvas);
	char c = (char)waitKey(1);
	//if (c == 27 || c == 'q' || c == 'Q')
	//	break;

	//Now create a depth map
	{
		Mat img1 = goodImageList[0], img2 = goodImageList[1], rimg, cimg;

		float scale = 1.0f;
		Mat M1, D1, M2, D2;
		M1 = m_cameraMatrix[0];
		D1 = m_distCoeffs[0];
		M2 = m_cameraMatrix[1];
		D2 = m_distCoeffs[1];
		M1 *= scale;
		M2 *= scale;


		//m_R, m_T, m_R1, m_R2, m_P1, m_P2
		Mat R = m_R, T = m_T, R1 = m_R1, P1 = m_P1, R2 = m_R2, P2 = m_P2, Q;

		Rect roi1, roi2;
		stereoRectify(M1, D1, M2, D2, imageSize, R, T, R1, R2, P1, P2, Q, CALIB_ZERO_DISPARITY, -1, imageSize, &roi1, &roi2);

		Mat map11, map12, map21, map22;
		initUndistortRectifyMap(M1, D1, R1, P1, imageSize, CV_16SC2, map11, map12);
		initUndistortRectifyMap(M2, D2, R2, P2, imageSize, CV_16SC2, map21, map22);

		Mat img1r, img2r;
		remap(img1, img1r, map11, map12, INTER_LINEAR);
		remap(img2, img2r, map21, map22, INTER_LINEAR);

		imshow("img1 - before distortion correction", img1);
		moveWindow("img1 - before distortion correction", 0, 0);
		waitKey(5);
		imshow("img1 - after distortion correction", img1r);
		moveWindow("img1 - after distortion correction", 640, 0);
		waitKey(5);

		img1 = img1r;
		img2 = img2r;

		enum { STEREO_BM = 0, STEREO_SGBM = 1, STEREO_HH = 2, STEREO_VAR = 3 };
		int alg = STEREO_SGBM;
		int SADWindowSize = 7, numberOfDisparities = 0;
		numberOfDisparities = 16;// numberOfDisparities > 0 ? numberOfDisparities : ((imageSize.width / 8) + 15) & -16;
		bool no_display = false;

		StereoBM bm;
		StereoSGBM sgbm;
		StereoVar var;

		bm.state->roi1 = roi1;
		bm.state->roi2 = roi2;
		bm.state->preFilterCap = 31;
		bm.state->SADWindowSize = SADWindowSize > 0 ? SADWindowSize : 9;
		bm.state->minDisparity = 0;
		bm.state->numberOfDisparities = numberOfDisparities;
		bm.state->textureThreshold = 10;
		bm.state->uniquenessRatio = 15;
		bm.state->speckleWindowSize = 100;
		bm.state->speckleRange = 32;
		bm.state->disp12MaxDiff = 1;

		sgbm.preFilterCap = 63;
		sgbm.SADWindowSize = SADWindowSize > 0 ? SADWindowSize : 3;

		int cn = img1.channels();

		sgbm.P1 = 8 * cn*sgbm.SADWindowSize*sgbm.SADWindowSize;
		sgbm.P2 = 32 * cn*sgbm.SADWindowSize*sgbm.SADWindowSize;
		sgbm.minDisparity = 0;
		sgbm.numberOfDisparities = numberOfDisparities;
		sgbm.uniquenessRatio = 10;
		sgbm.speckleWindowSize = bm.state->speckleWindowSize;
		sgbm.speckleRange = bm.state->speckleRange;
		sgbm.disp12MaxDiff = 1;
		sgbm.fullDP = alg == STEREO_HH;

		var.levels = 3;                                 // ignored with USE_AUTO_PARAMS
		var.pyrScale = 0.5;                             // ignored with USE_AUTO_PARAMS
		var.nIt = 25;
		var.minDisp = -numberOfDisparities;
		var.maxDisp = 0;
		var.poly_n = 3;
		var.poly_sigma = 0.0;
		var.fi = 15.0f;
		var.lambda = 0.03f;
		var.penalization = var.PENALIZATION_TICHONOV;   // ignored with USE_AUTO_PARAMS
		var.cycle = var.CYCLE_V;                        // ignored with USE_AUTO_PARAMS
		var.flags = var.USE_SMART_ID | var.USE_AUTO_PARAMS | var.USE_INITIAL_DISPARITY | var.USE_MEDIAN_FILTERING;

		Mat disp, disp8;

		int64 t = getTickCount();
		if (alg == STEREO_BM)
			bm(img1, img2, disp);
		else if (alg == STEREO_VAR) {
			var(img1, img2, disp);
		}
		else if (alg == STEREO_SGBM || alg == STEREO_HH)
			sgbm(img1, img2, disp);
		t = getTickCount() - t;
		printf("Time elapsed: %fms\n", t * 1000 / getTickFrequency());

		//disp = dispp.colRange(numberOfDisparities, img1p.cols);
		if (alg != STEREO_VAR)
			disp.convertTo(disp8, CV_8U, 255 / (numberOfDisparities*16.));
		else
			disp.convertTo(disp8, CV_8U);
		if (!no_display)
		{
			namedWindow("left", 1);
			imshow("left", img1);
			namedWindow("right", 1);
			imshow("right", img2);
			namedWindow("disparity", 0);
			imshow("disparity", disp8);
			printf("press any key to continue...");
			fflush(stdout);
			waitKey(1);
			printf("\n");
		}

		printf("storing the point cloud...");
		fflush(stdout);
		Mat xyz;
		reprojectImageTo3D(disp, xyz, Q, true);
		printf("\n");
	}

	//img1 = img1r;
	//img2 = img2r;

}


