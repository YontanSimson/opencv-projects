#pragma once

#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/highgui/highgui.hpp"

#include <cctype>
#include <stdio.h>
#include <string.h>
#include <time.h>

using namespace cv;
using namespace std;

enum Pattern { CHESSBOARD, CIRCLES_GRID, ASYMMETRIC_CIRCLES_GRID };
enum { DETECTION = 0, CAPTURING = 1, CALIBRATED = 2 };

class CCalibSingleCam{
	const char* m_liveCaptureHelp =
		"When the live video from camera is used as input, the following hot-keys may be used:\n"
		"  <ESC>, 'q' - quit the program\n"
		"  'g' - start capturing images\n"
		"  'u' - switch undistortion on/off\n";

	Size m_boardSize, m_imageSize;
	float m_squareSize, m_aspectRatio;
	Mat m_cameraMatrix, m_distCoeffs;
	const char* m_outputFilename;
	const char* m_inputFilename;

	int m_nframes;
	bool m_writeExtrinsics, m_writePoints;
	bool m_undistortImage;
	int m_flags;

	bool m_flipVertical;
	bool m_showUndistorted;
	bool m_videofile;
	int m_delay;
	clock_t m_prevTimestamp;
	int m_mode;

	vector<vector<Point2f> > m_imagePoints;
	vector<string> m_imageList;
	Pattern m_pattern;

	//Video input
	int m_cameraId;
	VideoCapture m_capture;

private:
	//x_hat=PX, projection error = (x - x_hat)^2
	double computeReprojectionErrors(
		const vector<vector<Point3f> >& objectPoints,
		const vector<vector<Point2f> >& imagePoints,
		const vector<Mat>& rvecs, const vector<Mat>& tvecs,
		const Mat& cameraMatrix, const Mat& distCoeffs,
		vector<float>& perViewErrors);

	//Calc 3D points of Chessboard
	void calcChessboardCorners(Size boardSize, float squareSize, vector<Point3f>& corners, Pattern patternType = CHESSBOARD);

	//Calculate intrinsic and extrinsic parameters
	bool runCalibration(vector<vector<Point2f> >& imagePoints,
		Size imageSize, Size boardSize, Pattern patternType,
		float squareSize, float aspectRatio,
		int flags, Mat& cameraMatrix, Mat& distCoeffs,
		vector<Mat>& rvecs, vector<Mat>& tvecs,
		vector<float>& reprojErrs,
		double& totalAvgErr,
		vector<vector<Point3f> >& objectPoints);

	void saveCameraParams(const string& filename,
		Size imageSize, Size boardSize,
		float squareSize, float aspectRatio, int flags,
		const Mat& cameraMatrix, const Mat& distCoeffs,
		const vector<Mat>& rvecs, const vector<Mat>& tvecs,
		const vector<float>& reprojErrs,
		const vector<vector<Point2f> >& imagePoints, 
		const vector<vector<Point3f> >& objectPoints,//3D points
		double totalAvgErr);

	bool readStringList(const string& filename, vector<string>& l);

	bool runAndSave();

public:
	//Default Ctor
	CCalibSingleCam();

	//Ctor
	CCalibSingleCam(
		const Size& boardSize_,
		const Size& imageSize_,
		const float& squareSize_,
		const float& aspectRatio_,
		const char* outputFilename_,
		const int& nframes_,
		const bool& writeExtrinsics_,
		const bool& writePoints_,
		const bool& undistortImage_,
		const int& flags_,
		const bool& flipVertical_,
		const bool& showUndistorted_,
		const bool& videofile_,
		const int& delay_,
		const clock_t& prevTimestamp_,
		const int& mode_,
		const vector<string>& imageList_,
		const Pattern& pattern_);

	//Init Capture from file or webcam (inputFilename_ == NULL -> init from webcam)
	int InitCapture(const char* inputFilename_, const int& cameraId_);

	//Start Calibration
	int run();

	//SetGet
	void set_imageSize(const Size& imageSize_){ m_imageSize = imageSize_; }

};

