// SchedulingOptimization.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Utils.h"
#include "SchedulingOptimization.h"

template<class T>
struct greater {
	bool operator() (const T& lhs, const T& rhs) const
	{
		return lhs>rhs;
	}
};

template<class T>
struct pair_greater {
	bool operator() (const T& lhs, const T& rhs) const
	{
		const int64_t &lhs_key = std::get<0>(lhs), &rhs_key = std::get<0>(rhs);
		const int &lhs_w = std::get<1>(lhs), &rhs_w = std::get<1>(rhs);
		if (lhs_key > rhs_key)
		{
			return true;
		}
		else if (lhs_key == rhs_key)//tie breaker
		{
			return lhs_w > rhs_w;
		}
		else
		{
			return false;
		}
	}
};


void CSchedulingOptimization::ReadDataFromFile(const std::string& fileName)
{
	std::ifstream inFile(fileName.c_str());
	std::string line;
	size_t i = 0;
	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_jobs]
		m_number_of_jobs = std::stoi(line);

		m_weights = new int[m_number_of_jobs];
		m_lengths = new int[m_number_of_jobs];
		while (inFile.good())
		{
			getline(inFile, line);//[job_i_weight] [job_i_length]
			if (line != "")
			{
				std::vector<int> weight_and_length;
				split(line, ' ', weight_and_length);
				m_weights[i] = weight_and_length[0];
				m_lengths[i] = weight_and_length[1];
				i++;
			}
			
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
	}
}


CSchedulingOptimization::CSchedulingOptimization():
	m_weights(nullptr),
	m_lengths(nullptr),
	m_number_of_jobs(0)
{
}

CSchedulingOptimization::~CSchedulingOptimization()
{
	if (m_weights != nullptr)
		delete[] m_weights;
	m_weights = nullptr;

	if (m_lengths != nullptr)
		delete[] m_lengths;
	m_lengths = nullptr;
}

void CSchedulingOptimization::Init(const std::string& fileName)
{
	ReadDataFromFile(fileName);
}

void CSchedulingOptimization::Run(bool isDivision = true)
{
	assert(m_weights != nullptr && m_lengths != nullptr);

	if (!isDivision)
	{
		//insert into binary tree - allow non unique keys with multi map
		//std::multimap<int, std::pair<int, int>, greater<int>> jobs;
		//for (size_t i = 0; i < (size_t)m_number_of_jobs; i++)
		//{//TBD break ties with higher weight first
		//	jobs.insert( std::make_pair(m_weights[i] - m_lengths[i], std::make_pair(m_weights[i], m_lengths[i])) );
		//}
		//init list of pairs
		std::vector< std::tuple<int64_t, int, int> > tuple_list((size_t)m_number_of_jobs);
		for (size_t i = 0; i < (size_t)m_number_of_jobs; i++)
		{
			tuple_list[i] = std::make_tuple(m_weights[i] - m_lengths[i], m_weights[i], m_lengths[i]);
		}

		//sort list
		std::sort(tuple_list.begin(), tuple_list.end(), pair_greater<std::tuple<int64_t, int, int> >());

		//compute cost
		int64_t cost = 0;
		int64_t accumulated_length = 0;
		const int* p_weight = m_weights;
		for (auto&& it : tuple_list) // access by reference
		{
			//accumulated_length += it.second.second;
			//cost += (it.second.first)*accumulated_length;//wj*Cj
			accumulated_length += std::get<2>(it);
			cost += std::get<1>(it)*accumulated_length;//wj*Cj
		}
		std::cout << "Non Optimal Cost is: " << cost << std::endl;
	}
	else
	{
		//insert into binary tree - allow non unique keys with multi map
		std::multimap<double, std::pair<int, int>, greater<double>> jobs;
		for (size_t i = 0; i < (size_t)m_number_of_jobs; i++)
		{
			jobs.insert(std::make_pair(m_weights[i]/double(m_lengths[i]), std::make_pair(m_weights[i], m_lengths[i])));
		}

		//compute cost
		int64_t cost = 0;
		int64_t accumulated_length = 0;
		const int* p_weight = m_weights;
		for (auto&& it : jobs) // access by reference
		{
			accumulated_length += it.second.second;
			cost += (it.second.first)*accumulated_length;//wj*Cj
		}
		std::cout << "Optimal Cost is: " << cost << std::endl;
	}
}

