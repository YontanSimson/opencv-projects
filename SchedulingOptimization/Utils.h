#pragma once
#include "stdafx.h"

void split(const std::string &s, const char delim, std::vector<int>& elems)
{
	elems.clear();
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(stoi(item));
	}
}