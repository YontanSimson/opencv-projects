#include "stdafx.h"

class CSchedulingOptimization
{
public:
	CSchedulingOptimization();
	~CSchedulingOptimization();

	void Init(const std::string& fileName);
	void Run(bool isDivision);
private:
	void ReadDataFromFile(const std::string& fileName);

	int* m_weights;
	int* m_lengths;
	int m_number_of_jobs;

};

