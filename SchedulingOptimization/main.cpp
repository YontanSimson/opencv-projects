#include "stdafx.h"
#include "SchedulingOptimization.h"
#include <fstream>
#include <iostream>
#include <string>

#include <conio.h> //_getch
#include <cstdio> //printf
#include <chrono> //std::chrono::high_resolution_clock::now



using namespace std;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string fileName;
	bool isDivisionOrdered = false;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	CSchedulingOptimization opt;
	opt.Init(fileName);

	//timing
	auto t1 = std::chrono::high_resolution_clock::now();

	// Alg
	opt.Run(false);

	//timing
	auto t2 = std::chrono::high_resolution_clock::now();
	std::cout << "NonOptimaized Scheduling Optimization took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds\n";

	//timing
	auto t3 = std::chrono::high_resolution_clock::now();

	// Alg
	opt.Run(true);

	//timing
	auto t4 = std::chrono::high_resolution_clock::now();
	std::cout << "Optimaized Scheduling Optimization took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
		<< " milliseconds\n";


	cout << "Press any key" << endl;
	int c = _getch();
	if (c)
		cout << "A key is pressed from keyboard" << std::endl;
	else
		cout << "An error occurred" << std::endl;
	return 0;
}