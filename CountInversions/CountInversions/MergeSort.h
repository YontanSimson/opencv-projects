#pragma once

#include "stdafx.h"

typedef unsigned long long uint64;

class CMergeSort
{
public:
	CMergeSort() :m_pArr(NULL), m_arrSize(0), m_pTemp(NULL){}
	~CMergeSort();

	void Init(int* pArr, const int arrSize);
	uint64 Run();
	uint64 GetNumOfInversions() const { return m_numOfInversions; }
protected:

	uint64 merge_and_count_split_inv(int* arr, const int& arrSize);
	uint64 count_and_sort(int* arr, const int& arrSize);

	int* m_pArr;
	int* m_pTemp;
	int m_arrSize;
	uint64 m_numOfInversions;

};