// CountInversions.cpp 
//
#include <algorithm>    // std::swap
#include "MergeSort.h"

using namespace std;

CMergeSort::~CMergeSort()
{
	if (m_pTemp != NULL)
	{
		delete[] m_pTemp;
		m_pTemp = NULL;
	}
}

void CMergeSort::Init(int* pArr, const int arrSize)
{
	m_pArr = pArr;
	m_arrSize = arrSize;

	if (m_pTemp == NULL)
	{
		m_pTemp = new int[arrSize]();
	}
	else
	{
		delete[] m_pTemp;
		m_pTemp = new int[arrSize]();
	}
}

uint64 CMergeSort::Run()
{
	m_numOfInversions = count_and_sort(m_pArr, m_arrSize);
	return m_numOfInversions;
}


uint64 CMergeSort::count_and_sort(int* arr, const int& arrSize)
{
	if (arrSize == 1)
	{
		return 0;
	}
	else if (arrSize == 2)
	{
		if (arr[0] > arr[1])
		{
			std::swap(arr[0], arr[1]);
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		//left side
		uint64 x = count_and_sort(arr, arrSize / 2);
		//right side
		uint64 y = count_and_sort(arr + arrSize / 2, arrSize - arrSize / 2);
		//merge
		uint64 z = merge_and_count_split_inv(arr, arrSize);
		return x + y + z;
	}
}

uint64 CMergeSort::merge_and_count_split_inv(int* arr, const int& arrSize)
{

	memcpy(m_pTemp, arr, arrSize*sizeof(int));
	int leftLength  = arrSize / 2;
	int rightLength = arrSize - arrSize / 2;

	int *leftArray = m_pTemp;
	int *rightArray = m_pTemp + leftLength;

	int i = 0;
	int j = 0;
	uint64 splitInversions = 0;
	for (int k = 0; k < arrSize; k++)
	{
		if ((leftArray[i] < rightArray[j] && i < leftLength) || j >= rightLength)
		{
			arr[k] = leftArray[i];
			i++;
		}
		else//leftArray[i] >= rightArray[j]
		{
			arr[k] = rightArray[j];
			j++;
			splitInversions += (leftLength - i);
		}
	}

	return splitInversions;
}