#pragma once
#include "stdafx.h"

typedef unsigned long long uint64;

enum EPivotChoice
{
	FIRST = 0, 
	RANDOM = 1,
	LAST = 2,
	MEDIAN_OF_THREE = 3,
	NUM_OF_PIVOT_CHOICES
};

template<class T>
T triple(const T& a, const T& b, const int& c)
{
	T mx = max(max(a, b), c);
	T mn = min(min(a, b), c);
	T md = a^b^c^mx^mn;
	return md;
}

//returns the index for median of three
inline int median_of_three(int* arr, const int& arrSize)
{
	int a = arr[0];
	int b = arr[(arrSize - 1) / 2];
	int c = arr[arrSize - 1];

	if (a > b) 
	{
		if (b > c) 
		{
			return (arrSize - 1) / 2;
		}
		else if (a > c) 
		{
			return arrSize - 1;
		}
		else 
		{
			return 0;
		}
	}
	else {
		if (a > c) 
		{
			return 0;
		}
		else if (b > c) 
		{
			return arrSize - 1;
		}
		else 
		{
			return (arrSize - 1) / 2;
		}
	}

}

class CQuickSort
{
public:
	CQuickSort() :m_pArr(NULL), m_arrSize(0){}
	~CQuickSort();

	void Init(int* pArr, const int arrSize, EPivotChoice pivotType);
	uint64 Run();
	uint64 GetNumOfInversions() const { return m_numOfInversions; }
protected:

	uint64 count_and_sort(int* arr, const int& arrSize);
	int choose_pivot(int* arr, const int& arrSize);
	int partition_around_pivot(int* arr, const int& arrSize, const int& p);

	int* m_pArr;
	//int* m_pTemp;
	int m_arrSize;
	uint64 m_numOfInversions;
	EPivotChoice m_pivotType;

};