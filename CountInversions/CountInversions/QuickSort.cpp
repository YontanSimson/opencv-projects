// CountInversions.cpp 
//
#include <algorithm>    // std::swap
#include "QuickSort.h"
#include <cstdlib>     /* srand, rand */

using namespace std;

CQuickSort::~CQuickSort()
{
}

void CQuickSort::Init(int* pArr, const int arrSize, EPivotChoice pivotType)
{
	m_pArr      = pArr;
	m_arrSize   = arrSize;
	m_pivotType = pivotType;
	srand(123);
}

uint64 CQuickSort::Run()
{
	m_numOfInversions = count_and_sort(m_pArr, m_arrSize);
	return m_numOfInversions;
}

int CQuickSort::choose_pivot(int* arr, const int& arrSize)
{
	if (m_pivotType == FIRST)
	{
		return 0;
	}
	else if (m_pivotType == RANDOM)
	{
		return (rand() % (arrSize - 1));
	}
	else if (m_pivotType == LAST)
	{
		return arrSize - 1;
	}
	else if (m_pivotType == MEDIAN_OF_THREE)
	{
		return median_of_three(arr, arrSize);
	}
	else
		return 0;
}

uint64 CQuickSort::count_and_sort(int* arr, const int& arrSize)
{
	if (arrSize <= 1)
	{
		return 0;
	}

	//choose pivot
	int p = choose_pivot(arr, arrSize);

	//sort around pivot
	int i = partition_around_pivot(arr, arrSize, p);

	//left side
	uint64 x = count_and_sort(arr, i - 1);

	//right side
	uint64 y = count_and_sort(arr + i, arrSize - i);

	return x + y + arrSize - 1;
}

int CQuickSort::partition_around_pivot(int* arr, const int& arrSize, const int& p)
{

	int i = 1;
	int pVal = *(arr + p);

	if (p != 0)
	{
		std::swap(arr[0], arr[p]);
	}

	for (int j = 1; j < arrSize; ++j)
	{
		if (arr[j] < pVal)
		{
			std::swap(arr[i], arr[j]);
			i++;
		}
	}
	std::swap(arr[i - 1], arr[0]);

	return i;
}
