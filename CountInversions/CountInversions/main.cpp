// main.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <string>
#include <cassert>
#include <chrono>

#include "MergeSort.h"
#include "QuickSort.h"

#pragma warning(disable:4996)

static const int NUM_OF_VALS = 100000;
using namespace std;

//Read array from text file and intialize array
void ReadArrayFromFile(const string& fileName, int** ppArr, const int& arrSize)
{
	*ppArr = new int[arrSize]();
	int* pArr = *ppArr;

	std::ifstream inFile(fileName.c_str());
	string line;
	int i = 0;
	if (inFile.is_open())
	{
		while (inFile.good() && i < arrSize)
		{
			getline(inFile, line);
			if (line != "")
			{
				*pArr = stoi(line);
				pArr++;
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

bool CompareArrays(int* arr1, int* arr2, const int& arrSize)
{
	bool isEqual = true;
	for (size_t i = 0; i < (size_t)arrSize; i++, ++arr1, ++arr2)
	{
		if (*arr1 != *arr2)
		{
			isEqual = false;
			break;
		}
	}
	return isEqual;
}

int main(int argc, char *argv[])
{
	typedef std::chrono::high_resolution_clock Clock;
	typedef std::chrono::duration<double> sec;
	Clock::time_point t0 = Clock::now();
	const int N = 10000000;

	int *pArr = NULL;
	int *pTemp1 = NULL;
	int *pTemp2 = NULL;
	string fileName;
	int numOfVals = 0;
	if (argc == 3)
	{
		fileName  = argv[1];
		numOfVals = stoi(argv[2]);
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	ReadArrayFromFile(fileName, &pArr, numOfVals);

	pTemp1 = new int[numOfVals]();
	pTemp2 = new int[numOfVals]();
	copy(pArr, pArr + numOfVals, pTemp1);
	copy(pArr, pArr + numOfVals, pTemp2);

	uint64 inversions = 0;

	{
		CMergeSort countInversions;
		countInversions.Init(pTemp1, numOfVals);
		countInversions.Run();
		inversions = countInversions.GetNumOfInversions();
		cout << "MergeSort: Number of inversions: " << inversions << endl;
	}
	Clock::time_point t1 = Clock::now();
	std::cout << sec(t1 - t0).count() << " [sec]\n";

	{
		EPivotChoice pivotType = MEDIAN_OF_THREE;
		CQuickSort countInversions;
		countInversions.Init(pTemp2, numOfVals, pivotType);
		countInversions.Run();
		inversions = countInversions.GetNumOfInversions();
		cout << "QuickSort: Number of inversions: " << inversions << endl;
	}
	Clock::time_point t2 = Clock::now();
	std::cout << sec(t2 - t1).count() << " [sec]\n";

	bool isEqual = CompareArrays(pTemp1, pTemp2, numOfVals);
	
	delete[] pTemp1;
	delete[] pTemp2;
	delete[] pArr;
	return 0;
	//FIRST - 162085
	//LAST - 164123
	//Median - 138382
}
