#include "GraphStruct.h"


CGraph::CGraph(const CGraph& other)
{
	// Iterate over map and create all nodes
	for (auto iterator = other.work.begin(); iterator != other.work.end(); iterator++) {
		// iterator->first = key
		// iterator->second = value
		uint64_t this_node = iterator->first;
		this->addvertex(this_node);
		//add edges
		for (auto adj_it = iterator->second->adj.begin(); adj_it != iterator->second->adj.end(); adj_it++)
		{
			uint64_t edge_node = adj_it->second->idx;
			this->addvertex(edge_node);
			this->addedge(this_node, edge_node, adj_it->first);
		}
	}
}


CGraph& CGraph::operator = (const CGraph & other)
{
	if (this != &other) // protect against invalid self-assignment
	{
		// Iterate over map and create all nodes
		for (auto iterator = other.work.begin(); iterator != other.work.end(); iterator++) {
			// iterator->first = key
			// iterator->second = value
			uint64_t this_node = iterator->first;
			this->addvertex(this_node);
			//add edges
			for (auto adj_it = iterator->second->adj.begin(); adj_it != iterator->second->adj.end(); adj_it++)
			{
				uint64_t edge_node = adj_it->second->idx;
				this->addvertex(edge_node);
				this->addedge(this_node, edge_node, adj_it->first);
			}
		}
	}
	// by convention, always return *this
	return *this;
}


size_t CGraph::get_num_edges(const uint64_t &idx) const
{
	auto itr = work.begin();
	itr = work.find(idx);
	if (itr != work.end())
	{
		return itr->second->adj.size();
	}
	return 0;//not found
}


void CGraph::addvertex(const uint64_t &idx)
{
	auto itr = work.begin();
	itr = work.find(idx);
	if (itr == work.end())
	{
		vertex *v;
		v = new vertex(idx);
		work[idx] = v;
		return;
	}
}

//only for an undirected graph - remove node
void CGraph::remove_vertex_undirected(const uint64_t& idx)
{
	auto itr = work.begin();
	itr = work.find(idx);
	if (itr != work.end())//if found
	{
		auto edge_it = itr->second->adj.begin();
		for (; edge_it != itr->second->adj.end(); edge_it++)//iterate over edges
		{
			uint64_t other_node = edge_it->second->idx;
			//go to node and remove pointer from adj list
			vmap::iterator other_it = work.find(other_node);
			assert(other_it != work.end());//none found
			other_it->second->adj.remove_if(is_pointing_to(idx));
		}
	}
	//deallocate vertex
	delete itr->second;

	//remove from map - by iterator
	work.erase(itr);
}


void CGraph::addedge(const uint64_t& from, const uint64_t& to, double cost)
{
	vertex *f = (work.find(from)->second);
	vertex *t = (work.find(to)->second);
	pair<double, vertex *> edge = make_pair(cost, t);
	f->adj.push_back(edge);

}


void CGraph::add_undirected_edge(const uint64_t& idx1, const uint64_t& idx2, double cost)
{
	addedge(idx1, idx2, cost);
	addedge(idx2, idx1, cost);
}

//first value is the node to be added and the rest are the nodes to link to
void CGraph::add_node_from_adjacency_list(const vector<uint64_t>& node_and_edges)
{
	auto it = node_and_edges.begin();
	addvertex(*it);
	++it;

	//check that the rest of the nodes to link to exist. If not, they are to be added
	for (; it != node_and_edges.end(); it++)
	{
		addvertex(*it);
	}

	//add edges
	uint64_t node_idx = *node_and_edges.begin();
	it = node_and_edges.begin() + 1;
	for (; it != node_and_edges.end(); it++)
	{
		addedge(node_idx, *it, 1.0);
	}

}


//first value is the node to be added and the rest are the nodes to link to
void CGraph::add_node_and_weighted_edge_from_list(const vector< vector<uint64_t> >& node_and_weighted_edges)
{
	auto it = node_and_weighted_edges.begin();
	uint64_t node_idx = (*it)[0];
	addvertex(node_idx);
	++it;

	//check that the rest of the nodes to link to exist. If not, they are to be added
	for (; it != node_and_weighted_edges.end(); it++)
	{
		//add vertice if non existent
		addvertex((*it)[0]);
		//add edge
		addedge(node_idx, (*it)[0], (double)(*it)[1]);
	}
}

//print graph to screen
void CGraph::print_graph()
{

	for (auto itr = work.begin(); itr != work.end(); itr++)
	{
		cout << itr->first << ":";
		auto edge_it = itr->second->adj.begin();
		for (; edge_it != itr->second->adj.end(); edge_it++)
		{
			cout << ' ' << edge_it->second->idx;
		}
		cout << endl;
	}

}

//contract edge (u<-v)
void CGraph::contract_edge(const uint64_t& u, const uint64_t& v)
{
	//merge both lists
	vmap::iterator itr_u, itr_v;
	itr_u = work.find(u);
	itr_v = work.find(v);

	if (itr_u != work.end() && itr_v != work.end())//if found put v into u
	{
		//make u point to all vertices v points to
		itr_v->second->adj.remove_if(is_pointing_to(u));//first remove pointer to v->u
		//remove edge that used to point  from u->v
		itr_u->second->adj.remove_if(is_pointing_to(v));

		//list edges of u = list edges of u + list edges of v
		itr_u->second->adj.splice(itr_u->second->adj.end(), itr_v->second->adj);//add to end of list
	}
	else
	{
		assert(!"Edge not found");
	}

	//all vertices that point to v should now point to u instead
	//iterate over map and check all nodes
	for (auto iterator = work.begin(); iterator != work.end(); iterator++) 
	{
		// iterator->first = key
		// iterator->second = value
		uint64_t this_node = iterator->first;
		if (this_node == u)//avoid circular nodes
		{
			continue;
		}
		if (this_node == v)//v is going to be removed anyway
		{
			continue;
		}
		//loop over edges
		for (auto adj_it = iterator->second->adj.begin(); adj_it != iterator->second->adj.end(); adj_it++)
		{
			uint64_t edge_node = adj_it->second->idx;
			if (edge_node == v)//points at v
			{
				adj_it->second = itr_u->second;//point it at u instead - edge weight stays the same
			}
		}
	}
	//deallocate vertex
	delete itr_v->second;

	//remove from map - by iterator
	work.erase(itr_v);

}

//contract undirected edge (u<-v)
void CGraph::contract_undirected_edge(const uint64_t& u, const uint64_t& v)
{
	//merge both lists
	auto itr_u = work.find(u);
	auto itr_v = work.find(v);

	if (itr_u != work.end() && itr_v != work.end())//if found put v into u
	{
		//make u point to all vertices v points to
		itr_v->second->adj.remove_if(is_pointing_to(u));//first remove pointer to v->u
		//remove edge that used to point  from u->v
		itr_u->second->adj.remove_if(is_pointing_to(v));

		//any node pointing to v should now point to u instead
		for (auto adj_it = itr_v->second->adj.begin(); adj_it != itr_v->second->adj.end(); adj_it++)
		{
			uint64_t edge_node = adj_it->second->idx;
			auto it_edge = get_it_to_vertex(edge_node);

			//loop over edges pointing to v
			for (auto it = it_edge->second->adj.begin(); it != it_edge->second->adj.end(); it++)
			{
				if (it->second->idx == v)//points at v
				{
					it->second = itr_u->second;//point it at u instead - edge weight stays the same
				}
			}
		}

		//list edges of u = list edges of u + list edges of v
		itr_u->second->adj.splice(itr_u->second->adj.end(), itr_v->second->adj);//add to end of list
	}
	else
	{
		assert(!"Edge not found");
	}

	//deallocate vertex
	delete itr_v->second;

	//remove from map - by iterator
	work.erase(itr_v);
}


void CGraph::clear()
{
	//iterate over map and release all vertices
	for (auto iterator = work.begin(); iterator != work.end(); iterator++)
	{
		delete iterator->second;
	}
	work.clear();
}


CGraph::~CGraph()
{
	//iterate over map and release all vertices
	for (auto iterator = work.begin(); iterator != work.end(); iterator++)
	{
		delete iterator->second;
	}
}


void CGraph::reverse_graph(CGraph& rev) const
{

	// Iterate over map and create all nodes
	for (auto iterator = work.begin(); iterator != work.end(); iterator++) {
		// iterator->first = key
		// iterator->second = value
		uint64_t this_node = iterator->first;
		rev.addvertex(this_node);
		//add edges
		for (auto adj_it = iterator->second->adj.begin(); adj_it != iterator->second->adj.end(); adj_it++)
		{
			uint64_t edge_node = adj_it->second->idx;
			rev.addvertex(edge_node);
			rev.addedge(edge_node, this_node, adj_it->first);
		}
	}
}



