#include <fstream>
#include <iostream>
#include <string>

#include <conio.h>
#include <cstdio>

#include "GraphStruct.h"
#include "Utils.h"
#include "MinCut.h"
#include "DFS.h"
#include "ShortestPaths.h"

using namespace std;

#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
	#ifndef DBG_NEW
		#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
		#define new DBG_NEW
	#endif
#endif


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	
	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	CGraph graph;

	int hw = 5;
	if (hw == 3 || hw == 4)
	{
		ReadCGraphFromFile(fileName, &graph);
	}
	else if (hw == 5)
	{
		ReadWeightedGraphFromFile(fileName, &graph);
	}
	//cout << "Org" << endl;
	//graph.print_graph();
	
	if (hw == 3)
	{
		CMinCut minCut(graph);
		minCut.Run();
		cout << "Min Cuts for the graph are: " << minCut.getNumOfMinCuts() << endl;
	}
	else if (hw == 4)
	{ 
		map<uint64_t, uint64_t> SCC;
		CDFS dfs;
		dfs.calc_SCC(graph, SCC);
	}
	else //if (hw == 5)
	{
		CShortestPaths sp(&graph);
		sp.Run(1);
	}


	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}