﻿#pragma once 

#include "GraphStruct.h"


//Knuth shuffle
//To shuffle an array a of n elements(indices 0..arr_size - 1) :
inline void knuth_shuffle(int* arr, const int arr_size, const int& num_of_shuffles)
{
	//init arr from 0->arr_size-1
	for (int i = 0; i < arr_size; i++)
	{
		arr[i] = i;
	}

	int idx = arr_size - 1;
	for (int shuffe_idx = 0; idx > 0 && shuffe_idx < num_of_shuffles; idx--, shuffe_idx++)
	{
		//j <- random integer with 0 <= j <= i
		int j = rand() % (idx + 1);
		std::swap(arr[j], arr[idx]);
	}
}

class CMinCut
{
	CGraph m_Graph;
	int m_rngSeed;
	uint64_t m_numOfMinCuts;

	uint64_t CalcSingleCut();

public:
	CMinCut(const CGraph& Graph) : m_Graph(Graph), m_rngSeed(123){}
	~CMinCut(){}

	void Run();

	//getters
	uint64_t getNumOfMinCuts() const { return m_numOfMinCuts; }
};

