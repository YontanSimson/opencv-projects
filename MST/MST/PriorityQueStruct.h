#pragma once

#include <cstdint>		// uint64_t
#include <utility>      // std::pair, std::make_pair
#include <ostream>		// std::ostream
#include <unordered_map> 
#include <cassert>

template<class KeyType, class ValueType>
class CKeyCompare
{
public:
	virtual bool operator()(const std::pair<KeyType, ValueType>& a, const std::pair<KeyType, ValueType>& b) const = 0;
	virtual ~CKeyCompare(){}
};

template<class KeyType, class ValueType>
class CKeyGreater : public CKeyCompare<KeyType, ValueType>
{
public:
	bool operator()(const std::pair<KeyType, ValueType>& a, const std::pair<KeyType, ValueType>& b) const { return a.first > b.first; }//gt
};

template<typename KeyType, typename ValueType>
class CKeyLesser : public CKeyCompare<KeyType, ValueType>
{
public:
	bool operator()(const std::pair<KeyType, ValueType>& a, const std::pair<KeyType, ValueType>& b) const { return a.first < b.first; }//lt
};

class CPriorityQueStruct
{
public:
	CPriorityQueStruct(const std::pair<double, uint64_t>* arr, const size_t& arr_size, CKeyCompare<double, uint64_t>* pred);
	CPriorityQueStruct(const size_t& arr_size, CKeyCompare<double, uint64_t>* pred);
	CPriorityQueStruct(CKeyCompare<double, uint64_t>* pred);
	~CPriorityQueStruct();
	size_t Size() const { return m_arr_size; }
	void Init(const std::pair<double, uint64_t>* arr, const size_t& arr_size);
	void InitUnordered(const std::vector<uint64_t> vertices, const size_t& start_vertex);
	void Reserve(const size_t& arr_size);
	void Clear();

	std::pair<double, uint64_t> Pop();
	std::pair<double, uint64_t> Front() const;
	void Insert(const std::pair<double, uint64_t>& obj);
	void DecreaseKey(const size_t& keyPos, const std::pair<double, uint64_t>& key);
	void DecreaseKey(const size_t& keyPos, const double& key);
	double GetKey(const size_t& keyPos);
	bool IsValueInQue(const uint64_t& val){ assert(m_vertex_pos_in_que.size() == m_arr_size); return m_vertex_pos_in_que.find(val) != m_vertex_pos_in_que.end(); }
	size_t ValuePositionInQue(const uint64_t& val){ assert(m_vertex_pos_in_que.find(val) != m_vertex_pos_in_que.end()); return m_vertex_pos_in_que[val]; }
	void print();
	void Swap(const size_t& idx1, const size_t& idx2);
private:
	void heapify(std::pair<double, uint64_t>* a, const size_t& arr_size);
	void siftDown(std::pair<double, uint64_t>* a, const size_t& start, const size_t& end);
	std::pair<double, uint64_t>* m_que;
	std::unordered_map<uint64_t, size_t> m_vertex_pos_in_que;
	size_t m_arr_size;
	size_t m_reserved_size;
	CKeyCompare<double, uint64_t>* m_pred;
	bool m_is_min_heap;
};

std::ostream& operator<< (std::ostream& stream, const std::pair<double, uint64_t>& matrix);
