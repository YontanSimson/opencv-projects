#pragma once

class CHeap
{
public:
	CHeap(const double* arr, const size_t& arr_size);
	CHeap(const size_t& arr_size);
	CHeap();
	~CHeap();

	void Init(const double* arr, const size_t& arr_size);
	void Reserve(const size_t& arr_size);
	void Clear();
	double Pop();
	double Front() const;
	void Insert(const double& key);
	void DecreaseKey(const size_t& keyPos, const double& key);
private:
	void heapify();
	void siftDown(double* a, const size_t& start, const size_t& end);
	double* m_arr;
	size_t m_arr_size;
	size_t m_reserved_size;
};

