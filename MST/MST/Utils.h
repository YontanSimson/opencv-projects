#pragma once 

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>

#include "GraphStruct.h"

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
	std::vector<std::string> elems;
	split(s, delim, elems);
	return elems;
}

template<class T>
void split(const std::string &s, const char delim, std::vector<T>& elems) 
{
	elems.clear();
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back((T)stoull(item));
	}
}

void split_double(const std::string &s, const char delim, std::vector<double>& elems)
{
	elems.clear();
	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back(stod(item));
	}
}

template<class T>
std::vector<T> split(const std::string &s, const char delim )
{
	std::vector<T> elems;

	std::stringstream ss(s);
	std::string item;
	while (std::getline(ss, item, delim)) {
		elems.push_back((T)stoull(item));
	}
	return elems;
}

//only works with vector<string>
template < class ContainerT >
void tokenize(const std::string& str, ContainerT& tokens,
	const std::string& delimiters = " ", bool trimEmpty = false)
{
	std::string::size_type pos, lastPos = 0;

	using value_type = typename ContainerT::value_type;
	using size_type = typename ContainerT::size_type;

	while (true)
	{
		pos = str.find_first_of(delimiters, lastPos);
		if (pos == std::string::npos)
		{
			pos = str.length();

			if (pos != lastPos || !trimEmpty)
				tokens.push_back(value_type(str.data() + lastPos,
				(size_type)pos - lastPos));

			break;
		}
		else
		{
			if (pos != lastPos || !trimEmpty)
				tokens.push_back(value_type(str.data() + lastPos,
				(size_type)pos - lastPos));
		}

		lastPos = pos + 1;
	}
}

//Read array from text file and initialize directed Graph
inline void ReadCGraphFromFile(const std::string& fileName, CGraph* pGraph)
{

	std::ifstream inFile(fileName.c_str());
	std::string line;
	int i = 0;
	if (inFile.is_open())
	{
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{

				vector<uint64_t> node_and_edges;
				split(line, ' ', node_and_edges);
				pGraph->add_node_from_adjacency_list(node_and_edges);
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}

//Read array from text file and initialize directed Graph
inline void ReadWeightedUndirectedGraphFromFile(const std::string& fileName, CGraph* pGraph)
{

	std::ifstream inFile(fileName.c_str());
	std::string line;
	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_nodes] [number_of_edges]
		vector<uint64_t> num_of_nodes_and_edges;
		split(line, ' ', num_of_nodes_and_edges);
		pGraph->init(num_of_nodes_and_edges[0]);
		while (inFile.good())
		{
			getline(inFile, line);//[one_node_of_edge_1] [other_node_of_edge_1] [edge_1_cost]
			if (line != "")
			{
				vector<double> node_and_edges;
				split_double(line, ' ', node_and_edges);
				pair<uint64_t, double> edge_undirected((uint64_t)node_and_edges[1], node_and_edges[2]);
				pGraph->add_undirected_eges_and_nodes_from_adjacency_list((uint64_t)node_and_edges[0], edge_undirected);
			}
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
	}

}

//Read array from text file and intialize Graph
inline void ReadWeightedGraphFromFile(const string& fileName, CGraph* pGraph)
{

	std::ifstream inFile(fileName.c_str());
	string line;
	int i = 0;
	if (inFile.is_open())
	{
		while (inFile.good())
		{
			getline(inFile, line);
			if (line != "")
			{

				vector<string> node_and_edges;
				split(line, '\t', node_and_edges);
				vector< vector<uint64_t> > node_and_weighted_edges;
				auto it = node_and_edges.begin();
				node_and_weighted_edges.push_back(vector<uint64_t>(1, stoull(*it)));
				it++;
				for (; it != node_and_edges.end(); it++)
				{
					node_and_weighted_edges.push_back(split<uint64_t>(*it, ','));
				}
				pGraph->add_node_and_weighted_edge_from_list(node_and_weighted_edges);
				i++;
			}
		}
		inFile.close();
	}
	else
	{
		std::cout << "Unable to open file" << fileName << std::endl;
	}

}