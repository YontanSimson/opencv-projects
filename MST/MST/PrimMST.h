#pragma once

#include "GraphStruct.h"

class CPrimsMST
{
	const CGraph* m_pGraph;
	CGraph m_MST;
	double m_mst_total_weight;
public:
	void Init(const CGraph* pGraph);
	void Run();
	double get_mst_weight() const { return m_mst_total_weight; }
	CPrimsMST(){}
	~CPrimsMST(){}
};

