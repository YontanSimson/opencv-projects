#include "PrimsOptMST.h"
#include "PriorityQueStruct.h"

#include <unordered_set>

void CPrimsOptMST::Init(const CGraph* pGraph)
{
	m_pGraph = pGraph;
}

void CPrimsOptMST::Run()
{
	size_t num_of_vertices = m_pGraph->get_num_of_vertices();
	//Prims algorithm
	uint64_t start_vertex = 1;
	m_mst_total_weight = 0;
	m_MST.reserve(num_of_vertices);

	//unordered_set<uint64_t> nodes_in_mst(num_of_vertices);
	//nodes_in_mst.insert(1);

	CPriorityQueStruct heap(num_of_vertices, new CKeyGreater<double, uint64_t>());
	for (auto it_graph = m_pGraph->begin(); it_graph != m_pGraph->end(); it_graph++)
	{
		heap.Insert(make_pair(DBL_MAX, it_graph->first));
	}
	size_t start_idx =  heap.ValuePositionInQue(start_vertex);
	heap.DecreaseKey(start_idx, 0);

	//heap.print();

	m_MST.insert(std::make_pair(start_vertex, std::make_pair(-1, 0.0)));

	while (heap.Size() > 0)
	{

		auto it_u = heap.Pop();
		
		double cheapest_edge_weight = it_u.first;
		uint64_t cheapest_node_from = it_u.second;

		uint64_t cheapest_node_to = -1;
		auto it_v = m_pGraph->edge_begin(cheapest_node_from);
		for (; it_v != m_pGraph->edge_end(cheapest_node_from); ++it_v)//loop over edges to adjust cost for vertices neighboring u
		{
			cheapest_node_to = it_v->second->idx;
			double edge_cost = it_v->first;

			if (heap.IsValueInQue(cheapest_node_to))
			{
				size_t que_idx = heap.ValuePositionInQue(cheapest_node_to);
				if (edge_cost < heap.GetKey(que_idx))
				{
					heap.DecreaseKey(que_idx, edge_cost);
					m_MST[cheapest_node_to] = std::make_pair(cheapest_node_from, edge_cost);
				}
			}
		}

		m_mst_total_weight += cheapest_edge_weight;
	}
}