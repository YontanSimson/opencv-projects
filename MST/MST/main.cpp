#include <fstream>
#include <iostream>
#include <string>

#include <conio.h>
#include <cstdio>
#include <chrono>

#include "GraphStruct.h"
#include "Utils.h"
#include "PrimsOptMST.h"
#include "PrimMST.h"
#include "HeapStruct.h"
#include "PriorityQueStruct.h"

using namespace std;

#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC
	#include <stdlib.h>
	#include <crtdbg.h>
	#ifndef DBG_NEW
		#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
		#define new DBG_NEW
	#endif
#endif


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif
	
	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}

	auto t0 = std::chrono::high_resolution_clock::now();
	CGraph graph;
	ReadWeightedUndirectedGraphFromFile(fileName, &graph);
	//graph.print_weighted_graph();

	//timing
	auto t1 = std::chrono::high_resolution_clock::now();

	// Alg
	CPrimsMST prims;
	prims.Init(&graph);
	prims.Run();
	cout << "MST total weight is: " << int64_t(prims.get_mst_weight()) << endl;

	//timing
	auto t2 = std::chrono::high_resolution_clock::now();
	std::cout << "Generic MST took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
		<< " milliseconds\n";


	//timing
	auto t3 = std::chrono::high_resolution_clock::now();

	//Alg
	CPrimsOptMST primsOpt;
	primsOpt.Init(&graph);
	primsOpt.Run();

	cout << "Heap based MST total weight is: " << int64_t(primsOpt.get_mst_weight()) << endl;

	//timing
	auto t4 = std::chrono::high_resolution_clock::now();
	std::cout << "Heap based Prim's MST took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t2).count()
		<< " milliseconds\n";

	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}