#pragma once

#include "GraphStruct.h"

class CPrimsOptMST
{
	const CGraph* m_pGraph;
	std::unordered_map<uint64_t, std::pair<uint64_t, double>> m_MST;
	double m_mst_total_weight;
public:
	void Init(const CGraph* pGraph);
	void Run();
	double get_mst_weight() const { return m_mst_total_weight; }
	CPrimsOptMST(){}
	~CPrimsOptMST(){}
};

