#include "HeapStruct.h"
#include <cmath>
#include <cstdint>
#include <algorithm>
#include <cassert>

inline size_t calc_parent(const size_t& child){ return child == 0 ? 0 : (child - 1) / 2; }
inline size_t calc_left(const size_t& parent){ return parent * 2 + 1; }
inline size_t calc_right(const size_t& parent){ return parent * 2 + 2; }

CHeap::CHeap(const double* arr, const size_t& arr_size):
	m_arr_size(arr_size),
	m_reserved_size(arr_size),
	m_arr(nullptr)
{
	m_arr = new double[arr_size];
	std::copy(arr, arr + arr_size, m_arr);
	heapify();
}

CHeap::CHeap(const size_t& arr_size) :
	m_arr_size(0),
	m_reserved_size(arr_size),
	m_arr(nullptr)
{
	m_arr = new double[arr_size];
}

CHeap::CHeap():
	m_arr_size(0),
	m_reserved_size(0),
	m_arr(nullptr)
{
}

CHeap::~CHeap()
{
	if (m_arr != nullptr)
		delete[] m_arr;
	m_arr = nullptr;
}

void CHeap::heapify()
{
	int64_t start = (int64_t)std::floor((m_arr_size - 2) / 2);//last parent node
	while (start >= 0)
	{
		//sift down the node at index 'start' to the proper place such that all nodes below
		//	the start index are in heap order
		siftDown(m_arr, start, m_arr_size - 1);
		//siftDown(a, start, count - 1);
		start--;
		//after sifting down the root all nodes / elements are in heap order
	}
}

void CHeap::siftDown(double* a, const size_t& start, const size_t& end)
{
	size_t root = start;

	while (root * 2 + 1 <= end) //While the root has at least one child
	{
		size_t child = root * 2 + 1;//Left child
		size_t swap_idx = root;// (Keeps track of child to swap with)

		if (a[swap_idx] >= a[child])//is parent bigger than child?
		{
			swap_idx = child;
		}

		//If there is a right child and that child is greater
		if (child + 1 <= end && a[swap_idx] >= a[child + 1])
		{ 
			swap_idx  = child + 1;
		}
		if (swap_idx == root)
		{
			//The root holds the smallest element.Since we assume the heaps rooted at the
				//children are valid, this means that we are done.
			return;
		}
		else
		{
			std::swap(a[root], a[swap_idx]);
			root = swap_idx; //repeat to continue sifting down the child now
		}

	}

}

void CHeap::Init(const double* arr, const size_t& arr_size)
{
	m_arr_size = arr_size;
	if (m_reserved_size != arr_size)
	{
		if (m_arr != nullptr)
		{
			delete[] m_arr;
		}
		m_arr = new double[arr_size];
	}
	
	
	std::copy(arr, arr + arr_size, m_arr);
	heapify();
}

void CHeap::Reserve(const size_t& arr_size)
{
	m_arr_size = 0;
	m_reserved_size = arr_size;
	if (m_arr != nullptr)
	{
		delete[] m_arr;
	}
	m_arr = new double[m_reserved_size];
}

void CHeap::Clear()
{
	if (m_arr != nullptr)
		delete[] m_arr;
	m_arr = nullptr;
	m_arr_size = 0;
	m_reserved_size = 0;
}

double CHeap::Pop()
{
	assert(m_arr_size > 0);
	double max_val = m_arr[0];
	m_arr[0] = m_arr[m_arr_size - 1];
	m_arr_size--;
	if (m_arr_size == 0)
		return max_val;

	siftDown(m_arr, 0, m_arr_size - 1);
	return max_val;
}

double CHeap::Front() const
{
	assert(m_arr_size > 0);
	return m_arr[0];
}

void CHeap::Insert(const double& key)
{
	if (m_arr_size == m_reserved_size)//allocate new memory
	{
		double* temp = new double[m_reserved_size+1];
		std::copy(m_arr, temp + m_reserved_size, temp);
		if (m_arr != nullptr)
			delete[] m_arr;
		m_arr = temp;
		m_arr_size++;
		m_reserved_size++;
	}
	else
	{
		m_arr_size++;
		//pop element on end
		m_arr[m_arr_size-1] = DBL_MAX;
		DecreaseKey(m_arr_size - 1, key);
	}
}

//keyPos actual position of key in array - you may need a hash table to keep track of this
void CHeap::DecreaseKey(const size_t& keyPos, const double& key)
{
	assert(key <= m_arr[keyPos]);
	assert(keyPos < m_arr_size);
	m_arr[keyPos] = key;
	size_t child = keyPos;
	size_t parent = calc_parent(child);
	//bubble up if smaller than parent
	while (child >= 0 && m_arr[parent] > m_arr[child])
	{
		std::swap(m_arr[parent], m_arr[child]);
		child = parent;
		parent = calc_parent(parent);
	}
}
