#pragma once 

#include <iostream>
#include <vector>
#include <list>
#include <map>
#include <unordered_map>
#include <string>
#include <cassert>
#include <cstdint>
using namespace std;

struct vertex{
	typedef pair<double, vertex*> ve;
	list<ve> adj; //cost of edge, destination vertex
	uint64_t idx;
	vertex(const uint64_t& _idx)
	{
		idx = _idx;
	}

	auto edge_begin() const -> decltype(adj.cbegin())	{return adj.cbegin();}
	auto edge_begin() -> decltype(adj.begin())	{ return adj.begin();}

	auto edge_end() const -> decltype(adj.cend()) {	return adj.cend();}
	auto edge_end() -> decltype(adj.end())	{return adj.end();	}

};

struct is_pointing_to 
{
	is_pointing_to(const uint64_t& _trg) : trg(_trg) {}
	uint64_t trg;
	bool operator() (const pair<double, vertex*>& value) { return (value.second->idx == trg); }
};

//vertex map
typedef unordered_map<uint64_t, vertex *> vmap;

//Graph structure
class CGraph
{
	vmap work;
public:

	void init(const uint64_t&);
	void addvertex(const uint64_t&);
	void remove_vertex(const uint64_t&);
	void addedge(const uint64_t& from, const uint64_t& to, double cost);
	void add_undirected_edge(const uint64_t& idx1, const uint64_t& idx2, double cost);
	void add_node_from_adjacency_list(const vector<uint64_t>& node_and_edges);
	void add_undirected_eges_and_nodes_from_adjacency_list(const uint64_t& src_node, const pair<uint64_t, double>& node_and_edge_weight);
	void add_node_and_weighted_edge_from_list(const vector< vector<uint64_t> >& node_and_weighted_edges);
	void remove_vertex_undirected(const uint64_t&idx);
	void contract_edge(const uint64_t& u, const uint64_t& v);
	void contract_undirected_edge(const uint64_t& u, const uint64_t& v);
	void print_graph();
	void print_weighted_graph();

	//getters
	size_t get_num_of_vertices() const { return work.size(); }
	size_t get_num_edges(const uint64_t &idx) const;
	//uint64_t get_max_vertice_id() const {return work.rbegin()->first;}//only true is using map
	void get_list_of_vertexes(std::vector<uint64_t>&) const;

	//const vertex* get_vertex(const T &idx) const { return (const vertex*)(work[idx]); }
	vertex* get_vertex(const uint64_t &idx) { return work[idx]; }

	auto get_it_to_vertex(const uint64_t &idx) const -> decltype(work.find(idx))
	{
		auto it_i = work.find(idx);
		assert(it_i != work.end());
		return it_i;
	}

	
	auto get_it_to_vertex(const uint64_t &idx)-> decltype(work.find(idx))
	{
		auto it_i = work.find(idx);
		assert(it_i != work.end());
		return it_i;
	}

	auto edge_begin(const uint64_t &idx) const -> decltype(work.find(idx)->second->edge_begin())
	{
		assert(work.find(idx) != work.end());
		auto edge_it = work.find(idx)->second->edge_begin();
		return edge_it;
	}

	auto edge_begin(const uint64_t &idx) -> decltype(work.find(idx)->second->edge_begin())
	{
		assert(work.find(idx) != work.end());
		auto edge_it = work.find(idx)->second->edge_begin();
		return edge_it;
	}

	auto edge_end(const uint64_t &idx) const -> decltype(work.find(idx)->second->edge_end())
	{
		assert(work.find(idx) != work.end());
		auto edge_it = work.find(idx)->second->edge_end();
		return edge_it;
	}

	auto edge_end(const uint64_t &idx) -> decltype(work.find(idx)->second->edge_end())
	{
		assert(work.find(idx) != work.end());
		auto edge_it = work.find(idx)->second->edge_end();
		return edge_it;
	}

	auto begin() const -> decltype(work.cbegin()){ return work.cbegin(); }
	auto begin()-> decltype(work.begin()) { return work.begin(); }

	auto end() const -> decltype(work.cend()){ return work.cend(); }
	auto end() -> decltype(work.cend()) { return work.end(); }

	auto rbegin() const -> decltype(work.crbegin()){ return work.crbegin(); }
	auto rbegin()-> decltype(work.rbegin()){ return work.rbegin(); }

	auto rend() const -> decltype(work.crend()){ return work.crend(); }
	auto rend() -> decltype(work.rend()){ return work.rend(); }

	//setters

	//assignment operator
	CGraph& operator= (const CGraph& other);

	//reverse graph
	void reverse_graph(CGraph& rev) const;

	//clear
	void clear();

	//Ctr, Dctot, Copy Ctor
	CGraph(){}
	CGraph(const CGraph&);
	~CGraph();

};


