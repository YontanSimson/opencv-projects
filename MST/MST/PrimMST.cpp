#include "PrimMST.h"

#include <unordered_set>

void CPrimsMST::Init(const CGraph* pGraph)
{
	m_pGraph = pGraph;
}

void CPrimsMST::Run()
{
	size_t num_of_vertices = m_pGraph->get_num_of_vertices();
	//Prims algorithm
	m_mst_total_weight = 0;
	unordered_set<uint64_t> nodes_in_mst(num_of_vertices);
	nodes_in_mst.insert(1);
	m_MST.addvertex(1);
	while (nodes_in_mst.size() != num_of_vertices)
	{
		uint64_t cheapest_node_to = -1;
		uint64_t cheapest_node_from = -1;
		double cheapest_edge_weight = DBL_MAX;

		//find frontier edge
		for (auto it = nodes_in_mst.begin(); it != nodes_in_mst.end(); it++)
		{
			auto it_edge = m_pGraph->edge_begin(*it);
			for (; it_edge != m_pGraph->edge_end(*it); ++it_edge)//loop over edges to find frontier edges
			{
				if (nodes_in_mst.find(it_edge->second->idx) == nodes_in_mst.end())//not found in set so must be frontier
				{
					double& edge_cost = it_edge->first;
					if (edge_cost < cheapest_edge_weight)
					{
						cheapest_edge_weight = edge_cost;
						cheapest_node_to = it_edge->second->idx;
						cheapest_node_from = *it;
					}
				} 
			}
			
		}

		//add cheapest node to the set
		nodes_in_mst.insert(cheapest_node_to);
		m_MST.addvertex(cheapest_node_to);
		m_MST.add_undirected_edge(cheapest_node_from, cheapest_node_to, cheapest_edge_weight);
		m_mst_total_weight += cheapest_edge_weight;
	}
}

