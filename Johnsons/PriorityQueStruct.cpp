#include "PriorityQueStruct.h"
#include <cmath>
#include <cstdint>
#include <algorithm>
#include <cassert>
#include <iostream> //std::cout

inline size_t calc_parent(const size_t& child){ return child == 0 ? 0 : (child - 1) / 2; }
inline size_t calc_left(const size_t& parent){ return parent * 2 + 1; }
inline size_t calc_right(const size_t& parent){ return parent * 2 + 2; }

std::ostream& operator<<(std::ostream& stream,
	const std::pair<double, uint32_t>& obj) {
	stream << "Key: " << obj.first << " Value: " << obj.second;
	return stream;
}

CPriorityQueStruct::CPriorityQueStruct(const std::pair<double, uint32_t>* arr, const size_t& arr_size, CKeyCompare<double, uint32_t>* pred) :
	m_arr_size(arr_size),
	m_reserved_size(arr_size),
	m_que(nullptr),
	m_pred(pred),
	m_is_min_heap((*m_pred)(std::make_pair(2.0, 0), std::make_pair(1.0, 0)))
{
	m_que = new std::pair<double, uint32_t>[arr_size];
	m_vertex_pos_in_que.reserve(arr_size);

	for (int i = 0; i < arr_size; ++i)
	{
		m_que[i] = arr[i];
	}

	//std::copy(arr, arr + arr_size, m_que);
	heapify(m_que, m_arr_size);
}

CPriorityQueStruct::CPriorityQueStruct(const size_t& arr_size, CKeyCompare<double, uint32_t>* pred) :
	m_arr_size(0),
	m_reserved_size(arr_size),
	m_que(nullptr),
	m_pred(pred),
	m_is_min_heap((*m_pred)(std::make_pair(2.0, 0), std::make_pair(1.0, 0)))
{
	m_que = new std::pair<double, uint32_t>[arr_size];
	m_vertex_pos_in_que.reserve(arr_size);
}

CPriorityQueStruct::CPriorityQueStruct(CKeyCompare<double, uint32_t>* pred ) :
	m_arr_size(0),
	m_reserved_size(0),
	m_que(nullptr),
	m_pred(pred),
	m_is_min_heap((*m_pred)(std::make_pair(2.0, 0), std::make_pair(1.0, 0)))
{

}

CPriorityQueStruct::~CPriorityQueStruct()
{
	if (m_que != nullptr)
		delete[] m_que;
	m_que = nullptr;
	if (m_pred != nullptr)
		delete m_pred;
	m_que = nullptr;
}

void CPriorityQueStruct::heapify(std::pair<double, uint32_t>* arr, const size_t& arr_size)
{
	int32_t start = (int32_t)std::floor((arr_size - 2) / 2);//last parent node
	while (start >= 0)
	{
		//sift down the node at index 'start' to the proper place such that all nodes below
		//	the start index are in heap order
		siftDown(arr, start, arr_size - 1);
		start--;
		//after sifting down the root all nodes / elements are in heap order
	}
}

void CPriorityQueStruct::siftDown(std::pair<double, uint32_t>* a, const size_t& start, const size_t& end)
{
	size_t root = start;

	while (root * 2 + 1 <= end) //While the root has at least one child
	{
		size_t child = root * 2 + 1;//Left child
		size_t swap_idx = root;// (Keeps track of child to swap with)

		if ((*m_pred)(a[swap_idx], a[child]))//Is the parent bigger/smaller than the child?
		{
			swap_idx = child;
		}

		//If there is a right child and that child is greater
		if (child + 1 <= end && (*m_pred)(a[swap_idx], a[child + 1]))
		{
			swap_idx = child + 1;
		}
		if (swap_idx == root)
		{
			//The root holds the smallest/largest element.Since we assume the heaps rooted at the
			//children are valid, this means that we are done.
			return;
		}
		else
		{
			Swap(root, swap_idx);
			root = swap_idx; //repeat to continue sifting down the child now
		}

	}

}

void CPriorityQueStruct::Init(const std::pair<double, uint32_t>* arr, const size_t& arr_size)
{
	m_arr_size = arr_size;
	if (m_reserved_size != arr_size)
	{
		if (m_que != nullptr)
		{
			delete[] m_que;
		}
		m_que = new std::pair<double, uint32_t>[arr_size];
	}

	for (int i = 0; i < arr_size; ++i)
	{
		m_que[i] = arr[i];
	}
	//std::copy(arr, arr + arr_size, m_que);
	heapify(m_que, m_arr_size);
}

//Init with a list of vertices. All keys will be -inf/inf except for the start_idx which will be set to 0
//For use in dijkastra and Prims MST algorithms
void CPriorityQueStruct::InitUnordered(const std::vector<uint32_t> vertices, const uint32_t& start_vertex)
{

	m_arr_size		= vertices.size();
	m_reserved_size = vertices.size();

	if (m_que != nullptr)
	{
		delete[] m_que;
	}
	m_que = new std::pair<double, uint32_t>[m_arr_size];
	m_vertex_pos_in_que.reserve(m_arr_size);
	size_t i = 0;
	auto it_que = m_que;
	double init_key_val = m_is_min_heap ? DBL_MAX : -DBL_MAX;
	for (auto it_list = vertices.begin(); it_list != vertices.end(); it_list++, i++, it_que)
	{
		m_vertex_pos_in_que[*it_list] = i;
		it_que->first  = init_key_val;
		it_que->second = *it_list;
	}

	//set start vertex edge cost to zero and place it at the front of the heap
	const size_t& start_idx = m_vertex_pos_in_que[start_vertex];
	m_que[start_idx].first = 0;
	if (start_idx != 0)
	{
		Swap(0, start_idx);
	}
	
}

void CPriorityQueStruct::Reserve(const size_t& arr_size)
{
	m_arr_size = 0;
	m_reserved_size = arr_size;
	if (m_que != nullptr)
	{
		delete[] m_que;
	}
	m_que = new std::pair<double, uint32_t>[m_reserved_size];
}

void CPriorityQueStruct::Clear()
{
	if (m_que != nullptr)
		delete[] m_que;
	m_que = nullptr;
	m_arr_size = 0;
	m_reserved_size = 0;
}

std::pair<double, uint32_t> CPriorityQueStruct::Pop()
{
	assert(m_arr_size > 0);
	std::pair<double, uint32_t> max_val = m_que[0];
	//put end of array onto the begining of the array
	m_vertex_pos_in_que.erase(m_que[0].second);
	m_que[0] = m_que[m_arr_size - 1];
	m_arr_size--;

	if (m_arr_size == 0)
		return max_val;

	m_vertex_pos_in_que[m_que[0].second] = 0;

	//sink down if violating heap property
	siftDown(m_que, 0, m_arr_size - 1);
	return max_val;
}

std::pair<double, uint32_t> CPriorityQueStruct::Front() const
{
	assert(m_arr_size > 0);
	return m_que[0];
}

void CPriorityQueStruct::Insert(const std::pair<double, uint32_t>& obj)
{
	if (m_arr_size == m_reserved_size)//allocate new memory
	{
		std::pair<double, uint32_t>* temp = new std::pair<double, uint32_t>[m_reserved_size + 1];
		for (int i = 0; i < m_reserved_size; ++i)
		{
			temp[i] = m_que[i];
		}
		//std::copy(m_que, temp + m_reserved_size, temp);
		if (m_que != nullptr)
			delete[] m_que;
		m_que = temp;
		m_reserved_size++;
	}

	m_arr_size++;
	//pop element on end
	m_que[m_arr_size - 1] = std::make_pair(m_is_min_heap ? DBL_MAX : -DBL_MAX, obj.second);
	m_vertex_pos_in_que[obj.second] = m_arr_size - 1; 
	DecreaseKey(m_arr_size - 1, obj.first);
	assert(m_vertex_pos_in_que.size() == m_arr_size);
}

//Get Key by position in array
double CPriorityQueStruct::GetKey(const size_t& keyPos)
{
	assert(m_vertex_pos_in_que.size() == m_arr_size);
	assert(keyPos < m_arr_size);
	return m_que[keyPos].first;
}

//keyPos actual position of key in array - you may need a hash table to keep track of this
void CPriorityQueStruct::DecreaseKey(const size_t& keyPos, const double& newKey)
{
	assert(keyPos < m_arr_size);
	assert(m_vertex_pos_in_que.size() == m_arr_size);

	uint32_t value = m_que[keyPos].second;

	DecreaseKey(keyPos, std::make_pair(newKey, value));
}

//keyPos actual position of key in array - you may need a hash table to keep track of this
void CPriorityQueStruct::DecreaseKey(const size_t& keyPos, const std::pair<double, uint32_t>& obj)
{
	assert(!(*m_pred)(obj, m_que[keyPos]));//is new key valid?
	assert(keyPos < m_arr_size);
	m_que[keyPos] = obj;
	size_t child = keyPos;
	size_t parent = calc_parent(child);
	//bubble up if smaller/greater than parent
	while (child >= 0 && (*m_pred)(m_que[parent], m_que[child]))
	{
		Swap(parent, child);

		child = parent;
		parent = calc_parent(parent);
	}
}

void CPriorityQueStruct::print()
{
	for (size_t i = 0; i < m_arr_size; i++)
	{
		std::cout << m_que[i] << std::endl;
	}
}

void CPriorityQueStruct::Swap(const size_t& idx1, const size_t& idx2)
{
	std::swap(m_que[idx1], m_que[idx2]);
	std::swap(m_vertex_pos_in_que[m_que[idx1].second], m_vertex_pos_in_que[m_que[idx2].second]);
	assert(m_vertex_pos_in_que.size() == m_arr_size);
}


