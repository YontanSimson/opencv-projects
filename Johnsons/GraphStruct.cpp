#include "GraphStruct.h"

#include <iostream>
#include <fstream>
#include <string>
#include <cassert>

//#include "FileUtilities.h"
using namespace std;

static size_t GetNumberOfLinesInFile(const std::string& fileName)
{
	size_t NumOfLines = 0;
	std::ifstream f(fileName.c_str());
	if (!f.is_open())
	{
		std::cerr << "Failed to open file: " << fileName << std::endl;
		exit(1);
	}
	std::string line;
	for (; std::getline(f, line); ++NumOfLines);

	return NumOfLines;
}


void CGraph::Init(const std::string fileName)
{
	size_t numOfLines = GetNumberOfLinesInFile(fileName);
	size_t numOfVertexes = numOfLines - 1;
	
	m_edgeList.reserve(numOfVertexes);
	m_vertexList.reserve(numOfVertexes);

	std::ifstream inFile(fileName.c_str());
	std::string line;

	if (inFile.is_open())
	{
		getline(inFile, line);//[number_of_lines][num of edges] 

		uint32_t numOfVertexes_, numOfEdges;
		sscanf_s(line.c_str(), "%d %d", &numOfVertexes_, &numOfEdges);
		assert(numOfEdges == numOfVertexes);
		int lineIdx = 0;
		for (size_t i = 0; i<numOfEdges; ++i, ++lineIdx)  // Enter E undirected edges (u, v, weight)
		{
			getline(inFile, line);
			uint32_t edgeFromIdx, edgeToIdx;
			double edgeWeight;
			sscanf_s(line.c_str(), "%d %d %lf", &edgeFromIdx, &edgeToIdx, &edgeWeight);

			//add edge to list if it does not exist
			m_vertexList.insert(edgeFromIdx);
			m_vertexList.insert(edgeToIdx);
			//add vertex if it does not exist
			m_edgeList[edgeFromIdx].push_back(SEdge(edgeToIdx, edgeWeight));
		}
		inFile.close();
	}
	else
	{
		std::cerr << "Unable to open file: " << fileName << std::endl;
		std::exit(1);
	}
}

void CGraph::Reserve(const size_t& size)
{
	m_edgeList.reserve(size);
	m_vertexList.reserve(size);
}

void CGraph::Print()
{
	cout << "Printing graph" << endl;
	for (auto&& it : m_edgeList) 
	{
		const uint32_t& from = it.first;
		auto& localEdges = it.second;
		for (auto&& it_edge : localEdges)
		{
			cout << from << " " << it_edge.to << " " << it_edge.weight << std::endl;
		}
		
	}
}

void CGraph::AddVertex(const uint32_t& idx)
{
	m_vertexList.insert(idx);
}

void CGraph::AddEdge(
	const uint32_t& from, 
	const SEdge& edge)
{
	m_edgeList[from].push_back(edge);
}

void CGraph::AddSink(const uint32_t& src)
{
	for (auto&& it : m_vertexList)
	{
		AddEdge(src, SEdge(it, 0.));
	}
	m_vertexList.insert(src);
}

size_t CGraph::getNumOfEdges() const
{
	size_t numOfEdges = 0;
	for (auto&& it : m_edgeList)
	{
		numOfEdges += it.second.size();
	}
	return numOfEdges;
}

// a predicate implemented as a class:
struct is_pointing_to {
	is_pointing_to(const uint32_t& _ref) : ref(_ref){}
	uint32_t ref;
	bool operator() (const SEdge& edge) { return edge.to == ref; }
};

void CGraph::RemoveVertex(const uint32_t& idx)
{
	m_edgeList.erase(idx);
	for (auto&& it : m_edgeList)
	{
		const uint32_t& from = it.first;
		auto& localEdges = it.second;
		localEdges.remove_if(is_pointing_to(idx));
	}

	m_vertexList.erase(idx);
}

