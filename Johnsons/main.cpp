//#include "FileUtiles.h"

#include <iostream>
#include <string>
#include <conio.h>
#include <cstdio>
#include <chrono>
#include <memory>

#include "GraphStruct.h"
#include "BellmanFord.h"
#include "Dijkstra.h"
#include "Johnsons.h"

using namespace std;

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
#ifndef DBG_NEW
#define DBG_NEW new ( _NORMAL_BLOCK , __FILE__ , __LINE__ )
#define new DBG_NEW
#endif
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))


int main(int argc, char *argv[])
{
#ifdef _DEBUG
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	string fileName;
	if (argc == 2)
	{
		fileName = argv[1];
	}
	else
	{
		cerr << "Not enough command line parameters" << endl;
		return -1;
	}


	auto T0 = std::chrono::high_resolution_clock::now();

	for (int i = 0; i < 3; ++i)
	{

		string FileName = "g" + std::to_string(i + 1) + ".txt";
		//read Data
		auto t0 = std::chrono::high_resolution_clock::now();

		CGraph G;
		G.Init(FileName);

		//timing
		//auto t1 = std::chrono::high_resolution_clock::now();

		//Alg
		CJohnsons johnsons(&G);
		bool success = johnsons.Run();

		double min_min_dijkstra = johnsons.GetMinMinCost();
		//timing
		//auto t2 = std::chrono::high_resolution_clock::now();

		if (success)
			cout << "Johnsons solution is: " << min_min_dijkstra << endl;
		else
			cout << "Johnsons solution is: " << "NULL" << endl;

	}
	auto T1 = std::chrono::high_resolution_clock::now();
	std::cout << "Johnsons took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(T1 - T0).count()
		<< " milliseconds\n";


	//sanity check
	auto T2 = std::chrono::high_resolution_clock::now();
	for (int i = 0; i < 3; ++i)
	{
		string FileName = "g" + std::to_string(i + 1) + ".txt";
		//read Data
		auto t0 = std::chrono::high_resolution_clock::now();

		CGraph G;
		G.Init(FileName);

		CJohnsons johnsons(&G);
		bool success = johnsons.RunRef();

		double min_min_bf = johnsons.GetMinMinCost();
		//timing
		auto t3 = std::chrono::high_resolution_clock::now();

		if (success)
			cout << "n*Belman-Ford solution is: " << min_min_bf << endl;
		else
			cout << "n*Belman-Ford solution is: " << "NULL" << endl;
	}

	auto T3 = std::chrono::high_resolution_clock::now();
	std::cout << "n*Belman-Ford took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(T3 - T2).count()
		<< " milliseconds\n";


	printf("Press any key\n");
	int c = _getch();
	if (c)
		printf("A key is pressed from keyboard \n");
	else
		printf("An error occurred \n");
	return 0;
}