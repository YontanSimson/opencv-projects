#include <iostream>

#include "Johnsons.h"
#include "BellmanFord.h"
#include "Dijkstra.h"

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

using namespace std;

CJohnsons::CJohnsons(const CGraph *inGraph) :
	m_inGraph(inGraph)
{
	m_graph = *inGraph;//keep a local copy
}

CJohnsons::~CJohnsons()
{
}

//return true if no negative cycle, false for negative cycle
//Alg based Corman's Introduction to Algorithms 3Ed Chap. 25 pg. 704
bool CJohnsons::Run()
{
	bool noNegativeCycle = true;
	m_min_min_cost = DBL_MAX;

	//Johnson's All pairs shortest paths

	///////////////////////////////////////////
	//Do Bellman ford for calculating paths ///
	///////////////////////////////////////////

	//add sink node
	uint32_t numOfVerteces = m_graph.getNumOfVertices();
	uint32_t numOfEdges = m_graph.getNumOfEdges();
	uint32_t sinkVertex = numOfVerteces + 1;
	m_graph.AddSink(sinkVertex);

	//run Bellman-Ford on graph with sink
	CBellmanFord ShortestPaths(&m_graph);
	if (!ShortestPaths.Run(sinkVertex))
	{
		noNegativeCycle = false;
		return noNegativeCycle;
	}

	//////////////////////////////
	//Reweight graph weights /////
	//////////////////////////////
	const double *dist = ShortestPaths.GetDistances();
	//double * weights = new double[numOfEdges];
	for (auto&& it : m_graph.m_edgeList)
	{
		const uint32_t& from = it.first;
		auto& localEdges = it.second;
		//skip over sink vertex
		if (from == sinkVertex)
		{
			continue;
		}

		//iterate over edges
		for (auto&& it_edge : localEdges)
		{
			double weight = it_edge.weight;
			const double& p_u = dist[from - 1];
			const double& p_v = dist[it_edge.to - 1];
			it_edge.weight += (p_u - p_v);//c_e'= ce + p_u - p_v
		}
	}

	//remove sink before n*dijkstra
	m_graph.RemoveVertex(numOfVerteces + 1);//not really needed

	//////////////////////////////////
	// Run Dijkstra for each vertex //
	//////////////////////////////////
	for (auto&& it_u : m_inGraph->m_vertexList)
	{
		if (it_u == sinkVertex)//skip over sink Vertex
		{
			continue;
		}

		CDijkstra dijkstra(&m_graph);
		dijkstra.Run(it_u);

		double *distDijkstra = dijkstra.GetDistances();
		//reconstruct original costs
		for (auto&& it_v : m_inGraph->m_vertexList)
		{
			//using wieghts from Bellman Ford
			const double& p_u = dist[it_u - 1];
			const double& p_v = dist[it_v - 1];
			//correct the weights in the Dijkstra algorithm
			distDijkstra[it_v - 1] -= (p_u - p_v);
		}

		uint32_t v;
		double minCost = dijkstra.calcMinPathCost(it_u, v);
		m_min_min_cost = MIN(m_min_min_cost, minCost);
	}

	return noNegativeCycle;
}

//Bellman-Ford implementation for reference
bool CJohnsons::RunRef()
{
	bool noNegativeCycle = true;
	m_min_min_cost = DBL_MAX;

	//Do Bellman-Ford for each vertex
	CBellmanFord ShortestPaths(m_inGraph);
	for (auto&& it : m_inGraph->m_vertexList)
	{
		if (ShortestPaths.Run(it))
		{
			//ShortestPaths.Print();
			m_min_min_cost = MIN(m_min_min_cost, ShortestPaths.calcMinPathCost(it));
		}
		else
		{
			noNegativeCycle = false;
			break;
		}
	}
	return noNegativeCycle;
}
