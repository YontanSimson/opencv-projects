#pragma once
#include <cstdint>
#include <unordered_map>
#include <unordered_set>
#include <string>
#include <list>



struct SEdge {
	SEdge(const uint32_t& _to, const double& _weight) :
		to(_to), weight(_weight)
	{}
	SEdge() :
		to(-1), weight(-DBL_MAX)
	{}
	uint32_t to;
	double weight;
};


class CGraph
{
public:
	void Init(const std::string fileName);
	void Reserve(const size_t& size);
	void Print();

	size_t getNumOfVertices() const { return m_vertexList.size(); }
	size_t getNumOfEdges() const;

	std::unordered_map<uint32_t, std::list<SEdge>> m_edgeList;
	std::unordered_set<uint32_t> m_vertexList;

	void AddVertex(const uint32_t& idx);
	void AddEdge(
		const uint32_t& from, 
		const SEdge& edge);
	void AddSink(const uint32_t& idx);
	void RemoveVertex(const uint32_t& idx);
private:
	double* m_distance;
	double* m_predecessor;

};
