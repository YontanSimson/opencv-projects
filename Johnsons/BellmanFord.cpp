#include "BellmanFord.h"

#include <iostream>

using namespace std;

CBellmanFord::CBellmanFord(const CGraph *graph):
	m_graph(graph),
	m_distance(NULL),
	m_predecessor(NULL)
{
	m_numOfVertexes = m_graph->getNumOfVertices();
}

CBellmanFord::~CBellmanFord()
{
	if (m_distance != NULL)
	{
		delete [] m_distance;
	}

	if (m_predecessor != NULL)
	{
		delete[] m_predecessor;
	}

}

//Alg taken from http://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm
//and pg. 658 Section 24.3 of Cormen's Introduction to Algorithms 3rd Ed.
bool CBellmanFord::Run(const uint32_t& source)
{
	
	if (m_distance == nullptr)
	{
		m_distance = new double[m_numOfVertexes];
	}
	else
	{
		memset(m_distance, 0, m_numOfVertexes*sizeof(double));
	}

	if (m_predecessor == nullptr)
	{
		m_predecessor = new uint32_t[m_numOfVertexes];
	}
	else
	{
		memset(m_predecessor, 0, m_numOfVertexes*sizeof(uint32_t));
	}

	// This implementation takes in a graph, represented as
	// lists of vertices and edges, and fills two arrays
	// (distance and predecessor) with shortest-path
	// (less cost/distance/metric) information

	// Step 1: initialize graph - assumes node numbering from 1..V
	for (auto&& vertex : m_graph->m_vertexList)
	{
		if (vertex == source)
		{
			m_distance[vertex - 1] = 0;
		}
		else
		{
			m_distance[vertex - 1] = DBL_MAX;
			//distancePrev[vertex - 1] = DBL_MAX;
		}
		m_predecessor[vertex - 1] = -1;
	}

	// Step 2: relax edges repeatedly
	for (uint32_t i = 1; i < m_numOfVertexes; ++i)
	{
		for (auto&& itEdge : m_graph->m_edgeList)
		{
			uint32_t u = itEdge.first;
			for (auto&& itLocal : itEdge.second)
			{
				const uint32_t& v = itLocal.to;
				const double& w	= itLocal.weight;//d(u,v)
				if (m_distance[u - 1] + w < m_distance[v - 1])
				{
					m_distance[v - 1] = m_distance[u - 1] + w;
					m_predecessor[v - 1] = u;
				}
			}
		}
	}

	// Step 3: check for negative-weight cycles
	for (auto&& itEdge : m_graph->m_edgeList)
	{
		uint32_t u = itEdge.first;
		for (auto&& itLocal : itEdge.second)
		{
			const uint32_t& v = itLocal.to;
			const double& w = itLocal.weight;//d(u,v)
			if (m_distance[u - 1] + w < m_distance[v - 1])
			{
				return false;//found negative cycle
			}
		}
	}


	return true;
}

void CBellmanFord::Print()
{
	size_t numOfVertexes = m_graph->getNumOfVertices();

	cout << "Cost" << endl;
	for (size_t i = 0; i < numOfVertexes; ++i)
	{
		cout << m_distance[i] << ",";
	}
	cout << endl;

	cout << "Prev" << endl;
	for (size_t i = 0; i < numOfVertexes; ++i)
	{
		cout << m_predecessor[i] << ",";
	}
	cout << endl;
}

double CBellmanFord::calcMinPathCost(const uint32_t& source)
{
	size_t numOfVertexes = m_graph->getNumOfVertices();
	double minDist = DBL_MAX;
	for (size_t i = 0; i < numOfVertexes; i++)
	{
		if (source == i + 1)//exclude the source path
			continue;
		if (m_distance[i] < minDist)
		{
			minDist = m_distance[i];
		}
	}
	return minDist;
}

//min cost beteween u->v and the matching u,v vertices
double CBellmanFord::calcMinPathCost(const uint32_t& source, uint32_t& u, uint32_t& v)
{
	uint32_t numOfVertexes = m_graph->getNumOfVertices();
	double minDist = DBL_MAX;
	u = -1; v = -1;
	for (uint32_t i = 0; i < numOfVertexes; i++)
	{
		if (source == i + 1)//exclude the source path
			continue;
		if (m_distance[i] < minDist)
		{
			minDist = m_distance[i];

			//get matching u->v
			v = i + 1;
			u = m_predecessor[i];
		}
	}
	return minDist;
}


