#pragma once
#include "GraphStruct.h"

class CJohnsons
{
public:
	CJohnsons(const CGraph *inGraph);
	~CJohnsons();

	bool Run();
	bool RunRef();
	double GetMinMinCost() const{ return m_min_min_cost; }

private:
	const CGraph *m_inGraph;
	double m_min_min_cost;
	CGraph m_graph;
};

