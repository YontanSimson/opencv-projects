#include "Dijkstra.h"
#include "PriorityQueStruct.h"

#include <iostream>

using namespace std;


CDijkstra::CDijkstra(const CGraph* Graph) :
	m_dist(nullptr),
	m_prev(nullptr),
	m_graph(Graph),
	m_size(Graph->getNumOfVertices())
{
}

CDijkstra::~CDijkstra()
{
	if (m_dist != nullptr)
	{
		delete[] m_dist;
	}

	if (m_prev != nullptr)
	{
		delete[] m_prev;
	}
}

//Alg from http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
void CDijkstra::Run(const uint32_t& src_vertex)
{
	size_t num_of_vertices = m_graph->getNumOfVertices();
	if (m_dist == nullptr)
	{
		m_dist = new double[num_of_vertices];
	}
	else
	{
		memset(m_dist, 0, num_of_vertices*sizeof(double));
	}

	if (m_prev == nullptr)
	{
		m_prev = new uint32_t[num_of_vertices];
	}
	else
	{
		memset(m_prev, 0, num_of_vertices*sizeof(uint32_t));
	}

	//Prims algorithm
	uint64_t start_vertex = 1;
	m_min_path = 0;

	CPriorityQueStruct heap(num_of_vertices, new CKeyGreater<double, uint32_t>());
	for (auto&& it : m_graph->m_vertexList)
	{
		heap.Insert(make_pair(DBL_MAX, it));
	}
	for (size_t i = 0; i < num_of_vertices; i++)
	{
		m_dist[i] = DBL_MAX;// Unknown distance from source to v
		m_prev[i] = -1;// Predecessor of v
	}
	m_dist[src_vertex - 1] = 0;//assumes nodes are numbered from 1..V

	size_t start_idx = heap.ValuePositionInQue(src_vertex);
	heap.DecreaseKey(start_idx, 0);

	while (heap.Size() > 0)
	{
		auto it_u = heap.Pop();// Remove and return best vertex
		double& w_uv = it_u.first;//d(u,v)
		uint32_t& u = it_u.second;

		if (m_graph->m_edgeList.find(u) == m_graph->m_edgeList.end())
		{
			continue;
		}
		for (auto&& it_u : m_graph->m_edgeList.find(u)->second)//for each neighbor v of u
		{
			double alt = m_dist[u - 1] + it_u.weight;
			const uint32_t& v = it_u.to;
			if (alt < m_dist[v - 1])
			{
				m_dist[v - 1] = alt;
				m_prev[v - 1] = u;
				size_t que_idx = heap.ValuePositionInQue(v);
				heap.DecreaseKey(que_idx, alt);
			}
		}
	}
}

void CDijkstra::Print()
{
	cout << "Dist" << endl;
	for (size_t i = 0; i < m_size; ++i)
	{
		cout << m_dist[i] << ",";
	}
	cout << endl;

	cout << "Prev" << endl;
	for (size_t i = 0; i < m_size; ++i)
	{
		cout << m_prev[i] << ",";
	}
	cout << endl;
}

//min cost beteween source->v
double CDijkstra::calcMinPathCost(const uint32_t& source, uint32_t& v)
{
	uint32_t numOfVertexes = m_graph->getNumOfVertices();
	double minDist = DBL_MAX;
	v = -1;
	for (uint32_t i = 0; i < numOfVertexes; i++)
	{
		if (source == i + 1)//exclude the source path
			continue;
		if (m_dist[i] < minDist)
		{
			minDist = m_dist[i];

			//get matching source->v
			v = i + 1;
		}
	}
	return minDist;
}
