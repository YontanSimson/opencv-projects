#pragma once
#include <cstdint>

#include "GraphStruct.h"

class CDijkstra
{
public:
	CDijkstra(const CGraph* Graph);
	~CDijkstra();
	void Run(const uint32_t& src_vertex);
	void Print();//print results
	double calcMinPathCost(const uint32_t& source, uint32_t& v);
	const double* GetDistances() const { return m_dist; }
	double* GetDistances() { return m_dist; }
private:

	double * m_dist;   // Distance from source to source
	uint32_t * m_prev;
	size_t m_size;
	const CGraph* m_graph;

	double m_min_path;
};

