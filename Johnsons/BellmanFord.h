#pragma once

#include "GraphStruct.h"


class CBellmanFord
{
public:
	CBellmanFord(const CGraph *graph);
	~CBellmanFord();

	bool Run(const uint32_t& source);
	void Print();
	//double GetShortestPath(const uint32_t& source);
	double calcMinPathCost(const uint32_t& source);
	//min cost beteween u->v and the matching u,v vertices
	double calcMinPathCost(const uint32_t& source, uint32_t& u, uint32_t& v);

	const double* GetDistances(){ return m_distance; }
private:

	const CGraph *m_graph;
	double* m_distance;
	uint32_t* m_predecessor;
	size_t m_numOfVertexes;
};

