function showSegmentation(fig, grayIm, Phi0)

Segmented = Phi0 > 0;

%find contour
[B,~] = bwboundaries(Segmented);

figure(fig);
imagesc(grayIm);axis image; colormap gray;
hold on
for k = 1:length(B)
    boundary = B{k};
    plot(boundary(:,2), boundary(:,1), 'r', 'LineWidth', 1)
end
hold off;


