function displayGrad(Im, Gx, Gy, spacing)
[rows, cols] = size(Im);
    
[XX, YY] = meshgrid(1:spacing:cols, 1:spacing:rows);
maxVal = max(Im(:));
minVal = min(Im(:));

figure;
imagesc((double(Im) - minVal)/(maxVal - minVal));%image axis;% colormap gray;

hold on;
quiver(XX, YY, ...
    Gx(1:spacing:rows,1:spacing:cols), Gy(1:spacing:rows,1:spacing:cols));
hold off;
    
end