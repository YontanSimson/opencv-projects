function H = HeaviSideEps(Phi0, h_eps)
%Heaviside function - similar to a step function

H = 0.5*(1 + (2.0 / pi)*atan(Phi0 / h_eps));

