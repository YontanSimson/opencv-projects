function Phi0 = InitMask(grayIm, initialization_method)
	

[rows, cols] = size(grayIm);

if (initialization_method == 0)
%Calculate Laplacian

LaplaceIm = del2(double(grayIm));

mask = zeros(rows, cols, 'uint8');

maxVal = max(max(LaplaceIm));
thre = maxVal*.5;
ThreshIm = LaplaceIm > thre;

%Calculate center of mass for thresholded image
rowSum = sum(int32(ThreshIm), 1);

colSum = sum(int32(ThreshIm), 2);		
		
M00 = sum(rowSum);


M01 = 0;%Mx
for ii = 1:cols,
	M01 = M01 + (ii-1)*rowSum(ii);
end

M10 = 0;%My
for jj = 1:rows,
    M10 = M10 + (jj-1)*colSum(jj);
end

M01 = M01 / M00;
M10 = M10 / M00;

%Calc radius of initial contour - avoid going out of image
initRad = min(min(M01, cols - M01), min(M10, rows - M10));
initRad = max((2 * initRad) / 3, 25);
	
[XX, YY] = meshgrid(1:cols, 1:rows);

mask((XX-M01).^2 + (YY-M10).^2 <= initRad^2) = 1;

BW_in  = bwdist(1-mask);
BW_out = bwdist(mask);

Phi0 = -BW_out+BW_in-im2double(mask)+.5; %check why it is not the same as theone in C

elseif(initialization_method == 1)%checker board pattern
    [XX, YY] = meshgrid(1:cols, 1:rows);

    Phi0 = sin(pi/10*XX).*sin(pi/10*YY);
else
    error('Unsupported initialization method');
end

    

end