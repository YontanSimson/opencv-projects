
%Mask demo

mask = zeros(201, 201);

center = [100 100];
rad    =  50;

[X, Y] = meshgrid(1:size(mask,2), 1:size(mask,1));

mask((X-center(1)).^2+(Y-center(2)).^2<rad.^2) = 1;


BW_in  = bwdist(1-mask);
BW_out = bwdist(mask);

Phi0 = -BW_out+BW_in-im2double(mask)+.5; 

sigma = 3;

[grad_x, grad_y] = smoothGradient(Phi0, sigma);

displayGrad(Phi0, grad_x, grad_y, 4);