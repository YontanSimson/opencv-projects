function D = DiracEps(Phi0, h_eps)
%Delta function - differential of the step/Heaviside function

D = (h_eps / pi)./ (h_eps^2 + Phi0.^2);

