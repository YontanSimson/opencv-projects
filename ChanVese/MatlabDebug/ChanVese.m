clear;
close all;

%% Parameters
delta_t        = 0.1;
heavyside_eps  = 1;
num_iter_inner = 10;
num_iter       = 2000;
mu             = 0.1*255^2;
eta            = eps;
initilization_method = 0; % 0-a medium sized ball, 1-checkerboard patterd

%% Input 
    
Im = imread('../Inputs/brain_nk.jpg');
% Im = imread('../Inputs/adapt-thresh-tee.gif');
% Im = imread('../Inputs/CV3.jpg');
Im = imresize(Im, [64, 64]);

%Convert to gray
if (size(Im, 3) > 1)
    grayIm = rgb2gray(Im);
else
    grayIm = Im;
end
[rows, cols] = size(grayIm);

%Convert gray im to floating point
grayIm32F = im2double(grayIm)*255;

%% Initialize Level Set Function
Phi0 = InitMask(grayIm, initilization_method);


%Main loop
H = zeros(rows, cols, 'single');
D = zeros(rows, cols, 'single');
imageforce = zeros(rows, cols, 'single');
levelSetForce = zeros(rows, cols, 'single');

fig1 = figure;
fig2 = figure;

for loop_idx = 1:num_iter,
    PhiOld1 = Phi0;

    if (mod(loop_idx-1, 20) == 0)
        showSegmentation(fig1, grayIm, Phi0);
        pause(0.1);	
    end
	H = HeaviSideEps(Phi0, heavyside_eps);%H = HeaviSide(Phi0)

    numOfPixelsInside  = sum(sum(H));
    numOfPixelsOutside = sum(sum(1-H));

    %Calc average values c1,c2 for the inside and outside accordingly
    LayerInside = grayIm32F.*H;%grayIm32F.*Heaviside(Phi0)
    LayerOutside = grayIm32F.*(1 - H);%grayIm32F.*(1-Heaviside(Phi0))
    c1 = sum(sum(LayerInside));
    c2 = sum(sum(LayerOutside));
    c1 = c1/numOfPixelsInside;%average inside, Phi0 > 0
    c2 = c2/numOfPixelsOutside;%average outside, Phi0 < 0

    %Calc Image force
    Im_C1_squared = (grayIm32F - c1).*(grayIm32F - c1);
    Im_C2_squared = (grayIm32F - c2).*(grayIm32F - c2);
    
    imageforce    = -Im_C1_squared + Im_C2_squared;%sum force field on all layers - used for vector image

    %Solve PDE iteratively
    %d(phi)/dt = F(phi) = delta(phi)*[mu*div(grad(phi)/mag(phi)) - nu - (u0-c1)^2 + (u0-c1)^2]
    %Phi(n+1) = Phi(n) + dt*F(Phi(n)) - Fwd Euler Method
    
    for inner_loop_idx = 1:num_iter_inner,

        %Calc Level Set curvature force
        levelSetForce = Curvature(Phi0);

        %New Phi0
        D = DiracEps(Phi0, heavyside_eps);
        Temp = mu*levelSetForce + imageforce;
        Temp = Temp.*D;
        PhiOld2 = Phi0;
        Phi0 = Phi0 + delta_t*Temp;
%         showNormalizedIm(fig2, Phi0, 'Phi0 PDE being solved');
    end
    showNormalizedIm(fig2, Phi0, 'Phi0 After inner Loop');
    axis image
    %stopping criterion
    MSE1 = mean2((PhiOld1 - Phi0).^2);
    if ( MSE1 < 0.001)
        disp(['CV Segmentation converged at iteration: ' num2str(loop_idx)])
        break;
    end
    disp(['loop # ' num2str(loop_idx)])
end

level = graythresh(grayIm);

figure;
subplot(221);
imagesc(grayIm);
colormap gray
axis image;
title('Original Image');
subplot(222);
showSegmentation(gcf, grayIm, Phi0);
title('Segmentation results');
subplot(223);
imagesc(grayIm > level*max(max(grayIm)));
axis image
title('Threshold segmentation with Otsu')
subplot(224);
imagesc((Phi0>0)*c1 + (Phi0<=0)*c2);
colormap gray
axis image;
title('Chan Vese Segmentated Image');






        
    