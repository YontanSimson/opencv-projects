function GaussSeidelPDESolver(Phi0, imageforce, eta)
%Semi implicit Gauss-Seidel method

padding = 2;
%Padd Phi
PhiPadded = padarray(Phi0, [padding padding], 'replicate');
D = DiracEps(Phi0, heavyside_eps);

for ii = 1:rows,
    for jj = 1:cols,
        Aij   = mu*CalcA(ii, jj, PhiPadded, eta, padding);
        Aim1j = mu*CalcA(ii-1, jj, PhiPadded, eta, padding);
        Bij   = mu*CalcB(ii, jj, PhiPadded, eta, padding);
        Bijm1 = mu*CalcB(ii, jj-1, PhiPadded, eta, padding);

        ABPhi = Aij*PhiPadded(ii+1+padding, jj+padding)+...
            Aim1j*PhiPadded(ii-1+padding, jj+padding)+...
            Bij*PhiPadded(ii+padding, jj+1+padding)+...
            Bijm1*PhiPadded(ii+padding, jj+1+padding);
        AB = Aij + Aim1j + Bij + Bijm1;

        denom = 1 + delta_t*D(ii, jj)*AB; 
        TempPhi = ...
            PhiPadded(ii+padding, jj+padding) + ...
            delta_t*D(ii, jj)*(ABPhi + imageforce(ii, jj));
        PhiPadded(ii+padding, jj+padding) = TempPhi/denom;
    end
end
Phi0 = PhiPadded(padding+1:end-padding, padding+1:end-padding);
showNormalizedIm(fig2, Phi0, 'Phi0 after GS');
