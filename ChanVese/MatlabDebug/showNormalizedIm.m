function showNormalizedIm(fig, Im, title_str)


maxVal = max(Im(:));
minVal = min(Im(:));

figure(fig);
imagesc((double(Im) - minVal)/(maxVal - minVal));
% image axis;
% colormap gray;
title(title_str)


