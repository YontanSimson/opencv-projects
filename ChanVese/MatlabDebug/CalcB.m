function Bij   = CalcB(ii, jj, PhiPadded, eta, padding)

Phi_ip1j = PhiPadded(ii + 1 + padding, jj + padding);
Phi_ij   = PhiPadded(ii + padding, jj + padding);
Phi_im1j = PhiPadded(ii - 1 + padding, jj + padding);
Phi_ijp1 = PhiPadded(ii + padding, jj + 1 + padding);

Bij = 1/sqrt(eta^2 + (Phi_ijp1 - Phi_ij)^2 + ((Phi_ip1j - Phi_im1j)/2)^2);
