
function Curv = Curvature(Phi0)
%Calc Curvature of Image/Phi
%div(grad(phi)/mag(phi))
%where mag(phi) = sqrt((d(phi)/dx)^2 + (d(phi)/dy)^2)

sigma = 7;

[grad_x, grad_y] = gradient(Phi0);%smoothGradient(Phi0, sigma);

mag = sqrt(grad_x.^2 + grad_y.^2) + eps;

%normGrad_x = grad_x./mag, normGrad_y = grad_y./mag
normGrad_x = grad_x./mag;
normGrad_y = grad_y./mag;

[divGradPhi_x, ~] = gradient(normGrad_x);%smoothGradient(normGrad_x, sigma);
[~, divGradPhi_y] = gradient(normGrad_y);%smoothGradient(normGrad_y, sigma);


Curv = divGradPhi_x + divGradPhi_y;

%for debug
% displayGrad(Phi0, grad_x, grad_y, 4);
% displayGrad(Phi0, divGradPhi_x, divGradPhi_y, 4);

end


