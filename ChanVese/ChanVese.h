#include "DataType.h"

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/imgproc/imgproc_c.h"

class CChanVese{
	CInputParameters m_inputParameters;
	cv::Mat m_inputIm;
	cv::Mat m_outputIm;

private:
/////////////////////////////////////////////
	cv::Mat builtInMask(
		const cv::Mat& grayIm, 
		const std::string& maskType);
////////////////////////////////////////////
	void HeaviSideEps(
		const cv::Mat& Phi0, 
		cv::Mat& H, 
		const double& eps);
//////////////////////////////////////////////
	void DiracEps(
		const cv::Mat& Phi0, 
		cv::Mat& D, 
		const double& eps);
//////////////////////////////////////////////
	void Curvature(
		const cv::Mat& Phi0, 
		cv::Mat& Curv);
/////////////////////////////////////////////
	void InitLevelSetFunc(
		const std::string& maskType,
		const cv::Mat& grayIm,
		cv::Mat& Phi0);
/////////////////////////////////////////////
	void showSegmentation(
		const cv::Mat& grayIm32F, 
		const cv::Mat& Phi0);
////////////////////////////////////////////
	void SolvePDE(
		const cv::Mat& imageforce,
		cv::Mat& D,
		cv::Mat& levelSetForce,
		cv::Mat& Phi0,
		const double eps,
		const int num_iter_inner,
		const float delta_t,
		const float mu);
//////////////////////////////////////////////
	float CalcA(
		const int i, 
		const int j, 
		const cv::Mat& Phi0Padded, 
		const float& eta,
		const unsigned& padding);
/////////////////////////////////////////////
	float CalcB(
		const int i, 
		const int j, 
		const cv::Mat& Phi0Padded, 
		const float& eta, 
		const unsigned& padding);
//////////////////////////////////////////////
public:
	CChanVese(
		CInputParameters& inputParameters);

	cv::Mat segment(
		const cv::Mat& inputIm);
};

//For debug

inline void ShowNormalizedIm(const cv::Mat& Im, const std::string& winName)
{
	double maxVal, minVal;
	int maxIdx[2];
	cv::minMaxIdx(Im, &minVal, &maxVal, NULL, maxIdx);
	cv::namedWindow(winName, CV_WINDOW_NORMAL);
	cv::imshow(winName, (Im - minVal) / (maxVal - minVal));
	cv::waitKey(10);
}