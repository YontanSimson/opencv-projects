#include "stdafx.h"
#include <string>
#include <vector>
#include <cassert>

#include "Platforms.h"


#include "rapidjson/document.h"		// rapidjson's DOM-style API
#include "rapidjson/prettywriter.h"	// for stringify JSON
#include "rapidjson/filestream.h"	// wrapper of C stream for prettywriter as output

static const float CV_EPS32F = FLT_EPSILON * 10;

struct CInputParser{
	std::string acceptString(const rapidjson::Document& document, const std::string& paramName)
	{
		assert(document.HasMember(paramName.c_str()));
		assert(document[paramName.c_str()].IsString());
		return document[paramName.c_str()].GetString();
	}

	int acceptInt(const rapidjson::Document& document, const std::string& paramName)
	{
		assert(document[paramName.c_str()].IsNumber());	// Number is a JSON type, but C++ needs more specific type.
		assert(document[paramName.c_str()].IsInt());		// In this case, IsUint()/IsInt64()/IsUInt64() also return true.
		return document[paramName.c_str()].GetInt();
	}

	double acceptDouble(const rapidjson::Document& document, const std::string& paramName)
	{
		assert(document[paramName.c_str()].IsNumber());
		assert(document[paramName.c_str()].IsDouble() || document[paramName.c_str()].IsInt());
		if ( document[paramName.c_str()].IsDouble() )
			return document[paramName.c_str()].GetDouble();
		else
			return document[paramName.c_str()].IsInt();
	}

	bool acceptBool(const rapidjson::Document& document, const std::string& paramName)
	{
		assert(document[paramName.c_str()].IsBool());
		return document[paramName.c_str()].GetBool();
	}

	void acceptIntArray(const rapidjson::Document& document, const std::string& paramName, std::vector<int>& Array)
	{
		const rapidjson::Value& jsonValue = document[paramName.c_str()]; // Using a reference for consecutive access is handy and faster.
		assert(jsonValue.IsArray());
		Array.resize(jsonValue.Size());
		rapidjson::Value::ConstValueIterator it = jsonValue.Begin();
		for (rapidjson::SizeType i = 0; i < jsonValue.Size(); i++, it++) // rapidjson uses SizeType instead of size_t.
			Array[i] = it->GetInt();
	}


};

struct CInputParameters : public CInputParser{

	CInputParameters()://default values
		resize_to(2, 256)
	{
		BuiltInMask    = "medium";
		num_iter       = 500;
		mu             = 0.1;
		delta_t        = 0.1;
		heavyside_eps  = 1;
		num_iter_inner = 10;
		resize_input = false;
	}

	std::string BuiltInMask;
	int num_iter;
	double mu;
	//std::string method;
	double delta_t;
	double heavyside_eps;
	int num_iter_inner;

	bool resize_input;
	std::vector<int> resize_to;

	//Set methods
	void setInputParams(const rapidjson::Document& document)
	{
		BuiltInMask = acceptString(document, "BuiltInMask");
		num_iter = acceptInt(document, "num_iter");
		mu = acceptDouble(document, "mu");
		delta_t = acceptDouble(document, "delta_t");
		heavyside_eps = acceptDouble(document, "heavyside_eps");
		num_iter_inner = acceptInt(document, "num_iter_inner");
		resize_input = acceptBool(document, "resize_input");
		acceptIntArray(document, "resize_to", resize_to);
	}

};

template<class T> T clip(T InVal, T minVal, T maxVal)
{
	return min(max(InVal, minVal), maxVal);
}

#define SQR(X) (X)*(X)
