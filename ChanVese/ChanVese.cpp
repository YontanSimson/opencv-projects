#include "stdafx.h"
#include "ChanVese.h"
#include "MatrixOperations.h"
#include "cvplot.h"//For debug with plots

#include <iostream>

using namespace cv;
using namespace CvPlot;

CChanVese::CChanVese(CInputParameters& inputParameters) :
	m_inputParameters(inputParameters)
{

}

cv::Mat CChanVese::segment(const cv::Mat& inputIm)
{
	//Set member
	m_inputIm = inputIm;

	//Init Parameters
	const int		rows = m_inputIm.rows;
	const int		cols = m_inputIm.cols;
	const int		numOfChannels = m_inputIm.channels();
	const double	eps = m_inputParameters.heavyside_eps;
	const double	delta_t = m_inputParameters.delta_t;
	const int		num_iter_inner = m_inputParameters.num_iter_inner;
	const double	mu = m_inputParameters.mu;
	const int		num_iter = m_inputParameters.num_iter;
	const string	BuiltInMask = m_inputParameters.BuiltInMask;

	//Convert to gray
	Mat grayIm;
	if ( numOfChannels > 1)
		cvtColor(m_inputIm, grayIm, CV_BGR2GRAY);
	else
		grayIm = m_inputIm;


	CV_Assert(grayIm.type() == CV_32SC1 || grayIm.type() == CV_16SC1 || grayIm.type() == CV_8SC1 ||
		grayIm.type() == CV_16UC1 || grayIm.type() == CV_8UC1);//input must be of integer form

	//Convert gray im to floating point
	Mat grayIm32F;
	grayIm.convertTo(grayIm32F, CV_32F, 1.0);//Scale from 0:255 to 0:255.0

	//Initialize Level Set Function
	Mat Phi0;
	InitLevelSetFunc(
		BuiltInMask,
		grayIm,
		Phi0);
	
	//Main loop
	Mat H(rows, cols, CV_32FC1);
	Mat D(rows, cols, CV_32FC1);
	Mat imageforce(rows, cols, CV_32FC1, Scalar(0.0));
	Mat levelSetForce(rows, cols, CV_32FC1, Scalar(0.0));
	for (size_t loop_idx = 0; loop_idx < (size_t)num_iter; loop_idx++)
	{
		showSegmentation(grayIm, Phi0);
		
		HeaviSideEps(Phi0, H, eps);//H = HeaviSide(Phi0)

		Scalar numOfPixelsInside = sum(H);
		Scalar numOfPixelsOutside = sum(1-H);

		//Calc average values c1,c2 for the inside and outside accordingly
		Mat LayerInside = grayIm32F.mul(H);//grayIm32F.*Heaviside(Phi0)
		Mat LayerOutside = grayIm32F.mul(Scalar(1) - H);//grayIm32F.*(1-Heaviside(Phi0))
		Scalar c1 = sum(LayerInside);//average inside of Phi0
		Scalar c2 = sum(LayerOutside);//average outside of Phi0
		c1[0] /= numOfPixelsInside[0];
		c2[0] /= numOfPixelsOutside[0];


		//Calc Image force: -(f-c1)^2 + (f-c2)^2
		Mat Im_C1_squared = grayIm32F - c1[0];
		Mat Im_C2_squared = grayIm32F - c2[0];

		Im_C1_squared = Im_C1_squared.mul(Im_C1_squared);//raise each coefficient seperately to square
		Im_C2_squared = Im_C2_squared.mul(Im_C2_squared);//raise each coefficient seperately to square
		imageforce    = -Im_C1_squared + Im_C2_squared;//sum force field on all layers - used for vector image

		//d(phi)/dt = F(phi) = delta(phi)*[mu*div(grad(phi)/mag(phi)) - nu - (u0-c1)^2 + (u0-c1)^2]

		//Solve PDE iteratively using Fwd Euler method
		//Phi(n+1) = Phi(n) + dt*F(Phi(n))
		for (size_t inner_loop_idx = 0; inner_loop_idx < (size_t)num_iter_inner; inner_loop_idx++)
		{
			//Calc Level Set curvature force
			Curvature(Phi0, levelSetForce);
			
			//New Phi0
			DiracEps(Phi0, D, eps);
			Mat Temp = mu*levelSetForce + imageforce;
			Temp = Temp.mul(D);
			Mat PhiOld(Phi0);
			Phi0 = Phi0 + delta_t*Temp;

		}

		//ShowNormalizedIm(Phi0, "Phi0 After inner Loop");
	}
		
	return Phi0 > 0;
}

//Heaviside function - similar to a step function
void CChanVese::HeaviSideEps(const cv::Mat& Phi0, cv::Mat& H, const double& eps)
{
	CV_Assert(Phi0.type() == CV_32F);
	CV_Assert(H.type() == CV_32F);
	CV_ARE_SIZES_EQ(&Phi0, &H);

	H.resize(Phi0.rows, Phi0.cols);
	for (size_t i = 0; i < (size_t)H.rows; i++)
	{
		for (size_t j = 0; j < (size_t)H.cols; j++)
		{
			float phi = Phi0.at<float>(i, j);
			H.at<float>(i, j) = static_cast<float>(0.5*(1 + (2.0 / CV_PI)*atan(phi / eps)));
		}
	}
}

//Delta function - differential of the step/Heaviside function
void CChanVese::DiracEps(const cv::Mat& Phi0, cv::Mat& D, const double& eps)
{
	CV_Assert(Phi0.type() == CV_32F);
	CV_Assert(D.type() == CV_32F);
	CV_ARE_SIZES_EQ(&Phi0, &D);

	D.resize(Phi0.rows, Phi0.cols);
	for (size_t i = 0; i < (size_t)D.rows; i++)
	{
		for (size_t j = 0; j < (size_t)D.cols; j++)
		{
			float phi = Phi0.at<float>(i, j);
			D.at<float>(i, j) = static_cast<float>((eps / CV_PI) / (eps*eps + phi*phi));
		}
	}
}

//Creates Built-In mask for initilization
//The Multi-phase version requires two channels
cv::Mat CChanVese::builtInMask(const cv::Mat& grayIm, const string& maskType)
{
	int rows = grayIm.rows;
	int cols = grayIm.cols;


	//Create built-In mask 
	Mat mask(rows, cols, CV_8UC1, Scalar(0));
	if (maskType.compare("medium_circle")==0)
	{
		//Calculate Laplacian
		Mat LaplaceIm;
		Laplacian(grayIm, LaplaceIm, CV_16SC1, 1, 1, 0, BORDER_DEFAULT);

		double maxVal;
		int maxIdx[2];
		minMaxIdx(LaplaceIm, 0, &maxVal, NULL, maxIdx);

		double thre = maxVal*.5;
		Mat ThreshIm = LaplaceIm > thre;

		//Calculate center of mass for thresholded image
		Mat rowSum(1, ThreshIm.rows, CV_32SC1);//Assuming input is in integer form
		reduce(ThreshIm, rowSum, 0, CV_REDUCE_SUM, CV_32SC1);//sum over rows (going down) - reduce to single row
		
		Mat colSum(ThreshIm.cols, 1, CV_32SC1);
		reduce(ThreshIm, colSum, 1, CV_REDUCE_SUM, CV_32SC1);//sum over columns (going right) - reduce to single column
		
		Scalar M00 = sum(rowSum);

		int M01 = 0;//Mx
		for (size_t i = 0; i < (size_t)rowSum.cols; i++)
		{
			M01 += i*rowSum.at<int>(i);
		}

		int M10 = 0;//My
		for (size_t i = 0; i < (size_t)colSum.rows; i++)
		{
			M10 += i*colSum.at<int>(i);
		}

		M01 += (int(M00[0]) / 2);
		M10 += (int(M00[0]) / 2);

		M01 /= int(M00[0]);
		M10 /= int(M00[0]);

		//Calc radius of initial contour - avoid going out of image
		int initRad = min(min(M01, cols - M01), min(M10, rows - M10));
		initRad = max((2 * initRad) / 3, 25);
		
		circle(mask, Point(M01, M10), initRad, Scalar(1), -1);//filled circle
	}
	else
	{
		cerr << "Acceptable inputs for \"BuiltInMask\" are \"medium_circle\" "<<endl;
		assert(!"Other Mask types are not yet supported");
	}
	return mask;
}

//Approximate version
//Calc Curvature of Image/Phi
//div(grad(phi)/mag(phi))
//where mag(phi) = sqrt((d(phi)/dx)^2 + (d(phi)/dy)^2)
void CChanVese::Curvature(const cv::Mat& Phi0, cv::Mat& Curv)
{
	CV_ARE_SIZES_EQ(&Phi0, &Curv);
	static const double scale = 1;
	static const int delta = 0;
	static const int ksize = 3;
	static const int ddepth = CV_32F;

	Mat Phi0Smoothed;
	GaussianBlur(Phi0, Phi0Smoothed, Size(3, 3), 0, 0, BORDER_REFLECT);

	/// Generate grad_x and grad_y
	Mat grad_x, grad_y;
	Mat abs_grad_x, abs_grad_y;

	/// Gradient X
	//Scharr(Phi0Smoothed, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT);
	Sobel(Phi0Smoothed, grad_x, ddepth, 1, 0, ksize, scale, delta, BORDER_REFLECT);
	/// Gradient Y
	//Scharr(Phi0Smoothed, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT);
	Sobel(Phi0Smoothed, grad_y, ddepth, 0, 1, ksize, scale, delta, BORDER_REFLECT);

	Mat mag;
	mag.create(Phi0.rows, Phi0.cols, Phi0.type());

	for (size_t i = 0; i < (size_t)mag.rows; i++)
	{
		for (size_t j = 0; j < (size_t)mag.cols; j++)
		{
			float Gx = grad_x.at<float>(i, j);
			float Gy = grad_y.at<float>(i, j);

			mag.at<float>(i, j) = sqrtf(Gx*Gx + Gy*Gy) + CV_EPS32F;
		}
	}

	//normGrad_x = grad_x./mag, normGrad_y = grad_y./mag
	Mat normGrad_x, normGrad_y;
	divide(grad_x, mag, normGrad_x);
	divide(grad_y, mag, normGrad_y);

	Mat divGradPhi_x, divGradPhi_y;
	//Scharr(normGrad_x, divGradPhi_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT);
	//Scharr(normGrad_y, divGradPhi_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT);
	Sobel(normGrad_x, divGradPhi_x, ddepth, 1, 0, ksize, scale, delta, BORDER_REFLECT);
	Sobel(normGrad_y, divGradPhi_y, ddepth, 0, 1, ksize, scale, delta, BORDER_REFLECT);

	Curv = divGradPhi_x + divGradPhi_y;
	//ShowNormalizedIm(Curv, "Curv");
}

//Initialize Level Set Function
void CChanVese::InitLevelSetFunc(
	const std::string& maskType,
	const cv::Mat& grayIm, 
	cv::Mat& Phi0)
{
	const int rows = grayIm.rows;
	const int cols = grayIm.cols;

	Phi0.create(rows, cols, CV_32FC1);

	if (maskType.compare("medium_circle") == 0)
	{
		Mat initContour = builtInMask(grayIm, maskType);

		//inside and outside distance transform
		Mat DistOut;//32 bit single
		distanceTransform(1 - initContour, DistOut, CV_DIST_L2, 3);

		Mat DistIn;//32 bit single
		distanceTransform(initContour, DistIn, CV_DIST_L2, 3);

		//Phi0 = DistOut - DistIn + initContour - 0.5;
		DistIn.copyTo(Phi0);

		Phi0 -= DistOut;

		Mat initContourSingle(initContour.size(), initContour.type());//convert 8bit mask to 32bit float
		initContour.convertTo(initContourSingle, CV_32FC1);
		Phi0 -= initContourSingle;

		Phi0 += Scalar(0.5);
	}
	else if (maskType.compare("checker_board") == 0)
	{
		for (size_t i = 0; i < (size_t)rows; i++)
		{
			for (size_t j = 0; j < (size_t)cols; j++)
			{
				Phi0.at<float>(i, j) = static_cast<float>(sin(CV_PI / 5 * i)*sin(CV_PI / 5 * j));
			}
		}
	}
	else
	{
		cerr << "Acceptable inputs for \"InitLevelSetFunc\" are \"medium_circle\" and \"checker_board\" " << endl;
		assert(!"Other Mask types are not yet supported");
	}



	ShowNormalizedIm(Phi0, "Phi0");

}

//Use Phi0 > 0 to get Level Set segmentation and display result overlayed on grayIm
void CChanVese::showSegmentation(
	const cv::Mat& grayIm, 
	const cv::Mat& Phi0)
{
	CV_ARE_SIZES_EQ(&Phi0, &grayIm);
	CV_Assert(grayIm.type() == CV_8UC1);

	//Parameters for FindContours
	static const int thickness = 1;//CV_FILLED - filled contour
	static const int lineType = 8;//8:8-connected,	4:4-connected line, CV_AA: antialiased line.
	Scalar           color = CV_RGB(255, 20, 20); // line color - light red

	//Segmented image
	Mat Segmented = Phi0 > 0;

	//namedWindow("Segmented", CV_WINDOW_NORMAL);
	//imshow("Segmented", Segmented);
	//waitKey(10);
	//destroyWindow("Segmented");

	////////////////////////////////////////////////////////////////////
	/// Find contours - use old style C since C++ version has bugs. 

	//Target image for sketching contours of segmentation
	Mat             drawing;
	cvtColor(grayIm, drawing, CV_GRAY2RGB);
	IplImage        drawingIpl = drawing; //just clone header with no copying of data

	//Data containers for FindContour
	IplImage        SegmentedIpl = Segmented;//just clone header with no copying of data
	CvMemStorage*   storage = cvCreateMemStorage(0);
	CvSeq*          contours = 0;
	int             numCont = 0;
	int             contAthresh = 45;

	numCont = cvFindContours(&SegmentedIpl, storage, &contours, sizeof(CvContour),
		CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE, cvPoint(0, 0));

	for (; contours != 0; contours = contours->h_next)
	{
		cvDrawContours(&drawingIpl, contours, color, color, -1, thickness, lineType, cvPoint(0, 0));
	}

	//This produces segmentation faults
	//vector<vector<Point> > contours;
	//vector<Vec4i> hierarchy;
	//findContours(Segmented, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0));
	//findContours(Segmented, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	///// Draw contours
	//Mat drawing;
	//cvtColor(grayIm, drawing, CV_GRAY2RGB);
	//for (size_t i = 0; i < contours.size(); i++)
	//{
	//	Scalar color = Scalar(20, 20, 200);
	//	drawContours(drawing, contours, i, color, thickness, lineType, hierarchy, 0, Point());
	//}

	/// Show in a window
	string win_name = "Contour";
	//Mat drawing = cvarrToMat(&drawingIpl); //Mat(&IplImage) is soon to be deprecated OpenCV 3.X.X
	namedWindow(win_name, CV_WINDOW_NORMAL);
	imshow(win_name, drawing);
	waitKey(10);


}

//d(phi)/dt = F(phi) = delta(phi)*[mu*div(grad(phi)/mag(phi)) - nu - (u0-c1)^2 + (u0-c1)^2]
//Solve PDE in one semi-implicit Gauss-Seidel Sweep - raster scan
void CChanVese::SolvePDE(
	const cv::Mat& imageforce, 
	cv::Mat& D, 
	cv::Mat& levelSetForce, 
	cv::Mat& Phi0,
	const double eps, 
	const int num_iter_inner,
	const float delta_t,
	const float mu)
{

	CV_ARE_SIZES_EQ(&Phi0, &imageforce);
	CV_ARE_SIZES_EQ(&Phi0, &D);
	CV_ARE_SIZES_EQ(&Phi0, &levelSetForce);
	
	static const unsigned padding = 2;

	DiracEps(Phi0, D, eps);

	ShowNormalizedIm(D, "D");

	Mat PhiPadded;
	copyMakeBorder(Phi0, PhiPadded, 2, 2, 2, 2, BORDER_REPLICATE);

	ShowNormalizedIm(D, "D");
	ShowNormalizedIm(PhiPadded, "Before GS");

	int rows = Phi0.rows;
	int cols = Phi0.cols;
	for (size_t i = 0; i < (size_t)rows; i++)
	{
		for (size_t j = 0; j < (size_t)cols; j++)
		{

			float Aij   = mu*CalcA(i, j, PhiPadded, CV_EPS32F, padding);
			float Aim1j = mu*CalcA(i - 1, j, PhiPadded, CV_EPS32F, padding);
			float Bij   = mu*CalcB(i, j, PhiPadded, CV_EPS32F, padding);
			float Bim1j = mu*CalcB(i, j - 1, PhiPadded, CV_EPS32F, padding);

			float ABPhi = Aij*PhiPadded.at<float>(i + 1 + padding, j + padding) +
				Aim1j*PhiPadded.at<float>(i - 1 + padding, j + padding) + 
				Bij*PhiPadded.at<float>(i + padding, j + 1 + padding) + 
				Bim1j*PhiPadded.at<float>(i + padding, j + 1 + padding);
			float AB = Aij + Aim1j + Bij + Bim1j;

			float denom = 1 + delta_t*D.at<float>(i, j)*AB;
	
			float TempPhi = PhiPadded.at<float>(i + padding, j + padding) + 
				delta_t*D.at<float>(i, j)*(ABPhi + imageforce.at<float>(i, j));
			PhiPadded.at<float>(i + padding, j + padding) = TempPhi / denom;
		}
	}

	Phi0 = PhiPadded(Range(padding, padding + rows - 1), Range(padding, padding + cols - 1));

	ShowNormalizedIm(Phi0, "Phi0 after GS");
}

float CChanVese::CalcA(const int i, const int j, const cv::Mat& Phi0Padded, const float& eta, const unsigned& padding)
{

	float Phi_ip1j = Phi0Padded.at<float>(i + 1 + padding, j + padding);
	float Phi_ij   = Phi0Padded.at<float>(i + padding, j + padding);
	float Phi_ijp1 = Phi0Padded.at<float>(i + padding, j + 1 + padding);
	float Phi_ijm1 = Phi0Padded.at<float>(i + padding, j - 1 + padding);

	float Aij = 1 / sqrt(SQR(eta) + SQR(Phi_ip1j - Phi_ij) + SQR((Phi_ijp1 - Phi_ijm1) / 2));
	return Aij;
}

float CChanVese::CalcB(const int i, const int j, const cv::Mat& Phi0Padded, const float& eta, const unsigned& padding)
{
	float Phi_ip1j = Phi0Padded.at<float>(i + 1 + padding, j + padding);
	float Phi_ij   = Phi0Padded.at<float>(i + padding, j + padding);
	float Phi_im1j = Phi0Padded.at<float>(i - 1 + padding, j + padding);
	float Phi_ijp1 = Phi0Padded.at<float>(i + padding, j + 1 + padding);

	float Bij = 1 / sqrt(SQR(eta) + SQR(Phi_ijp1 - Phi_ij) + SQR((Phi_ip1j - Phi_im1j) / 2));
	return Bij;
}



