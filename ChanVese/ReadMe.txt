========================================================================
    CONSOLE APPLICATION : Chan Vese Project Overview
========================================================================

	Not finished yet
	
main.cpp
    This is the main application source file.
	
DataType.h
	Contains gloabl definition and object for interfacing with rapid Json Object

ChanVese.cpp/h
	Main object for segmenting with Chane Vese Level Set Function method.
	Based on the paper  T.Chan and L. Vese, "Active contours without edges,"  IEEE Trans. Image Process., vol. 10,  no. 2, Feb 2001


ChanVeseParams.json
	Project input parameters - input at command line or via debug parameters
	
cvplot.cpp/h
	Functions for displaying graphic plot. Downloaded from Google code. Useful for debug
	Credit: Changbo (zoccob@gmail).

MatrixOperations.h
	General Matrix operations - currently not in use

rapidjson
	Folder containing code for inputing and outputting Json files

targetver.h
	Not in use

ChanVese.sln
	Project solution
ChanVese.vcxproj
	Project files and definition
ChanVese.vcxproj.filters
	Project File Filters

/////////////////////////////////////////////////////////////////////////////
Other standard files:

StdAfx.h, StdAfx.cpp
    These files are used to build a precompiled header (PCH) file
    named ConsoleApplication1.pch and a precompiled types file named StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Other notes:

/////////////////////////////////////////////////////////////////////////////
