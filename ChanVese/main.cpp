/*
* Copyright (c) 2014. Yonatan Simson <simsonyee[at]gmail[dot]com>.
* Released to public domain under terms of the BSD Simplified license.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*   * Redistributions of source code must retain the above copyright
*     notice, this list of conditions and the following disclaimer.
*   * Redistributions in binary form must reproduce the above copyright
*     notice, this list of conditions and the following disclaimer in the
*     documentation and/or other materials provided with the distribution.
*   * Neither the name of the organization nor the names of its contributors
*     may be used to endorse or promote products derived from this software
*     without specific prior written permission.
*
*   See <http://www.opensource.org/licenses/bsd-license>
*/

#include "stdafx.h"

#include "rapidjson/document.h"		// rapidjson's DOM-style API
#include "rapidjson/prettywriter.h"	// for stringify JSON
#include "rapidjson/filestream.h"	// wrapper of C stream for prettywriter as output
#include "ChanVese.h"
#include <cstdio>
#include <fstream>
#include <iostream>

using namespace rapidjson;
using namespace std;
using namespace cv;

int main(int argc, char const *argv[])
{

	string paramFile;// "ChanVeseParams.json";
	if (argv[1])
	{
		paramFile = argv[1];
	}
	else
	{
		cerr << "Please supply an parameter file" << endl;
		return 1;
	}

	string inputImFile;// "Input/brain.jpg";
	if (argv[2])
	{
		inputImFile = argv[2];
	}
	else
	{
		cerr << "Please supply an input image" << endl;
		return 1;
	}


	FILE * pFile = fopen(paramFile.c_str(), "rt");
	if (pFile == NULL)
	{
		cerr << "Failed to open file: " << argv[1] << std::endl;
		return 1;
	}

	Document document;
	FileStream is(pFile);
	document.ParseStream<0>(is);

	//Print to screen for debug
	//FileStream f(stdout);
	//PrettyWriter<FileStream> writer(f);
	//document.Accept(writer);	// Accept() traverses the DOM and generates Handler events.

	CInputParameters inputParameters;
	inputParameters.setInputParams(document);

	//Init Chan Vese with parameters
	CChanVese chanVese(inputParameters);

	//read input image
	Mat inputIm = imread(inputImFile);

	//check if read was succesful
	if (!inputIm.data) // Check for invalid input
	{
		cout << "Could not open image file: " << paramFile<< std::endl;
		return 1;
	}

	//Run chan Vese
	Mat segmentedIm = chanVese.segment(inputIm);

	return 0;
}

