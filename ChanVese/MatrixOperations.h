#include "stdafx.h"

#include "opencv2/core/core.hpp"

template<class T>
void rowSum(const cv::Mat& src, cv::Mat& dst)
{
	int rows = src.rows;
	int srcType = src.type();
	dst = cv::Mat::zeros(rows, 1, srcType);//allocate

	T* data = (T*)src.data;
	for (int j = 0; j<src.cols; j++)
	{
		T rowSum = 0;
		for (int i = 0; i < src.rows; i++)
		{
			rowSum += data[j + i*width];
		}
		*dst.ptr<T>(j) = rowSum;
	}
}

template<class T>
void colSum(const cv::Mat& src, cv::Mat& dst)
{
	int cols = src.cols;
	int srcType = src.type();
	dst = cv::Mat::zeros(1, cols, srcType);//allocate

	T* data = (T*)src.data;
	for (int i = 0; i < src.rows; i++)
	{
		T colSum = 0;
		T* pRow = src.ptr<T>(i);
		for (int j = 0; j<src.cols; j++)
		{
			colSum += data[j + i*width];
		}
		*dst.ptr<T>(j) = colSum;
	}
}
